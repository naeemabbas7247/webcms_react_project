export const MainNav = [
  {
    icon: "pe-7s-browser",
    label: "Pages",
    content: [
      {
        label: "Login",
        to: "#/pages/login",
      },
      {
        label: "Login Boxed",
        to: "#/pages/login-boxed",
      },
      {
        label: "Register",
        to: "#/pages/register",
      },
      {
        label: "Register Boxed",
        to: "#/pages/register-boxed",
      },
      {
        label: "Forgot Password",
        to: "#/pages/forgot-password",
      },
      {
        label: "Forgot Password Boxed",
        to: "#/pages/forgot-password-boxed",
      },
    ],
  },
];
