import React, { Component } from "react";
import { useDispatch } from "react-redux";
import {
  setCmsArray,
  setCmsModel,
  setCmsKey,
  setCmsValue,
  setThemeBackgroundColor,
  setThemeColor,
  setEditorState,
  setCmsEnable,
  setModuleCmsObject,
  setCmsEnableUploadImage,
  setCmsModelImage,
  setSectionBackgroundColor,
  setIconModel,
  setShowIcon,
  setLogoIcon,
  setNewBlogKey
} from "../../reducers/ThemeOptions";
import Icon from "./Icon.js";
import { connect } from "react-redux";
import {
  EditorState,
  ContentState,
  convertFromHTML,
  convertFromRaw,
} from "draft-js";
import ReactPlayer from "react-player";
import CloneIcon from './CloneIcon.js';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {faTimes } from "@fortawesome/free-solid-svg-icons";
import { Editor } from "react-draft-wysiwyg";
import htmlToDraft from "html-to-draftjs";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import CmsSet from "./Functions/CmsSet.js";
import {
    Carousel,
    CarouselItem,
    CarouselControl,
    CarouselIndicators,
    CarouselCaption,
  } from "reactstrap";
import ApiCall from "../../admin/Partial/Function/ApiCall";
var temp_background_icon=[];
class Blog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      module_key: "blog",
      imageLogo: "",
      imageLogo2: "",
      count:1,
      new_key:[],
    };
  }
  SetCms = (key, section_id) => {
    if (this.props.cmsEnable) {
      var tempArray = {
        key: section_id + "blog_background",
        value: this.props.cmsArray[section_id + "blog_background"],
      };
      const { setSectionBackgroundColor } = this.props;
      setSectionBackgroundColor(tempArray);
      const contentBlock = htmlToDraft(this.props.cmsArray[section_id + key]);
      if (contentBlock) {
        const contentState = ContentState.createFromBlockArray(
          contentBlock.contentBlocks
        );
        const editorState = EditorState.createWithContent(contentState);
        const { setEditorState } = this.props;
        setEditorState(editorState);
      }
      const { cmsKey, setCmsKey } = this.props;
      setCmsKey(section_id + key);
      const { cmsValue, setCmsValue } = this.props;
      setCmsValue(this.props.cmsArray[section_id + key]);
      const { cmsModel, setCmsModel } = this.props;
      setCmsModel(!this.props.cmsModel);
    }
  };
  SetImage = async (key, section_id) => {
    if (this.props.cmsEnable) {
      var tempArray = {
        key: section_id + "blog_background",
        value: this.props.cmsArray[section_id + "blog_background"],
      };
      const { setSectionBackgroundColor } = this.props;
      setSectionBackgroundColor(tempArray);
      const { cmsModelImage, setCmsModelImage } = this.props;
      setCmsModelImage(!this.props.cmsModelImage);
      const { cmsEnableUploadImage, setCmsEnableUploadImage } = this.props;
      setCmsEnableUploadImage(!this.props.cmsEnableUploadImage);
      const { cmsKey, setCmsKey } = this.props;
      const { cmsValue, setCmsValue } = this.props;
      setCmsValue(this.props.cmsArray[key]);
      setCmsKey(key);
    }
  };
  blogImg(key,section_id) {
    var imageStyle={'position':"relative",'height':"100%",'width':"100%"};
        if (this.props.cmsArray[section_id + key]) {
          if (this.props.cmsArray[section_id + key].length > 0) {
            if (
              (this.props.cmsArray[section_id + key][0]["images_backups"]["image"]).includes("http")
            ) {
              if (this.props.cmsArray[section_id + key][0].length == 1) {
                return (
                  <div style={{position: "relative", paddingTop :  "56.25%"}}>
                    <ReactPlayer
                      onClick={() =>
                        this.SetImage(section_id + key, section_id)
                      }
                      url={
                        this.props.cmsArray[section_id + key][
                          "images_backups"
                        ]["image"]
                      }
                      style={{ 
                        position: "absolute",
                        top: 0,
                        left: 0,
                        padding: 12, 
                      }}
                      controls
                      width='100%'
                      height='100%'
                    />
                  </div>
                );
              } else {
                const slides = this.props.cmsArray[section_id + key].map(
                  (item) => {
                    return (
                      <CarouselItem
                        onExiting={() => this.setState({ animating: true })}
                        onExited={() => this.setState({ animating: false })}
                        key={item.id}
                      >
                        <div style={{position: "relative", paddingTop :  "56.25%"}}>
                          <ReactPlayer
    
                            onClick={() =>
                              this.SetImage(section_id + key, section_id)
                            }
                            url={
                              item.images_backups.image
                            }
                            style={{ 
                              position: "absolute",
                              top: 0,
                              left: 0,
                              padding: 12, 
                            }}
                            controls
                            width='100%'
                            height='100%'
                          />
                        </div>
                      </CarouselItem>
                    );
                  }
                );
                return (
                    <Carousel
                      onClick={() =>
                        this.SetImage(section_id + key, section_id)
                      }
                      activeIndex={this.state.activeIndex}
                      next={this.next}
                      previous={this.previous}
                    >
                      <CarouselIndicators
                        items={this.props.cmsArray[section_id + key]}
                        activeIndex={this.state.activeIndex}
                        onClickHandler={this.goToIndex}
                      />
                      {slides}
                      <CarouselControl
                        direction="prev"
                        directionText="Previous"
                        onClickHandler={() => this.previous()}
                      />
                      <CarouselControl
                        direction="next"
                        directionText="Next"
                        onClickHandler={() => this.next()}
                      />
                    </Carousel>
                );
              }
            } else {
              if (this.props.cmsArray[section_id + key].length == 1) {
                return (
                  <img
                    style={this.props.cmsArray[section_id + key+"style"] ? [this.props.cmsArray[section_id + key+"style"],imageStyle] : imageStyle}
                    onClick={() =>
                      this.SetImage(section_id + key, section_id)
                    }
                    className="image-1"
                    src={
                      this.props.cmsArray[section_id + key][0][
                        "images_backups"
                      ]["image"]
                    }
                    alt="App Landing"
                  />
                );
              } else {
                const slides = this.props.cmsArray[section_id + key].map(
                  (item) => {
                    return (
                      <CarouselItem
                        onExiting={() => this.setState({ animating: true })}
                        onExited={() => this.setState({ animating: false })}
                        key={item.id}
                      >
                        <img
                          src={item.images_backups.image}
                          alt={item.altText}
                          onClick={() =>
                            this.SetImage(section_id + key, section_id)
                          }
                          style={{
                            ...this.props.cmsArray[section_id + key+"style"],
                            position:"relative",
                            height:"100%",
                            width:"100%"
                          }}
                        />
                      </CarouselItem>
                    );
                  }
                );
                return (
                    <Carousel
                      onClick={() =>
                        this.SetImage(section_id + key, section_id)
                      }
                      activeIndex={this.state.activeIndex}
                      next={this.next}
                      previous={this.previous}
                    >
                      <CarouselIndicators
                        items={this.props.cmsArray[section_id + key]}
                        activeIndex={this.state.activeIndex}
                        onClickHandler={this.goToIndex}
                      />
                      {slides}
                      <CarouselControl
                        direction="prev"
                        directionText="Previous"
                        onClickHandler={() => this.previous()}
                      />
                      <CarouselControl
                        direction="next"
                        directionText="Next"
                        onClickHandler={() => this.next()}
                      />
                    </Carousel>
                );
              }
            }
          } else {
              return(
                <img
                style={{position:"relative",
                height:"100%",
                width:"100%"}}
                onClick={() =>
                  this.SetImage(section_id + key, section_id)
                }
                className="image-1"
                src={
                  "../../assets/images/app/mobile-2.png"
                }
                alt="App Landing"
              />
            );
          }
        }
      }
      SetIcon = async (key, section_id) => {
        
        if (this.props.cmsEnable) {
          var tempArray = {
            key: section_id + "banner_background",
            value: this.props.cmsArray[section_id + "banner_background"],
          };

          const { setSectionBackgroundColor } = this.props;
          setSectionBackgroundColor(tempArray);
          
          const { iconModel, setIconModel } = this.props;
          setIconModel(!this.props.iconModel);

          const { cmsKey, setCmsKey } = this.props;
          const { cmsValue, setCmsValue } = this.props;
          setCmsValue(this.props.cmsArray[key]);
          setCmsKey(key);
        }
      };
    blogIcon(section_id) {
     return (  
      <div>
       {this.props.cmsArray[section_id + "blog_icon"] ? this.props.cmsArray[section_id + "blog_icon"][0]['cms_page']['is_icon'] == 0 ? 
        <img
        style={this.props.cmsArray[section_id + "blog_iconstyle"]}
        onClick={() =>this.SetIcon(section_id + "blog_icon", section_id)}
        className="image-1"
        src={this.props.cmsArray[section_id + "blog_icon"] ? this.props.cmsArray[section_id + "blog_icon"][0][ "images_backups" ]["image"] : ""}
        controls
        width='5%'
        height='5%'
        /> :  
        <i 
        style={this.props.cmsArray[section_id + "blog_iconstyle"]}
        onClick={() =>this.SetIcon(section_id + "blog_icon", section_id)}
        className={this.props.cmsArray[section_id + "blog_icon"][0]["images_backups"]["image"]} ></i>
        :
        <img
          style={{position:"relative",
          height:"3%",
          width:"3%"}}
          onClick={() =>
          this.SetIcon(section_id + "blog_icon", section_id)
          }
          className="image-1"
          src={
          "../../assets/images/app/mobile-2.png"
          }
          alt="App Landing" 
          />
         }
      
      </div>
         )
      }
      componentDidUpdate(prevProps) {
        var section_id = this.props[0] + "_";  
        var newArray2 = this.props.cmsArray[section_id + "blog_icon"];
      }
      callAPI=()=>
      {
        ApiCall.postAPICall("setCmsData/custimze",temp_background_icon);

      }
      sendAPICall=(section_id,module_key,count)=>
      {
        if(this.props.cmsArray[section_id+module_key+'_title_'+count])
        {
          temp_background_icon.push(section_id+module_key+'_background_'+count);
          temp_background_icon = temp_background_icon.filter((item, i, ar) => ar.indexOf(item) === i);
          const { setNewBlogKey } = this.props;
          setNewBlogKey(temp_background_icon);
        }
      }
      render() {
        let key_id=this.props[0];
        let section_id=key_id+'_';
        let count = 0;
        var sectionBackgroundColorArray = {
          key: section_id + "blog_background",
          value: this.props.cmsArray[section_id + "blog_background"],
        };
        return (
          <div
            className="blog-area pt--120 pt_md--80 pt_sm--80"
            id={section_id}
            style={{
              backgroundColor: this.props.cmsArray[section_id + "blog_background"]? this.props.cmsArray[section_id + "blog_background"][section_id + "blog_background"] : "red",
            }}
          >
            <CloneIcon current_section={this.props.current_section} class_set={this.props.class_set} {...[key_id]} section_flag={this.props.section_flag} getData={this.props.getData ? this.props.getData : null} getColors={this.props.getColors ? this.props.getColors : null} module={this.props.module} addable={true}/>
            <div className="container">
              <div className="row">
                <div className="col-lg-12">
                  <div className="section-title text-center mb--80">
                    <CmsSet
                      sectionBackgroundArray={sectionBackgroundColorArray}
                      styleProp={
                        this.props.cmsArray[section_id + "blog_headingstyle"]
                      }
                      htmlProp={this.props.cmsArray[section_id + "blog_heading"]}
                      sectionKey={section_id}
                      sectionId={"blog_heading"}
                      getData={this.props.getData ? this.props.getData : null}
                    />
                    {this.blogIcon(section_id)} 
                    <CmsSet
                      sectionBackgroundArray={sectionBackgroundColorArray}
                      styleProp={
                        this.props.cmsArray[section_id + "blog_pstyle"]
                      }
                      htmlProp={this.props.cmsArray[section_id + "blog_p"]}
                      sectionKey={section_id}
                      sectionId={"blog_p"}
                      getData={this.props.getData ? this.props.getData : null}
                    />
                  </div>
                </div>
              </div>
              <div className="row">
                {Object.keys(this.props.moduleCmsObject).length != 0
                  ? Object.keys(
                      this.props.moduleCmsObject[this.state.module_key]
                    ).map((val, i) => (
                    <div className="col-lg-4 col-md-6 col-12 "  key={++count} style={{'display':this.props.cmsArray[section_id+this.state.module_key+'_title_'+(i+this.state.count)] ? 'block': 'none'}}>
                    {this.props.cmsArray[section_id+this.state.module_key+'_title_'+(i+this.state.count)] ?
                      <div className={JSON.stringify(section_id+this.state.module_key+'_background_'+(i+this.state.count))} style={{backgroundColor: this.props.cmsArray[section_id+this.state.module_key+'_background_'+(i+this.state.count)]? this.props.cmsArray[section_id+this.state.module_key+'_background_'+(i+this.state.count)][section_id+this.state.module_key+'_background_'+(i+this.state.count)] : "green",padding: 32}}>
                        
                        <div
                          className="pull-right mr-2 cloneIcon text-danger mt-2"
                          style={{ display:this.props.cmsEnable ? 'block':'none' }}
                          onClick={() => this.props.sectionDelete(key_id,(i+this.state.count))}
                        >
                          <FontAwesomeIcon icon={faTimes} size="2x" />
                        </div>
                        <div className="blog">
                          <div className="thumb">
                            {this.blogImg(this.state.module_key +"_icon_" +val,section_id)}
                          </div>
                          <div className="inner">
                            <div className="title">
                              <a>
                                <CmsSet
                                  sectionBackgroundArray={sectionBackgroundColorArray}
                                  styleProp={
                                    this.props.cmsArray[section_id+this.state.module_key+'_title_'+(i+this.state.count)+'style']
                                  }
                                  backgroundKeyFlag={true}
                                  htmlProp={this.props.cmsArray[section_id+this.state.module_key+'_title_'+(i+this.state.count)]}
                                  sectionKey={section_id}
                                  backgroundKey={this.state.module_key+'_background_'+(i+this.state.count)}
                                  sectionId={this.state.module_key+'_title_'+(i+this.state.count)}
                                  getData={this.props.getData ? this.props.getData : null}
                                />
                              </a>
                            </div>
                            <ul className="meta">
                              <li>
                                <CmsSet
                                  sectionBackgroundArray={sectionBackgroundColorArray}
                                  styleProp={
                                    this.props.cmsArray[section_id+this.state.module_key+'_date_'+(i+this.state.count)+'style']
                                  }
                                  backgroundKeyFlag={true}
                                  htmlProp={this.props.cmsArray[section_id+this.state.module_key+'_date_'+(i+this.state.count)]}
                                  sectionKey={section_id}
                                  backgroundKey={this.state.module_key+'_background_'+(i+this.state.count)}
                                  sectionId={this.state.module_key+'_date_'+(i+this.state.count)}
                                  getData={this.props.getData ? this.props.getData : null}
                                />
                              </li>
                              <li>
                                <CmsSet
                                  sectionBackgroundArray={sectionBackgroundColorArray}
                                  styleProp={
                                    this.props.cmsArray[section_id+this.state.module_key+'_postBy_'+(i+this.state.count)+'style']
                                  }
                                  backgroundKeyFlag={true}
                                  htmlProp={this.props.cmsArray[section_id+this.state.module_key+'_postBy_'+(i+this.state.count)]}
                                  sectionKey={section_id}
                                  backgroundKey={this.state.module_key+'_background_'+(i+this.state.count)}
                                  sectionId={this.state.module_key+'_postBy_'+(i+this.state.count)}
                                  getData={this.props.getData ? this.props.getData : null}
                                />
                              </li>
                            </ul>
                            <div className="desc mt--10 mb--30">
                              <CmsSet
                                  sectionBackgroundArray={sectionBackgroundColorArray}
                                  styleProp={
                                    this.props.cmsArray[section_id+this.state.module_key+'_body_'+(i+this.state.count)+'style']
                                  }
                                  backgroundKeyFlag={true}
                                  htmlProp={this.props.cmsArray[section_id+this.state.module_key+'_body_'+(i+this.state.count)]}
                                  sectionKey={section_id}
                                  backgroundKey={this.state.module_key+'_background_'+(i+this.state.count)}
                                  sectionId={this.state.module_key+'_body_'+(i+this.state.count)}
                                  getData={this.props.getData ? this.props.getData : null}
                                />
                            </div>
                            <div className="blog-btn">
                              <a className="button-link">
                                <CmsSet
                                  sectionBackgroundArray={sectionBackgroundColorArray}
                                  styleProp={
                                    this.props.cmsArray[section_id+this.state.module_key+'_btn_'+(i+this.state.count)+'style']
                                  }
                                  backgroundKeyFlag={true}
                                  htmlProp={this.props.cmsArray[section_id+this.state.module_key+'_btn_'+(i+this.state.count)]}
                                  sectionKey={section_id}
                                  backgroundKey={this.state.module_key+'_background_'+(i+this.state.count)}
                                  sectionId={this.state.module_key+'_btn_'+(i+this.state.count)}
                                  getData={this.props.getData ? this.props.getData : null}
                                />
                              </a>
                            </div>
                          </div>
                        </div>
                      </div>
                      :null
                    }
                    </div>
                    ))
                  : ""}
              </div>
            </div>
          </div>
        );
      }
    }
const mapStateToProps = (state) => ({
  cmsArray: state.ThemeOptions.cmsArray,
  cmsModel: state.ThemeOptions.cmsModel,
  cmsKey: state.ThemeOptions.cmsKey,
  cmsValue: state.ThemeOptions.cmsValue,
  themeColor: state.ThemeOptions.themeColor,
  themeBackgroundColor: state.ThemeOptions.themeBackgroundColor,
  editorState: state.ThemeOptions.editorState,
  cmsEnable: state.ThemeOptions.cmsEnable,
  moduleCmsObject: state.ThemeOptions.moduleCmsObject,
  cmsEnableUploadImage: state.ThemeOptions.cmsEnableUploadImage,
  cmsModelImage: state.ThemeOptions.cmsModelImage,
  sectionBackgroundColor: state.ThemeOptions.sectionBackgroundColor,
  iconModel:state.ThemeOptions.iconModel,
  showIcon:state.ThemeOptions.showIcon,
  logoIcon:state.ThemeOptions.logoIcon,
  newBlogKey:state.ThemeOptions.logoIcon,
});
const mapDispatchToProps = (dispatch) => ({
  setCmsArray: (enable) => dispatch(setCmsArray(enable)),
  setCmsModel: (enable) => dispatch(setCmsModel(enable)),
  setCmsKey: (enable) => dispatch(setCmsKey(enable)),
  setCmsValue: (enable) => dispatch(setCmsValue(enable)),
  setThemeColor: (enable) => dispatch(setThemeColor(enable)),
  setThemeBackgroundColor: (enable) =>
    dispatch(setThemeBackgroundColor(enable)),
  setEditorState: (enable) => dispatch(setEditorState(enable)),
  setCmsEnable: (enable) => dispatch(setCmsEnable(enable)),
  setModuleCmsObject: (enable) => dispatch(setModuleCmsObject(enable)),
  setCmsModelImage: (enable) => dispatch(setCmsModelImage(enable)),
  setCmsEnableUploadImage: (enable) =>
    dispatch(setCmsEnableUploadImage(enable)),
  setSectionBackgroundColor: (enable) =>
    dispatch(setSectionBackgroundColor(enable)),
  setIconModel: enable => dispatch(setIconModel(enable)),
  setShowIcon: enable => dispatch(setShowIcon(enable)),
  setLogoIcon: enable => dispatch(setLogoIcon(enable)),
  setNewBlogKey: enable => dispatch(setNewBlogKey(enable)),
});
export default connect(mapStateToProps, mapDispatchToProps)(Blog);
