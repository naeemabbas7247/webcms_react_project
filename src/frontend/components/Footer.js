import React, { Component } from "react";
import { useDispatch } from "react-redux";
import {
  setCmsArray,
  setCmsModel,
  setCmsKey,
  setCmsValue,
  setThemeBackgroundColor,
  setThemeColor,
  setEditorState,
  setCmsEnable,
  setCmsEnableBackground,
  setSectionBackgroundColor,
  setThemeSectionBackgroundColor,
  setSectionId,
  setContactUsEmail,
  setSubscriptionEmail,
  setSocialModel,
} from "../../reducers/ThemeOptions";
import CmsSet from "./Functions/CmsSet.js";
import Icon from "./Icon.js";
import { connect } from "react-redux";
import {
  EditorState,
  ContentState,
  convertFromHTML,
  convertFromRaw,
} from "draft-js";
import { Editor } from "react-draft-wysiwyg";
import htmlToDraft from "html-to-draftjs";
import { faCogs } from "@fortawesome/free-solid-svg-icons";
import CloneIcon from "./CloneIcon.js";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
class Footer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      background_btn: false,
      addMessage:'add-text-',
    };
  }
  SetCms = (key, section_id) => {
    if (this.props.cmsEnable) {
      var tempArray = {
        key: section_id + "footer_background",
        value: this.props.cmsArray[section_id + "footer_background"],
      };
      const { setSectionBackgroundColor } = this.props;
      setSectionBackgroundColor(tempArray);
      const contentBlock = htmlToDraft(this.props.cmsArray[section_id + key]);
      if (contentBlock) {
        const contentState = ContentState.createFromBlockArray(
          contentBlock.contentBlocks
        );
        const editorState = EditorState.createWithContent(contentState);
        const { setEditorState } = this.props;
        setEditorState(editorState);
      }
      const { cmsKey, setCmsKey } = this.props;
      setCmsKey(section_id + key);
      const { cmsValue, setCmsValue } = this.props;
      setCmsValue(this.props.cmsArray[section_id + key]);
      const { cmsModel, setCmsModel } = this.props;
      setCmsModel(!this.props.cmsModel);
      const { setSectionId } = this.props;
      setSectionId(section_id);
      const { setSubscriptionEmail } = this.props;
      setSubscriptionEmail(
        this.props.cmsArray[section_id + "subscription_email"]
      );
      const { setContactUsEmail } = this.props;
      setContactUsEmail(this.props.cmsArray[section_id + "contact_us_email"]);
    }
  };
  SetImage = async (key, section_id) => {
    if (this.props.cmsEnable) {
      var tempArray = {
        key: section_id + "footer_background",
        value: this.props.cmsArray[section_id + "footer_background"],
      };
      const { setSectionBackgroundColor } = this.props;
      setSectionBackgroundColor(tempArray);
      const { cmsModelImage, setCmsModelImage } = this.props;
      setCmsModelImage(!this.props.cmsModelImage);
      const { cmsEnableUploadImage, setCmsEnableUploadImage } = this.props;
      setCmsEnableUploadImage(!this.props.cmsEnableUploadImage);
      const { cmsKey, setCmsKey } = this.props;
      const { cmsValue, setCmsValue } = this.props;
      setCmsValue(this.props.cmsArray[section_id + key]);
      setCmsKey(section_id + key);
    }
  };
  SetSoicalCms = async (key, section_id) => {
    if (this.props.cmsEnable) {
      const { cmsKey, setCmsKey } = this.props;
      setCmsKey(section_id + key);
      const { cmsValue, setCmsValue } = this.props;
      setCmsValue(this.props.cmsArray[section_id + key]);
      const { cmsModel, setCmsModel } = this.props;
      const { setSocialModel } = this.props;
      setSocialModel(!this.props.socialModel);
    }
  };
  render() {
    let key_id = this.props[0];
    let section_id = key_id + "_";
    var sectionBackgroundColorArray = {
      key: section_id + "footer_background",
      value: this.props.cmsArray[section_id + "footer_background"]?this.props.cmsArray[section_id + "footer_background"][section_id + "footer_background"]:null,
    };
    return (
      <div>
        <div
          style={{
            background: this.props.cmsArray[section_id + "footer_background"] ? this.props.cmsArray[section_id + "footer_background"][section_id + "footer_background"] : "red",
            paddingTop: 12,
            paddingBottom: 36,
          }}
          id={section_id}
        >
          <div className="footer-bg"></div>
          <CloneIcon
            current_section={this.props.current_section}
            section_flag={this.props.section_flag}
            {...[key_id]}
            class_set={this.props.class_set}
            getData={this.props.getData ? this.props.getData : null}
            getColors={this.props.getColors ? this.props.getColors : null}
          />
          <div className="container">
            <div className="row">
              <div className="col-lg-8 offset-lg-2 col-xl-4 offset-xl-4">
                <div className="footer-links text-center">
                  {!this.props.cmsEnable ? (
                    <a
                      target="_blank"
                      href={this.props.cmsArray[section_id + "footer_facebook"] ? this.props.cmsArray[section_id + "footer_facebook"][section_id + "footer_facebook"] : null}
                    >
                      <i 
                      className={this.props.cmsArray[section_id + "footer_facebook_icon"] ? this.props.cmsArray[section_id + "footer_facebook_icon"][section_id + "footer_facebook_icon"] : null}></i>
                    </a>
                  ) : (
                    <a
                      onClick={() =>
                        this.SetSoicalCms("footer_facebook", section_id)
                      }
                    >
                      <i 
                      className={this.props.cmsArray[section_id + "footer_facebook_icon"] ? this.props.cmsArray[section_id + "footer_facebook_icon"][section_id + "footer_facebook_icon"] : null}></i>
                    </a>
                  )}
                  {!this.props.cmsEnable ? (
                    <a
                      target="_blank"
                      href={this.props.cmsArray[section_id + "footer_twitter"] ? this.props.cmsArray[section_id + "footer_twitter"][section_id + "footer_twitter"] : null}
                    >
                      <i className={this.props.cmsArray[section_id + "footer_twitter_icon"] ? this.props.cmsArray[section_id + "footer_twitter_icon"][section_id + "footer_twitter_icon"] : null}></i>
                    </a>
                  ) : (
                    <a
                      onClick={() =>
                        this.SetSoicalCms("footer_twitter", section_id)
                      }
                    >
                      <i className={this.props.cmsArray[section_id + "footer_twitter_icon"] ? this.props.cmsArray[section_id + "footer_twitter_icon"][section_id + "footer_twitter_icon"] : null}></i>
                    </a>
                  )}
                  {!this.props.cmsEnable ? (
                    <a
                      target="_blank"
                      href={this.props.cmsArray[section_id + "footer_google"] ? this.props.cmsArray[section_id + "footer_google"][section_id + "footer_google"] : null}
                    >
                      <i className={this.props.cmsArray[section_id + "footer_google_icon"] ? this.props.cmsArray[section_id + "footer_google_icon"][section_id + "footer_google_icon"] : null}></i>
                    </a>
                  ) : (
                    <a
                      onClick={() =>
                        this.SetSoicalCms("footer_google", section_id)
                      }
                    >
                      <i className={this.props.cmsArray[section_id + "footer_google_icon"] ? this.props.cmsArray[section_id + "footer_google_icon"][section_id + "footer_google_icon"] : null}></i>
                    </a>
                  )}
                  {!this.props.cmsEnable ? (
                    <a
                      target="_blank"
                      href={this.props.cmsArray[section_id + "footer_linkedin"] ? this.props.cmsArray[section_id + "footer_linkedin"][section_id + "footer_linkedin"] : null}
                    >
                      <i className={this.props.cmsArray[section_id + "footer_linkedin_icon"] ? this.props.cmsArray[section_id + "footer_linkedin_icon"][section_id + "footer_linkedin_icon"] : null}></i>
                    </a>
                  ) : (
                    <a
                      onClick={() =>
                        this.SetSoicalCms("footer_linkedin", section_id)
                      }
                    >
                      <i className={this.props.cmsArray[section_id + "footer_linkedin_icon"] ? this.props.cmsArray[section_id + "footer_linkedin_icon"][section_id + "footer_linkedin_icon"] : null}></i>
                    </a>
                  )}
                  {!this.props.cmsEnable ? (
                    <a
                      target="_blank"
                      href={this.props.cmsArray[section_id + "footer_pinterest"] ? this.props.cmsArray[section_id + "footer_pinterest"][section_id + "footer_pinterest"] : null}
                    >
                      <i className={this.props.cmsArray[section_id + "footer_pinterest_icon"] ? this.props.cmsArray[section_id + "footer_pinterest_icon"][section_id + "footer_pinterest_icon"] : null}></i>
                    </a>
                  ) : (
                    <a
                      onClick={() =>
                        this.SetSoicalCms("footer_pinterest", section_id)
                      }
                    >
                      <i className={this.props.cmsArray[section_id + "footer_pinterest_icon"] ? this.props.cmsArray[section_id + "footer_pinterest_icon"][section_id + "footer_pinterest_icon"] : null}></i>
                    </a>
                  )}
                  {!this.props.cmsEnable ? (
                    <a
                      target="_blank"
                      href={this.props.cmsArray[section_id + "footer_youtube"] ? this.props.cmsArray[section_id + "footer_youtube"][section_id + "footer_youtube"] : null}
                    >
                      <i className={this.props.cmsArray[section_id + "footer_youtube_icon"] ? this.props.cmsArray[section_id + "footer_youtube_icon"][section_id + "footer_youtube_icon"] : null}></i>
                    </a>
                  ) : (
                    <a
                      onClick={() =>
                        this.SetSoicalCms("footer_youtube", section_id)
                      }
                    >
                      <i className={this.props.cmsArray[section_id + "footer_youtube_icon"] ? this.props.cmsArray[section_id + "footer_youtube_icon"][section_id + "footer_youtube_icon"] : null}></i>
                    </a>
                  )}
                </div>
                <div className="footer-text text-center">
                  <CmsSet
                    sectionBackgroundArray={sectionBackgroundColorArray}
                    styleProp={
                      this.props.cmsArray[section_id + "footer_copyrightstyle"]
                    }
                    htmlProp={
                      this.props.cmsArray[section_id + "footer_copyright"]
                    }
                    sectionKey={section_id}
                    sectionId={"footer_copyright"}
                    getData={this.props.getData ? this.props.getData : null}
                    addMessage={this.state.addMessage}
                  />
               
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="tap-top" style={this.props.themeBackgroundColor}>
          <div>
            <i className="zmdi zmdi-long-arrow-up"></i>
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => ({
  cmsArray: state.ThemeOptions.cmsArray,
  cmsModel: state.ThemeOptions.cmsModel,
  cmsKey: state.ThemeOptions.cmsKey,
  cmsValue: state.ThemeOptions.cmsValue,
  themeColor: state.ThemeOptions.themeColor,
  themeBackgroundColor: state.ThemeOptions.themeBackgroundColor,
  editorState: state.ThemeOptions.editorState,
  cmsEnable: state.ThemeOptions.cmsEnable,
  cmsEnableBackground: state.ThemeOptions.cmsEnableBackground,
  themeSectionBackgroundColor: state.ThemeOptions.themeSectionBackgroundColor,
  sectionId: state.ThemeOptions.sectionId,
  contact_us_email: state.ThemeOptions.contact_us_email,
  subscription_email: state.ThemeOptions.subscription_email,
  socialModel: state.ThemeOptions.socialModel,
});
const mapDispatchToProps = (dispatch) => ({
  setCmsArray: (enable) => dispatch(setCmsArray(enable)),
  setCmsModel: (enable) => dispatch(setCmsModel(enable)),
  setCmsKey: (enable) => dispatch(setCmsKey(enable)),
  setCmsValue: (enable) => dispatch(setCmsValue(enable)),
  setThemeColor: (enable) => dispatch(setThemeColor(enable)),
  setThemeBackgroundColor: (enable) =>
    dispatch(setThemeBackgroundColor(enable)),
  setEditorState: (enable) => dispatch(setEditorState(enable)),
  setCmsEnable: (enable) => dispatch(setCmsEnable(enable)),
  setCmsEnableBackground: (enable) => dispatch(setCmsEnableBackground(enable)),
  setSectionBackgroundColor: (enable) =>
    dispatch(setSectionBackgroundColor(enable)),
  setThemeSectionBackgroundColor: (enable) =>
    dispatch(setThemeSectionBackgroundColor(enable)),
  setSectionId: (enable) => dispatch(setSectionId(enable)),
  setContactUsEmail: (enable) => dispatch(setContactUsEmail(enable)),
  setSubscriptionEmail: (enable) => dispatch(setSubscriptionEmail(enable)),
  setSocialModel: (enable) => dispatch(setSocialModel(enable)),
});
export default connect(mapStateToProps, mapDispatchToProps)(Footer);
