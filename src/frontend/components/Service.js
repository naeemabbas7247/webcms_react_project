import React, { Component } from "react";
import { useDispatch } from 'react-redux';
import {
    setCmsArray,
    setCmsModel,
    setCmsKey,
    setCmsValue,
    setThemeBackgroundColor,
    setThemeColor,
    setEditorState,
    setModuleCmsObject,
    setCmsEnable,
    setCmsEnableBackground,
    setSectionBackgroundColor,
    setThemeSectionBackgroundColor,
    setBackgroundCmsKey,
    setIconModel,
    setShowIcon,
    setLogoIcon,
    setCmsEnableUploadImage,
} from '../../reducers/ThemeOptions';
import CmsSet from "./Functions/CmsSet.js";
import {connect} from 'react-redux';
import { EditorState, ContentState, convertFromHTML,convertFromRaw,convertToRaw } from 'draft-js'
import { Editor } from 'react-draft-wysiwyg';
import htmlToDraft from 'html-to-draftjs';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import {
  faCogs
} from "@fortawesome/free-solid-svg-icons";
import CloneIcon from './CloneIcon.js';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Row,Col,FormGroup,Label,Input,Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
class Service extends Component{
    constructor(props){
        super(props);
        this.state={
            module_key:'service',
            count:1,
        }
    }
    SetCms=(key,section_id,background_key=null) => {
        if(this.props.cmsEnable)
        {
            var tempArray={'key':section_id+'service_background','value':this.props.cmsArray[section_id+'service_background']}
            const {setSectionBackgroundColor } = this.props;
            setSectionBackgroundColor( tempArray); 
            const contentBlock = htmlToDraft(this.props.cmsArray[section_id+key]);
            if (contentBlock) {
                const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks);
                const editorState = EditorState.createWithContent(contentState);
                const {setEditorState } = this.props;
                setEditorState(editorState);
            }
            if(background_key)
            {
                const {setBackgroundCmsKey } = this.props;
                setBackgroundCmsKey(section_id+background_key);
            }
            const { cmsKey, setCmsKey } = this.props;
            setCmsKey(section_id+key);
            const { cmsValue, setCmsValue } = this.props;
            setCmsValue(this.props.cmsArray[section_id+key]);
            const { cmsModel, setCmsModel } = this.props;
            setCmsModel(!this.props.cmsModel);
        }
    }
    SetImage=async(key, section_id)=>
    {
        if(this.props.cmsEnable)
        {
            var tempArray={'key':section_id+'service_background','value':this.props.cmsArray[section_id+'service_background']}
            const {setSectionBackgroundColor } = this.props;
            setSectionBackgroundColor( tempArray); 
            const { cmsModelImage, setCmsModelImage } = this.props;
            setCmsModelImage(!this.props.cmsModelImage);
            const { cmsEnableUploadImage, setCmsEnableUploadImage } = this.props;
            setCmsEnableUploadImage(!this.props.cmsEnableUploadImage);
            const { cmsKey, setCmsKey } = this.props;
            const { cmsValue, setCmsValue } = this.props;
            setCmsValue(this.props.cmsArray[section_id+key]);
            setCmsKey(section_id+key);
        }
    }
    SetIcon = async (key, section_id) => {
        if (this.props.cmsEnable) {
          var tempArray = {
            key: section_id + "banner_background",
            value: this.props.cmsArray[section_id + "banner_background"],
          };
          const { setSectionBackgroundColor } = this.props;
          setSectionBackgroundColor(tempArray);
          
          const { iconModel, setIconModel } = this.props;
          setIconModel(!this.props.iconModel);

          const { cmsEnableUploadImage, setCmsEnableUploadImage } = this.props;
          setCmsEnableUploadImage(!this.props.cmsEnableUploadImage);
          const { cmsKey, setCmsKey } = this.props;
          const { cmsValue, setCmsValue } = this.props;
          setCmsValue(this.props.cmsArray[key]);
          setCmsKey(key);
        }
      };

    serviceIcon(section_id) {
        var i=0;
     return (  
         
      <div>
        {this.props.cmsArray[section_id +this.state.module_key +"_icon_"+(i+this.state.count)] ? this.props.cmsArray[section_id +this.state.module_key +"_icon_"+(i+this.state.count)][0]['cms_page']['is_icon'] == 0 ? 
            <img
            style={{marginTop:"20px",}}
            onClick={() =>
            this.SetIcon(section_id + this.state.module_key+"_icon_"+(i+this.state.count), section_id)
            }
            className="image-1"
            src={ this.props.cmsArray[section_id + this.state.module_key+"_icon_"+(i+this.state.count)] ? this.props.cmsArray[section_id + this.state.module_key+"_icon_"+(i+this.state.count)][0][ "images_backups" ]["image"] : ""}
            controls
            width='50%'
            height='50%'
            /> : 
            <i 
            onClick={() =>
            this.SetIcon(section_id + this.state.module_key+"_icon_"+(i+this.state.count), section_id)
            }
            className={this.props.cmsArray[section_id + this.state.module_key+"_icon_"+(i+this.state.count)][0]["images_backups"]["image"]} 
            src={
            this.props.cmsArray[section_id + this.state.module_key+"_icon_"+(i+this.state.count)]
            }></i> 
            : 
            <img
            style={{position:"relative",
            marginTop: '0.8rem',
            height:"50%",
            width:"50%"}}
            onClick={() =>
            this.SetIcon(section_id + this.state.module_key+"_icon_"+(i+this.state.count), section_id)
            }
            className="image-1"
            src={
            "../../assets/images/app/mobile-2.png"
            }
            alt="App Landing" 
            />
             }
      </div>
     
      )
    }
    render(){
        let key_id=this.props[0];
        let section_id=key_id+'_';
        var sectionBackgroundColorArray = {
              key: section_id + "service_background",
              value: this.props.cmsArray[section_id + "service_background"]?this.props.cmsArray[section_id + "service_background"][section_id + "service_background"]:null,
            };
        return (
                <div className={` service-area ${this.props.horizontal}`}
                style={{
                  backgroundColor: this.props.cmsArray[section_id + "service_background"]
                    ? this.props.cmsArray[section_id + "service_background"][
                        section_id + "service_background"
                      ]
                    : "orange",
                }}
                 id={section_id}>
                     <CloneIcon current_section={this.props.current_section} section_flag={this.props.section_flag} {...[key_id]} getData={this.props.getData ? this.props.getData : null} getColors={this.props.getColors ? this.props.getColors : null}/>
                    <div className="container">                   
                        <div className="row">
                            {
                                Object.keys(this.props.moduleCmsObject).length != 0 ? 
                                Object.keys(this.props.moduleCmsObject[this.state.module_key]).map((val, i) => (
                                    this.props.cmsArray[section_id+this.state.module_key+'_title_'+(i+this.state.count)]?
                                        <div className="col-lg-4 service-column" id={JSON.stringify()} key={val}>  
                                            <div className="single-service text-center">
                                                <div className="service-icon"  style={{background:this.props.cmsArray[section_id+'service_'+(i+this.state.count)+'_global']?this.props.cmsArray[section_id+'service_'+(i+this.state.count)+'_global'][section_id+'service_'+(i+this.state.count)+'_global']:null}}> 
                                                    {this.serviceIcon(section_id + this.state.module_key+"_icon_"+(i+this.state.count), section_id)}
                                                </div>
                                                <div className="title">
                                                    <CmsSet
                                                      sectionBackgroundArray={sectionBackgroundColorArray}
                                                      styleProp={
                                                        this.props.cmsArray[section_id+this.state.module_key+'_title_'+(i+this.state.count)+'style']
                                                      }
                                                      backgroundKeyFlag={true}
                                                      htmlProp={this.props.cmsArray[section_id+this.state.module_key+'_title_'+(i+this.state.count)]}
                                                      sectionKey={section_id}
                                                      backgroundKey={'service_'+(i+this.state.count)+'_global'}
                                                      sectionId={this.state.module_key+'_title_'+(i+this.state.count)}
                                                      getData={this.props.getData ? this.props.getData : null}
                                                    />
                                                </div>
                                                <div className="desc">
                                                    <CmsSet
                                                      sectionBackgroundArray={sectionBackgroundColorArray}
                                                      styleProp={
                                                        this.props.cmsArray[section_id+this.state.module_key+'_body_'+(i+this.state.count)+'style']
                                                      }
                                                      backgroundKeyFlag={true}
                                                      htmlProp={this.props.cmsArray[section_id+this.state.module_key+'_body_'+(i+this.state.count)]}
                                                      sectionKey={section_id}
                                                      backgroundKey={'service_'+(i+this.state.count)+'_global'}
                                                      sectionId={this.state.module_key+'_body_'+(i+this.state.count)}
                                                      getData={this.props.getData ? this.props.getData : null}
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                    :null
                                ))
                                :''
                            }
                        </div>
                    </div>
                </div>
        )
    }
}
const mapStateToProps = state => ({
    cmsArray:state.ThemeOptions.cmsArray,
    cmsModel:state.ThemeOptions.cmsModel,
    cmsKey:state.ThemeOptions.cmsKey,
    cmsValue:state.ThemeOptions.cmsValue,
    themeColor:state.ThemeOptions.themeColor,
    themeBackgroundColor:state.ThemeOptions.themeBackgroundColor,
    editorState:state.ThemeOptions.editorState,
    moduleCmsObject:state.ThemeOptions.moduleCmsObject,
    cmsEnable:state.ThemeOptions.cmsEnable,
    cmsEnableBackground:state.ThemeOptions.cmsEnableBackground,
    sectionBackgroundColor:state.ThemeOptions.sectionBackgroundColor,
    themeSectionBackgroundColor:state.ThemeOptions.themeSectionBackgroundColor,
    backgroundCmsKey:state.ThemeOptions.backgroundCmsKey,
    sectionBackgroundColor:state.ThemeOptions.sectionBackgroundColor,
    themeSectionBackgroundColor:state.ThemeOptions.themeSectionBackgroundColor,
    backgroundCmsKey:state.ThemeOptions.backgroundCmsKey,
    iconModel:state.ThemeOptions.iconModel,
    showIcon:state.ThemeOptions.showIcon,
    logoIcon:state.ThemeOptions.logoIcon,
    cmsEnableUploadImage:state.ThemeOptions.cmsEnableUploadImage,
});
const mapDispatchToProps = dispatch => ({
    setCmsArray: enable => dispatch(setCmsArray(enable)),
    setCmsModel: enable => dispatch(setCmsModel(enable)),
    setCmsKey: enable => dispatch(setCmsKey(enable)),
    setCmsValue: enable => dispatch(setCmsValue(enable)),
    setThemeColor: enable => dispatch(setThemeColor(enable)),
    setThemeBackgroundColor: enable => dispatch(setThemeBackgroundColor(enable)),
    setEditorState: enable => dispatch(setEditorState(enable)),
    setModuleCmsObject: enable => dispatch(setModuleCmsObject(enable)),
    setCmsEnable: enable => dispatch(setCmsEnable(enable)),
    setCmsEnableBackground: enable => dispatch(setCmsEnableBackground(enable)),
    setSectionBackgroundColor: enable => dispatch(setSectionBackgroundColor(enable)),
    setThemeSectionBackgroundColor: enable => dispatch(setThemeSectionBackgroundColor(enable)),
    setBackgroundCmsKey:enable=>dispatch(setBackgroundCmsKey(enable)),
    setThemeSectionBackgroundColor: enable => dispatch(setThemeSectionBackgroundColor(enable)),
    setBackgroundCmsKey:enable=>dispatch(setBackgroundCmsKey(enable)),
    setIconModel: enable => dispatch(setIconModel(enable)),
    setShowIcon: enable => dispatch(setShowIcon(enable)),
    setLogoIcon: enable => dispatch(setLogoIcon(enable)),
    setCmsEnableUploadImage: enable => dispatch(setCmsEnableUploadImage(enable)),
    
});
export default connect(mapStateToProps, mapDispatchToProps)(Service);