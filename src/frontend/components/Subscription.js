import React, { Component } from "react";
import { useDispatch } from "react-redux";
import {
  setCmsArray,
  setCmsModel,
  setCmsKey,
  setCmsValue,
  setThemeBackgroundColor,
  setThemeColor,
  setEditorState,
  setCmsEnable,
  setCmsEnableBackground,
  setSectionBackgroundColor,
  setThemeSectionBackgroundColor,
  setSectionId,
  setContactUsEmail,
  setSubscriptionEmail,
} from "../../reducers/ThemeOptions";
import CmsSet from "./Functions/CmsSet.js";
import Icon from "./Icon.js";
import { connect } from "react-redux";
import {
  EditorState,
  ContentState,
  convertFromHTML,
  convertFromRaw,
} from "draft-js";
import ApiCall from "../../admin/Partial/Function/ApiCall";
import { Editor } from "react-draft-wysiwyg";
import htmlToDraft from "html-to-draftjs";
import { faCogs } from "@fortawesome/free-solid-svg-icons";
import CloneIcon from "./CloneIcon.js";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
class Subscription extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email:null,
      cms_key_subscription: "subscription_background",
      addMessage:'add-text-'
    };
  }
  SetCms = (key, section_id) => {
    if (this.props.cmsEnable) {
      var tempArray = {
        key: section_id + this.state.cms_key_subscription,
        value: this.props.cmsArray[
          section_id + this.state.cms_key_subscription
        ],
      };
      const { setSectionBackgroundColor } = this.props;
      setSectionBackgroundColor(tempArray);
      const contentBlock = htmlToDraft(this.props.cmsArray[section_id + key]);
      if (contentBlock) {
        const contentState = ContentState.createFromBlockArray(
          contentBlock.contentBlocks
        );
        const editorState = EditorState.createWithContent(contentState);
        const { setEditorState } = this.props;
        setEditorState(editorState);
      }
      const { cmsKey, setCmsKey } = this.props;
      setCmsKey(section_id + key);
      const { cmsValue, setCmsValue } = this.props;
      setCmsValue(this.props.cmsArray[section_id + key]);
      const { cmsModel, setCmsModel } = this.props;
      setCmsModel(!this.props.cmsModel);
      const { setSectionId } = this.props;
      setSectionId(section_id);
      const { setSubscriptionEmail } = this.props;
      setSubscriptionEmail(
        this.props.cmsArray[section_id + "subscription_email"]
      );
      const { setContactUsEmail } = this.props;
      setContactUsEmail(this.props.cmsArray[section_id + "contact_us_email"]);
    }
  };
  SetImage = async (key, section_id) => {
    if (this.props.cmsEnable) {
      var tempArray = {
        key: section_id + this.state.cms_key_subscription,
        value: this.props.cmsArray[
          section_id + this.state.cms_key_subscription
        ],
      };
      const { setSectionBackgroundColor } = this.props;
      setSectionBackgroundColor(tempArray);
      const { cmsModelImage, setCmsModelImage } = this.props;
      setCmsModelImage(!this.props.cmsModelImage);
      const { cmsEnableUploadImage, setCmsEnableUploadImage } = this.props;
      setCmsEnableUploadImage(!this.props.cmsEnableUploadImage);
      const { cmsKey, setCmsKey } = this.props;
      const { cmsValue, setCmsValue } = this.props;
      setCmsValue(this.props.cmsArray[section_id + key]);
      setCmsKey(section_id + key);
    }
  };
  sendEmail = async () => {
    var data = {
      email: this.state.email,
    };
    await ApiCall.postAPICall("subscription", data);
  };
  render() {
    let key_id = this.props[0];
    let section_id = key_id + "_";
    var sectionBackgroundColorArray = {
      key: section_id + this.state.cms_key_subscription,
      value: this.props.cmsArray[section_id + this.state.cms_key_subscription]?this.props.cmsArray[section_id + this.state.cms_key_subscription][section_id + this.state.cms_key_subscription]:null,
    };
    return (
      <div>
        <div
          style={{
            background: this.props.cmsArray[
              section_id + this.state.cms_key_subscription
            ] ? this.props.cmsArray[
              section_id + this.state.cms_key_subscription
            ][section_id + this.state.cms_key_subscription] : "blue",
            paddingBottom: 36,
          }}
          id={section_id}
        >
          <CloneIcon
            current_section={this.props.current_section}
            section_flag={this.props.section_flag}
            class_set={this.props.class_set}
            {...[key_id]}
            getData={this.props.getData ? this.props.getData : null}
            getColors={this.props.getColors ? this.props.getColors : null}
          />
          <div className="container">
            <div className="row">
              <div className="col-lg-8 offset-lg-2 col-xl-6 offset-xl-3">
                <div
                  className="newsletter text-center"
                  style={{ marginTop: 30 }}
                >
                  <div className="text-white title">
                    <CmsSet
                      sectionBackgroundArray={sectionBackgroundColorArray}
                      styleProp={
                        this.props.cmsArray[
                          section_id + "subscription_newsletter_titlestyle"
                        ]
                      }
                      htmlProp={
                        this.props.cmsArray[
                          section_id + "subscription_newsletter_title"
                        ]
                      }
                      sectionKey={section_id}
                      sectionId={"subscription_newsletter_title"}
                      getData={this.props.getData ? this.props.getData : null}
                      addMessage={this.state.addMessage}
                    />
                  </div>
                  <p className="text-white">
                    <CmsSet
                      sectionBackgroundArray={sectionBackgroundColorArray}
                      styleProp={
                        this.props.cmsArray[
                          section_id + "subscription_newsletter_pstyle"
                        ]
                      }
                      htmlProp={
                        this.props.cmsArray[
                          section_id + "subscription_newsletter_p"
                        ]
                      }
                      sectionKey={section_id}
                      sectionId={"subscription_newsletter_p"}
                      getData={this.props.getData ? this.props.getData : null}
                      addMessage={this.state.addMessage}
                    />
                  </p>
                    <div className="newsletter-content">
                      <input
                        type="text"
                        name="email"
                        onChange={e => this.setState({email: e.target.value})}
                        placeholder={(this.props.cmsArray[section_id + "subscription_newsletter_placeholder_email"])?this.props.cmsArray[section_id + "subscription_newsletter_placeholder_email"][section_id + "subscription_newsletter_placeholder_email"]:'email'}
                        onClick={() =>
                          this.props.SetPlaceholder(key_id,"subscription_newsletter_placeholder_email")
                        }
                      />
                       <span
                        onClick={(e) => this.sendEmail(e)}
                        >
                        <CmsSet
                          sectionBackgroundArray={sectionBackgroundColorArray}
                          styleProp={
                            this.props.cmsArray[
                              section_id + "subscription_newsletter_btnstyle"
                            ]
                          }
                          type="grey_button"
                          htmlProp={
                            this.props.cmsArray[
                              section_id + "subscription_newsletter_btn"
                            ]
                          }
                          sectionKey={section_id}
                          sectionId={"subscription_newsletter_btn"}
                          getData={
                            this.props.getData ? this.props.getData : null
                          }
                          addMessage={this.state.addMessage}
                        />
                      </span>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="tap-top" style={this.props.themeBackgroundColor}>
          <div>
            <i className="zmdi zmdi-long-arrow-up"></i>
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => ({
  cmsArray: state.ThemeOptions.cmsArray,
  cmsModel: state.ThemeOptions.cmsModel,
  cmsKey: state.ThemeOptions.cmsKey,
  cmsValue: state.ThemeOptions.cmsValue,
  themeColor: state.ThemeOptions.themeColor,
  themeBackgroundColor: state.ThemeOptions.themeBackgroundColor,
  editorState: state.ThemeOptions.editorState,
  cmsEnable: state.ThemeOptions.cmsEnable,
  cmsEnableBackground: state.ThemeOptions.cmsEnableBackground,
  themeSectionBackgroundColor: state.ThemeOptions.themeSectionBackgroundColor,
  sectionId: state.ThemeOptions.sectionId,
  contact_us_email: state.ThemeOptions.contact_us_email,
  subscription_email: state.ThemeOptions.subscription_email,
});
const mapDispatchToProps = (dispatch) => ({
  setCmsArray: (enable) => dispatch(setCmsArray(enable)),
  setCmsModel: (enable) => dispatch(setCmsModel(enable)),
  setCmsKey: (enable) => dispatch(setCmsKey(enable)),
  setCmsValue: (enable) => dispatch(setCmsValue(enable)),
  setThemeColor: (enable) => dispatch(setThemeColor(enable)),
  setThemeBackgroundColor: (enable) =>
    dispatch(setThemeBackgroundColor(enable)),
  setEditorState: (enable) => dispatch(setEditorState(enable)),
  setCmsEnable: (enable) => dispatch(setCmsEnable(enable)),
  setCmsEnableBackground: (enable) => dispatch(setCmsEnableBackground(enable)),
  setSectionBackgroundColor: (enable) =>
    dispatch(setSectionBackgroundColor(enable)),
  setThemeSectionBackgroundColor: (enable) =>
    dispatch(setThemeSectionBackgroundColor(enable)),
  setSectionId: (enable) => dispatch(setSectionId(enable)),
  setContactUsEmail: (enable) => dispatch(setContactUsEmail(enable)),
  setSubscriptionEmail: (enable) => dispatch(setSubscriptionEmail(enable)),
});
export default connect(mapStateToProps, mapDispatchToProps)(Subscription);
