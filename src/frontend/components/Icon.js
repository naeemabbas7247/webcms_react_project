import React ,  { Component } from "react";
import { useDispatch } from 'react-redux';
import {
    setCmsArray,
    setCmsModel,
    setCmsKey,
    setCmsValue,
    setThemeBackgroundColor,
    setThemeColor,
    setEditorState,
    setCmsEnable,
    setCmsEnableUploadImage,
    setCmsModelImage
} from '../../reducers/ThemeOptions';
import {connect} from 'react-redux';
class Icon extends Component{
    render(){
        return(
            <div>
                <svg height="30pt" className="svgImageRotation" viewBox="-200 -350 469.33333 469" width="80pt" xmlns="http://www.w3.org/2000/svg">
                    <g fill={this.props.themeColor['color']}>
                        <path d="m437.332031.167969h-405.332031c-17.664062 0-32 14.335937-32 32v21.332031c0 17.664062 14.335938 32 32 32h405.332031c17.664063 0 32-14.335938 32-32v-21.332031c0-17.664063-14.335937-32-32-32zm0 0"/>
                    </g>
                </svg>
                <svg height="30pt"  className="svgImageRotation ml-minus" viewBox="200-10 469.33333 469" width="80pt" xmlns="http://www.w3.org/2000/svg">
                    <g fill={this.props.themeColor['color']}>
                        <path d="m437.332031.167969h-405.332031c-17.664062 0-32 14.335937-32 32v21.332031c0 17.664062 14.335938 32 32 32h405.332031c17.664063 0 32-14.335938 32-32v-21.332031c0-17.664063-14.335937-32-32-32zm0 0"/>
                    </g>
                </svg>
            </div>
        )
    }
}
const mapStateToProps = state => ({
    cmsArray:state.ThemeOptions.cmsArray,
    cmsModel:state.ThemeOptions.cmsModel,
    cmsKey:state.ThemeOptions.cmsKey,
    cmsValue:state.ThemeOptions.cmsValue,
    themeColor:state.ThemeOptions.themeColor,
    themeBackgroundColor:state.ThemeOptions.themeBackgroundColor,
    editorState:state.ThemeOptions.editorState,
    cmsEnable:state.ThemeOptions.cmsEnable,
    cmsEnableUploadImage:state.ThemeOptions.cmsEnableUploadImage,
    cmsModelImage:state.ThemeOptions.cmsModelImage,
});
const mapDispatchToProps = dispatch => ({
    setCmsArray: enable => dispatch(setCmsArray(enable)),
    setCmsModel: enable => dispatch(setCmsModel(enable)),
    setCmsKey: enable => dispatch(setCmsKey(enable)),
    setCmsValue: enable => dispatch(setCmsValue(enable)),
    setThemeColor: enable => dispatch(setThemeColor(enable)),
    setThemeBackgroundColor: enable => dispatch(setThemeBackgroundColor(enable)),
    setEditorState: enable => dispatch(setEditorState(enable)),
    setCmsEnable: enable => dispatch(setCmsEnable(enable)),
    setCmsModelImage: enable => dispatch(setCmsModelImage(enable)),
    setCmsEnableUploadImage: enable => dispatch(setCmsEnableUploadImage(enable)),
});
export default connect(mapStateToProps, mapDispatchToProps)(Icon);








