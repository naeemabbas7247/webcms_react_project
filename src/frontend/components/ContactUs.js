import React, { Component } from "react";
import { useDispatch } from "react-redux";
import {
  setCmsArray,
  setCmsModel,
  setCmsKey,
  setCmsValue,
  setThemeBackgroundColor,
  setThemeColor,
  setEditorState,
  setCmsEnable,
  setCmsEnableBackground,
  setSectionBackgroundColor,
  setThemeSectionBackgroundColor,
  setSectionId,
  setContactUsEmail,
  setSubscriptionEmail,
  setBackgroundCmsKey,
  setIconModel,
  setShowIcon,
  setLogoIcon,
  setModuleCmsObject,
} from "../../reducers/ThemeOptions";
import ApiCall from "../../admin/Partial/Function/ApiCall";
import Icon from "./Icon.js";
import CmsSet from "./Functions/CmsSet.js";
import { connect } from "react-redux";
import {
  EditorState,
  ContentState,
  convertFromHTML,
  convertFromRaw,
} from "draft-js";
import { Editor } from "react-draft-wysiwyg";
import htmlToDraft from "html-to-draftjs";
import { faCogs } from "@fortawesome/free-solid-svg-icons";
import CloneIcon from "./CloneIcon.js";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
class ContactUs extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cms_key_contact: "contactus_background",
      name: "",
      email: "",
      message: "",
      sent: false,
      imageLogo: "",
      imageLogo2: "",
      addMessage: "add-text-",
      removeMessage: "remove-text-",
      module_key: "contactus",
    };
  }
  SetCms = (key, section_id, background_key = null) => {
    if (this.props.cmsEnable) {
      var tempArray = {
        key: section_id + this.state.cms_key_contact,
        value: this.props.cmsArray[section_id + this.state.cms_key_contact],
      };
      const { setSectionBackgroundColor } = this.props;
      setSectionBackgroundColor(tempArray);
      const contentBlock = htmlToDraft(this.props.cmsArray[section_id + key]);
      if (contentBlock) {
        const contentState = ContentState.createFromBlockArray(
          contentBlock.contentBlocks
        );
        const editorState = EditorState.createWithContent(contentState);
        const { setEditorState } = this.props;
        setEditorState(editorState);
      }
      if (background_key) {
        const { setBackgroundCmsKey } = this.props;
        setBackgroundCmsKey(section_id + background_key);
      }
      const { cmsKey, setCmsKey } = this.props;
      setCmsKey(section_id + key);
      const { cmsValue, setCmsValue } = this.props;
      setCmsValue(this.props.cmsArray[section_id + key]);
      const { cmsModel, setCmsModel } = this.props;
      setCmsModel(!this.props.cmsModel);
      const { setSectionId } = this.props;
      setSectionId(section_id);
      const { setSubscriptionEmail } = this.props;
      setSubscriptionEmail(
        this.props.cmsArray[section_id + "subscription_email"]
      );
      const { setContactUsEmail } = this.props;
      setContactUsEmail(this.props.cmsArray[section_id + "contact_us_email"]);
    }
  };
  SetImage = async (key, section_id) => {
    if (this.props.cmsEnable) {
      var tempArray = {
        key: section_id + this.state.cms_key_contact,
        value: this.props.cmsArray[section_id + this.state.cms_key_contact],
      };
      const { setSectionBackgroundColor } = this.props;
      setSectionBackgroundColor(tempArray);
      const { cmsModelImage, setCmsModelImage } = this.props;
      setCmsModelImage(!this.props.cmsModelImage);
      const { cmsEnableUploadImage, setCmsEnableUploadImage } = this.props;
      setCmsEnableUploadImage(!this.props.cmsEnableUploadImage);
      const { cmsKey, setCmsKey } = this.props;
      const { cmsValue, setCmsValue } = this.props;
      setCmsValue(this.props.cmsArray[section_id + key]);
      setCmsKey(section_id + key);
    }
  };

  sendEmail = async () => {
    var data = {
      name: this.state.name,
      email: this.state.email,
      msg: this.state.message,
      admin_email: this.props.cmsArray[this.props[0] + "_contact_us_email"],
    };
    await ApiCall.postAPICall("contact/us", data);
    this.setState({ sent: true });
    setTimeout(() => this.setState({ sent: false }), 1500);
  };
  SetIcon = async (key, section_id) => {
    if (this.props.cmsEnable) {
      var tempArray = {
        key: section_id + "banner_background",
        value: this.props.cmsArray[section_id + "banner_background"],
      };
      const { setSectionBackgroundColor } = this.props;
      setSectionBackgroundColor(tempArray);

      const { iconModel, setIconModel } = this.props;
      setIconModel(!this.props.iconModel);

      const { cmsKey, setCmsKey } = this.props;
      const { cmsValue, setCmsValue } = this.props;
      setCmsValue(this.props.cmsArray[key]);
      setCmsKey(key);
    }
  };
  contactUsIcon(section_id) {
    return (
      <div>
        {this.props.cmsArray[section_id + this.state.module_key + "_icon"] ? (
          this.props.cmsArray[section_id + this.state.module_key + "_icon"][0][
            "cms_page"
          ]["is_icon"] == 0 ? (
            <img
              style={
                this.props.cmsArray[
                  section_id + this.state.module_key + "_iconstyle"
                ]
              }
              onClick={() =>
                this.SetIcon(
                  section_id + this.state.module_key + "_icon",
                  section_id
                )
              }
              className="image-1"
              src={
                this.props.cmsArray[
                  section_id + this.state.module_key + "_icon"
                ]
                  ? this.props.cmsArray[
                      section_id + this.state.module_key + "_icon"
                    ][0]["images_backups"]["image"]
                  : ""
              }
              controls
              width="5%"
              height="5%"
            />
          ) : (
            <i
              style={
                this.props.cmsArray[
                  section_id + this.state.module_key + "_iconstyle"
                ]
              }
              onClick={() =>
                this.SetIcon(
                  section_id + this.state.module_key + "_icon",
                  section_id
                )
              }
              className={
                this.props.cmsArray[
                  section_id + this.state.module_key + "_icon"
                ][0]["images_backups"]["image"]
              }
            ></i>
          )
        ) : (
          <img
            style={{ position: "relative", height: "3%", width: "3%" }}
            onClick={() =>
              this.SetIcon(
                section_id + this.state.module_key + "_icon",
                section_id
              )
            }
            className="image-1"
            src={"../../assets/images/app/mobile-2.png"}
            alt="App Landing"
          />
        )}
      </div>
    );
  }
  contactUs2Icon(section_id, count) {
    var i = 0;
    return (
      <div>
        {this.props.cmsArray[
          section_id + this.state.module_key + "_icon_" + (count)
        ] ? (
          this.props.cmsArray[
            section_id +
              this.state.module_key +
              "_icon_" +
              (count)
          ][0]?.cms_page.is_icon == 0 ? (
            <img
              style={{ marginTop: "0.8rem" }}
              onClick={() =>
                this.SetIcon(
                  section_id +
                    this.state.module_key +
                    "_icon_" +
                    (count),
                  section_id
                )
              }
              className="image-1"
              src={
                this.props.cmsArray[
                  section_id +
                    this.state.module_key +
                    "_icon_" +
                    (count)
                ]
                  ? this.props.cmsArray[
                      section_id +
                        this.state.module_key +
                        "_icon_" +
                        (count)
                    ][0]["images_backups"]["image"]
                  : ""
              }
              controls
              marginTop="0.8rem"
              width="50%"
              height="50%"
            />
          ) : this.props.cmsArray[
            section_id +
              this.state.module_key +
              "_icon_" +
              (count)
          ][0]?.cms_page.is_icon == 1 ?(
            <i
              onClick={() =>
                this.SetIcon(
                  section_id +
                    this.state.module_key +
                    "_icon_" +
                    (count),
                  section_id
                )
              }
              className={
                this.props.cmsArray[
                  section_id +
                    this.state.module_key +
                    "_icon_" +
                    (count)
                ][0]["images_backups"]["image"]
              }
            ></i>
          ) : (
            <img
              style={{
                position: "relative",
                marginTop: "0.8rem",
                height: "50%",
                width: "50%",
              }}
              onClick={() =>
                this.SetIcon(
                  section_id +
                    this.state.module_key +
                    "_icon_" +
                    (count),
                  section_id
                )
              }
              className="image-1"
              src={"../../assets/images/app/mobile-2.png"}
              alt="App Landing"
            />
          )
        ) : (
          <img
            style={{
              position: "relative",
              marginTop: "0.8rem",
              height: "50%",
              width: "50%",
            }}
            onClick={() =>
              this.SetIcon(
                section_id +
                  this.state.module_key +
                  "_icon_" +
                  (count),
                section_id
              )
            }
            className="image-1"
            src={"../../assets/images/app/mobile-2.png"}
            alt="App Landing"
          />
        )}
      </div>
    );
  }
  componentDidUpdate(prevProps) {
    var section_id = this.props[0] + "_";
    var newArray2 = this.props.cmsArray[section_id + "contactus_icon"];
  }
  render() {
    let key_id = this.props[0];
    let section_id = key_id + "_";
    var sectionBackgroundColorArray = {
      key: section_id + this.state.cms_key_contact,
      value: this.props.cmsArray[section_id + this.state.cms_key_contact]
        ? this.props.cmsArray[section_id + this.state.cms_key_contact][
            section_id + this.state.cms_key_contact
          ]
        : null,
    };
    return (
      <div>
        <div
          className={`footer-area ${this.props.class_set}`}
          style={{ paddingBottom: 10 }}
          id={section_id}
        >
          <div
            className="footer-bg"
            style={{
              background: this.props.cmsArray[
                section_id + this.state.cms_key_contact
              ],
            }}
          ></div>
          <CloneIcon
            class_set={this.props.class_set}
            current_section={this.props.current_section}
            section_flag={this.props.section_flag}
            {...[key_id]}
            getData={this.props.getData ? this.props.getData : null}
            getColors={this.props.getColors ? this.props.getColors : null}
          />
          <div className="container">
            <div className="row">
              <div className="col-lg-12">
                <div className="section-title text-center mb--40">
                  <div
                    className="text-white"
                    onClick={() => this.SetCms("contactus_heading", section_id)}
                  >
                    <CmsSet
                      sectionBackgroundArray={sectionBackgroundColorArray}
                      styleProp={
                        this.props.cmsArray[
                          section_id + "contactus_headingstyle"
                        ]
                      }
                      htmlProp={
                        this.props.cmsArray[section_id + "contactus_heading"]
                      }
                      sectionKey={section_id}
                      sectionId={"contactus_heading"}
                      getData={this.props.getData ? this.props.getData : null}
                      addMessage={this.state.addMessage}
                    />
                  </div>
                  {this.contactUsIcon(section_id)}
                  <p
                    className="text-white"
                    onClick={() => this.SetCms("contactus_p", section_id)}
                    addMessage={this.state.addMessage}
                  >
                    <CmsSet
                      sectionBackgroundArray={sectionBackgroundColorArray}
                      styleProp={
                        this.props.cmsArray[section_id + "contactus_pstyle"]
                      }
                      htmlProp={this.props.cmsArray[section_id + "contactus_p"]}
                      sectionKey={section_id}
                      sectionId={"contactus_p"}
                      getData={this.props.getData ? this.props.getData : null}
                      addMessage={this.state.addMessage}
                    />
                  </p>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-5 col-xl-4 offset-xl-1">
                <div className="contact-inner">
                  <div className="title">
                    <CmsSet
                      sectionBackgroundArray={sectionBackgroundColorArray}
                      styleProp={
                        this.props.cmsArray[section_id + "contactus_titlestyle"]
                      }
                      htmlProp={
                        this.props.cmsArray[section_id + "contactus_title"]
                      }
                      sectionKey={section_id}
                      sectionId={"contactus_title"}
                      getData={this.props.getData ? this.props.getData : null}
                      addMessage={this.state.addMessage}
                    />
                  </div>
                  <form className="contact-form">
                    <div className="input-box">
                      <input
                        type="text"
                        placeholder={
                          this.props.cmsArray[section_id + "contactus_placeholder_name"]?this.props.cmsArray[section_id + "contactus_placeholder_name"][section_id + "contactus_placeholder_name"]:'name'
                        }
                        value={this.state.name}
                        onChange={(e) =>
                          this.setState({ name: e.target.value })
                        }
                        onClick={() =>
                          this.props.SetPlaceholder(key_id,"contactus_placeholder_name")
                        }
                      />
                    </div>
                    <div className="input-box">
                      <input
                        type="text"
                        placeholder={this.props.cmsArray[section_id + "contactus_placeholder_email"]?this.props.cmsArray[section_id + "contactus_placeholder_email"][section_id + "contactus_placeholder_email"]:'email'}
                        value={this.state.email}
                        onChange={(e) =>
                          this.setState({ email: e.target.value })
                        }
                        onClick={() =>
                          this.props.SetPlaceholder(key_id,"contactus_placeholder_email")
                        }
                      />
                    </div>
                    <div className="input-box">
                      <textarea
                        placeholder={this.props.cmsArray[section_id + "contactus_placeholder_message"]?this.props.cmsArray[section_id + "contactus_placeholder_message"][section_id + "contactus_placeholder_message"]:'message'}
                        value={this.state.message}
                        onChange={(e) =>
                          this.setState({ message: e.target.value })
                        }
                        onClick={() =>
                          this.props.SetPlaceholder(key_id,"contactus_placeholder_message")
                        }
                      ></textarea>
                    </div>
                    <div className="input-box">
                      <button
                        className="submite-button"
                        style={{
                          background: this.props.cmsArray[
                            section_id + "contactus_btn_1_global"
                          ],
                        }}
                        onClick={(e) => this.sendEmail(e)}
                      >
                        <CmsSet
                          sectionBackgroundArray={sectionBackgroundColorArray}
                          styleProp={{
                            ...this.props.cmsArray[
                              section_id + "contactus_btnstyle"
                            ],
                            background: this.props.cmsArray[
                              section_id + "contactus_btn_global"
                            ]
                              ? this.props.cmsArray[
                                  section_id + "contactus_btn_global"
                                ][section_id + "contactus_btn_global"]
                              : null,
                          }}
                          htmlProp={
                            this.props.cmsArray[section_id + "contactus_btn"]
                          }
                          sectionKey={section_id}
                          globalBtn={true}
                          backgroundKey={"contactus_btn_global"}
                          backgroundStyle={
                            this.props.cmsArray[
                              section_id + "contactus_btn_global"
                            ]
                              ? this.props.cmsArray[
                                  section_id + "contactus_btn_global"
                                ][section_id + "contactus_btn_global"]
                              : null
                          }
                          submitBtnFlag={true}
                          isIcon={true}
                          sectionId={"contactus_btn"}
                          getData={
                            this.props.getData ? this.props.getData : null
                          }
                          addMessage={this.state.addMessage}
                        />
                      </button>
                    </div>
                  </form>
                </div>
              </div>
              <div className="col-lg-5 offset-lg-2 col-xl-4 offset-xl-2 mt_md--40 mt_sm--40">
                <div className="contact-inner">
                  <div className="title">
                    <CmsSet
                      sectionBackgroundArray={sectionBackgroundColorArray}
                      styleProp={
                        this.props.cmsArray[
                          section_id + "contactus_title_contactstyle"
                        ]
                      }
                      htmlProp={
                        this.props.cmsArray[
                          section_id + "contactus_title_contact"
                        ]
                      }
                      sectionKey={section_id}
                      sectionId={"contactus_title_contact"}
                      getData={this.props.getData ? this.props.getData : null}
                      addMessage={this.state.addMessage}
                    />
                  </div>
                  <div className="conatct-info">
                    <div className="single-contact-info">
                      <div className="contact-icon">
                        {this.contactUs2Icon(section_id, 1)}
                      </div>
                      <div className="contact-text">
                        <span>
                          <CmsSet
                            sectionBackgroundArray={sectionBackgroundColorArray}
                            styleProp={
                              this.props.cmsArray[
                                section_id + "contactus_number_1style"
                              ]
                            }
                            htmlProp={
                              this.props.cmsArray[
                                section_id + "contactus_number_1"
                              ]
                            }
                            sectionKey={section_id}
                            sectionId={"contactus_number_1"}
                            getData={
                              this.props.getData ? this.props.getData : null
                            }
                            addMessage={this.state.addMessage}
                          />
                          <br />
                          <CmsSet
                            sectionBackgroundArray={sectionBackgroundColorArray}
                            styleProp={
                              this.props.cmsArray[
                                section_id + "contactus_number_2style"
                              ]
                            }
                            htmlProp={
                              this.props.cmsArray[
                                section_id + "contactus_number_2"
                              ]
                            }
                            sectionKey={section_id}
                            sectionId={"contactus_number_2"}
                            getData={
                              this.props.getData ? this.props.getData : null
                            }
                            addMessage={this.state.addMessage}
                          />
                        </span>
                      </div>
                    </div>
                    <div className="single-contact-info">
                      <div className="contact-icon">
                        {this.contactUs2Icon(section_id, 2)}
                      </div>
                      <div className="contact-text">
                        <span>
                          <CmsSet
                            sectionBackgroundArray={sectionBackgroundColorArray}
                            styleProp={
                              this.props.cmsArray[
                                section_id + "contactus_emailstyle"
                              ]
                            }
                            htmlProp={
                              this.props.cmsArray[
                                section_id + "contactus_email"
                              ]
                            }
                            sectionKey={section_id}
                            sectionId={"contactus_email"}
                            getData={
                              this.props.getData ? this.props.getData : null
                            }
                            addMessage={this.state.addMessage}
                          />
                          <br />
                          <CmsSet
                            sectionBackgroundArray={sectionBackgroundColorArray}
                            styleProp={
                              this.props.cmsArray[
                                section_id + "contactus_websitestyle"
                              ]
                            }
                            htmlProp={
                              this.props.cmsArray[
                                section_id + "contactus_website"
                              ]
                            }
                            sectionKey={section_id}
                            sectionId={"contactus_website"}
                            getData={
                              this.props.getData ? this.props.getData : null
                            }
                            addMessage={this.state.addMessage}
                          />
                        </span>
                      </div>
                    </div>
                    <div className="single-contact-info">
                      <div className="contact-icon">
                        {this.contactUs2Icon(section_id, 3)}
                      </div>
                      <div className="contact-text">
                        <span>
                          <CmsSet
                            sectionBackgroundArray={sectionBackgroundColorArray}
                            styleProp={
                              this.props.cmsArray[
                                section_id + "contactus_addressstyle"
                              ]
                            }
                            htmlProp={
                              this.props.cmsArray[
                                section_id + "contactus_address"
                              ]
                            }
                            sectionKey={section_id}
                            sectionId={"contactus_address"}
                            getData={
                              this.props.getData ? this.props.getData : null
                            }
                            addMessage={this.state.addMessage}
                          />
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="tap-top" style={this.props.themeBackgroundColor}>
          <div>
            <i className="zmdi zmdi-long-arrow-up"></i>
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => ({
  cmsArray: state.ThemeOptions.cmsArray,
  cmsModel: state.ThemeOptions.cmsModel,
  cmsKey: state.ThemeOptions.cmsKey,
  cmsValue: state.ThemeOptions.cmsValue,
  themeColor: state.ThemeOptions.themeColor,
  themeBackgroundColor: state.ThemeOptions.themeBackgroundColor,
  editorState: state.ThemeOptions.editorState,
  cmsEnable: state.ThemeOptions.cmsEnable,
  cmsEnableBackground: state.ThemeOptions.cmsEnableBackground,
  themeSectionBackgroundColor: state.ThemeOptions.themeSectionBackgroundColor,
  sectionId: state.ThemeOptions.sectionId,
  contact_us_email: state.ThemeOptions.contact_us_email,
  subscription_email: state.ThemeOptions.subscription_email,
  backgroundCmsKey: state.ThemeOptions.backgroundCmsKey,
  iconModel: state.ThemeOptions.iconModel,
  showIcon: state.ThemeOptions.showIcon,
  logoIcon: state.ThemeOptions.logoIcon,
  moduleCmsObject: state.ThemeOptions.moduleCmsObject,
});
const mapDispatchToProps = (dispatch) => ({
  setCmsArray: (enable) => dispatch(setCmsArray(enable)),
  setCmsModel: (enable) => dispatch(setCmsModel(enable)),
  setCmsKey: (enable) => dispatch(setCmsKey(enable)),
  setCmsValue: (enable) => dispatch(setCmsValue(enable)),
  setThemeColor: (enable) => dispatch(setThemeColor(enable)),
  setThemeBackgroundColor: (enable) =>
    dispatch(setThemeBackgroundColor(enable)),
  setEditorState: (enable) => dispatch(setEditorState(enable)),
  setCmsEnable: (enable) => dispatch(setCmsEnable(enable)),
  setCmsEnableBackground: (enable) => dispatch(setCmsEnableBackground(enable)),
  setSectionBackgroundColor: (enable) =>
    dispatch(setSectionBackgroundColor(enable)),
  setThemeSectionBackgroundColor: (enable) =>
    dispatch(setThemeSectionBackgroundColor(enable)),
  setSectionId: (enable) => dispatch(setSectionId(enable)),
  setContactUsEmail: (enable) => dispatch(setContactUsEmail(enable)),
  setSubscriptionEmail: (enable) => dispatch(setSubscriptionEmail(enable)),
  setBackgroundCmsKey: (enable) => dispatch(setBackgroundCmsKey(enable)),
  setIconModel: (enable) => dispatch(setIconModel(enable)),
  setShowIcon: (enable) => dispatch(setShowIcon(enable)),
  setLogoIcon: (enable) => dispatch(setLogoIcon(enable)),
  setModuleCmsObject: (enable) => dispatch(setModuleCmsObject(enable)),
});
export default connect(mapStateToProps, mapDispatchToProps)(ContactUs);
