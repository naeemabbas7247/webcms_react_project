import React ,  { Component } from "react";
import {
    setCmsArray,
    setCmsModel,
    setCmsKey,
    setCmsValue,
    setThemeBackgroundColor,
    setThemeColor,
    setEditorState,
    setModuleCmsObject,
    setCmsEnable,
    setCmsModelImage,
    setCmsEnableUploadImage,
    setSectionBackgroundColor,
    setThemeSectionBackgroundColor,
    setBackgroundCmsKey,
    setIconModel,
    setShowIcon,
    setLogoIcon,
} from '../../reducers/ThemeOptions';
import {connect} from 'react-redux';
import Icon from './Icon.js';
import CloneIcon from './CloneIcon.js';
import { EditorState, ContentState, convertFromHTML,convertFromRaw } from 'draft-js'
import { Editor } from 'react-draft-wysiwyg';
import htmlToDraft from 'html-to-draftjs';
import ReactPlayer from "react-player";
import {
  Carousel,
  CarouselItem,
  CarouselControl,
  CarouselIndicators,
  CarouselCaption,
} from "reactstrap";
import CmsSet from "./Functions/CmsSet.js";
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
class Feature extends Component{
    constructor(props){
      super(props);
      this.state={
        module_key:'feature',
        background:'',
        activeIndex: 0,
        animating: false,
        imageLogo: "",
        imageLogo2: "",
        count:1,
        addMessage:'edit-text-',
        removeMessage:'remove-text-',
        arrowUpMessage:'move-to-top'
      }
    }
    SetCms=(key,section_id,background_key=null) => {
      if(this.props.cmsEnable)
      {
          var tempArray={'key':section_id+'feature_background','value':this.props.cmsArray[section_id+'feature_background']}
          const {setSectionBackgroundColor } = this.props;
          setSectionBackgroundColor( tempArray); 
          const contentBlock = htmlToDraft(this.props.cmsArray[section_id+key]);
          if (contentBlock) {
              const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks);
              const editorState = EditorState.createWithContent(contentState);
              const {setEditorState } = this.props;
              setEditorState(editorState);
          }
          if(background_key)
          {
              const {setBackgroundCmsKey } = this.props;
              setBackgroundCmsKey(section_id+background_key);
          }
          const { cmsKey, setCmsKey } = this.props;
          setCmsKey(section_id+key);
          const { cmsValue, setCmsValue } = this.props;
          setCmsValue(this.props.cmsArray[section_id+key]);
          const { cmsModel, setCmsModel } = this.props;
          setCmsModel(!this.props.cmsModel);
      }
  }
  SetImage = async (key, section_id) => {
    if (this.props.cmsEnable) {
      var tempArray = {
        key: section_id + "banner_background",
        value: this.props.cmsArray[section_id + "banner_background"],
      };
      const { setSectionBackgroundColor } = this.props;
      setSectionBackgroundColor(tempArray);
      const { cmsModelImage, setCmsModelImage } = this.props;
      setCmsModelImage(!this.props.cmsModelImage);
      const { cmsEnableUploadImage, setCmsEnableUploadImage } = this.props;
      setCmsEnableUploadImage(!this.props.cmsEnableUploadImage);
      const { cmsKey, setCmsKey } = this.props;
      const { cmsValue, setCmsValue } = this.props;
      setCmsValue(this.props.cmsArray[key]);
      setCmsKey(key);
    }
  };
  bannerSet(section_id) {
    if (this.props.cmsArray[section_id + "feature_img"]) {
      if (this.props.cmsArray[section_id + "feature_img"].length > 0) {
        if (
          (this.props.cmsArray[section_id + "feature_img"][0]["images_backups"]["image"]).includes("http")
        ) {
          if (this.props.cmsArray[section_id + "feature_img"][0].length == 1) {
            return (
              <div style={{position: "relative", paddingTop :  "56.25%"}}>
                <ReactPlayer
                  onClick={() =>
                    this.SetImage(section_id + "feature_img", section_id)
                  }
                  url={
                    this.props.cmsArray[section_id + "feature_img"][
                      "images_backups"
                    ]["image"]
                  }
                  style={{ 
                    position: "absolute",
                    top: 0,
                    left: 0,
                    padding: 12, 
                  }}
                  controls
                  width='100%'
                  height='100%'
                />
              </div>
            );
          } else {
            const slides = this.props.cmsArray[section_id + "feature_img"].map(
              (item) => {
                return (
                  <CarouselItem
                    onExiting={() => this.setState({ animating: true })}
                    onExited={() => this.setState({ animating: false })}
                    key={item.id}
                  >
                    <div style={{position: "relative", paddingTop :  "56.25%"}}>
                      <ReactPlayer

                        onClick={() =>
                          this.SetImage(section_id + "feature_img", section_id)
                        }
                        url={
                          item.images_backups.image
                        }
                        style={{ 
                          position: "absolute",
                          top: 0,
                          left: 0,
                          padding: 12, 
                        }}
                        controls
                        width='100%'
                        height='100%'
                      />
                    </div>
                  </CarouselItem>
                );
              }
            );
            return (
                <Carousel
                  onClick={() =>
                    this.SetImage(section_id + "feature_img", section_id)
                  }
                  activeIndex={this.state.activeIndex}
                  next={this.next}
                  previous={this.previous}
                >
                  <CarouselIndicators
                    items={this.props.cmsArray[section_id + "feature_img"]}
                    activeIndex={this.state.activeIndex}
                    onClickHandler={this.goToIndex}
                  />
                  {slides}
                  <CarouselControl
                    direction="prev"
                    directionText="Previous"
                    onClickHandler={() => this.previous()}
                  />
                  <CarouselControl
                    direction="next"
                    directionText="Next"
                    onClickHandler={() => this.next()}
                  />
                </Carousel>
            );
          }
        } else {
          if (this.props.cmsArray[section_id + "feature_img"][0].length == 1) {
            return (
              <img
                style={[this.props.cmsArray[section_id + "feature_imgstyle"],{position:"relative",
                height:"100%",
                width:"100%"}]}
                onClick={() =>
                  this.SetImage(section_id + "feature_img", section_id)
                }
                className="image-1"
                src={
                  this.props.cmsArray[section_id + "feature_img"][0][
                    "images_backups"
                  ]["image"]
                }
                alt="App Landing"
              />
            );
          } else {
            const slides = this.props.cmsArray[section_id + "feature_img"].map(
              (item) => {
                return (
                  <CarouselItem
                    onExiting={() => this.setState({ animating: true })}
                    onExited={() => this.setState({ animating: false })}
                    key={item.id}
                  >
                    <img
                      src={item.images_backups.image} alt={item.altText} onClick={() => this.SetImage(section_id + "feature_img", section_id)}
                      style={{
                        ...this.props.cmsArray[section_id + "feature_imgstyle"],
                        position:"relative",
                        height:"100%",
                        width:"100%"
                      }}
                      />
                    </CarouselItem>    
                      );
              }
            );
            return (
                <Carousel
                  onClick={() =>
                    this.SetImage(section_id + "feature_img", section_id)
                  }
                  activeIndex={this.state.activeIndex}
                  next={this.next}
                  previous={this.previous}
                >
                  <CarouselIndicators
                    items={this.props.cmsArray[section_id + "feature_img"]}
                    activeIndex={this.state.activeIndex}
                    onClickHandler={this.goToIndex}
                  />
                  {slides}
                  <CarouselControl
                    direction="prev"
                    directionText="Previous"
                    onClickHandler={() => this.previous()}
                  />
                  <CarouselControl
                    direction="next"
                    directionText="Next"
                    onClickHandler={() => this.next()}
                  />
                </Carousel>
            );
          }
        }
      } else {
          return(
            <img
            style={{position:"relative",
            height:"100%",
            width:"100%"}}
            onClick={() =>
              this.SetImage(section_id + "feature_img", section_id)
            }
            className="image-1"
            src={
              "../../assets/images/app/mobile-2.png"
            }
            alt="App Landing"
          />
        );
      }
    }
    else {
      return(
        <img
        style={{position:"relative",
        height:"100%",
        width:"100%"}}
        onClick={() =>
          this.SetImage(section_id + "feature_img", section_id)
        }
        className="image-1"
        src={
          "../../assets/images/app/mobile-2.png"
        }
        alt="App Landing"
      />
    );
  }
  }
  next = () => {
    if (this.state.animating) return;
    const nextIndex =
      this.state.activeIndex ===
      this.props.cmsArray[this.props[0] + "_" + "feature_img"].length - 1
        ? 0
        : this.state.activeIndex + 1;
    this.setState({ activeIndex: nextIndex });
  };

  previous = () => {
    if (this.state.animating) return;
    const nextIndex =
      this.state.activeIndex === 0
        ? this.props.cmsArray[this.props[0] + "_" + "feature_img"].length - 1
        : this.state.activeIndex - 1;
    // console.log(nextIndex);
    this.setState({ activeIndex: nextIndex });
  };
  SetIcon = async (key, section_id) => {
        if (this.props.cmsEnable) {
          var tempArray = {
            key: section_id + "banner_background",
            value: this.props.cmsArray[section_id + "banner_background"],
          };
          const { setSectionBackgroundColor } = this.props;
          setSectionBackgroundColor(tempArray);
          
          const { iconModel, setIconModel } = this.props;
          setIconModel(!this.props.iconModel);

          const { cmsEnableUploadImage, setCmsEnableUploadImage } = this.props;
          setCmsEnableUploadImage(!this.props.cmsEnableUploadImage);
          const { cmsKey, setCmsKey } = this.props;
          const { cmsValue, setCmsValue } = this.props;
          setCmsValue(this.props.cmsArray[key]);
          setCmsKey(key);
        }
      };
  featureIcon(section_id) {
     return (  
      <div>
        {this.props.cmsArray[section_id + "feature_icon"] ? this.props.cmsArray[section_id + "feature_icon"][0]['cms_page']['is_icon'] == 0 ? 
            <img
            style={this.props.cmsArray[section_id + "feature_iconstyle"]}
            onClick={() =>
            this.SetIcon(section_id + "feature_icon", section_id)
            }
            className="image-1"
            src={ this.props.cmsArray[section_id + "feature_icon"] ? this.props.cmsArray[section_id + "feature_icon"][0][ "images_backups" ]["image"] : ""}
            controls
            width='5%'
            height='5%'
            />
            : 
            <i 
            style={this.props.cmsArray[section_id + "feature_iconstyle"]}
            onClick={() => this.SetIcon(section_id + "feature_icon", section_id)}
            className={this.props.cmsArray[section_id + "feature_icon"][0]["images_backups"]["image"]} 
            src={this.props.cmsArray[section_id + "feature_icon"]}>
            </i>
            :
            <img
            style={{position:"relative", height:"3%",width:"3%"}}
            onClick= { () => this.SetIcon(section_id + "feature_icon", section_id) }
            className="image-1" src={"../../assets/images/app/mobile-2.png"} alt="App Landing"
            />
          }  
      </div>
      )
           }
           featureIcon2(section_id) {
             var i=0;
            return (  
             
                          <div>
                {this.props.cmsArray[section_id + this.state.module_key+"_icon_"+(i+this.state.count)] ? this.props.cmsArray[section_id + this.state.module_key+"_icon_"+(i+this.state.count)][0]['cms_page']['is_icon'] == 0 ? 
                   <img
                   style={{marginTop:"20px",}}
                   onClick={() =>
                   this.SetIcon(section_id + this.state.module_key+"_icon_"+(i+this.state.count), section_id)
                   }
                   className="image-1"
                   src={ this.props.cmsArray[section_id + this.state.module_key+"_icon_"+(i+this.state.count)] ? this.props.cmsArray[section_id + this.state.module_key+"_icon_"+(i+this.state.count)][0][ "images_backups" ]["image"] : ""}
                   controls
                   width='50%'
                   height='50%'
                   /> : 
                   <i 
                   
                   onClick={() =>
                   this.SetIcon(section_id + this.state.module_key+"_icon_"+(i+this.state.count), section_id)
                   }
                   className={this.props.cmsArray[section_id + this.state.module_key+"_icon_"+(i+this.state.count)][0]["images_backups"]["image"]} 
                   src={
                   this.props.cmsArray[section_id + this.state.module_key+"_icon_"+(i+this.state.count)]
                   }></i> 
                   : 
                   <img
                   style={{position:"relative",
                   marginTop: '0.8rem',
                   height:"50%",
                   width:"50%"}}
                   onClick={() =>
                   this.SetIcon(section_id + this.state.module_key+"_icon_"+(i+this.state.count), section_id)
                   }
                   className="image-1"
                   src={
                   "../../assets/images/app/mobile-2.png"
                   }
                   alt="App Landing" 
                   />
                    }
                    </div>
                   
             )
                  }
           

  componentDidUpdate(prevProps) {
    var section_id = this.props[0] + "_";
    var oldArray = prevProps.cmsArray[section_id + "feature_img"];
    var newArray = this.props.cmsArray[section_id + "feature_img"];
    var newArray2 = this.props.cmsArray[section_id + "feature_icon"];
    var newArray3 = this.props.cmsArray[section_id + "feature_icon2"];
    var check = JSON.stringify(oldArray) != JSON.stringify(newArray);
    if (check) {
      this.bannerSet(section_id);
    }
  }

  goToIndex = (newIndex) => {
    
    if (this.state.animating) return;
    this.setState({ activeIndex: newIndex });
  };
    render() {
      
      let key_id=this.props[0];
      
      let section_id=key_id+'_';
      
      var sectionBackgroundColorArray = {
          key: section_id + "feature_background",
          value: this.props.cmsArray[section_id + "feature_background"]?this.props.cmsArray[section_id + "feature_background"][section_id + "feature_background"]:null,
        };
       return(
          <div className= {`feature-area pb--50 ${this.props.horizontalfeature}`} id={section_id} style={{'backgroundColor': this.props.cmsArray[section_id+'feature_background']? this.props.cmsArray[section_id+'feature_background'][section_id+'feature_background'] : "red", paddingTop:12}}>
             <CloneIcon current_section={this.props.current_section} section_flag={this.props.section_flag} class_set={this.props.class_set} {...[key_id]} getData={this.props.getData ? this.props.getData : null} getColors={this.props.getColors ? this.props.getColors : null}
             arrowUpMessage={this.state.arrowUpMessage}/>
            <div className="container" >
              <div className="row" >
                <div className="col-lg-12" >
                  <div  className="section-title text-center mb--40">
                    <CmsSet
                      sectionBackgroundArray={sectionBackgroundColorArray}
                      styleProp={this.props.cmsArray[section_id + "feature_headingstyle"]}
                      htmlProp={this.props.cmsArray[section_id + "feature_heading"]}
                      sectionKey={section_id}
                      sectionId={"feature_heading"}
                      getData={this.props.getData ? this.props.getData : null}
                      addMessage={this.state.addMessage}
                      removeMessage={this.state.removeMessage}
                      
                      />  
                      
                    <CmsSet
                      sectionBackgroundArray={sectionBackgroundColorArray}
                      styleProp={this.props.cmsArray[section_id + "feature_pstyle"]}
                      htmlProp={this.props.cmsArray[section_id + "feature_p"]}
                      sectionKey={section_id}
                      sectionId={"feature_p"}
                      getData={this.props.getData ? this.props.getData : null}  
                      addMessage={this.state.addMessage}
                      removeMessage={this.state.removeMessage}
                      
                    /> 
                    </div>
                </div>
              </div>
              <div className="row">
                <div className="col-lg-6" style={{justifyContent:"center", alignItems:"center", textAlign:"center"}}>
                  <div style={{height: "100vh", /* Magic here */
                    "display": "flex",
                    "justify-content": "center",
                    "align-items": "center",}}
                    >
                      <div style={{height:"50%"}}>
                      {this.bannerSet(section_id)}
                      </div>
                  </div>
                </div>  
                 {/* <img style={this.props.cmsArray[section_id+'feature_imgstyle']}  onClick={() => this.SetImage('feature_img',section_id)} className="image-1 feature_image feature_image_position" src={this.props.cmsArray[section_id+'feature_img']} alt="App Landing"/> */}
                <div className="col-lg-6 ">
                  <div className="feature-list">
                   {
                      Object.keys(this.props.moduleCmsObject).length != 0 ? 
                        Object.keys(this.props.moduleCmsObject).map((val, j) => (
                          <div>
                            {this.props.cmsArray[section_id+this.state.module_key+'_title_'+(j+this.state.count)] ?
                              <div className="feature" key={j}>
                                  <div className="feature-icon" id={section_id+'feature_'+(j+this.state.count)+'_global'} style={{background:this.props.cmsArray[section_id+'feature_'+(j+this.state.count)+'_global'][section_id+'feature_'+(j+this.state.count)+'_global']}}> 
                                    {this.featureIcon2(section_id + this.state.module_key+"_icon_"+(j+this.state.count), section_id)}
                                  </div>
                                   <div className="content">
                                    <div className="title">
                                        <CmsSet //headings
                                             sectionBackgroundArray={sectionBackgroundColorArray}
                                             styleProp={this.props.cmsArray[section_id+this.state.module_key+'_title_'+(j+this.state.count)+'style']}
                                             htmlProp={this.props.cmsArray[section_id+this.state.module_key+'_title_'+(j+this.state.count)]}
                                             sectionKey={section_id}
                                             backgroundKey={'feature_'+(j+this.state.count)+'_global'}
                                             sectionId={this.state.module_key+' _title_'+(j+this.state.count)}
                                             getData={this.props.getData ? this.props.getData : null}
                                             addMessage={this.state.addMessage}
                                             removeMessage={this.state.removeMessage}
                                             />
                                      </div>    
                                      <div className="desc">
                                        <CmsSet //content
                                             sectionBackgroundArray={sectionBackgroundColorArray}
                                             styleProp={this.props.cmsArray[section_id+this.state.module_key+'_body_'+(j+this.state.count)+'style'] }
                                             htmlProp={this.props.cmsArray[section_id+this.state.module_key+'_body_'+(j+this.state.count)]}
                                             sectionKey={section_id}
                                             backgroundKey={'feature_'+(j+this.state.count)+'_global'}
                                             sectionId={this.state.module_key+'_body_'+(j+this.state.count)}
                                             getData={this.props.getData ? this.props.getData : null}
                                             addMessage={this.state.addMessage}
                                             removeMessage={this.state.removeMessage}
                                             /> 
                                      </div>  
                                      </div>
                              </div>
                            :null}
                          </div>
                        ))
                      :''
                    }
                    
                  </div>
                </div>
              </div>
            </div>
          </div>
        )
    }
}

const mapStateToProps = state => ({
    cmsArray:state.ThemeOptions.cmsArray,
    cmsModel:state.ThemeOptions.cmsModel,
    cmsKey:state.ThemeOptions.cmsKey,
    cmsValue:state.ThemeOptions.cmsValue,
    themeColor:state.ThemeOptions.themeColor,
    themeBackgroundColor:state.ThemeOptions.themeBackgroundColor,
    editorState:state.ThemeOptions.editorState,
    cmsEnable:state.ThemeOptions.cmsEnable,
    moduleCmsObject:state.ThemeOptions.moduleCmsObject,
    cmsEnableUploadImage:state.ThemeOptions.cmsEnableUploadImage,
    cmsModelImage:state.ThemeOptions.cmsModelImage,
    sectionBackgroundColor:state.ThemeOptions.sectionBackgroundColor,
    themeSectionBackgroundColor:state.ThemeOptions.themeSectionBackgroundColor,
    backgroundCmsKey:state.ThemeOptions.backgroundCmsKey,
    iconModel:state.ThemeOptions.iconModel,
    showIcon:state.ThemeOptions.showIcon,
    logoIcon:state.ThemeOptions.logoIcon,
});
const mapDispatchToProps = dispatch => ({
    setCmsArray: enable => dispatch(setCmsArray(enable)),
    setCmsModel: enable => dispatch(setCmsModel(enable)),
    setCmsKey: enable => dispatch(setCmsKey(enable)),
    setCmsValue: enable => dispatch(setCmsValue(enable)),
    setThemeColor: enable => dispatch(setThemeColor(enable)),
    setThemeBackgroundColor: enable => dispatch(setThemeBackgroundColor(enable)),
    setEditorState: enable => dispatch(setEditorState(enable)),
    setCmsEnable: enable => dispatch(setCmsEnable(enable)),
    setModuleCmsObject: enable => dispatch(setModuleCmsObject(enable)),
    setCmsModelImage: enable => dispatch(setCmsModelImage(enable)),
    setCmsEnableUploadImage: enable => dispatch(setCmsEnableUploadImage(enable)),
    setSectionBackgroundColor: enable => dispatch(setSectionBackgroundColor(enable)),
    setThemeSectionBackgroundColor: enable => dispatch(setThemeSectionBackgroundColor(enable)),
    setBackgroundCmsKey:enable=>dispatch(setBackgroundCmsKey(enable)),
    setIconModel: enable => dispatch(setIconModel(enable)),
    setShowIcon: enable => dispatch(setShowIcon(enable)),
    setLogoIcon: enable => dispatch(setLogoIcon(enable)),
});
export default connect(mapStateToProps, mapDispatchToProps)(Feature);

