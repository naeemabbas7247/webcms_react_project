import React, { Component } from "react";
import { useDispatch } from "react-redux";
import { withRouter } from "react-router-dom";
import Login from ".../../admin/Login/Login/";
import { Link } from "react-router-dom";
import { browserHistory } from "react-router";
import CloneIcon from "./CloneIcon.js";
import {
  setCmsArray,
  setCmsModel,
  setCmsKey,
  setCmsValue,
  setThemeBackgroundColor,
  setThemeColor,
  setEditorState,
  setCmsEnable,
  setCmsEnableUploadImage,
  setCmsModelImage,
  setCmsEnableBackground,
  setThemeSectionBackgroundColor,
  setSectionBackgroundColor,
  setCmsImageArray,
  setCmsSection,
  setSectionId,
  setBackgroundCmsKey,
  setIconModel,
  setShowIcon,
  setLogoIcon,
  setPlaceholderValue,
} from "../../reducers/ThemeOptions";
import ReactPlayer from "react-player";
import { connect } from "react-redux";
import CmsSet from "./Functions/CmsSet.js";
import {
  EditorState,
  ContentState,
  convertFromHTML,
  convertFromRaw,
} from "draft-js";
import { Editor } from "react-draft-wysiwyg";
import htmlToDraft from "html-to-draftjs";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import "react-tabs/style/react-tabs.css";
import { faCogs } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { stat } from "fs-extra";
import {
  UncontrolledButtonDropdown,
  DropdownToggle,
  Dropdown,
  DropdownMenu,
  DropdownItem,
  Nav,
  NavItem,
  NavLink,
  Container,
  CustomInput,
} from "reactstrap";

import {
  Carousel,
  CarouselItem,
  CarouselControl,
  CarouselIndicators,
  CarouselCaption,
} from "reactstrap";

class Banner extends Component {
  constructor(props) {
    super(props);
    this.state = {
      enable: false,
      background_btn: false,
      cms_class: "check_cms button-default button-red",
      activeIndex: 0,
      animating: false,
      addMessage:"add-text",
      removeMessage:"remove-text"
    };
  }

  SetCms = (key, section_id, background_key = null) => {
    //
    if (this.props.cmsEnable) {
      var tempArray = {
        key: section_id + "banner_background",
        value: this.props.cmsArray[section_id + "banner_background"],
      };
      const { setSectionBackgroundColor } = this.props;
      setSectionBackgroundColor(tempArray);
      const contentBlock = htmlToDraft(this.props.cmsArray[key]);
      if (contentBlock) {
        const contentState = ContentState.createFromBlockArray(
          contentBlock.contentBlocks
        );
        const editorState = EditorState.createWithContent(contentState);
        const { setEditorState } = this.props;
        setEditorState(editorState);
      }
      if (background_key) {
        const { setBackgroundCmsKey } = this.props;
        setBackgroundCmsKey(section_id + background_key);
      }
      const { cmsKey, setCmsKey } = this.props;
      setCmsKey(key);
      const { cmsValue, setCmsValue } = this.props;
      setCmsValue(this.props.cmsArray[key]);
      const { cmsModel, setCmsModel } = this.props;
      setCmsModel(!this.props.cmsModel);
    }
  };

  bannerSet(section_id) {
    if (this.props.cmsArray[section_id + "banner_img"]) {
      if (this.props.cmsArray[section_id + "banner_img"].length > 0) {
        if (
          this.props.cmsArray[section_id + "banner_img"][0]["images_backups"]['is_video'] == "1"
        ) {
          if (this.props.cmsArray[section_id + "banner_img"][0].length == 1) {
            return (
              <div style={{ position: "relative", paddingTop: "56.25%" }}>
                <ReactPlayer
                  onClick={() =>
                    this.SetImage(section_id + "banner_img", section_id)
                  }
                  url={
                    this.props.cmsArray[section_id + "banner_img"][
                      "images_backups"
                    ]["image"]
                  }
                  style={{
                    position: "absolute",
                    backgroundColor:'red',
                    top: 0,
                    left: 0,
                    padding: 12,
                  }}
                  controls
                  width="100%"
                  height="100%"
                />
              </div>
            );
          } else {
            const slides = this.props.cmsArray[section_id + "banner_img"].map(
              (item) => {
                return (
                  <CarouselItem
                    onExiting={() => this.setState({ animating: true })}
                    onExited={() => this.setState({ animating: false })}
                    key={item.id}
                  >
                    <div style={{ position: "relative", paddingTop: "56.25%" }}>
                      <ReactPlayer
                        onClick={() =>
                          this.SetImage(section_id + "banner_img", section_id)
                        }
                        url={item.images_backups.image.replace('/public','')}
                        style={{
                          position: "absolute",
                          top: 0,
                          left: 0,
                          padding: 12,
                        }}
                        controls
                        width="100%"
                        height="100%"
                      />
                    </div>
                  </CarouselItem>
                );
              }
            );
            return (
              <Carousel
                onClick={() =>
                  this.SetImage(section_id + "banner_img", section_id)
                }
                activeIndex={this.state.activeIndex}
                next={this.next}
                previous={this.previous}
              >
                <CarouselIndicators
                  items={this.props.cmsArray[section_id + "banner_img"]}
                  activeIndex={this.state.activeIndex}
                  onClickHandler={this.goToIndex}
                />
                {slides}
                <CarouselControl
                  direction="prev"
                  directionText="Previous"
                  onClickHandler={() => this.previous()}
                />
                <CarouselControl
                  direction="next"
                  directionText="Next"
                  onClickHandler={() => this.next()}
                />
              </Carousel>
            );
          }
        } else {
          if (this.props.cmsArray[section_id + "banner_img"][0].length == 1) {
            return (
              <img
                style={[
                  this.props.cmsArray[section_id + "banner_imgstyle"],
                  { position: "relative", height: "100%", width: "100%" },
                ]}
                onClick={() =>
                  this.SetImage(section_id + "banner_img", section_id)
                }
                className="image-1"
                src={
                  this.props.cmsArray[section_id + "banner_img"][0][
                    "images_backups"
                  ]["image"].replace('/public','')
                }
                alt="App Landing"
              />
            );
          } else {
            //problem is here
            // const slides1=this.props.cmsArray[section_id + "banner_img"];
            // if(slides1===null){
            //   alert('data is not here')
            // }
            // else
            // {
            //   alert('Data is here'+JSON.stringify(slides1));
            // }
            const slides = this.props.cmsArray[section_id + "banner_img"].map(
              (item) => {

                return (
                  <CarouselItem
                    onExiting={() => this.setState({ animating: true })}
                    onExited={() => this.setState({ animating: false })}
                    key={item.id}
                  >
                    <img
                      src={item.images_backups.image.replace('/public','')}
                      alt={item.altText}
                      onClick={() =>
                        this.SetImage(section_id + "banner_img", section_id)
                      }
                      style={{
                        ...this.props.cmsArray[section_id + "banner_imgstyle"],
                        position: "relative",
                        height: "100%",
                        width: "100%",
                      }}
                    />
                  </CarouselItem>
                );
              }
            );
            return (
              <Carousel
                onClick={() =>
                  this.SetImage(section_id + "banner_img", section_id)
                }
                activeIndex={this.state.activeIndex}
                next={this.next}
                previous={this.previous}
              >
                <CarouselIndicators
                  items={this.props.cmsArray[section_id + "banner_img"]}
                  activeIndex={this.state.activeIndex}
                  onClickHandler={this.goToIndex}
                />
                {slides}
                <CarouselControl
                  direction="prev"
                  directionText="Previous"
                  onClickHandler={() => this.previous()}
                />
                <CarouselControl
                  direction="next"
                  directionText="Next"
                  onClickHandler={() => this.next()}
                />
              </Carousel>
            );
          }
        }
      } else {
        return (
          <img
            style={{ position: "relative", height: "100%", width: "100%" }}
            onClick={() => this.SetImage(section_id + "banner_img", section_id)}
            className="image-1"
            src={"../../assets/images/app/mobile-2.png"}
            alt="App Landing"
          />
        );
      }
    }
  }
  next = () => {
    if (this.state.animating) return;
    const nextIndex =
      this.state.activeIndex ===
      this.props.cmsArray[this.props[0] + "_" + "banner_img"].length - 1
        ? 0
        : this.state.activeIndex + 1;
    this.setState({ activeIndex: nextIndex });
  };

  previous = () => {
    if (this.state.animating) return;
    const nextIndex =
      this.state.activeIndex === 0
        ? this.props.cmsArray[this.props[0] + "_" + "banner_img"].length - 1
        : this.state.activeIndex - 1;
    // console.log(nextIndex);
    this.setState({ activeIndex: nextIndex });
  };

  componentDidUpdate(prevProps) {
    var section_id = this.props[0] + "_";
    var oldArray = prevProps.cmsArray[section_id + "banner_img"];
    var newArray = this.props.cmsArray[section_id + "banner_img"];
    var check = JSON.stringify(oldArray) != JSON.stringify(newArray);
    if (check) {
      this.bannerSet(section_id);
    }
  }

  goToIndex = (newIndex) => {
    // console.log(newIndex);
    if (this.state.animating) return;
    this.setState({ activeIndex: newIndex });
  };

  SetImage = async (key, section_id) => {
   
    if (this.props.cmsEnable) {
      var tempArray = {
        key: section_id + "banner_background",
        value: this.props.cmsArray[section_id + "banner_background"][section_id + "banner_background"],
      };
      const { setSectionBackgroundColor } = this.props;
      setSectionBackgroundColor(tempArray);
      const { cmsModelImage, setCmsModelImage } = this.props;
      setCmsModelImage(!this.props.cmsModelImage);
      const { cmsEnableUploadImage, setCmsEnableUploadImage } = this.props;
      setCmsEnableUploadImage(!this.props.cmsEnableUploadImage);
      const { cmsKey, setCmsKey } = this.props;
      const { cmsValue, setCmsValue } = this.props;
      setCmsValue(this.props.cmsArray[key]);
      setCmsKey(key);
    }
  };
  CmsFunctionBackground = async (key, section_id) => {
    const { setThemeSectionBackgroundColor } = this.props;
    await setThemeSectionBackgroundColor(true);
    const { cmsEnableBackground, setCmsEnableBackground } = this.props;
    await setCmsEnableBackground(!this.props.cmsEnableBackground);
    await this.SetCms(key, section_id);
  };
  modelEnable = async () => {
    const { cmsEnable, setCmsEnable } = this.props;
    await setCmsEnable(!this.props.cmsEnable);
  };
  modelToggle = async () => {
    const { cmsModel, setCmsModel } = this.props;
    await setCmsModel(!this.props.cmsModel);
  };

  nextPath(path) {
    this.props.history.push(path);
  }

  handleLogOut = async (e) => {
    await localStorage.removeItem("isLogin");
    console.log(this.props);
    browserHistory.push("/#/admin/login");
    window.location.reload();
  };
  handleLogIn = async (e) => {
    browserHistory.push("/#/admin/login");
    window.location.reload();
  };

  headerSection(value) {
    if (value["is_header"]) {
      return true;
    } else {
      return false;
    }
  }

  CmsFunction = async () => {
    await this.modelEnable();
    if (this.props.cmsEnable) {
      this.setState({ cms_class: "check_cms button-default button-blue" });
    } else {
      this.setState({ cms_class: "check_cms button-default button-red" });
    }
    this.setState({ background_btn: !this.state.background_btn });
  };
  render() {
    let key_id = this.props[0];
    let section_id = key_id + "_";
    var sectionBackgroundColorArray = {
      key: section_id + "banner_background",
      value: this.props.cmsArray[section_id + "banner_background"]?this.props.cmsArray[section_id + "banner_background"][section_id + "banner_background"]:null,
    };
    let div_height=0;
    if(this.props.cmsArray[section_id + "banner_p"])
    {
      div_height=Object.keys(this.props.cmsArray[section_id + "banner_p"]).length+Object.keys(this.props.cmsArray[section_id + "banner_heading"]).length;
      div_height=div_height-2;
      div_height=div_height*40;
      div_height=div_height+1196;
    }
    return (
      <div>
        <div
          className={`slider-area bg-color ${this.props.class_set} ${this.props.bgshape}`}
          style={{
            backgroundColor: this.props.cmsArray[
              section_id + "banner_background"
            ]
              ? this.props.cmsArray[section_id + "banner_background"][
                  section_id + "banner_background"
                ]
              : "green",'height':div_height
          }}
          id={section_id}>
          <div className="container h-100">
            <div className="row">
              <div className="col-lg-7 h-100">
                <div className="banner-text">
                  <div className="banner-table-cell">
                    <CmsSet
                      sectionBackgroundArray={sectionBackgroundColorArray}
                      styleProp={
                        this.props.cmsArray[section_id + "banner_headingstyle"]
                      }
                      htmlProp={
                        this.props.cmsArray[section_id + "banner_heading"]
                      }
                      sectionKey={section_id}
                      sectionId={"banner_heading"}
                      getData={this.props.getData ? this.props.getData : null}
                      addMessage={this.state.addMessage}
                      removeMessage={this.state.removeMessage}
                    />    
                    <CmsSet
                      sectionBackgroundArray={sectionBackgroundColorArray}
                      styleProp={
                        this.props.cmsArray[section_id + "banner_pstyle"]
                      }
                      htmlProp={this.props.cmsArray[section_id + "banner_p"]}
                      sectionKey={section_id}
                      sectionId={"banner_p"}
                      getData={this.props.getData ? this.props.getData : null}
                      addMessage={this.state.addMessage}
                      removeMessage={this.state.removeMessage}
                      
                    />
                    <div className="banner-buttons d-inline-block">
                      <CmsSet
                        sectionBackgroundArray={sectionBackgroundColorArray}
                        styleProp={{
                          ...this.props.cmsArray[
                            section_id + "banner_btn_1style"
                          ],
                        }}
                        htmlProp={this.props.cmsArray[section_id + "banner_btn_1"]}
                        sectionKey={section_id}
                        globalBtn={true}
                        backgroundStyle={
                          this.props.cmsArray[
                            section_id + "banner_btn_1_global"
                          ]?this.props.cmsArray[
                            section_id + "banner_btn_1_global"
                          ][
                            section_id + "banner_btn_1_global"
                          ]:null
                        }
                        backgroundKey={'banner_btn_1_global'}
                        isIcon={true}
                        sectionId={"banner_btn_1"}
                        getData={this.props.getData ? this.props.getData : null}
                        addMessage={this.state.addMessage}
                      removeMessage={this.state.removeMessage}
                      />
                      <CmsSet
                        sectionBackgroundArray={sectionBackgroundColorArray}
                        styleProp={
                          this.props.cmsArray[section_id + "banner_btn_2style"]
                        }
                        htmlProp={this.props.cmsArray[section_id + "banner_btn_2"]}
                        sectionKey={section_id}
                        Btn={true}
                        isIcon={true}
                        sectionId={"banner_btn_2"}
                        getData={this.props.getData ? this.props.getData : null}
                        addMessage={this.state.addMessage}
                        removeMessage={this.state.removeMessage}
                      />
                    </div>
                  </div>
                </div>

                <div className="banner-apps">
                  {/* Start Sinhle app */}
                  <div
                    className="single-app"
                    style={{
                      background: this.props.cmsArray[
                        section_id + "banner_img1_global"
                      ]?this.props.cmsArray[
                        section_id + "banner_img1_global"
                      ][
                        section_id + "banner_img1_global"
                      ]:'',
                    }}
                  >
                    <div className="single-app-table-cell" id={JSON.stringify(section_id + "banner_img1_iconstyle")}>
                      <CmsSet
                          sectionBackgroundArray={sectionBackgroundColorArray}
                          styleProp={
                            this.props.cmsArray[
                              section_id + "banner_img1_iconstyle"
                            ]
                          }
                          htmlProp={
                            this.props.cmsArray[section_id + "banner_img1_icon"]
                          }
                          backgroundKeyFlag={true}
                          backgroundKey={'banner_img1_global'}
                          backgroundStyle={
                            this.props.cmsArray[
                              section_id + "banner_img1_global"
                            ]?this.props.cmsArray[
                              section_id + "banner_img1_global"
                            ][
                              section_id + "banner_img1_global"
                            ]:null
                          }
                          isIcon={true}
                          flagPadding={true}
                          sectionKey={section_id}
                          sectionId={"banner_img1_icon"}
                          getData={
                            this.props.getData ? this.props.getData : null
                          }
                          addMessage={this.state.addMessage}
                          removeMessage={this.state.removeMessage}
                        />
                      <h4>
                        <CmsSet
                          sectionBackgroundArray={sectionBackgroundColorArray}
                          styleProp={
                            this.props.cmsArray[
                              section_id + "banner_img1_h4style"
                            ]
                          }
                          backgroundKeyFlag={true}
                          backgroundKey={'banner_img1_global'}
                          htmlProp={
                            this.props.cmsArray[section_id + "banner_img1_h4"]
                          }
                          sectionKey={section_id}
                          sectionId={"banner_img1_h4"}
                          getData={
                            this.props.getData ? this.props.getData : null
                          }
                          addMessage={this.state.addMessage}
                          removeMessage={this.state.removeMessage}
                        />
                      </h4>
                      <h3>
                        <CmsSet
                          sectionBackgroundArray={sectionBackgroundColorArray}
                          styleProp={
                            this.props.cmsArray[
                              section_id + "banner_img1_h3style"
                            ]
                          }
                          backgroundKeyFlag={true}
                          backgroundKey={'banner_img1_global'}
                          htmlProp={
                            this.props.cmsArray[section_id + "banner_img1_h3"]
                          }
                          sectionKey={section_id}
                          sectionId={"banner_img1_h3"}
                          getData={
                            this.props.getData ? this.props.getData : null
                          }
                          addMessage={this.state.addMessage}
                          removeMessage={this.state.removeMessage}
                        />
                      </h3>
                    </div>
                  </div>

                  {/* Start Sinhle app */}
                  <div
                    className="single-app"
                    style={{
                      background: this.props.cmsArray[
                        section_id + "banner_img2_global"
                      ]?this.props.cmsArray[
                        section_id + "banner_img2_global"
                      ][
                        section_id + "banner_img2_global"
                      ]:'',
                    }}
                  >
                    <div className="single-app-table-cell">
                      <CmsSet
                          sectionBackgroundArray={sectionBackgroundColorArray}
                          styleProp={
                            this.props.cmsArray[
                              section_id + "banner_img2_iconstyle"
                            ]
                          }
                          htmlProp={
                            this.props.cmsArray[section_id + "banner_img2_icon"]
                          }
                          backgroundKeyFlag={true}
                          backgroundKey={'banner_img2_global'}
                          isIcon={true}
                          backgroundStyle={
                            this.props.cmsArray[
                              section_id + "banner_img2_global"
                            ]?this.props.cmsArray[
                              section_id + "banner_img2_global"
                            ][
                              section_id + "banner_img2_global"
                            ]:null
                          }
                          flagPadding={true}
                          sectionKey={section_id}
                          sectionId={"banner_img2_icon"}
                          getData={
                            this.props.getData ? this.props.getData : null
                          }
                          addMessage={this.state.addMessage}
                          removeMessage={this.state.removeMessage}
                        />
                      <h4>
                      <CmsSet
                          sectionBackgroundArray={sectionBackgroundColorArray}
                          styleProp={
                            this.props.cmsArray[
                              section_id + "banner_img2_h4style"
                            ]
                          }
                          backgroundKeyFlag={true}
                          backgroundKey={'banner_img2_global'}
                          htmlProp={
                            this.props.cmsArray[section_id + "banner_img2_h4"]
                          }
                          sectionKey={section_id}
                          sectionId={"banner_img2_h4"}
                          getData={
                            this.props.getData ? this.props.getData : null
                          }
                          addMessage={this.state.addMessage}
                          removeMessage={this.state.removeMessage}
                        />
                      </h4>
                      <h3>
                      <CmsSet
                          sectionBackgroundArray={sectionBackgroundColorArray}
                          styleProp={
                            this.props.cmsArray[
                              section_id + "banner_img2_h3style"
                            ]
                          }
                          backgroundKeyFlag={true}
                          backgroundKey={'banner_img2_global'}
                          htmlProp={
                            this.props.cmsArray[section_id + "banner_img2_h3"]
                          }
                          sectionKey={section_id}
                          sectionId={"banner_img2_h3"}
                          getData={
                            this.props.getData ? this.props.getData : null
                          }
                          addMessage={this.state.addMessage}
                          removeMessage={this.state.removeMessage}
                        />
                      </h3>
                    </div>
                  </div>

                  {/* Start Sinhle app */}

                  <div
                    className="single-app"
                    style={{
                      background: this.props.cmsArray[
                        section_id + "banner_img3_global"
                      ]?this.props.cmsArray[
                        section_id + "banner_img3_global"
                      ][
                        section_id + "banner_img3_global"
                      ]:'',
                    }}
                  >
                    <div className="single-app-table-cell">
                      <CmsSet
                          sectionBackgroundArray={sectionBackgroundColorArray}
                          styleProp={
                            this.props.cmsArray[
                              section_id + "banner_img3_iconstyle"
                            ]
                          }
                          backgroundKeyFlag={true}
                          htmlProp={
                            this.props.cmsArray[section_id + "banner_img3_icon"]
                          }
                          isIcon={true}
                          flagPadding={true}
                          backgroundKey={'banner_img3_global'}
                          sectionKey={section_id}
                          backgroundStyle={
                            this.props.cmsArray[
                              section_id + "banner_img3_global"
                            ]?this.props.cmsArray[
                              section_id + "banner_img3_global"
                            ][
                              section_id + "banner_img3_global"
                            ]:null
                          }
                          sectionId={"banner_img3_icon"}
                          getData={
                            this.props.getData ? this.props.getData : null
                          }
                          addMessage={this.state.addMessage}
                          removeMessage={this.state.removeMessage}
                        />
                      <h4>
                      <CmsSet
                          sectionBackgroundArray={sectionBackgroundColorArray}
                          styleProp={
                            this.props.cmsArray[
                              section_id + "banner_img3_h4style"
                            ]
                          }
                          backgroundKeyFlag={true}
                          backgroundKey={'banner_img3_global'}
                          htmlProp={
                            this.props.cmsArray[section_id + "banner_img3_h4"]
                          }
                          sectionKey={section_id}
                          sectionId={"banner_img3_h4"}
                          getData={
                            this.props.getData ? this.props.getData : null
                          }
                          addMessage={this.state.addMessage}
                          removeMessage={this.state.removeMessage}
                        />
                      </h4>
                      <h3>
                      <CmsSet
                          sectionBackgroundArray={sectionBackgroundColorArray}
                          styleProp={
                            this.props.cmsArray[
                              section_id + "banner_img3_h3style"
                            ]
                          }
                          backgroundKey={'banner_img3_global'}
                          htmlProp={
                            this.props.cmsArray[section_id + "banner_img3_h3"]
                          }
                          backgroundKeyFlag={true}
                          sectionKey={section_id}
                          sectionId={"banner_img3_h3"}
                          getData={
                            this.props.getData ? this.props.getData : null
                          }
                          addMessage={this.state.addMessage}
                          removeMessage={this.state.removeMessage}
                        />
                      </h3>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-lg-5">
                <div className="banner-product-image text-right">
                  {this.bannerSet(section_id)}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => ({
  cmsArray: state.ThemeOptions.cmsArray,
  cmsModel: state.ThemeOptions.cmsModel,
  cmsKey: state.ThemeOptions.cmsKey,
  cmsValue: state.ThemeOptions.cmsValue,
  themeColor: state.ThemeOptions.themeColor,
  themeBackgroundColor: state.ThemeOptions.themeBackgroundColor,
  editorState: state.ThemeOptions.editorState,
  cmsEnable: state.ThemeOptions.cmsEnable,
  cmsEnableUploadImage: state.ThemeOptions.cmsEnableUploadImage,
  cmsModelImage: state.ThemeOptions.cmsModelImage,
  cmsEnableBackground: state.ThemeOptions.cmsEnableBackground,
  themeSectionBackgroundColor: state.ThemeOptions.themeSectionBackgroundColor,
  sectionBackgroundColor: state.ThemeOptions.sectionBackgroundColor,
  cmsSection: state.ThemeOptions.cmsSection,
  cmsImageArray: state.ThemeOptions.cmsImageArray,
  sectionId: state.ThemeOptions.sectionId,
  backgroundCmsKey: state.ThemeOptions.backgroundCmsKey,
  iconModel:state.ThemeOptions.iconModel,
  showIcon:state.ThemeOptions.showIcon,
  logoIcon:state.ThemeOptions.logoIcon,
  placeholderValue: state.ThemeOptions.placeholderValue,
});
const mapDispatchToProps = (dispatch) => ({
  setCmsArray: (enable) => dispatch(setCmsArray(enable)),
  setCmsModel: (enable) => dispatch(setCmsModel(enable)),
  setCmsKey: (enable) => dispatch(setCmsKey(enable)),
  setCmsValue: (enable) => dispatch(setCmsValue(enable)),
  setThemeColor: (enable) => dispatch(setThemeColor(enable)),
  setThemeBackgroundColor: (enable) =>
    dispatch(setThemeBackgroundColor(enable)),
  setEditorState: (enable) => dispatch(setEditorState(enable)),
  setCmsEnable: (enable) => dispatch(setCmsEnable(enable)),
  setCmsEnableUploadImage: (enable) =>
    dispatch(setCmsEnableUploadImage(enable)),
  setCmsModelImage: (enable) => dispatch(setCmsModelImage(enable)),
  setCmsEnableBackground: (enable) => dispatch(setCmsEnableBackground(enable)),
  setThemeSectionBackgroundColor: (enable) =>
    dispatch(setThemeSectionBackgroundColor(enable)),
  setSectionBackgroundColor: (enable) =>
    dispatch(setSectionBackgroundColor(enable)),
  setCmsImageArray: (enable) => dispatch(setCmsImageArray(enable)),
  setCmsSection: (enable) => dispatch(setCmsSection(enable)),
  setSectionId: (enable) => dispatch(setSectionId(enable)),
  setBackgroundCmsKey: (enable) => dispatch(setBackgroundCmsKey(enable)),
  setIconModel: enable => dispatch(setIconModel(enable)),
  setShowIcon: enable => dispatch(setShowIcon(enable)),
  setLogoIcon: enable => dispatch(setLogoIcon(enable)),
  setPlaceholderValue: (enable) => dispatch(setPlaceholderValue(enable)),
});
export default connect(mapStateToProps, mapDispatchToProps)(Banner);
