import React, { Component } from "react";
import "../../../index.scss";
import ApiCall from "../../../admin/Partial/Function/ApiCall";
import { SketchPicker } from "react-color";
import ImageUploader from "react-images-upload";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faClone,
  faTrash,
  faPlus,
  faArrowUp,
  faArrowDown,
  faPencilAlt,
  faCheckCircle,
} from "@fortawesome/free-solid-svg-icons";
import {
  setCmsArray,
  setCmsModel,
  setCmsKey,
  setCmsValue,
  setThemeBackgroundColor,
  setThemeColor,
  setEditorState,
  setModuleCmsObject,
  setCmsEnable,
  setCmsEnableBackground,
  setCmsEnableUploadImage,
  setCmsModelImage,
  setThemeSectionBackgroundColor,
  setSectionBackgroundColor,
  setCmsImageArray,
  setPreviousColorsValue,
  setCmsVideoArray,
  setCmsSection,
  setBackgroundCmsKey,
  setContactUsEmail,
  setSubscriptionEmail,
  setIsGlobal,
  setCustomStyle,
  setAddModel,
  setSectionArrayModel,
  setCurrentSection,
  setAddModelData,
  setEditSectionModel,
  setEditSectionModelEnable,
  setEditSectionModelArray,
  setIsHorizontal,
  setSelectedEdit,
} from "../../../reducers/ThemeOptions";
import { connect } from "react-redux";
import {
  EditorState,
  ContentState,
  convertFromHTML,
  convertFromRaw,
  convertToRaw,
} from "draft-js";
import Navbar from "../Navbar";
import Banner from "../Banner";
import About from "../About";
import Service from "../Service";
import Feature from "../Feature";
import Download from "../Download";
import Pricing from "../Pricing";
import Testimonial from "../Testimonial";
import Subscription from "../Subscription";
import Screenshot from "../Screenshot";
import Blog from "../Blog";
import Footer from "../Footer";
import ContactUs from "../ContactUs";
import { Editor } from "react-draft-wysiwyg";
import draftToHtml from "draftjs-to-html";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import Dropzone from "react-dropzone";
import Resizer from "react-image-file-resizer";
import {
  UncontrolledButtonDropdown,
  DropdownToggle,
  Dropdown,
  DropdownMenu,
  DropdownItem,
  Nav,
  NavItem,
  NavLink,
  Container,
  CustomInput,
} from "reactstrap";
import { Card, CardBody, CardTitle, Collapse } from "reactstrap";
import {
  Row,
  Col,
  ListGroup,
  ListGroupItem,
  FormGroup,
  Label,
  Input,
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
} from "reactstrap";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import "react-tabs/style/react-tabs.css";
let flag = 0;
class AddModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      section_id: null,
      section_name: null,
      section_key:null,
      section_array: {},
      section_image_name: null,
      section_image_array: {},
      styles: {},
      is_dummy_content: null,
      section_flag: false,
      image_select: false,
      demo_page_select: false,
      colum_size: 6,
      count: 0,
      accordionId: null,
      openAccoridion: false,
      section_name_custom:null
    };
  }
  toggle = async () => {
    const { setAddModel } = this.props;
    setAddModel(!this.props.addModel);
    this.setState({ section_array: {} });
    this.setState({ section_name: null });
    this.setState({
      image_select: false,
    });
    this.setState({
      demo_page_select: false,
    });
    this.setState({ section_id: "" });
    this.setState({ section_image_name: "" });
    this.setState({ accordionId: null });
    this.setState({ styles: {} });
    this.setState({ is_dummy_content: null });
    this.setState({ section_flag: !this.state.section_flag });
    var set_empty_array = [];
    const { setEditSectionModelArray } = this.props;
    setEditSectionModelArray(set_empty_array);
    const { setEditSectionModelEnable } = this.props;
    setEditSectionModelEnable(false);
    const { setAddModelData } = this.props;
    setAddModelData(null);
  };
  toggleAccordion = async (id, key) => {
    this.setState({ openAccoridion: true });
    this.setState({ accordionId: id });
    this.cloneAddKey(key);
    this.setState({is_dummy_content: false,})
  };
  componentWillMount() {
    this.getSectionImages();
  }
  getSectionImages = async () => {
    let sectionImages = await ApiCall.getAPICall("sections/images");
    this.setState({
      section_image_array: sectionImages.images,
    });
    var stylesArray = {};
    var count = 0;
    Object.keys(sectionImages.images).map((image_index) => {
      stylesArray["image_style_" + image_index] = {};
      count++;
    });
    this.setState({
      styles: stylesArray,
    });
    this.setState({
      count: count,
    });
  };
  componentDidUpdate(prevProps) {
    if (this.props.editSectionModelEnable && !this.state.section_flag) {
      this.setState({
        section_flag: !this.state.section_flag,
      });
    }
    if (!this.state.section_array.length && this.props.editSectionModelEnable) {
      this.setState({
        section_array: this.props.editSectionModelArray,
      });
      this.setState({
        image_select: true,
      });
      this.setState({
        demo_page_select: true,
      });
      this.setState({ section_name: this.props.editSectionModel });
      this.handelSetcolumSize(this.props.editSectionModel);
    }
    if (this.props.addModel !== prevProps.addModel) {
      this.cmsmodelEnable();
    }
  }
  handelSetcolumSize = async (name) => {
    if (name !== "Download" && name !== "ContactUs") {
      this.setState({ colum_size: 12 });
    } else {
      this.setState({ colum_size: 6 });
    }
  };
  handleSetSection = async () => {
    var temp_array = [];
    await this.setState({ section_flag: true });
    // this.setState({ section_name: data['name'] });
    // this.setState({ section_id: data['id'] });
    // this.handelSetcolumSize(data['name']);
    var name;
    Object.keys(this.props.cmsSection).map((val, i) => {
      if (this.props.cmsSection[val]["id"] == this.state.section_id) {
        name = this.props.cmsSection[val]["module_name"];
        return;
      }
    });
    this.setState({ section_name: name });
    this.handelSetcolumSize(name);
    Object.keys(this.props.cmsSection).map((val, i) => {
      if (this.props.cmsSection[val]["module_name"] == name) {
        temp_array.push(this.props.cmsSection[val]);
        this.setState({
          section_array: temp_array,
        });
      }
    });
  };
  cloneAddKey = async (key) => {
    console.log(key);
    this.handelIsHorizontal();
    const { setAddModelData } = this.props;
    setAddModelData(key);
    const { setSelectedEdit } = this.props;
    var obj = {
      key: key,
      class: this.props.class_set ? this.props.class_set : "vertical",
    };
    setSelectedEdit(obj);
  };
  handelIsHorizontal = async () => {
    if (this.props.class_set && this.props.class_set != "vertical") {
      const { setIsHorizontal } = this.props;
      setIsHorizontal(true);
    } else {
      const { setIsHorizontal } = this.props;
      setIsHorizontal(false);
    }
  };
  submit = async () => {
    var data = {
      key: this.state.is_dummy_content
        ? this.state.section_key
        : this.props.addModelData,
      section_name:this.state.section_name_custom,
      is_horizontal: this.props.isHorizontal,
      order_get: this.props.currentSection,
      is_edit: this.props.editSectionModelEnable ? true : false,
    };
    if (this.state.is_dummy_content) {
      data["dummy"] = true;
    }
    var url = "sections/duplicate";
    await ApiCall.postAPICall(url, data);
    this.props.getData();
    const { setAddModelData } = this.props;
    setAddModelData(null);
    this.setState({
      image_select: false,
    });
    this.setState({
      demo_page_select: false,
    });
    this.toggle();
  };
  SetImageStyle = async (index, id, module_name,section_key) => {
    this.setState({
      section_image_name: this.state.section_image_array[index]["image"],
    });
    this.setState({ module_name });
    this.setState({ section_key });
    this.setState({ section_id: id });
    let style = {
      border: "1px solid ",
      padding: "10px",
      "box-shadow": "5px 10px",
      color: "green",
    };
    var temp = "image_style_" + index;
    var stylesStates = this.state.styles;
    for (var i = this.state.count - 1; i >= 0; i--) {
      if (index == i) {
        stylesStates[temp] = style;
      } else {
        stylesStates["image_style_" + i] = {};
      }
    }
    await this.setState({
      styles: stylesStates,
    });
  };
  cmsmodelEnable = async () => {
    const { cmsEnable, setCmsEnable } = this.props;
    await setCmsEnable(!this.props.cmsEnable);
  };
  demoPageEnable = async () => {
    this.setState({
      image_select: true,
    });
  };
  backToImagesPage= async () => {
    this.setState({
      image_select: false,
    });
  };
  setContentType = async (flag) => {
    console.log(flag);
    this.setState({
      is_dummy_content: flag,
    });
  };
  tempatePageEnable = async () => {
    this.setState({
      demo_page_select: true,
    });
    console.log(this.state.is_dummy_content);
    // if(this.state.is_dummy_content)
    // {
    this.handleSetSection();
    // }
  };
  backToTempletePage= async () => {
    this.setState({
      demo_page_select: false,
    });
    await this.setState({ section_flag: false });
    this.setState({ section_name: null });
    this.setState({section_array:[]});
  };
  handleSetSectionName = (e) => {
    this.setState({ section_name_custom: e.target.value });
  };
  render() {
    let cardnew = this.state.is_dummy_content
      ? { display: "flex", height: 400, border: "2px solid green" }
      : { display: "flex", height: 400 };
    let cardexisting =
      this.state.is_dummy_content == false
        ? {
            display: "flex",
            height: 400,
            borderStyle: "dashed",
            border: "2px solid green",
          }
        : { display: "flex", height: 400, borderStyle: "dashed" };
    let style = { "max-width": "90%" };
    return (
      <div>
        <Modal
          isOpen={this.props.addModel}
          fade={false}
          toggle={
            this.props.cmsEnableBackground ? this.toggleBackground : this.toggle
          }
          style={style}
          className={this.props.className}
        >
          <ModalHeader toggle={this.toggle}>SECTIONS</ModalHeader>
          <div
            className="form-inline"
            style={{ "z-index": "3", position: "relative" }}
          >
            {!this.state.image_select ? (
              <Col md={12}>
                <Row>
                  {Object.keys(this.state.section_image_array).map(
                    (image_index) => (
                      <Col md={4}>
                        <div className="card mb-2">
                          <div
                            id={image_index}
                            className="card-body"
                            style={
                              this.state["styles"]["image_style_" + image_index]
                            }
                          >
                            <img
                              className="image-1"
                              src={
                                this.state.section_image_array[image_index][
                                  "image"
                                ]
                              }
                              onClick={() => {
                                this.SetImageStyle(
                                  image_index,
                                  this.state.section_image_array[image_index][
                                    "section_id"
                                  ],
                                  this.state.section_image_array[image_index][
                                    "sections"
                                  ]["module_name"],
                                  this.state.section_image_array[image_index][
                                    "sections"
                                  ]["key"]
                                );
                              }}
                              alt="App Landing"
                            />
                          </div>
                        </div>
                      </Col>
                    )
                  )}
                </Row>
              </Col>
            ) : !this.state.demo_page_select ? (
              <Col md={12}>
                <p className="mt-2">How will you like to proceed?</p>
                <div className="mb-2 mr-sm-2 mb-sm-0 form-group">
                  <label for="section_name_custom" className="mr-sm-2">
                    Section Name
                  </label>
                  <input
                    name="section_name_custom"
                    onChange={(e) => this.handleSetSectionName(e)}
                    id="section_name_custom"
                    placeholder="Section Name"
                    type="text"
                    className="form-control"
                  />
                </div>
                <div className="mt-2">
                  <Row>
                    <Col md={6}>
                      <div
                        className="card"
                        style={cardnew}
                        onClick={() => {
                          this.setContentType(true);
                        }}
                      >
                        <p class="card-text" style={{ margin: "auto" }}>
                          Create blank template
                        </p>
                      </div>
                    </Col>
                    <Col md={6}>
                      <div
                        className="card"
                        style={cardexisting}
                        onClick={() => {
                          this.setContentType(false);
                        }}
                      >
                        <p class="card-text" style={{ margin: "auto" }}>
                          Choose from existing
                        </p>
                      </div>
                    </Col>
                  </Row>
                </div>
              </Col>
            ) : null}
          </div>
          <ModalBody>
            <div>
              {this.state.image_select ? (
                <Row>
                  <Col md={12}>
                    {this.props.editSectionModelEnable ? (
                      <Card
                        onClick={() => {
                          this.setState({ is_dummy_content: true });
                          this.setState({ accordionId: null });
                        }}
                        style={{ marginBottom: "1rem" }}
                      >
                        <CardBody>
                          <Row>
                            <Col md={10}>Create a Blank template</Col>
                            <Col
                              md={2}
                              style={{
                                display: "flex",
                                justifyContent: "flex-end",
                              }}
                            >
                              {this.state.is_dummy_content ? (
                                <FontAwesomeIcon
                                  icon={faCheckCircle}
                                  size="2x"
                                />
                              ) : null}
                            </Col>
                          </Row>
                        </CardBody>
                      </Card>
                    ) : null}
                    {this.state.section_array.length > 0
                      ? Object.keys(this.state.section_array).map((val, i) => (
                          <div>
                            <Card
                              onClick={() =>
                                this.toggleAccordion(
                                  this.state.section_array[val]["id"],
                                  this.state.section_array[val]["key"]
                                )
                              }
                              style={{ marginBottom: "1rem" }}
                            >
                              <CardBody>
                                {this.state.section_array[val]["key"]} - Created
                                on{" "}
                                {new Date(
                                  this.state.section_array[val]["created_at"]
                                ).toLocaleDateString("en-US", {
                                  weekday: "long",
                                  year: "numeric",
                                  month: "long",
                                  day: "numeric",
                                })}
                                .
                              </CardBody>
                            </Card>
                            <Collapse
                              isOpen={
                                this.state.accordionId ==
                                this.state.section_array[val]["id"]
                                  ? true
                                  : false
                              }
                            >
                              <div>
                                {this.state.section_array[val]["module_name"] ==
                                "Navbar" ? (
                                  <Navbar
                                    bgshape="bg-shape"
                                    class_set="vertical"
                                    current_section={this.props.currentSection}
                                    section_flag={this.state.section_flag}
                                    {...[this.state.section_array[val]["key"]]}
                                  />
                                ) : (
                                  ""
                                )}
                                {this.state.section_array[val]["module_name"] ==
                                "Banner" ? (
                                  <Banner
                                    bgshape="bg-shape"
                                    class_set="vertical"
                                    horizontal=""
                                    current_section={this.props.currentSection}
                                    section_flag={this.state.section_flag}
                                    {...[this.state.section_array[val]["key"]]}
                                  />
                                ) : (
                                  ""
                                )}
                                {this.state.section_array[val]["module_name"] ==
                                "About" ? (
                                  <About
                                    horizontalabout="vertical-about"
                                    class_set="vertical"
                                    current_section={this.props.currentSection}
                                    section_flag={this.state.section_flag}
                                    {...[this.state.section_array[val]["key"]]}
                                  />
                                ) : (
                                  ""
                                )}
                                {this.state.section_array[val]["module_name"] ==
                                "Service" ? (
                                  <Service
                                    horizontal="vertical-service"
                                    class_set="vertical"
                                    current_section={this.props.currentSection}
                                    section_flag={this.state.section_flag}
                                    {...[this.state.section_array[val]["key"]]}
                                  />
                                ) : (
                                  ""
                                )}
                                {this.state.section_array[val]["module_name"] ==
                                "Feature" ? (
                                  <Feature
                                    horizontalfeature="vertical-feature"
                                    class_set="vertical"
                                    current_section={this.props.currentSection}
                                    section_flag={this.state.section_flag}
                                    {...[this.state.section_array[val]["key"]]}
                                  />
                                ) : (
                                  ""
                                )}
                                {this.state.section_array[val]["module_name"] ==
                                "Download" ? (
                                  <Download
                                    horizontal=""
                                    class_set="vertical"
                                    section_flag={this.state.section_flag}
                                    class_set="vertical"
                                    current_section={this.props.currentSection}
                                    {...[this.state.section_array[val]["key"]]}
                                  />
                                ) : (
                                  ""
                                )}
                                {this.state.section_array[val]["module_name"] ==
                                "Pricing" ? (
                                  <Pricing
                                    horizontalpricing="vertical-pricing"
                                    class_set="vertical"
                                    current_section={this.props.currentSection}
                                    section_flag={this.state.section_flag}
                                    {...[this.state.section_array[val]["key"]]}
                                  />
                                ) : (
                                  ""
                                )}
                                {this.state.section_array[val]["module_name"] ==
                                "Testimonial" ? (
                                  <Testimonial
                                    section_flag={this.state.section_flag}
                                    class_set="vertical"
                                    current_section={this.props.currentSection}
                                    {...[this.state.section_array[val]["key"]]}
                                  />
                                ) : (
                                  ""
                                )}
                                {this.state.section_array[val]["module_name"] ==
                                "Blog" ? (
                                  <Blog
                                    section_flag={this.state.section_flag}
                                    class_set="vertical"
                                    current_section={this.props.currentSection}
                                    {...[this.state.section_array[val]["key"]]}
                                  />
                                ) : (
                                  ""
                                )}
                                {this.state.section_array[val]["module_name"] ==
                                "ContactUs" ? (
                                  <ContactUs
                                    horizontal="vertical-footer"
                                    class_set="vertical"
                                    current_section={this.props.currentSection}
                                    section_flag={this.state.section_flag}
                                    {...[this.state.section_array[val]["key"]]}
                                  />
                                ) : (
                                  ""
                                )}
                                {this.state.section_array[val]["module_name"] ==
                                "Subscription" ? (
                                  <Subscription
                                    horizontal="vertical-footer"
                                    class_set="vertical"
                                    current_section={this.props.currentSection}
                                    section_flag={this.state.section_flag}
                                    {...[this.state.section_array[val]["key"]]}
                                  />
                                ) : (
                                  ""
                                )}
                                {this.state.section_array[val]["module_name"] ==
                                "Footer" ? (
                                  <Footer
                                    horizontal="vertical-footer"
                                    class_set="vertical"
                                    current_section={this.props.currentSection}
                                    section_flag={this.state.section_flag}
                                    {...[this.state.section_array[val]["key"]]}
                                  />
                                ) : (
                                  ""
                                )}
                              </div>
                              {this.state.section_array.length > 0
                                ? Object.keys(this.state.section_array).map(
                                    (val, i) => (
                                      <div>
                                        {this.state.section_array[val][
                                          "module_name"
                                        ] == "Service" ? (
                                          <Service
                                            horizontal="vertical-service"
                                            class_set="horizontal"
                                            current_section={
                                              this.props.currentSection
                                            }
                                            section_flag={
                                              this.state.section_flag
                                            }
                                            {...[
                                              this.state.section_array[val][
                                                "key"
                                              ],
                                            ]}
                                          />
                                        ) : (
                                          ""
                                        )}
                                        {this.state.section_array[val][
                                          "module_name"
                                        ] == "Download" ? (
                                          <Download
                                            horizontal=""
                                            section_flag={
                                              this.state.section_flag
                                            }
                                            class_set="horizontal"
                                            current_section={
                                              this.props.currentSection
                                            }
                                            {...[
                                              this.state.section_array[val][
                                                "key"
                                              ],
                                            ]}
                                          />
                                        ) : (
                                          ""
                                        )}
                                        {this.state.section_array[val][
                                          "module_name"
                                        ] == "ContactUs" ? (
                                          <ContactUs
                                            horizontal="vertical-footer"
                                            class_set="horizontal"
                                            current_section={
                                              this.props.currentSection
                                            }
                                            section_flag={
                                              this.state.section_flag
                                            }
                                            {...[
                                              this.state.section_array[val][
                                                "key"
                                              ],
                                            ]}
                                          />
                                        ) : (
                                          ""
                                        )}
                                      </div>
                                    )
                                  )
                                : ""}
                            </Collapse>
                          </div>
                        ))
                      : null}
                  </Col>
                </Row>
              ) : null}
            </div>
          </ModalBody>
          <ModalFooter>
            <Button color="link" onClick={this.toggle}>
              Cancel
            </Button>
            {!this.state.image_select ? (
              <Button
                color={this.state.section_id ? "primary" : "secondary"}
                onClick={() => {
                  if (this.state.section_id) {
                    this.demoPageEnable();
                  }
                }}
              >
                Next
              </Button>
            ) : !this.state.demo_page_select ? (
              <div>
                <Button
                  className="mr-2"
                  color="primary"
                  onClick={() => {
                    if (this.state.section_id) {
                      this.backToImagesPage();
                    }
                  }}
                >
                  Back
                </Button>
                <Button
                  color={
                    this.state.is_dummy_content == true ||
                    this.state.is_dummy_content == false
                      ? "primary"
                      : "secondary"
                  }
                  onClick={() => {
                    if (
                      this.state.is_dummy_content == true ||
                      this.state.is_dummy_content == false
                    ) {
                      if (this.state.is_dummy_content) {
                        this.submit();
                      } else {
                        this.tempatePageEnable();
                      }
                    }
                  }}
                >
                  Next
                </Button>
              </div>
            ) : this.props.editSectionModelEnable ? (
                <div>
                <Button
                  color={
                    this.state.is_dummy_content || this.props.addModelData
                      ? "primary"
                      : "secondary"
                  }
                  onClick={() => {
                    if (this.state.is_dummy_content || this.props.addModelData) {
                      this.submit();
                    }
                  }}
                >
                  Save
                </Button>
              </div>
            ) : (
               <div>
                <Button
                  className="mr-2"
                  color="primary"
                  onClick={() => {
                      this.backToTempletePage();
                  }}
                >
                  Back
                </Button>
                <Button
                  color={this.props.addModelData ? "primary" : "secondary"}
                  onClick={() => {
                    if (this.props.addModelData) {
                      this.submit();
                    }
                  }}
                >
                  Save
                </Button>
              </div>
            )}
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}
const mapStateToProps = (state) => ({
  cmsArray: state.ThemeOptions.cmsArray,
  cmsModel: state.ThemeOptions.cmsModel,
  cmsKey: state.ThemeOptions.cmsKey,
  cmsValue: state.ThemeOptions.cmsValue,
  themeColor: state.ThemeOptions.themeColor,
  themeBackgroundColor: state.ThemeOptions.themeBackgroundColor,
  editorState: state.ThemeOptions.editorState,
  moduleCmsObject: state.ThemeOptions.moduleCmsObject,
  cmsEnableBackground: state.ThemeOptions.cmsEnableBackground,
  cmsEnable: state.ThemeOptions.cmsEnable,
  cmsEnableUploadImage: state.ThemeOptions.cmsEnableUploadImage,
  cmsModelImage: state.ThemeOptions.cmsModelImage,
  themeSectionBackgroundColor: state.ThemeOptions.themeSectionBackgroundColor,
  sectionBackgroundColor: state.ThemeOptions.sectionBackgroundColor,
  cmsImageArray: state.ThemeOptions.cmsImageArray,
  previousColors: state.ThemeOptions.previousColors,
  cmsVideoArray: state.ThemeOptions.cmsVideoArray,
  cmsSection: state.ThemeOptions.cmsSection,
  backgroundCmsKey: state.ThemeOptions.backgroundCmsKey,
  sectionId: state.ThemeOptions.sectionId,
  contact_us_email: state.ThemeOptions.contact_us_email,
  subscription_email: state.ThemeOptions.subscription_email,
  isGlobal: state.ThemeOptions.isGlobal,
  customStyle: state.ThemeOptions.customStyle,
  addModel: state.ThemeOptions.addModel,
  sectionArrayModel: state.ThemeOptions.sectionArrayModel,
  currentSection: state.ThemeOptions.currentSection,
  addModelData: state.ThemeOptions.addModelData,
  editSectionModel: state.ThemeOptions.editSectionModel,
  editSectionModelEnable: state.ThemeOptions.editSectionModelEnable,
  editSectionModelArray: state.ThemeOptions.editSectionModelArray,
  isHorizontal: state.ThemeOptions.isHorizontal,
});
const mapDispatchToProps = (dispatch) => ({
  setCmsArray: (enable) => dispatch(setCmsArray(enable)),
  setCmsModel: (enable) => dispatch(setCmsModel(enable)),
  setCmsKey: (enable) => dispatch(setCmsKey(enable)),
  setCmsValue: (enable) => dispatch(setCmsValue(enable)),
  setThemeColor: (enable) => dispatch(setThemeColor(enable)),
  setThemeBackgroundColor: (enable) =>
    dispatch(setThemeBackgroundColor(enable)),
  setEditorState: (enable) => dispatch(setEditorState(enable)),
  setModuleCmsObject: (enable) => dispatch(setModuleCmsObject(enable)),
  setCmsEnableBackground: (enable) => dispatch(setCmsEnableBackground(enable)),
  setCmsEnable: (enable) => dispatch(setCmsEnable(enable)),
  setCmsEnableUploadImage: (enable) =>
    dispatch(setCmsEnableUploadImage(enable)),
  setCmsModelImage: (enable) => dispatch(setCmsModelImage(enable)),
  setThemeSectionBackgroundColor: (enable) =>
    dispatch(setThemeSectionBackgroundColor(enable)),
  setSectionBackgroundColor: (enable) =>
    dispatch(setSectionBackgroundColor(enable)),
  setCmsImageArray: (enable) => dispatch(setCmsImageArray(enable)),
  setPreviousColorsValue: (enable) => dispatch(setPreviousColorsValue(enable)),
  setCmsVideoArray: (enable) => dispatch(setCmsVideoArray(enable)),
  setCmsSection: (enable) => dispatch(setCmsSection(enable)),
  setBackgroundCmsKey: (enable) => dispatch(setBackgroundCmsKey(enable)),
  setContactUsEmail: (enable) => dispatch(setContactUsEmail(enable)),
  setSubscriptionEmail: (enable) => dispatch(setSubscriptionEmail(enable)),
  setIsGlobal: (enable) => dispatch(setIsGlobal(enable)),
  setCustomStyle: (enable) => dispatch(setCustomStyle(enable)),
  setAddModel: (enable) => dispatch(setAddModel(enable)),
  setSectionArrayModel: (enable) => dispatch(setSectionArrayModel(enable)),
  setCurrentSection: (enable) => dispatch(setCurrentSection(enable)),
  setAddModelData: (enable) => dispatch(setAddModelData(enable)),
  setEditSectionModel: (enable) => dispatch(setEditSectionModel(enable)),
  setEditSectionModelEnable: (enable) =>
    dispatch(setEditSectionModelEnable(enable)),
  setEditSectionModelArray: (enable) =>
    dispatch(setEditSectionModelArray(enable)),
  setIsHorizontal: (enable) => dispatch(setIsHorizontal(enable)),
  setSelectedEdit: (enable) => dispatch(setSelectedEdit(enable)),
});
export default connect(mapStateToProps, mapDispatchToProps)(AddModal);
// <Screenshot />
