import React, { Component } from "react";
import "../../../index.scss";
import ApiCall from "../../../admin/Partial/Function/ApiCall";
import { useDispatch } from "react-redux";
import { SketchPicker } from "react-color";
import ImageUploader from "react-images-upload";
import namedColors from "color-name-list";
import {
  setCmsArray,
  setCmsModel,
  setCmsKey,
  setCmsValue,
  setThemeBackgroundColor,
  setThemeColor,
  setEditorState,
  setModuleCmsObject,
  setCmsEnable,
  setCmsEnableBackground,
  setCmsEnableUploadImage,
  setCmsModelImage,
  setIconModel,
  setThemeSectionBackgroundColor,
  setSectionBackgroundColor,
  setCmsImageArray,
  setPreviousColorsValue,
  setCmsVideoArray,
  setCmsSection,
  setBackgroundCmsKey,
  setContactUsEmail,
  setSubscriptionEmail,
  setIsGlobal,
  setCustomStyle,
  setShowIcon,
  setLogoIcon,
} from "../../../reducers/ThemeOptions";
import { connect } from "react-redux";
import {
  EditorState,
  ContentState,
  convertFromHTML,
  convertFromRaw,
  convertToRaw,
} from "draft-js";
import { Editor } from "react-draft-wysiwyg";
import draftToHtml from "draftjs-to-html";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import Dropzone from "react-dropzone";
import Resizer from "react-image-file-resizer";
import {
  UncontrolledButtonDropdown,
  DropdownToggle,
  Dropdown,
  DropdownMenu,
  DropdownItem,
  Nav,
  NavItem,
  NavLink,
  Container,
  CustomInput,
} from "reactstrap";
import { Card, CardBody, CardTitle } from "reactstrap";
import {
  Form,
  Row,
  Col,
  ListGroup,
  ListGroupItem,
  FormGroup,
  Label,
  Input,
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
} from "reactstrap";
import CloneIcon from '../CloneIcon.js';
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import "react-tabs/style/react-tabs.css";
import ReactResumableJs from "../../../ReactResumableJs";
import CSSTransitionGroup from "react-transition-group/CSSTransitionGroup";

import { library } from "@fortawesome/fontawesome-svg-core";
import { fab } from "@fortawesome/free-brands-svg-icons";
import {
  faCoffee,
  faCog,
  faSpinner,
  faQuoteLeft,
  faSquare,
  faCheckSquare,
  faAngleLeft,
  faAngleRight,
  faAngleUp,
  faAngry,
  faAnkh,
  faAppleAlt,
  faArchive,
  faCalendarAlt,
  faArchway,
  faArrowAltCircleDown,
  faArrowAltCircleLeft,
  faArrowAltCircleRight,
  faArrowAltCircleUp,
  faArrowCircleDown,
  faArrowCircleLeft,
  faArrowCircleRight,
  faArrowCircleUp,
  faArrowDown,
  faArrowLeft,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

library.add(
  fab,
  faCoffee,
  faCog,
  faSpinner,
  faQuoteLeft,
  faSquare,
  faCheckSquare,
  faAngleLeft,
  faCalendarAlt,
  faAngleRight,
  faAngleUp,
  faAngry,
  faAnkh,
  faAppleAlt,
  faArchive,
  faArchway,
  faArrowAltCircleDown,
  faArrowAltCircleLeft,
  faArrowAltCircleRight,
  faArrowAltCircleUp,
  faArrowCircleDown,
  faArrowCircleLeft,
  faArrowCircleRight,
  faArrowCircleUp,
  faArrowDown,
  faArrowLeft
);

let flag = 0;
class IconModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      icon: "",
      imageLogo: "",
      imageLogo2: "",
      pictures: [],
      cmsExtra: [{ key: "", value: "" }],
      inputs: ["input-0"],
      theme_color: "red",
      previousColors: [],
      editorState: EditorState.createWithContent(
        ContentState.createFromBlockArray(
          convertFromHTML("this.props.cmsValue")
        )
      ),
      videos: [],
      youtubeLink: null,
      is_global: false,
      contactusemail: "",
      subscriptionemail: "",
    };
  }
  addClick = () => {
    var tempObject = { key: "", value: "" };
    var tempNewCmsExtra = this.state.cmsExtra;
    tempNewCmsExtra.push(tempObject);
    this.setState((prevState) => ({ cmsExtra: tempNewCmsExtra }));
  };
  onCancel() {
    this.setState({
      pictures: [],
        
    });
    this.setState({ videos: [] });
  }
  onDrop = async (files) => {
    Resizer.imageFileResizer(
      files[0], // the file from input
      500, // width
      500, // height
      "PNG", // compress format WEBP, JPEG, PNG
      70, // quality
      0, // rotation
      (uri) => {
          var array = this.state.pictures;
          array.push(uri);
          this.setState({pictures: array, imageLogo: uri, imageLogo2: uri});
      },
      "base64"
    );
  };
  SetImage = async (index) => {
    var array = this.state.pictures;
    var uri = this.state.imageLogo;
    var uri = this.state.imageLogo2;
    array.push(this.props.cmsImageArray[index].image);
    this.setState({ pictures: array });
    this.setState({ imageLogo: uri });
    this.setState({ imageLogo2: uri });
    array.push(this.props.cmsImageArray[index].image);
  };
  setCstyle = async (data) =>{
    const { setCustomStyle } = this.props;
    await setCustomStyle(data);
  }
  setSectionArray = async (data) => {
    const { setCmsSection } = this.props;
    await setCmsSection(data);
  };
  setImageArray = async () => {
    let images = await ApiCall.postAPICall("images");
    const { setCmsImageArray } = this.props;
    await setCmsImageArray(images.images);
  };
  setVideoArray = async () => {
    let videos = await ApiCall.postAPICall("videos");
    const { setCmsVideoArray } = this.props;
    await setCmsVideoArray(videos.videos);
  };
  setCmsModule = async (data) => {
    const { moduleCmsObject, setModuleCmsObject } = this.props;
    await setModuleCmsObject(data);
  };
  setColorScheme = async () => {
    let color = {
      color: this.state.theme_color,
    };
    let background = {
      background: this.state.theme_color,
    };
    const { themeColor, setThemeColor } = this.props;
    await setThemeColor(color);
    const { themeBackgroundColor, setThemeBackgroundColor } = this.props;
    await setThemeBackgroundColor(background);
  };
  setCmsData = async (data) => {
    const { cmsArray, setCmsArray } = this.props;
    await setCmsArray(data);
  };
  toggle = async () => {
    flag = 0;
    this.setImageArray();
    var empty = [];
    this.setState({ cmsExtra: empty });
    this.setState({ is_global: this.props.isGlobal });
    const { cmsModel, setCmsModel } = this.props;
    await setCmsModel(false);
    const { setCmsEnableBackground } = this.props;
    await setCmsEnableBackground(false);
    const { setIconModel } = this.props;
    await setIconModel(false);
    const { setCmsEnableUploadImage } = this.props;
    setCmsEnableUploadImage(false);
    this.dynamicUi();
  };
  toggleBackground = async () => {
    const { cmsEnableBackground, setCmsEnableBackground } = this.props;
    await setCmsEnableBackground(!this.props.cmsEnableBackground);
    await this.toggle();
  };
  toggleImage = async () => {
    flag = 0;
    var empty = [];
    this.setState({ cmsExtra: empty });
    const { iconModel, setIconModel } = this.props;
    await setIconModel(!this.props.iconModel);
    const { cmsEnableUploadImage, setCmsEnableUploadImage } = this.props;
    setCmsEnableUploadImage(!this.props.cmsEnableUploadImage);
  };
  setValue = (e) => {
    let data = draftToHtml(
      convertToRaw(this.props.editorState.getCurrentContent())
    );
    const { setCmsValue } = this.props;
    setCmsValue(data);
  };
  makeGlobal = (setCmsArray) => {
    Object.keys(setCmsArray).map((val, i) => {
      var flag = val.includes("_global");
      if (flag) {
        setCmsArray[val] = this.state.theme_color;
      }
    });
    return setCmsArray;
  };
  handleMedia = async () => {
    const { setLogoIcon, setShowIcon} = this.props
    setShowIcon(this.state.imageLogo)
    setLogoIcon(this.state.imageLogo2)
    let setCmsArray = this.props.cmsArray;
    var data = {
      key: this.props.cmsKey,
      value: this.state.imageLogo ? JSON.stringify(this.state.imageLogo) :JSON.stringify(this.state.imageLogo2),
      flag: 1,
      is_video: this.state.pictures.length > 0 ? false : true,
    };
    var type;
    if(this.state.imageLogo){
      type = "2";
    } else if(this.state.imageLogo2) {
      type = "1";
      data.is_icon = 1;
    }
    var url = "insertImage";
    await ApiCall.postAPICall(url, data);
    var cmsarraydump = this.props.cmsArray[this.props.cmsKey];
    if(cmsarraydump && cmsarraydump.length > 0 ){
      cmsarraydump[0]['cms_page']['is_icon']= type == "1" ? 1 : 0;
      cmsarraydump[0]['cms_page']['is_multiple']= type == "2" ? 1 : 0;
      cmsarraydump[0]["images_backups"]["image"] = this.state.imageLogo ? this.state.imageLogo: this.state.imageLogo2;
    } else {
      cmsarraydump= [];
      var obj = {
        cms_page: {
          is_icon: type == "1" ? 1 : 0,
          is_multiple:  type == "2" ? 1 : 0,
        },
        images_backups : {
          image: this.state.imageLogo ? this.state.imageLogo: this.state.imageLogo2
        }
      }
      cmsarraydump.push(obj);
    }
    setCmsArray[this.props.cmsKey] =cmsarraydump;
    await this.setCmsData(setCmsArray);
    this.setState({
      icon: "",
      imageLogo: "",
      imageLogo2: "",
      pictures: [],
      videos: [],
      youtubeLink: null,
    });
    this.toggle();
  };
  handleKey = (e, i) => {
    let cmsExtraTemp = this.state.cmsExtra;
    cmsExtraTemp[i]["key"] = e.target.value;
    this.setState({ cmsExtra: cmsExtraTemp });
  };
  handleValue = (e, i) => {
    let cmsExtraTemp = this.state.cmsExtra;
    cmsExtraTemp[i]["value"] = e.target.value;
    this.setState({ cmsExtra: cmsExtraTemp });
    // console.log(this.state.cmsExtra);
  };
  handleChange=(event,i) => {
  //console.log('inside function');
    this.setState({imageLogo2: event.target.value});
  }
  handleSetStyle = (style, i) => {
    let cmsExtraTemp = this.state.cmsExtra;
    cmsExtraTemp[i]["style"] = style.key;
    cmsExtraTemp[i]["key"] = style.key;
    // cmsExtraTemp[i]["value"] = style.name;
    this.setState({ cmsExtra: cmsExtraTemp });
  };
  handleIcon = (value,i) =>{
    this.setState({ imageLogo2: value });
  }
  createUI = () => {
    return this.state.cmsExtra.map((el, i) => (
      <div key={i} className="form-inline" id={"div" + i}>
        <Col md={4}>
          <div className="mb-2 mr-sm-2 mb-sm-0 form-group">
            <label for={"examplekey" + i} className="mr-sm-2">
              Key
            </label>
            <input
              name="key[]"
              value={this.state.cmsExtra[i]["key"]}
              onChange={(e) => this.handleKey(e, i)}
              id={"examplekey" + i}
              placeholder="key"
              type="text"
              className="form-control"
            />
          </div>
        </Col>
        <Col md={4}>
          <div className="mb-2 mr-sm-2 mb-sm-0 form-group">
            <label for={"examplevalue" + i} className="mr-sm-2">
              Value
            </label>
             <input
              name="value[]"
              value={this.state.cmsExtra[i]["value"]}
              onChange={(e) => this.handleValue(e, i)}
              id={"examplevalue" + i}
              placeholder="value"
              type="text"
              className="form-control"
             />
          </div>
        </Col>
        <Col md={4} className="mt-4">
          <Button color="primary" onClick={this.removeClick.bind(this, i)}>
            {" "}
            Remove{" "}
          </Button>
        </Col>
      </div>
    ));
  };
  removeClick = async (i) => {
    let cmsExtra = [...this.state.cmsExtra];
    // console.log(i);
    cmsExtra.splice(i, 1);
    await this.setState({ cmsExtra: cmsExtra });
  };
  dynamicUi = () => {
    if (this.props.cmsArray[this.props.cmsKey + "style"]) {
      let cmsExtraTemp = [];
      Object.keys(this.props.cmsArray[this.props.cmsKey + "style"]).map(
        (val, i) => {
          let cmsObject = {
            key: val,
            value: this.props.cmsArray[this.props.cmsKey + "style"][val],
          };
          cmsExtraTemp[i] = cmsObject;
        }
      );
      this.setState({ cmsExtra: cmsExtraTemp });
    }
  };
  modelImage() {
    if (this.props.cmsArray) {
      return (
        <Col md="12">
          <div className="mb-2">
            <Row>
              {this.props.iconModel && this.props.cmsKey
                ? this.props.cmsArray[this.props.cmsKey].map((item) =>
                    item.images_backups && item.images_backups.is_video == 0 ? (
                      <Col md="3" className="mb-2">
                        <img src={item.images_backups.image} />
                      </Col>
                    ) : null
                  )
                : null}
              {this.state.pictures.map((item) => (
                <Col md="3">
                  <img src={item} />
                  {this.state.imageLogo}
                </Col>
              ))}
            </Row>
          </div>
        </Col>
      );
    } else {
      return null;
    }
  }
  iconOptions = [
      { value: "zmdi zmdi-eye",      name : <i className= "zmdi zmdi-eye"></i>     },
      { value: "zmdi zmdi-lock",     name : <i className= "zmdi zmdi-lock"></i>    },
      { value: "zmdi zmdi-settings", name : <i className= "zmdi zmdi-settings"></i>},
      { value: "zmdi zmdi-phone",    name : <i className= "zmdi zmdi-phone"></i>   },
      { value: "zmdi zmdi-copy",     name : <i className= "zmdi zmdi-copy"></i>    },
      { value: "zmdi zmdi-spinner",  name : <i className= "zmdi zmdi-spinner"></i> }
  ]

 
  render() {
    let key_id=this.props[0];
    let section_id=key_id+'_';
    if (this.props.cmsArray[this.props.cmsKey + "style"] && flag == 0) {
      if (this.props.cmsModel || this.props.iconModel) {
        flag++;
        this.dynamicUi();
      }
    }
    return (
      <div style={{ "--global-color-var": this.state.theme_color }}>
       <Modal
          isOpen={this.props.iconModel}
          fade={false}
          toggle={this.toggleImage}
          className={this.props.className}
        >
          <ModalHeader toggle={this.toggleImage}>Edit Icon</ModalHeader>
            <Tabs>
            <ModalBody>
              <TabList>
                <Tab>Icon</Tab>
                <Tab> Font Awesome Icon </Tab>
              </TabList>
            </ModalBody>
            <ModalBody>
              <TabPanel>
                <Col md="12">
                  <div className="mb-2">
                    <Row>
                      <Col md="4">
                        <img
                          onClick={() => this.SetImage()}
                          src={this.state.imageLogo}
                        />
                      </Col>      
                    </Row>
                  </div>
                </Col>
                <div className="dropzone-wrapper dropzone-wrapper-lg mb-2">
                  <Dropzone
                    onDrop={this.onDrop.bind(this)}
                    onFileDialogCancel={this.onCancel.bind(this)}
                  >
                    {({ getRootProps, getInputProps }) => (
                      <div {...getRootProps()}>
                        <input {...getInputProps()} />
                        <div className="dropzone-content">
                          <p>
                            Try dropping some files here, or click to select
                            files to upload.
                          </p>
                        </div>
                      </div>
                    )}
                  </Dropzone>
                </div>
                <Col md="12">  
                </Col>
                <form onSubmit={this.handleSubmit}>
                  <div className="pb-3 pt-3">
                    <Button
                      className="pull-right"
                      color="primary"
                      onClick={this.addClick.bind(this)}
                    >
                      Add More
                    </Button>
                  </div>
                  <div className="styleSheetDiv">{this.createUI()}</div>
                </form>
              </TabPanel>
              </ModalBody>
              <TabPanel> 
              <div className="form-inline" style={{'z-index':'3','position': 'relative'}}>
              <Col md={7}>
                <label>
                  Font Name: 
                  <input name="fontkey" placeholder="zmdi zmdi-" type="text" id="formGroupExampleInput" value={this.state.imageLogo2} onChange={this.handleChange} />
                </label>
              </Col>
              <Col md={2}>
                <i className={this.state.imageLogo2} ></i>
              </Col>
              <Col md={2}>
                <UncontrolledButtonDropdown>
                  <DropdownToggle caret className="" color="primary">
                   Select
                  </DropdownToggle>
                   <DropdownMenu>
                   {this.iconOptions.map(item => (
                    <DropdownItem  
                    onClick={this.handleIcon.bind(this, item.value)} 
                    >{item.name}</DropdownItem>
                    )) 
                  }
                   </DropdownMenu>
                </UncontrolledButtonDropdown>
              </Col>
              </div>
             </TabPanel>  
            </Tabs>
            <ModalBody>
            </ModalBody>
            <ModalBody>
            </ModalBody>
          <ModalFooter>
            <Button color="link" onClick={this.toggleImage}>
              Cancel
            </Button>
            <Button color="primary" onClick={this.handleMedia}>
              Save
            </Button>{" "}
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}
const mapStateToProps = (state) => ({
  cmsArray: state.ThemeOptions.cmsArray,
  cmsModel: state.ThemeOptions.cmsModel,
  cmsKey: state.ThemeOptions.cmsKey,
  cmsValue: state.ThemeOptions.cmsValue,
  themeColor: state.ThemeOptions.themeColor,
  themeBackgroundColor: state.ThemeOptions.themeBackgroundColor,
  editorState: state.ThemeOptions.editorState,
  moduleCmsObject: state.ThemeOptions.moduleCmsObject,
  cmsEnableBackground: state.ThemeOptions.cmsEnableBackground,
  cmsEnable: state.ThemeOptions.cmsEnable,
  cmsEnableUploadImage: state.ThemeOptions.cmsEnableUploadImage,
  cmsModelImage: state.ThemeOptions.cmsModelImage,
  iconModel: state.ThemeOptions.iconModel,
  themeSectionBackgroundColor: state.ThemeOptions.themeSectionBackgroundColor,
  sectionBackgroundColor: state.ThemeOptions.sectionBackgroundColor,
  cmsImageArray: state.ThemeOptions.cmsImageArray,
  previousColors: state.ThemeOptions.previousColors,
  cmsVideoArray: state.ThemeOptions.cmsVideoArray,
  cmsSection: state.ThemeOptions.cmsSection,
  backgroundCmsKey: state.ThemeOptions.backgroundCmsKey,
  sectionId: state.ThemeOptions.sectionId,
  contact_us_email: state.ThemeOptions.contact_us_email,
  subscription_email: state.ThemeOptions.subscription_email,
  isGlobal: state.ThemeOptions.isGlobal,
  customStyle: state.ThemeOptions.customStyle,
  showIcon: state.ThemeOptions.showIcon,
  logoIcon: state.ThemeOptions.logoIcon,
});
const mapDispatchToProps = (dispatch) => ({
  setCmsArray: (enable) => dispatch(setCmsArray(enable)),
  setCmsModel: (enable) => dispatch(setCmsModel(enable)),
  setCmsKey: (enable) => dispatch(setCmsKey(enable)),
  setCmsValue: (enable) => dispatch(setCmsValue(enable)),
  setThemeColor: (enable) => dispatch(setThemeColor(enable)),
  setThemeBackgroundColor: (enable) =>
    dispatch(setThemeBackgroundColor(enable)),
  setEditorState: (enable) => dispatch(setEditorState(enable)),
  setModuleCmsObject: (enable) => dispatch(setModuleCmsObject(enable)),
  setCmsEnableBackground: (enable) => dispatch(setCmsEnableBackground(enable)),
  setCmsEnable: (enable) => dispatch(setCmsEnable(enable)),
  setCmsEnableUploadImage: (enable) =>
    dispatch(setCmsEnableUploadImage(enable)),
  setCmsModelImage: (enable) => dispatch(setCmsModelImage(enable)),
  setIconModel: (enable) => dispatch(setIconModel(enable)),
  setShowIcon: (enable) => dispatch(setShowIcon(enable)),
  setLogoIcon: (enable) => dispatch(setLogoIcon(enable)),
  setThemeSectionBackgroundColor: (enable) =>
    dispatch(setThemeSectionBackgroundColor(enable)),
  setSectionBackgroundColor: (enable) =>
    dispatch(setSectionBackgroundColor(enable)),
  setCmsImageArray: (enable) => dispatch(setCmsImageArray(enable)),
  setPreviousColorsValue: (enable) => dispatch(setPreviousColorsValue(enable)),
  setCmsVideoArray: (enable) => dispatch(setCmsVideoArray(enable)),
  setCmsSection: (enable) => dispatch(setCmsSection(enable)),
  setBackgroundCmsKey: (enable) => dispatch(setBackgroundCmsKey(enable)),
  setContactUsEmail: (enable) => dispatch(setContactUsEmail(enable)),
  setSubscriptionEmail: (enable) => dispatch(setSubscriptionEmail(enable)),
  setIsGlobal: (enable) => dispatch(setIsGlobal(enable)),
  setCustomStyle: (enable) => dispatch(setCustomStyle(enable)),
});
export default connect(mapStateToProps, mapDispatchToProps)(IconModal);
// <Screenshot />
