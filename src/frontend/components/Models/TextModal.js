import React, { Component } from "react";
import "../../../index.scss";
import ApiCall from "../../../admin/Partial/Function/ApiCall";
import { SketchPicker } from "react-color";
import ImageUploader from "react-images-upload";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Placeholders from "../Placeholders.js";
import RichText from "../Functions/RichText";
import RichTextStyle from "../Functions/RichTextStyle";
import StyleSheet from "./Functions/StyleSheet";
import { faItalic,faBold,faUnderline} from "@fortawesome/free-solid-svg-icons";
import {
  faClone,
  faTrash,
  faPlus,
  faArrowUp,
  faArrowDown,
  faPencilAlt,
  faCheckCircle,
  faSync,
  faTimes,
  faCheck,
} from "@fortawesome/free-solid-svg-icons";
import {
  setCmsArray,
  setCmsModel,
  setCmsKey,
  setCmsValue,
  setThemeBackgroundColor,
  setThemeColor,
  setEditorState,
  setModuleCmsObject,
  setCmsEnable,
  setCmsEnableBackground,
  setCmsEnableUploadImage,
  setCmsModelImage,
  setThemeSectionBackgroundColor,
  setSectionBackgroundColor,
  setCmsImageArray,
  setPreviousColorsValue,
  setDynamicPlaceholderValue,
  setCmsVideoArray,
  setCmsSection,
  setBackgroundCmsKey,
  setContactUsEmail,
  setSubscriptionEmail,
  setIsGlobal,
  setCustomStyle,
  setPlaceholderValue,
  setAddNewCmsFlag,
  setCmsKeyInner,
  setTypesArray,
  setFontSize,
  setInlineStyle,
  setShowContact,
  setFontFamily,
  setEnableTextClone,
  setRichTextArray,
  setRichTextArrayStyle,
  setRichTextFontSize,
  setRichTextFontFamily,
  setEnableTextModel,
  setRichTextPositionEnd,
  setPlaceholderPosition,
  setInlineStyleEnable,
  setRichTextArrayStyleSheet,
  setEnableRichTextEditor,
} from "../../../reducers/ThemeOptions";
import { connect } from "react-redux";
import {
  EditorState,
  ContentState,
  convertFromHTML,
  convertFromRaw,
  convertToRaw,
  getDefaultKeyBinding,
  KeyBindingUtil,
  CompositeDecorator,
} from "draft-js";
import { Editor } from "react-draft-wysiwyg";
import draftToHtml from "draftjs-to-html";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import Dropzone from "react-dropzone";
import Resizer from "react-image-file-resizer";
import {
  UncontrolledButtonDropdown,
  DropdownToggle,
  Dropdown,
  DropdownMenu,
  DropdownItem,
  Nav,
  NavItem,
  NavLink,
  Container,
  CustomInput,
} from "reactstrap";
import { Card, CardBody, CardTitle } from "reactstrap";
import "../../../index.scss"
import "./richtext.css"
import {
  Row,
  Col,
  ListGroup,
  ListGroupItem,
  FormGroup,
  Label,
  Input,
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
} from "reactstrap";
import "../Placeholders.css"
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import { Loader, Types } from "react-loaders";
import LoadingOverlay from "react-loading-overlay";
import "react-tabs/style/react-tabs.css";
// Passing the customStyleMap is optional
import htmlToDraft from 'html-to-draftjs';
import bullet_styles from "./consts/bullet_styles.json"
let flag = 0;

const iconOptions = [
  { value: "zmdi zmdi-eye",      name : <i className= "zmdi zmdi-eye"></i>     },
  { value: "zmdi zmdi-lock",     name : <i className= "zmdi zmdi-lock"></i>    },
  { value: "zmdi zmdi-settings", name : <i className= "zmdi zmdi-settings"></i>},
  { value: "zmdi zmdi-phone",    name : <i className= "zmdi zmdi-phone"></i>   },
  { value: "zmdi zmdi-copy",     name : <i className= "zmdi zmdi-copy"></i>    },
  { value: "zmdi zmdi-spinner",  name : <i className= "zmdi zmdi-spinner"></i> }
]
const style_groups = [
  { value: 'Heading',icon:'h'},
  { value: 'Paragraph',icon:'p'},
  { value: 'Bullet',icon:'b'},
  { value: 'List',icon:'l'},
  { value: 'Button',icon:'btn'},
  
]
const {hasCommandModifier} = KeyBindingUtil;
let tempArrayRichText=[];
let selectionRichTextPart=null;
let selectionRichTextWholePart=null;

class TextModal extends Component {
  constructor(props) {
    super(props);
    this.handleKeyCommand = this.handleKeyCommand.bind(this);
    this.state = {
      sections: [],
      data: [],
      type_id: null,
      type_name: null,
      type_error: false,
      cmsExtra: [{ key: "", value: "", style: "" }],
      inputs: ["input-0"],
      theme_color: "red",
      previousColors: [],
      editorState: EditorState.createWithContent(
        ContentState.createFromBlockArray(
          convertFromHTML("this.props.cmsValue")
        )
      ),
      youtubeLink: null,
      is_global: false,
      is_global_text: false,
      tranparent:false,
      tranparent_individual:false,
      contactusemail: "",
      subscriptionemail: "",
      is_sticker:false,
      name: "",
      valueP: "",
      id: "",
      style_group:null,
      changed: false,
      changed2: false,
      selectStyleArray:[],
      type_genre: "",
      iterativeInputs : [],
      picture: "",
      message: [],
      no_image_icons: false,
      type_class : "",
      use_default_for_start: false,
      start_from : "",
      fontSizeArray: [6,8,12,16,18,24,30,40,60,80],
      fontFamilyArray: ['Lato','Raleway'],
      selectFont:16,
      selectFontFamily:'Lato',
      text:null,
      style:{},
      status:false,
      is_blod:false,
      is_italic:false,
      open:false,
      is_underline:false,
      font:{
        'fontSize':16
      },
      fontFamily:{
        'fontFamily':'Lato'
      },
      placeholdervalue : "",
      richTextTempData:"",
      tempStyleSheet:[],
      flag:false,
      loader:false,
    };
  }
  handleInlineStyle=async()=>
  {
    console.log(this.props.inlineStyleEnable);
    // const {setInlineStyleEnable} = this.props;
    // await setInlineStyleEnable(false);
    // this.handleRichTextStyle(this.props.inlineStyle);
  }
  handleClearInlineStyle=async()=>
  {
    var cmsValue = this.props.cmsValue.replace(/(<([^>]+)>)/gi, "");
    this.handleSetCmsValue(cmsValue);
  }
  handleSelectionButton=async()=>
  {
    let contentBlock = draftToHtml(
      convertToRaw(this.props.editorState.getCurrentContent())
    );
    if (contentBlock) {
      const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks);
      const editorState = EditorState.createWithContent(contentState);
      const {setEditorState } = this.props;
      setEditorState(editorState);
    }
    const { setCmsValue } = this.props;
    await setCmsValue(contentBlock);
    // this.handleSetCmsValue(data);
  }
  handleRemoveStyle=(data,style,input,start,search)=>
  {
    let first_char=data.trim().replace(/ .*/,'');
    let temp= input.split(first_char);
    // console.log(first_char);
    // console.log(temp);
    // temp[0]+first_Char+temp[1]
    let last_char=selectionRichTextWholePart.slice(0,start);
    last_char = last_char.trim().split(" ").pop();
    // console.log(last_char);

    let exactPostion=temp[(temp.length)-2].split(last_char);
    search=search.split(" ");
    for (var i = 0 ; i < search.length; i++) {
      // 
      Object.keys(this.state.tempStyleSheet).map(item => {
        var temp_style='<span '+this.state.tempStyleSheet[item]+'>'+search[i]+'</span>';
        if(this.state.tempStyleSheet[item] != style)
        {
          // console.log(exactPostion);
          // console.log(exactPostion[(temp.length)-1]);
          // console.log(temp_style);
          // console.log(search[i]);
          // console.log(temp.length-1);

          exactPostion[(exactPostion.length)-1]=exactPostion[(exactPostion.length)-1].replace(temp_style,search[i]);
        }
        // else
        // {
        //   console.log('in else part');
        //   console.log(style);
        //   console.log(this.state.tempStyleSheet[item]);
        //   console.log(search[i]);
        //     console.log(temp_style);
          // this.setState({ flag: true });
          // exactPostion[1]=exactPostion[1].replace(temp_style,search[i]);
        // }
      });
      // console.log(exactPostion[1]);
      // <span " +style_key+">"+selectionRichTextPart+"</span>
    }
    // console.log(input);
    if(temp.length > 2)
    {
      var extraData='';
      if (typeof(exactPostion[(exactPostion.length)-2]) != "undefined")
      {
        extraData=exactPostion[(exactPostion.length)-2]+last_char
      }
      input=temp[0]+first_char+extraData+exactPostion[(exactPostion.length)-1]+first_char+temp[(temp.length)-1];
    }
    else
    {
      input=exactPostion[(exactPostion.length)-2]+last_char+exactPostion[(exactPostion.length)-1]+first_char+temp[(temp.length)-1];

    }
    // console.log(input);
    return input;

  }
  handleSetTags=(input, search, style, start, end)=>{
    let tempArray=search.split(" ");
    let emptyArray=[];
    for (var i = 0; i < tempArray.length; i++) {
        emptyArray.push('<span class="public-DraftStyleDefault-ltr">'+tempArray[i]+'</span>');
    } 
    emptyArray=emptyArray.join(' ');
    // emptyArray=<span class='testing'>"+selectionRichTextPart+"</span>
    // console.log(search);

    // let temp=selectionRichTextWholePart.split(selectionRichTextPart);
    // console.log(temp);
    // console.log(selectionRichTextWholePart.length);
    // console.log(start);
    // console.log(selectionRichTextWholePart.substr(start))
    let textPosition=selectionRichTextWholePart.substr(start);
    let temp=textPosition.split(selectionRichTextPart);
    // console.log(temp);
    if(temp[1])
    {
      input=this.handleRemoveStyle(temp[1],style,input,start,search);
    }
    if(!this.state.flag)
    {
       this.setState({ flag: false });

    }
    var postion=input.indexOf(search,start);
    input=input.slice(0,postion)+input.slice(postion).replace(search, emptyArray);
    // console.log(input);
    return input;
  }
  handleReplaceAt=(input, style, replace, start, end)=>{
    // console.log(input);
    // console.log(style);
    // console.log('dfdf');
    let tempArray=replace.split(" ");
    let emptyArray=[];
    for (var i = 0; i < tempArray.length; i++) {
      emptyArray.push("<span " +style+">"+tempArray[i]+"</span>");
    }
    // replace 
    style=emptyArray.join(' ');
    // console.log(replace);
    // console.log(emptyArray);
    // console.log(end);
    // console.log(input.slice(start,end));
    // console.log(style.split(" "))
    start= parseInt(start);
    var postion=input.indexOf(style.trim(),start);
    // console.log(postion);
    // console.log(style);
    // console.log(start);
    // console.log(input.indexOf(style,start));
    // console.log(input.slice(start));
    input=input.slice(0,postion)+input.slice(postion).replace(style, replace);
    // console.log(input);
    // input.slice(0, start)
    //   + input.slice(start, end).replace(search, replace)
      // + input.slice(end)
      // console.log(input);
    return input;
  }
  handleSearchAt=(input, style_key,text,start)=>{
    // console.log(input);
    // console.log(text);
    // console.log(style_key);
    let textPosition=selectionRichTextWholePart.substr(start);
    // console.log(textPosition);
    let temp=textPosition.split(selectionRichTextPart);
    // console.log(temp);
    
    let first_char=temp[1].trim().replace(/ .*/,'');
    let temp_1= input.split(first_char);
    let last_char=selectionRichTextWholePart.slice(0,start);
    last_char = last_char.trim().split(" ").pop();
    // console.log(temp_1);
    if(temp_1.length > 1)
    {
      temp_1= temp_1[(temp_1.length)-2].split(last_char);
    }
    // console.log(last_char);


    let temp_style_key=style_key;
    let tempArray=text.split(" ");
    let emptyArray=[];
    for (var i = 0; i < tempArray.length; i++) {
      emptyArray.push("<span " +style_key+">"+tempArray[i]+"</span>");
    } 
    style_key=emptyArray.join(' ');
    // input=input.substring(start);
    // console.log(input);
    // input=input.substring(0,style_key.length);
    // console.log(temp_1);
    // console.log(style_key);
    // console.log(temp_1[1].includes(style_key));

    return temp_1[(temp_1.length)-1].includes(style_key);
  }
  handleSetCmsValue=async(data)=>
  {
    let temp=data;
    this.setState({ richTextTempData: temp });
    var contentBlock = htmlToDraft(data);
    if (contentBlock) {
      const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks);
      const editorState = EditorState.createWithContent(contentState);
      const {setEditorState } = this.props;
      setEditorState(editorState);
    }
    const { setCmsValue } = this.props;
    await setCmsValue(temp);
    console.log(this.props.cmsValue);
  }
  handleRichTextStyle=async(style_key)=>
  {
    var  tempArray={};
    Object.keys(this.props.richTextArrayStyleSheet).map(item => {
      let data=this.props.richTextArrayStyleSheet[item];
      let style='style="color:'+data['fontColor']+';font-size:'+data['fontSize']+';font-family:'+data['fontFamily']+';font-Weight:'+data['fontWeight']+';line-height:'+data['lineHeight']+';"';
      tempArray[item]=style;
    });
    this.setState({ tempStyleSheet: tempArray });
    var cmsValue=this.props.cmsValue;
    console.log(cmsValue);
    if (this.state.richTextTempData !== "") {
      cmsValue=this.state.richTextTempData
    }
    // replace(/<\/?span[^>]*>/g,"")
    cmsValue=cmsValue.replace(/<\/?p[^>]*>/g,"");
    cmsValue=cmsValue.replace(/<\/?ins[^>]*>/g,"");
    cmsValue=cmsValue.replace(/<\/?br[^>]*>/g,"");
    // var style="<span " +style_key+">"+selectionRichTextPart+"</span>";
    var style="<span class='testing'>"+selectionRichTextPart+"</span>";
    // console.log(selectionRichTextPart);
    var flag=this.handleSearchAt(cmsValue, style_key,selectionRichTextPart, this.props.placeholderPosition);
    if(flag)
    {
      cmsValue=this.handleReplaceAt(cmsValue,style_key,selectionRichTextPart, this.props.placeholderPosition, this.props.richTextPositionEnd);
    }
    else
    {
      // console.log(cmsValue);
      // console.log(this.state.tempStyleSheet);
      // // style_key
      // Object.keys(this.state.tempStyleSheet).map(item => {
      //   console.log(this.state.tempStyleSheet[item]);
      // });
      // console.log(this.props.richTextPositionEnd);
      // console.log(selectionRichTextWholePart.length);
      // 
      // console.log(selectionRichTextPart);
      let input=cmsValue.substring(this.props.placeholderPosition);
      // console.log(input);
      input=input.substring(0,style_key.length);
      // console.log(input);
      cmsValue=this.handleSetTags(cmsValue, selectionRichTextPart, style_key, this.props.placeholderPosition, this.props.richTextPositionEnd);
      // console.log(cmsValue);
    }
    this.handleSetCmsValue(cmsValue);
  }
  handleSetPositionEnd=async(position)=>
  {
    const {setRichTextPositionEnd} = this.props;
    await setRichTextPositionEnd(position);
  }
  handleSetPosition=async(position)=>
  {
    const {setPlaceholderPosition} = this.props;
    await setPlaceholderPosition(position);
  }
  getColors = async () => {
    let colors = await ApiCall.getAPICall("colors/get");
    const { setCmsImageArray } = this.props;
    await setPreviousColorsValue(colors.ColorsBackup);
    this.setState({ previousColors: colors.ColorsBackup });
  };
  addClick = () => {
    var tempObject = { key: "", value: "", style: "" };
    var tempNewCmsExtra = this.state.cmsExtra;
    tempNewCmsExtra.push(tempObject);
    this.setState((prevState) => ({ cmsExtra: tempNewCmsExtra }));
  };

  addIterativeInput = () => {
    var iterativeInputs = this.state.iterativeInputs;
    iterativeInputs.push("");
    this.setState({iterativeInputs});
  }

  handleIterativeInput = async (e, i) => {
    var iterativeInputs = this.state.iterativeInputs;
    iterativeInputs[i] = e.target.value;
    this.setState({iterativeInputs});
  }
  handleIcon = async (val) => {
    this.setState({icon: val})
  }

  addClickCreate = () => {
    this.create();
    this.getPlaceholders();
    const name = this.state.name;   
    const valueP = this.state.valueP;
    this.setState({name: ""});   
    this.setState({valueP: ""});
  };
  addClickDelete = () => {
    this.delete();
    this.getPlaceholders();
  };
  handleChangeComplete = async (color) => {
    if (this.props.themeSectionBackgroundColor) {
      const { setCmsValue } = this.props;
      setCmsValue(color.hex);
    } else {
      this.setState({ theme_color: color.hex });
    }
  };
  handleChangeBackgroundComplete = async (color) => {
    var tempArray = {
      key: this.props.sectionBackgroundColor["key"],
      value: color.hex,
    };
    const { setSectionBackgroundColor } = this.props;
    setSectionBackgroundColor(tempArray);
  };
  componentWillMount() {
    this.getPlaceholders();
    this.getStyleSheet();
    this.getDynamicPlaceholders();
  }
  setCstyle = async (data) => {
    const { setCustomStyle } = this.props;
    await setCustomStyle(data);
  };
  setSectionArray = async (data) => {
    const { setCmsSection } = this.props;
    await setCmsSection(data);
    this.setState({ sections: data });
  };
  getPlaceholders = async () => {
    let placeholderArray = await ApiCall.getAPICall("placeholder");
    await this.setPlaceholderArray(placeholderArray.data);
  };
  getContact = async () => {
    let contacts = await ApiCall.getAPICall("contact/us");
    await this.setContacts(contacts.data)
  };
  setContacts = async (data) => {
    const { setShowContact } = this.props;
    await setShowContact(data);
  }
  setPlaceholderArray = async (data) => {
    const { setPlaceholderValue } = this.props;
    await setPlaceholderValue(data);
  };
  getDynamicPlaceholders = async () => {
    let dynamicPlaceholderArray = await ApiCall.getAPICall("placeholder/dynamic");
    await this.setDynamicPlaceholderArray(dynamicPlaceholderArray.data);
  };
  setDynamicPlaceholderArray = async (data) => {
    const { setDynamicPlaceholderValue } = this.props;
    await setDynamicPlaceholderValue(data);
  };

  setCmsModule = async (data) => {
    const { moduleCmsObject, setModuleCmsObject } = this.props;
    await setModuleCmsObject(data);
  };
  setColorScheme = async () => {
    let color = {
      color: this.state.theme_color,
    };
    let background = {
      background: this.state.theme_color,
    };
    const { themeColor, setThemeColor } = this.props;
    await setThemeColor(color);
    const { themeBackgroundColor, setThemeBackgroundColor } = this.props;
    await setThemeBackgroundColor(background);
  };
  setCmsData = async (data) => {
    const { cmsArray, setCmsArray } = this.props;
    await setCmsArray(data);
  };
  toggle = async () => {
    flag = 0;
    var empty = [];
    const {setEnableTextModel } = this.props;
    setEnableTextModel(false);
    this.setState({ cmsExtra: empty });
    this.setState({ is_global: this.props.isGlobal });
    const { cmsModel, setCmsModel } = this.props;
    await setCmsModel(false);
    const { setCmsEnableBackground } = this.props;
    await setCmsEnableBackground(false);
    const { setCmsModelImage } = this.props;
    await setCmsModelImage(false);
    const { setCmsEnableUploadImage } = this.props;
    setCmsEnableUploadImage(false);
    const { setAddNewCmsFlag } = this.props;
    setAddNewCmsFlag(false);
    this.setState({pictures: "", icon: ""});
    this.dynamicUi();
    this.setState({iterativeInputs: []});
  };
  toggleBackground = async () => {
    const { cmsEnableBackground, setCmsEnableBackground } = this.props;
    await setCmsEnableBackground(!this.props.cmsEnableBackground);
    await this.toggle();
  };
  handleSetRichTextData=()=>
  {
    // console.log(window.getSelection().toString());
    var selectionState = this.props.editorState.getSelection();
    var start = selectionState.getStartOffset();
    var end = selectionState.getEndOffset();
    selectionRichTextPart=this.handleGetCursorPostionData(start,end);
    this.handleSetPosition(start);
    this.handleSetPositionEnd(end);  
  }
  handleGetCursorPostionData=(start,end=null)=>
  {
    var anchorKey = this.props.editorState.getSelection().getAnchorKey();
    var currentContent = this.props.editorState.getCurrentContent();
    var currentContentBlock = currentContent.getBlockForKey(anchorKey);
    selectionRichTextWholePart=currentContentBlock.getText();
     let data = draftToHtml(
      convertToRaw(this.props.editorState.getCurrentContent())
    );
     // console.log(data.slice(start, end));
    this.handleExactLocation(data);
    var selectedText = currentContentBlock.getText().slice(start, end);
    return selectedText;
  }
  handleExactLocation=(data)=>
  {
    // console.log(data);
  }
  setValue = (e) => {
    this.handleSetRichTextData();
    let data = draftToHtml(
      convertToRaw(this.props.editorState.getCurrentContent())
    );
    const { setCmsValue } = this.props;
    setCmsValue(data);
  };
  makeGlobal = (setCmsArray) => {
    Object.keys(setCmsArray).map((val, i) => {
      var flag = val.includes("_global");
      if (flag) {
        setCmsArray[val][val] = this.state.theme_color;
      }
    });
    return setCmsArray;
  };
  handleChangeName = async (e, i) => {
    let w = this.props.placeholderValue;
    w[i]["name"] = e.target.value;
    const { setPlaceholderValue } = this.props;
    await this.setPlaceholderArray(w);
    this.setState({changed: !this.state.changed})
  };
  handleChangeValue = async (e, i) => {
    let w = this.props.placeholderValue;
    w[i]["value"] = e.target.value;
    const { setPlaceholderValue } = this.props;
    await this.setPlaceholderArray(w);
    this.setState({changed2: !this.state.changed2})
  };
  handleSetNewValue=async()=>
  {
    var newCmsValue=this.props.cmsValue.replace(/<\/?span[^>]*>/g,"");
    var html_style='<span style="';
    var tempStyleKey=null;
    if(this.props.inlineStyle){
      Object.keys(this.props.inlineStyle).map((val, i) => {
        tempStyleKey=val;
        switch (val) {
          case 'fontWeight':
            tempStyleKey='font-weight';
            break;
          case 'fontStyle':
            tempStyleKey='font-style';
            break;
          case 'textDecorationLine':
            tempStyleKey='text-decoration';
            break;
        }
        html_style=html_style+tempStyleKey+':'+this.props.inlineStyle[val]+';';
        });
    }
    html_style=html_style+'font-size:'+this.props.fontSize+'px;';
    html_style=html_style+'font-family:'+this.props.fontFamily+'px;';
    html_style=html_style+'">';
    html_style=html_style+newCmsValue;
    html_style=html_style+'</span>';
    const { setCmsValue } = this.props;
    setCmsValue(html_style);
  }
  startLoader=()=>
  {
    this.setState({loader: !this.state.loader})

  }
  submit= async (flag) => {
    // if(typeof(this.props.cmsValue) == "string"){
    //   await this.handleSetNewValue();
    // }
    if (this.state.type_id) {
      this.startLoader();
      await this.getColors();
      let tempCmsStyle = {};
      let tempCmsStyle2 = {};
      let setCmsArray = this.props.cmsArray;
      let tempCmsExtra = this.state.cmsExtra;
      Object.keys(tempCmsExtra).map(function (keyName, keyIndex) {
        tempCmsStyle[tempCmsExtra[keyName]["key"]] =
          tempCmsExtra[keyName]["value"];
      });
      let sendDataCms = {};
      let colorCms = {};
      if (!this.state.is_global) {
        let globalKeyArray = {};
        globalKeyArray[this.props.backgroundCmsKey] = this.state.theme_color;
        colorCms["global"] = globalKeyArray;
        if (setCmsArray[this.props.backgroundCmsKey]) {
          setCmsArray[this.props.backgroundCmsKey][
            this.props.backgroundCmsKey
          ] = this.state.theme_color;
        }
      } else {
        colorCms["global"] = this.state.theme_color;
        setCmsArray = await this.makeGlobal(setCmsArray);
      }
      const { setIsGlobal } = this.props;
      setIsGlobal(this.state.is_global);
      colorCms["key"] = this.props.sectionBackgroundColor["key"];
      colorCms["value"] = this.props.sectionBackgroundColor["value"];
      colorCms["tranparent"]=this.state.tranparent;
      sendDataCms["inline_style"] = this.props.inlineStyle;
      var fontStyle={
        font_size:this.props.fontSize,
        font_family:this.props.fontFamily,
      };
      sendDataCms["tranparent"]=this.state.tranparent_individual;
      sendDataCms["font_style"]=fontStyle;
      sendDataCms["type_id"] = this.state.type_id;
      sendDataCms["type_name"] = this.state.type_name;
      sendDataCms["style_group"] = this.state.style_group;
      sendDataCms["is_global_text"] = this.state.is_global_text;
      sendDataCms["key"] = this.props.cmsKey;
      sendDataCms["innerkey"] = this.props.cmsKeyInner;
      sendDataCms["value"] = this.props.cmsValue;
      sendDataCms["new"] = this.props.addNewCmsFlag;
      sendDataCms["style"] = tempCmsStyle;
      sendDataCms["color"] = colorCms;
      sendDataCms["is_global"] = this.state.is_global;
      if(this.state.type_genre == "bullet" || this.state.type_genre == "list"){
        sendDataCms['type_genre'] = this.state.type_genre;
        sendDataCms['class_name'] = this.state.bullet_styles;
        if(this.state.pictures || this.state.icon){
          sendDataCms['image'] = this.state.pictures ? this.state.pictures:this.state.icon;
        }
        if(this.state.use_default_for_start){
          sendDataCms['starting_number'] = 1;
        } else if(this.state.start_from){
          sendDataCms['starting_number'] = this.state.start_from;
        }
        let richText={};
        richText['text']=this.props.richTextArray;
        richText['style']=this.props.richTextArrayStyle;
        richText['font']=this.props.richTextFontSize;
        richText['family']=this.props.richTextFontFamily;
        // this.state.iterativeInputs;
        sendDataCms['texts'] = richText;
        sendDataCms["value"] = "";
      }
      this.setColorScheme();
      // this.setColorBackup(this.state.theme_color);
      var url = "setCmsData";
      if (this.props.sectionId.includes("navbar")) {
        var sectionHeaderUrls = "sections/showSectionsInNavbar";
        var obj = {
          value: this.state.sections,
          flag:this.state.is_sticker,
        };
        await ApiCall.postAPICall(sectionHeaderUrls, obj);
      };
      var response = await ApiCall.postAPICall(url, sendDataCms);
      setCmsArray[this.props.sectionBackgroundColor["key"]][
        this.props.sectionBackgroundColor["key"]
      ] = this.props.sectionBackgroundColor["value"];
      if (setCmsArray[this.props.cmsKey + "style"]) {
        setCmsArray[this.props.cmsKey + "style"][
          this.props.cmsKeyInner + "style"
        ] = tempCmsStyle;
      }
      if (this.props.sectionId == "navbar") {
        setCmsArray[this.props.cmsKeyInner][
          this.props.cmsKeyInner
        ] = this.props.cmsValue;
      } else if (this.props.addNewCmsFlag) {
        setCmsArray[this.props.cmsKey][
          response.inner_key
        ] = this.props.cmsValue;
      } else {
        setCmsArray[this.props.cmsKey][
          this.props.cmsKeyInner
        ] = this.props.cmsValue;
      }
      this.setCmsData(setCmsArray);
      if (
        this.props.cmsKey.includes("contact") ||
        this.props.cmsKey.includes("subscription")
      ) {
        this.handleEmailChanges();
      }
      this.startLoader();
      if(flag)
      {
        this.toggle();
      }
      this.props.getData();
    }  else {
      this.setState({ type_error: true });
    }
  };
  handleTextClone=async()=>
  {
    const {setEnableTextClone } = this.props;
    await setEnableTextClone(false);
    var data = await ApiCall.getAPICall('type/get/'+this.props.sectionId);
    if(data.type_id){
      await this.handleSetType(data.type_id);
    } else {
      this.setState({type_name :""});
      this.setState({type_id :""});
    }
    this.submit(1);
  }
  setBulletRichTextData=async()=>
  {
    if(typeof(this.props.cmsArray[this.props.cmsKey]) != "undefined" && typeof(this.props.cmsArray[this.props.cmsKey][this.props.cmsKeyInner]?.content) !== "undefined")
    {
      tempArrayRichText=[];
      this.props.cmsArray[this.props.cmsKey][this.props.cmsKeyInner]['content']['texts'].map(item=>{
        // item=item.replace(/<\/?span[^>]*>/g,"");
        tempArrayRichText.push(item);
      })
      const { setRichTextArray } = this.props;
      await setRichTextArray(this.props.cmsArray[this.props.cmsKey][this.props.cmsKeyInner]['content']['texts']);
      // console.log(tempArrayRichText);
      var count=0;
      this.props.cmsArray[this.props.cmsKey][this.props.cmsKeyInner]['content']['styles'].map(item=>{
        this.setFontSizeForContent(item[0],count);
        this.setInlineStyleForContent(JSON.parse(item[1]),count);
        this.setFontFamilyForContend(item[2],count);
        count++;
      })
    }
  }
  setFontSizeForContent=async(font,count)=>
  {
    var style={'fontSize':font};
    var array=this.props.richTextFontSize;
    array[count]=style;
    const { setRichTextFontSize } = this.props;
    await setRichTextFontSize(array);
  }
  setFontFamilyForContend=async(family,count)=>
  {
    var style={'fontFamily':family};
    var array=this.props.richTextFontFamily;
    array[count]=style;
    const { setRichTextFontFamily } = this.props;
    await setRichTextFontFamily(array);
  }
  setInlineStyleForContent=async(style,count)=>
  {
    var arrayStyle=this.props.richTextArrayStyle;
    var styleArray={};
    styleArray['style']=style;
    if (typeof(arrayStyle[count]) != "undefined")
    {
      styleArray['style'] = {...styleArray['style'], ...arrayStyle[count]['style'] };
    }
    arrayStyle[count]=styleArray;
    const { setRichTextArrayStyle } = this.props;
    await setRichTextArrayStyle(arrayStyle);
  }
  componentDidUpdate(prevProps){
    if(this.props.inlineStyleEnable)
    {
      this.handleInlineStyle();
    }
    this.setBulletRichTextData();
    if(this.props.enableTextClone)
    {
      this.handleTextClone();
    }
    if ( this.props.cmsModel !== prevProps.cmsModel ) {
      if(this.props.cmsModel){
        if(typeof(this.props.cmsArray[this.props.backgroundCmsKey]) !='undefined' && this.props.cmsArray[this.props.backgroundCmsKey][this.props.backgroundCmsKey] =='unset')
        {
          this.setState({tranparent_individual:true}); 
        }
        if(this.props.cmsSection[0]['is_sticker'])
        {
          this.setState({is_sticker:true}); 
        }
        if(this.props.sectionBackgroundColor['value'] =='white')
        {
          this.setState({tranparent:true}); 
        }
        this.getType();
        var arr = [];
        var data = this.props.cmsArray[this.props.cmsKey][this.props.sectionId];
        if(data){
          if(data.type == "bullet" || data.type == "list"){
            // this.setState({pictures: data.content.image})
            data.content.texts.map(item=>(
              arr.push(item.value)
            ))
            this.setState({iterativeInputs: arr});
          }
        }
      } 
    }
  }

  getStyleSheet=async()=>
  {
    var data = await ApiCall.getAPICall('richtextstyle');
    const { setRichTextArrayStyleSheet } = this.props;
    await setRichTextArrayStyleSheet(data.data);
  }
  handleEmailChanges = async () => {
    var url = "setCmsData";
    let setCmsArray = this.props.cmsArray;
    var data = {};
    if (this.props.cmsKey.includes("contact")) {
      data["key"] = this.props.sectionId + "contact_us_email";
      data["value"] = this.props.contact_us_email;
      setCmsArray[
        this.props.sectionId + "contact_us_email"
      ] = this.props.contact_us_email;
    }
    if (this.props.cmsKey.includes("subscription")) {
      data["key"] = this.props.sectionId + "subscription_email";
      data["value"] = this.props.subscription_email;
      setCmsArray[
        this.props.sectionId + "subscription_email"
      ] = this.props.subscription_email;
    }
    ApiCall.postAPICall(url, data);
    this.setCmsData(setCmsArray);
    return;
  };
  onEditorStateChange = (editorState) => {
    const { setEditorState } = this.props;
    setEditorState(editorState);
  };
  setColorBackup = async (hex) => {
    let nearestColorName;
    let nearestColorHex;
    await fetch("https://api.color.pizza/v1/" + hex.split("#")[1])
      .then((res) => res.json())
      .then(
        (result) => {
          nearestColorName = result.colors[0].name;
          nearestColorHex = result.colors[0].hex;
        },
        (error) => {
          //console.log(error);
        }
      );
    var color = {
      color: nearestColorName,
      value: nearestColorHex,
    };
    var response = await ApiCall.postAPICall("colors/store", color);
  };
  handleKey = (e, i) => {
    let cmsExtraTemp = this.state.cmsExtra;
    cmsExtraTemp[i]["key"] = e.target.value;
    this.setState({ cmsExtra: cmsExtraTemp });
  };
  handleValue = (e, i) => {
    let cmsExtraTemp = this.state.cmsExtra;
    cmsExtraTemp[i]["value"] = e.target.value;
    this.setState({ cmsExtra: cmsExtraTemp });
  };
  handleSetStyle = (style, i) => {
    let cmsExtraTemp = this.state.cmsExtra;
    cmsExtraTemp[i]["style"] = style.key;
    cmsExtraTemp[i]["key"] = style.key;
    // cmsExtraTemp[i]["value"] = style.name;
    var selectStyleArray=this.state.selectStyleArray;
    selectStyleArray[i]=style.name;
    this.setState({ selectStyleArray: selectStyleArray });
    this.setState({ cmsExtra: cmsExtraTemp });
  };
  create =(e)=> {
    let data =  ApiCall.postAPICall("placeholder/create", {
      name: this.state.name,
      value: this.state.valueP,
    });
  }
  delete = (e) => {
    let data =  ApiCall.postAPICall("placeholder/delete", {
      id: e,
    });
    this.getPlaceholders(); 
  } 
  update = (e,i) => {
    let data =  ApiCall.postAPICall("placeholder/update", {
      id: e,
      name: this.props.placeholderValue[i]["name"],
      value: this.props.placeholderValue[i]["value"],
    }
    )
  }
  handleKeyCommand(command: string): DraftHandleValue {
    if (command === 'enter_command') {
      return 'handled';
    }

    if (command === 'ctrl_s_command') {
      return 'handled';
    }
    return 'not-handled';
  }
  myKeyBindingFn = (e) => {
    if (e.keyCode === 13 /* `enter` key */ ) {
      return 'enter_command';
    }
    if (e.keyCode === 83 /* `S` key */ && hasCommandModifier(e) /* + `Ctrl` key */) {
      return 'ctrl_s_command';
    }
    //else...
    return getDefaultKeyBinding(e);
  }
  createUI = () => {
    return this.state.cmsExtra ?this.state.cmsExtra.map((el, i) => (
      <div key={i} className="form-inline" id={"div" + i}>
        <Col md={3}>
          <UncontrolledButtonDropdown>
            <DropdownToggle caret className="mt-4" color="primary">
              {this.state.selectStyleArray[i]}
            </DropdownToggle>
            <DropdownMenu>
              {this.props.customStyle.map((object) => (
                <DropdownItem onClick={() => this.handleSetStyle(object, i)}>
                  {object.name}
                </DropdownItem>
              ))}
            </DropdownMenu>
          </UncontrolledButtonDropdown>
        </Col>
        <Col md={3} style={{display:'none'}}>
          <div className="mb-2 mr-sm-2 mb-sm-0 form-group">
            <label for={"examplekey" + i} className="mr-sm-2">
              
            </label>
            <input
              name="key[]"
              value={this.state.cmsExtra[i]["key"]}
              onChange={(e) => this.handleKey(e, i)}
              id={"examplekey" + i}
              placeholder="key"
              type="hidden"
              className="form-control"
            />
          </div>
        </Col>
        <Col md={3}>
          <div className="mb-2 mr-sm-2 mb-sm-0 form-group">
            <label for={"examplevalue" + i} className="mr-sm-2">
              Value
            </label>
            <input
              name="value[]"
              value={this.state.cmsExtra[i]["value"]}
              onChange={(e) => this.handleValue(e, i)}
              id={"examplevalue" + i}
              placeholder="value"
              type="text"
              className="form-control"
            />
          </div>
        </Col>
        <Col md={3} className="mt-4">
          <FontAwesomeIcon
            icon={faTrash}
            size="1x"
            color="red"
            onClick={this.removeClick.bind(this, i)}
          />
        </Col>
      </div>
    )): null;
  };
  createUI2 =  () => {
    return this.props.placeholderValue.map((el, i) => (
      <div name={i} className="form-inline" id={"div" + i}>
        <Col md={4}>
          <div className="mb-2 mr-sm-2 mb-sm-0 form-group">
            <label for={"examplekey" + i} className="mr-sm-2">
              Name
            </label>
            <input
              name="name"
              id={this.state.id}
              placeholder="name"
              type="text"
              value={el.name}
              onChange={(e) => this.handleChangeName(e, i)}
              className="form-control"
            />
          </div>
        </Col>
        <Col md={4}>
          <div className="mb-2 mr-sm-2 mb-sm-0 form-group">
            <label for={"examplevalue" + i} className="mr-sm-2">
              Value
            </label>
            <input
              name="value"
              id={this.state.id}
              placeholder="value"
              type="text"
              value={el.value}
              onChange={(e) => this.handleChangeValue(e, i)}
              className="form-control"
            />
          </div>
        </Col>
        <Col md={1}>
         <div className="mt-4">
            <FontAwesomeIcon
              icon={faSync}
              size="1x"
              color="voilet"
              background="MistyRose"
              onClick={(e) => this.update(el.id,i)}
            />
            </div> 
        </Col>
        <Col md={1} className="mt-4">
          <FontAwesomeIcon
            icon={faTrash}
            size="1x"
            color="red"
            onClick={(e) => this.delete(el.id)}            
          />
        </Col>    
      </div>
    ));
  };
  removeClick = async (i) => {
    let cmsExtra = [...this.state.cmsExtra];
    cmsExtra.splice(i, 1);
    await this.setState({ cmsExtra: cmsExtra });
  };
  handleSetType = async (data) => {
    this.setState({ type_error: false });
    this.setState({ type_id: data["id"] });
    this.setState({ type_name: data["name"] });
    this.setState({ style_group: data["group"] });
    this.calulateGenre({data});
    await this.dynamicHeaderStyle(data["id"]);
  };

  setTypeGenre = (genre) => {
    this.setState({ type_genre: genre });
  }

  calulateGenre = ({data}) => {
    if(data.is_bullet){
      this.setTypeGenre('bullet');
    } else if(data.is_list){
      this.setTypeGenre('list');
    } else if(data.is_button){
      this.setTypeGenre('button')
    } else {
      this.setTypeGenre('text');
    }
  }

  onDrop = async (files) => {
    Resizer.imageFileResizer(
      files[0], // the file from input
      500, // width
      500, // height
      "PNG", // compress format WEBP, JPEG, PNG
      70, // quality
      0, // rotation
      (uri) => {
        this.setState({ pictures: uri });
        //console.log(uri);
      },
      "base64"
    );
  };
  onCancel() {
    this.setState({
      pictures: ''
    });
  }
  dynamicHeaderStyle = (id) => {
    let cmsExtraTemp = [];
    let cmsObject = {};
    this.setState({ cmsExtra: null });
    Object.keys(this.props.typesArray).map((val, i) => {
      if (this.props.typesArray[i]["id"] == id) {
        Object.keys(this.props.typesArray[i]["style_sheet"]).map(
          (index, count) => {
            cmsObject = {
              key: this.props.typesArray[i]["style_sheet"][count]["style_key"],
              value: this.props.typesArray[i]["style_sheet"][count][
                "style_value"
              ],
            };
            var selectStyleArray=this.state.selectStyleArray;
            selectStyleArray[count]='select';
            Object.keys(this.props.customStyle).map(
            (styleIndex) => {
              if(this.props.customStyle[styleIndex]['key'] == this.props.typesArray[i]["style_sheet"][count]["style_key"] )
              {
                selectStyleArray[count]=this.props.customStyle[styleIndex]['name'];
              }
            });
            this.setState({ selectStyleArray: selectStyleArray });
            cmsExtraTemp[count] = cmsObject;
          }
        );
      }
    });
    this.setState({ cmsExtra: cmsExtraTemp });
  };
  dynamicUi = () => {
    if (this.props.cmsArray[this.props.cmsKey + "style"]) {
      let cmsExtraTemp = [];
      let cmsObject = {};
      Object.keys(this.props.cmsArray[this.props.cmsKey + "style"]).map(
        (val, i) => {
          Object.keys(
            this.props.cmsArray[this.props.cmsKey + "style"][val]
          ).map((index, count) => {
            cmsObject = {
              key: index,
              value: this.props.cmsArray[this.props.cmsKey + "style"][val][
                index
              ],
            };
            cmsExtraTemp[count] = cmsObject;
          });
        }
      );
      this.setState({ cmsExtra: cmsExtraTemp });
    }
  };
  handleChangeName1 = e => {
    this.setState({
      name: e.target.value
    });   
  };
  handleChangeValue1 = e => {
    this.setState({
      valueP: e.target.value
    });
  };
  getType = async () => {
    if(this.props.cmsModel){
      var data = await ApiCall.getAPICall('type/get/'+this.props.sectionId);
      if(data.type_id){
        await this.handleSetType(data.type_id);
      } else {
        this.setState({type_name :""});
        this.setState({type_id :""});
      }
    }
  }
  openGroupDropdown = () => this.setState({open: !this.state.open});
   listItem = this.props.placeholderValue.map(item => (
    <div>
      <li 
        className="rdw-dropdownoption-default placeholder-li"
      >
      {item.name}</li>
    </div>
  ))
  addGroup=(value)=>
  {
    this.setState({style_group: value});
    console.log(value);
  }
  handleSetGroup=(value)=>
  {
    this.setState({style_group: value});
  }
  displayStyleGroup=()=>
  {
    var display_value;
    style_groups.map((object) => {
      if(this.state.style_group == object.icon)
      {
        display_value=object.value;
      }
      })
    return display_value;
  }
  render() {
    if (this.props.cmsArray[this.props.cmsKey + "style"] && flag == 0) {
      if (this.props.cmsModel || this.props.cmsModelImage) {
        flag++;
        this.dynamicUi();
      }
    }
    return (
      <div style={{ "--global-color-var": this.state.theme_color }}>
        <Modal
          isOpen={this.props.cmsModel}
          fade={false}
          toggle={
            this.props.cmsEnableBackground ? this.toggleBackground : this.toggle
          }
          className={this.props.className}
          size="lg"
        >
          <ModalHeader className="custom-width" toggle={this.toggle}>
            <Row>
              <Col md={1}>CMS</Col>
              <Col md={5}>
                {this.state.type_error ? (
                  <div className="text-danger"> Please Select Type</div>
                ) : null}
              </Col>
              <Col md={3}>
                <Row>
                  <UncontrolledButtonDropdown className="custom-height-btn">
                    <DropdownToggle caret color="primary">
                      {this.state.style_group
                        ? this.displayStyleGroup()
                        : "Select Group"}
                    </DropdownToggle>
                    <DropdownMenu>
                      {style_groups.length > 0
                        ? style_groups.map((object) => (
                            <DropdownItem
                             onClick={() => this.handleSetGroup(object.icon)}
                            >
                              {object.value}
                            </DropdownItem>
                          ))
                        : ""}
                    </DropdownMenu>
                  </UncontrolledButtonDropdown>
                </Row>
              </Col>
              <Col md={3}>
                <Row>
                  <UncontrolledButtonDropdown className="custom-height-btn">
                    <DropdownToggle caret color="primary">
                      {this.state.type_name
                        ? this.state.type_name
                        : "Select Type"}
                    </DropdownToggle>
                    <DropdownMenu>               
                      {this.props.typesArray.length > 0
                        ? this.props.typesArray.map((object) => (
                          this.state.style_group == object.group
                            ?
                              <DropdownItem
                                onClick={() => this.handleSetType(object)}
                              >
                                {object.name}
                              </DropdownItem>
                            :null
                            ))
                        : ""}
                    </DropdownMenu>
                  </UncontrolledButtonDropdown>
                </Row>
              </Col>
            </Row>
          </ModalHeader>
          <ModalBody>
            {this.props.cmsEnableBackground ? (
              <Container fluid>
                <Row>
                  <Col lg="6">
                    <SketchPicker
                      color={
                        this.props.themeSectionBackgroundColor
                          ? this.props.cmsValue
                          : this.state.theme_color
                      }
                      onChangeComplete={this.handleChangeComplete}
                    />
                  </Col>
                  <Col lg="6">
                    <Card className="main-card mb-3">
                      <CardBody>
                        <CardTitle>PREVIOUSLY USED</CardTitle>
                        <UncontrolledButtonDropdown>
                          <DropdownToggle
                            caret
                            className="mb-2 mr-2"
                            color="primary"
                            onClick={() => this.getColors()}
                          >
                            Select Color
                          </DropdownToggle>
                          <DropdownMenu>
                            <Nav vertical>
                              {this.state.previousColors.map((item) => (
                                <NavItem>
                                  <NavLink
                                    href="#"
                                    style={{ color: item.value }}
                                    onClick={() =>
                                      this.handleChangeComplete({
                                        hex: item.value,
                                      })
                                    }
                                  >
                                    {item.color}
                                  </NavLink>
                                </NavItem>
                              ))}
                            </Nav>
                          </DropdownMenu>
                        </UncontrolledButtonDropdown>
                      </CardBody>
                    </Card>
                  </Col>
                </Row>
              </Container>
            ) : this.props.cmsEnableUploadImage ? (
              <ImageUploader
                withIcon={true}
                withPreview={true}
                buttonText="Choose images"
                onChange={this.onDrop}
                imgExtension={[".jpg", ".gif", ".png", ".gif"]}
                maxFileSize={5242880}
              />
            ) : (
              <Tabs>
                <TabList>
                  <Tab>Main</Tab>
                  {this.state.type_genre == "button" ? <Tab>Link</Tab> : null}
                  <Tab>Style</Tab>
                  {this.props.sectionId.includes("navbar") ? (
                    <Tab>Navbar options</Tab>
                  ) : null}
                  <Tab>Section Background</Tab>
                  {!this.props.enableTextModel ? 
                    <Tab>Background</Tab>
                    :null
                  }
                  <Tab>Placeholder</Tab>
                  <Tab>Rich Text Style</Tab>
                  {this.props.cmsKey.includes("contact") ||
                  this.props.cmsKey.includes("subscription") ? (
                    <Tab>Email Config</Tab>
                  ) : null}
                </TabList>
                <TabPanel>
                  <div>
                    {this.state.type_genre == "bullet" || this.state.type_genre == "list"? 
                      <Container fluid>
                        <Row>
                          <Col lg="6" className="mb-4 mt-4">
                            <Tabs>
                              <TabList>
                                {this.state.no_image_icons || this.state.type_genre == "list" ? null : 
                                  <Tab>Image</Tab> 
                                }
                                 {this.state.no_image_icons || this.state.type_genre == "list" ? null : 
                                  <Tab>Icon</Tab>
                                 }
                                <Tab>Bullet Types</Tab>
                                {this.state.type_genre == "list" ? <Tab>Number Configs</Tab> : null }
                              </TabList>
                              {this.state.no_image_icons || this.state.type_genre == "list"? null : 
                                <TabPanel>
                                  <div className="dropzone-wrapper dropzone-wrapper-lg mb-2">
                                    <Dropzone
                                      onDrop={this.onDrop.bind(this)}
                                      onFileDialogCancel={this.onCancel.bind(this)}
                                    >
                                      {({ getRootProps, getInputProps }) => (
                                        <div {...getRootProps()}>
                                          <input {...getInputProps()} />
                                          <div className="dropzone-content">
                                            <p>
                                              Try dropping some files here, or click to select
                                              files to upload.
                                            </p>
                                          </div>
                                        </div>
                                      )}
                                    </Dropzone>
                                  </div>
                                
                                </TabPanel>
                              }
                              {this.state.no_image_icons || this.state.type_genre == "list" ? null : 
                              <TabPanel>
                                <div className="form-inline" style={{'z-index':'3','position': 'relative'}}>
                                  <Col md={7}>
                                      <input name="fontkey" placeholder="zmdi zmdi-" type="text" id="formGroupExampleInput" value={this.state.pictures} onChange={(e)=>{
                                        this.setState({pictures: e.target.value})
                                      }} />
                                  </Col>
                                  <Col md={2}>
                                    <i className={this.state.pictures} ></i>
                                  </Col>
                                  <Col md={2}>
                                    <UncontrolledButtonDropdown>
                                      <DropdownToggle caret className="" color="primary">
                                      Select
                                      </DropdownToggle>
                                      <DropdownMenu>
                                      {iconOptions.map(item => (
                                        <DropdownItem  
                                        onClick={this.handleIcon.bind(this, item.value)} 
                                        >{item.name}</DropdownItem>
                                        )) 
                                      }
                                      </DropdownMenu>
                                    </UncontrolledButtonDropdown>
                                  </Col>
                                </div>
                              </TabPanel>
                              }
                               <TabPanel>
                                 <Row>
                                 {bullet_styles.map((item, i)=> (
                                    <Col md="3" onClick={()=>{this.setState({bullet_styles: item.name})}}>
                                      <div className="conatct-info" >
                                        <div className="single-contact-info">
                                          <div
                                            className={item.name} style={{overflow:"hidden",  background: "black"}}
                                          >
                                            {this.state.bullet_styles == item.name ? 
                                             <FontAwesomeIcon
                                                icon={faCheck}
                                                size="1x"
                                                color="white"
                                              /> : null }
                                          </div>
                                        </div>
                                      </div>
                                    </Col>
                                 ))}
                                 </Row>
                                </TabPanel>
                              {this.state.type_genre == "list" ? 
                                <TabPanel>
                                  {this.state.use_default_for_start ? null : 
                                    <div className="mb-2 mr-sm-2 mb-sm-0 form-group">
                                        <label for={"start_from"} className="mr-sm-2">
                                          Number starting from
                                        </label>
                                        <input
                                          name="key[]"
                                          value={this.state.start_from}
                                          onChange={(e) => this.setState({start_from: e.target.value})}
                                          placeholder={"Enter starting number, e.g. 1, 10, 20 .."}
                                          id="start_from"
                                          type="number"
                                          className="form-control"
                                        />
                                    </div>
                                  }
                                  <div className="mb-2 mr-sm-2 mb-sm-0 form-group mt-2">
                                      <Label for="use_default_for_start">Use default configuration</Label>
                                      <Input
                                        type="checkbox"
                                        onChange={(e) => {
                                          this.setState({
                                            use_default_for_start: !this.state.use_default_for_start,
                                          });
                                        }}
                                        checked={this.state.use_default_for_start}
                                        name="use_default_for_start"
                                        id="use_default_for_start"
                                        style={{ height: 20 }}
                                      />
                                  </div>
                                </TabPanel>
                              : null}
                            </Tabs>
                          </Col>
                          <Col lg="6">
                          <div className="pb-4 pt-4">
                            <Button
                              className="pull-right"
                              color="primary"
                              onClick={this.addIterativeInput.bind(this)}
                            >
                              Add More
                            </Button>
                          </div>
                          {this.state.iterativeInputs.map((item, i)=>(
                            <div className="mb-2 mr-sm-2 mb-sm-0 form-group">
                                <label for={"examplekey" + i} className="mr-sm-2">
                                  Text {(i+1).toString()}
                                </label>
                                <RichText flag='true' index={(i)} value={tempArrayRichText[i]} />
                            </div>
                          ))}
                          </Col>
                        </Row>
                        {this.state.type_genre == "list" ? null : 
                          <Row>
                          {this.state.pictures ? 
                            <Col md="2">
                              <div
                                className="pull-right mr-2 cloneIcon text-danger mt-2"
                               
                                onClick={() => {
                                  this.setState({pictures: ""})
                                }}
                              >
                                <FontAwesomeIcon icon={faTimes} size="1x" />
                              </div>
                           
                              <img src={this.state.pictures} />
                              {/* <i className={this.state.icon}></i> */}
                            </Col>
                          : this.state.icon ? <Col md="2">
                          <div
                            className="pull-right mr-2 cloneIcon text-danger mt-2"
                           
                            onClick={() => {
                              this.setState({icon: ""})
                            }}
                          >
                            <FontAwesomeIcon icon={faTimes} size="1x" />
                          </div>
                       
                          <i className={this.state.icon} style={{fontSize: 40}}/>
                        </Col> : null
                        }
                      </Row>
                        }
                        {this.state.type_genre == "list" ? null : 
                          <Col md="4">
                            <FormGroup >
                              <Label for="no_image_icons">No Image/ Icon?</Label>
                              <Input
                                type="checkbox"
                                onChange={(e) => {
                                  this.setState({
                                    no_image_icons: !this.state.no_image_icons,
                                  });
                                }}
                                checked={this.state.no_image_icons}
                                name="no_image_icons"
                                id="no_image_icons"
                                style={{ height: 20 }}
                              />
                            </FormGroup>
                          </Col> 
                        }
                    </Container>
                    : 

                    this.state.type_genre != "bullet" ?
                    <div className="richtext-toolbar">
                        <RichText/>
                    </div>
                     : null 
                    }
                  </div>
                </TabPanel>
                <TabPanel>
                  <form>
                    <FormGroup className="ml-2 pt-4">
                      <Label for="style_header"> Header Name</Label>
                      <Input
                        type="text"
                        onChange={(e) => {
                          this.setState({
                            type_name: e.target.value
                          });
                        }}
                        value={this.state.type_name?this.state.type_name:null}
                        name="style_header"
                        id="style_header"
                      />
                    </FormGroup>
                    <div onClick={this.openGroupDropdown} className="rdw-block-wrapper" aria-label="rdw-block-control">
                      <div className="rdw-dropdown-wrapper rdw-block-dropdown" aria-label="rdw-dropdown">
                        <div className="rdw-dropdown-selectedtext" title="Placeholders">
                          <span>
                            {this.state.style_group
                            ? this.displayStyleGroup()
                            : "Select Group"}
                          </span> 
                          <div className={`rdw-dropdown-caretto${this.state.open? "close": "open"}`}></div>
                        </div>
                        <ul className={`rdw-dropdown-optionwrapper ${this.state.open? "": "placeholder-ul"}`}>
                          {style_groups.map(item => (
                          <div>
                            <li 
                              onClick={(e) => {this.addGroup(item.icon)}}
                              className="rdw-dropdownoption-default placeholder-li"
                            >
                              {item.value}
                            </li>
                          </div>
                          ))}
                        </ul>
                      </div>
                    </div>
                    <div className="pb-4 pt-4">
                      <Button
                        className="pull-right"
                        color="primary"
                        onClick={this.addClick.bind(this)}
                      >
                        Add More
                      </Button>
                    </div>
                    <div className="styleSheetDiv mt-4">{this.createUI()}</div>
                  </form>
                  <FormGroup className="ml-2 pt-4">
                    <Label for="is_global_text">Global Text?</Label>
                    <Input
                      type="checkbox"
                      onChange={(e) => {
                        this.setState({
                          is_global_text: !this.state.is_global_text,
                        });
                      }}
                      checked={this.state.is_global_text}
                      name="is_global_text"
                      id="is_global_text"
                      style={{ height: 20,width: 100}}
                    />
                  </FormGroup>
                </TabPanel>
                {this.props.sectionId.includes("navbar") ? (
                  <TabPanel>
                    <Container fluid>
                      <Row>
                        <Col lg="12">
                          <Card className="main-card mb-3">
                            <CardBody>
                              <Row>
                                <Label for="is_sticker">Is Sticker?</Label>
                                <Input
                                  type="checkbox"
                                  onChange={(e) => {
                                    this.setState({
                                      is_sticker: !this.state.is_sticker,
                                    });
                                  }}
                                  checked={this.state.is_sticker}
                                  name="is_sticker"
                                  id="is_sticker"
                                  style={{ height: 20 }}
                                />
                              </Row>
                              <CardTitle>Sections</CardTitle>
                              <Row>
                                {/* {JSON.stringify(this.props.cmsSection)} */}
                                {this.props.cmsSection.map((item, i) => (
                                  <Col lg="12" key={item.id}>
                                    <Row>
                                      <Col lg="6">
                                        <Label for={item.key}>
                                          {this.props.cmsArray[item.key]
                                            ? this.props.cmsArray[item.key][
                                                item.key
                                              ].replace(/<[^>]*>?/gm, "")
                                            : null}
                                        </Label>
                                      </Col>
                                      <Col lg="6">
                                        <Input
                                          type="checkbox"
                                          onChange={async (e) => {
                                            var cmsSection = this.props
                                              .cmsSection;
                                            cmsSection[i]["is_header"] =
                                              item.is_header == 1 ? 0 : 1;
                                            await this.setSectionArray(
                                              cmsSection
                                            );
                                          }}
                                          checked={item.is_header}
                                          name={item.key}
                                          id={item.key}
                                          style={{ height: 20 }}
                                        />
                                      </Col>
                                    </Row>
                                  </Col>
                                ))}
                              </Row>
                            </CardBody>
                          </Card>
                        </Col>
                      </Row>
                    </Container>
                  </TabPanel>
                ) : null}
                <TabPanel>
                  <Container fluid>
                    <Row>
                      <Col lg="6">
                        <SketchPicker
                          color={this.props.sectionBackgroundColor["value"]}
                          onChangeComplete={this.handleChangeBackgroundComplete}
                        />
                      </Col>
                      <Col lg="6">
                        <Card className="main-card mb-3">
                          <CardBody>
                            <CardTitle>PREVIOUSLY USED</CardTitle>
                            <UncontrolledButtonDropdown>
                              <DropdownToggle
                                caret
                                className="mb-2 mr-2"
                                color="primary"
                                onClick={() => this.getColors()}
                              >
                                Select Color
                              </DropdownToggle>
                              <DropdownMenu>
                                <Nav vertical>
                                  {this.state.previousColors.map((item) => (
                                    <NavItem>
                                      <NavLink
                                        href="#"
                                        style={{ color: item.value }}
                                        onClick={() =>
                                          this.handleChangeBackgroundComplete({
                                            hex: item.value,
                                          })
                                        }
                                      >
                                        {item.color}
                                      </NavLink>
                                    </NavItem>
                                  ))}
                                </Nav>
                              </DropdownMenu>
                            </UncontrolledButtonDropdown>
                            <Row>
                              <Label for="tranparent">Tranparent?</Label>
                              <Input
                                type="checkbox"
                                onChange={(e) => {
                                  this.setState({
                                    tranparent: !this.state.tranparent,
                                  });
                                }}
                                checked={this.state.tranparent}
                                name="tranparent"
                                id="tranparent"
                                style={{ height: 20 }}
                              />
                            </Row>
                          </CardBody>
                        </Card>
                      </Col>
                    </Row>
                  </Container>
                </TabPanel>
                {!this.props.enableTextModel ? 
                  <TabPanel>
                    <Container fluid>
                      <Row>
                        <Col lg="6">
                          <SketchPicker
                            color={this.state.theme_color}
                            onChangeComplete={this.handleChangeComplete}
                          />
                        </Col>
                        <Col lg="6">
                          <Card className="main-card mb-3">
                            <CardBody>
                              <CardTitle>PREVIOUSLY USED</CardTitle>
                              <UncontrolledButtonDropdown>
                                <DropdownToggle
                                  caret
                                  className="mb-2 mr-2"
                                  color="primary"
                                  onClick={() => this.getColors()}
                                >
                                  Select Color
                                </DropdownToggle>
                                <DropdownMenu>
                                  <Nav vertical>
                                    {this.state.previousColors.map((item) => (
                                      <NavItem>
                                        <NavLink
                                          href="#"
                                          style={{ color: item.value }}
                                          onClick={() =>
                                            this.handleChangeComplete({
                                              hex: item.value,
                                            })
                                          }
                                        >
                                          {item.color}
                                        </NavLink>
                                      </NavItem>
                                    ))}
                                  </Nav>
                                </DropdownMenu>
                              </UncontrolledButtonDropdown>
                               <Row>
                              <Label for="individual">Tranparent?</Label>
                              <Input
                                type="checkbox"
                                onChange={(e) => {
                                  this.setState({
                                    tranparent_individual: !this.state.tranparent_individual,
                                  });
                                }}
                                checked={this.state.tranparent_individual}
                                name="tranparent_individual"
                                id="tranparent_individual"
                                style={{ height: 20 }}
                              />
                            </Row>
                            </CardBody>
                          </Card>
                          <FormGroup>
                            <Label for="is_global">Global Color?</Label>
                            <Input
                              type="checkbox"
                              onChange={(e) => {
                                this.setState({
                                  is_global: !this.state.is_global,
                                });
                              }}
                              checked={this.state.is_global}
                              name="is_global"
                              id="is_global"
                              style={{ height: 20 }}
                            />
                          </FormGroup>
                        </Col>
                      </Row>
                    </Container>
                  </TabPanel>
                 :null
                }
                <TabPanel>
                  <form>
                   <div className="form-inline">
                        <Col md={4}>
                          <div className="mb-2 mr-sm-2 mb-sm-0 form-group">
                            <label className="mr-sm-2">
                              Name
                            </label>
                            <input
                              name="name"
                              id={this.state.id}
                              placeholder="name"
                              type="text"
                              value={this.state.name}
                              onChange={this.handleChangeName1.bind(this)}
                              className="form-control"
                            />
                          </div>
                        </Col>
                        <Col md={4}>
                          <div className="mb-2 mr-sm-2 mb-sm-0 form-group">
                            <label  className="mr-sm-2">
                              Value
                            </label>
                            <input
                              name="value"
                              id={this.state.id}
                              placeholder="value"
                              type="text"
                              value={this.state.valueP}
                              onChange={this.handleChangeValue1.bind(this)}
                              className="form-control"
                             />
                          </div>
                        </Col>
                        <Col md={1}>
                         <div className="mt-4">
                            <FontAwesomeIcon
                              icon={faCheckCircle}
                              size="1x"
                              color="green"
                              background="MistyRose"
                              onClick={this.addClickCreate.bind(this)}
                            />
                            </div> 
                        </Col>
                        <Col md={1} className="mt-4">
                          <FontAwesomeIcon
                            icon={faTrash}
                            size="1x"
                            color="red"
                            onClick={this.addClickDelete.bind(this)}
                          />
                        </Col>
                      </div>
                    <div className="styleSheetDiv mt-4">{this.createUI2()}</div>
                  </form>
                </TabPanel>
                <TabPanel>
                  <StyleSheet getStyleSheet={this.getStyleSheet}/>
                </TabPanel>
                <TabPanel>
                  <div>
                    {this.props.cmsKey.includes("contact") ? (
                      <div className="mb-2 mr-sm-2 mb-sm-0 form-group mt-2">
                        <label for={"contact-us"} className="mr-sm-2">
                          CONTACT US
                        </label>
                        <input
                          name="contact-us"
                          value={this.props.contact_us_email}
                          onChange={async (e) => {
                            await this.props.setContactUsEmail(e.target.value);
                          }}
                          id={"contact-us"}
                          placeholder="Enter your contact us default email here"
                          type="email"
                          className="form-control"
                        />
                      </div>
                    ) : null}
                    {this.props.cmsKey.includes("subscription") ? (
                      <div className="mb-2 mr-sm-2 mb-sm-0 form-group mt-2">
                        <label for={"subscription"} className="mr-sm-2">
                          SUBSCRIPTION
                        </label>
                        <input
                          name="subscription"
                          value={this.props.subscription_email}
                          onChange={async (e) => {
                            await this.props.setSubscriptionEmail(
                              e.target.value
                            );
                          }}
                          id={"subscription"}
                          placeholder="Enter your subscription default email here"
                          type="email"
                          className="form-control"
                        />
                      </div>
                    ) : null}
                  </div>
                </TabPanel>
              </Tabs>
            )}
          </ModalBody>
          <ModalFooter>
          {
            this.state.loader
            ?
            <div className="mr-2" style={{height:10,marginRight:10}}>
              <LoadingOverlay tag="div" active={true}
                styles={{
                  overlay: (base) => ({
                    ...base,
                    background: "#fff",
                    opacity: 0.5,
                    marginRight:10
                  }),
                }}
                spinner={<Loader active type={'ball-clip-rotate-multiple'} />}>
              </LoadingOverlay>
            </div>
            :
            <div>
              <Button
                color="link"
                onClick={
                  this.props.cmsEnableBackground
                    ? this.toggleBackground
                    : this.toggle
                }
              >
                Cancel
              </Button>
              {this.props.enableRichTextEditor
                ?
                <LoadingOverlay tag="div" active={this.props.enableRichTextEditor}
                  styles={{
                    overlay: (base) => ({
                      ...base,
                      background: "#fff",
                      opacity: 0.5,
                    }),
                  }}
                  spinner={<Loader active type={'ball-clip-rotate-multiple'} />}>
                </LoadingOverlay>
                :
                <span>
                  <Button color="primary" onClick={() =>this.submit(0)}>
                    Save
                  </Button>
                  <Button color="primary ml-2" onClick={() =>this.submit(1)}>
                    Save & Close
                  </Button>
                </span>
              }
            </div>
          }
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}
const mapStateToProps = (state) => ({
  cmsArray: state.ThemeOptions.cmsArray,
  cmsModel: state.ThemeOptions.cmsModel,
  cmsKey: state.ThemeOptions.cmsKey,
  cmsValue: state.ThemeOptions.cmsValue,
  themeColor: state.ThemeOptions.themeColor,
  themeBackgroundColor: state.ThemeOptions.themeBackgroundColor,
  editorState: state.ThemeOptions.editorState,
  moduleCmsObject: state.ThemeOptions.moduleCmsObject,
  cmsEnableBackground: state.ThemeOptions.cmsEnableBackground,
  cmsEnable: state.ThemeOptions.cmsEnable,
  cmsEnableUploadImage: state.ThemeOptions.cmsEnableUploadImage,
  cmsModelImage: state.ThemeOptions.cmsModelImage,
  themeSectionBackgroundColor: state.ThemeOptions.themeSectionBackgroundColor,
  sectionBackgroundColor: state.ThemeOptions.sectionBackgroundColor,
  cmsImageArray: state.ThemeOptions.cmsImageArray,
  previousColors: state.ThemeOptions.previousColors,
  cmsVideoArray: state.ThemeOptions.cmsVideoArray,
  cmsSection: state.ThemeOptions.cmsSection,
  backgroundCmsKey: state.ThemeOptions.backgroundCmsKey,
  sectionId: state.ThemeOptions.sectionId,
  contact_us_email: state.ThemeOptions.contact_us_email,
  subscription_email: state.ThemeOptions.subscription_email,
  isGlobal: state.ThemeOptions.isGlobal,
  customStyle: state.ThemeOptions.customStyle,
  placeholderValue: state.ThemeOptions.placeholderValue,
  dynamicPlaceholderValue: state.ThemeOptions.dynamicPlaceholderValue,
  addNewCmsFlag: state.ThemeOptions.addNewCmsFlag,
  cmsKeyInner: state.ThemeOptions.cmsKeyInner,
  typesArray: state.ThemeOptions.typesArray,
  typeId: state.ThemeOptions.typeId,
  fontSize: state.ThemeOptions.fontSize,
  inlineStyle: state.ThemeOptions.inlineStyle,
  showContact: state.ThemeOptions.showContact,
  fontFamily:state.ThemeOptions.fontFamily,
  enableTextClone:state.ThemeOptions.enableTextClone,
  richTextArray: state.ThemeOptions.richTextArray,
  richTextArrayStyle: state.ThemeOptions.richTextArrayStyle,
  richTextArrayStyleSheet: state.ThemeOptions.richTextArrayStyleSheet,
  richTextFontSize: state.ThemeOptions.richTextFontSize,
  richTextFontFamily: state.ThemeOptions.richTextFontFamily,
  enableTextModel:state.ThemeOptions.enableTextModel,
  richTextPositionEnd:state.ThemeOptions.richTextPositionEnd,
  placeholderPosition: state.ThemeOptions.placeholderPosition,
  inlineStyleEnable: state.ThemeOptions.inlineStyleEnable,
  enableRichTextEditor: state.ThemeOptions.enableRichTextEditor,
});
const mapDispatchToProps = (dispatch) => ({
  setCmsArray: (enable) => dispatch(setCmsArray(enable)),
  setCmsModel: (enable) => dispatch(setCmsModel(enable)),
  setCmsKey: (enable) => dispatch(setCmsKey(enable)),
  setCmsValue: (enable) => dispatch(setCmsValue(enable)),
  setThemeColor: (enable) => dispatch(setThemeColor(enable)),
  setThemeBackgroundColor: (enable) =>
    dispatch(setThemeBackgroundColor(enable)),
  setEditorState: (enable) => dispatch(setEditorState(enable)),
  setModuleCmsObject: (enable) => dispatch(setModuleCmsObject(enable)),
  setCmsEnableBackground: (enable) => dispatch(setCmsEnableBackground(enable)),
  setCmsEnable: (enable) => dispatch(setCmsEnable(enable)),
  setCmsEnableUploadImage: (enable) =>
    dispatch(setCmsEnableUploadImage(enable)),
  setCmsModelImage: (enable) => dispatch(setCmsModelImage(enable)),
  setThemeSectionBackgroundColor: (enable) =>
    dispatch(setThemeSectionBackgroundColor(enable)),
  setSectionBackgroundColor: (enable) =>
    dispatch(setSectionBackgroundColor(enable)),
  setCmsImageArray: (enable) => dispatch(setCmsImageArray(enable)),
  setPreviousColorsValue: (enable) => dispatch(setPreviousColorsValue(enable)),
  setCmsVideoArray: (enable) => dispatch(setCmsVideoArray(enable)),
  setCmsSection: (enable) => dispatch(setCmsSection(enable)),
  setBackgroundCmsKey: (enable) => dispatch(setBackgroundCmsKey(enable)),
  setContactUsEmail: (enable) => dispatch(setContactUsEmail(enable)),
  setSubscriptionEmail: (enable) => dispatch(setSubscriptionEmail(enable)),
  setIsGlobal: (enable) => dispatch(setIsGlobal(enable)),
  setCustomStyle: (enable) => dispatch(setCustomStyle(enable)),
  setPlaceholderValue: (enable) => dispatch(setPlaceholderValue(enable)),
  setDynamicPlaceholderValue: (enable) => dispatch(setDynamicPlaceholderValue(enable)),
  setAddNewCmsFlag: (enable) => dispatch(setAddNewCmsFlag(enable)),
  setCmsKeyInner: (enable) => dispatch(setCmsKeyInner(enable)),
  setTypesArray: (enable) => dispatch(setTypesArray(enable)),
  setFontSize: (enable) => dispatch(setFontSize(enable)),
  setInlineStyle: (enable) => dispatch(setInlineStyle(enable)),
  setShowContact: (enable) => dispatch(setShowContact(enable)),
  setFontFamily: (enable) => dispatch(setFontFamily(enable)),
  setEnableTextClone: (enable) => dispatch(setEnableTextClone(enable)),
  setRichTextArray: (enable) => dispatch(setRichTextArray(enable)),
  setRichTextArrayStyle: (enable) => dispatch(setRichTextArrayStyle(enable)),
  setRichTextArrayStyleSheet: (enable) => dispatch(setRichTextArrayStyleSheet(enable)),
  setRichTextFontSize: (enable) => dispatch(setRichTextFontSize(enable)),
  setRichTextFontFamily: (enable) => dispatch(setRichTextFontFamily(enable)),
  setEnableTextModel: (enable) => dispatch(setEnableTextModel(enable)),
  setRichTextPositionEnd: (enable) => dispatch(setRichTextPositionEnd(enable)),
  setPlaceholderPosition: (enable) => dispatch(setPlaceholderPosition(enable)),
  setInlineStyleEnable: (enable) => dispatch(setInlineStyleEnable(enable)),
  setEnableRichTextEditor: (enable) => dispatch(setEnableRichTextEditor(enable)),
});
export default connect(mapStateToProps, mapDispatchToProps)(TextModal);
