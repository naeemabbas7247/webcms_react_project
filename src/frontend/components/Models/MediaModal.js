import React, { Component } from "react";
import "../../../index.scss";
import ApiCall from "../../../admin/Partial/Function/ApiCall";
import { useDispatch } from "react-redux";
import { SketchPicker } from "react-color";
import ImageUploader from "react-images-upload";
import namedColors from "color-name-list";
import {
  setCmsArray,
  setCmsModel,
  setCmsKey,
  setCmsValue,
  setThemeBackgroundColor,
  setThemeColor,
  setEditorState,
  setModuleCmsObject,
  setCmsEnable,
  setCmsEnableBackground,
  setCmsEnableUploadImage,
  setCmsModelImage,
  setThemeSectionBackgroundColor,
  setSectionBackgroundColor,
  setCmsImageArray,
  setPreviousColorsValue,
  setCmsVideoArray,
  setCmsSection,
  setBackgroundCmsKey,
  setContactUsEmail,
  setSubscriptionEmail,
  setIsGlobal,
  setAddNewCmsFlag,

} from "../../../reducers/ThemeOptions";
import { connect } from "react-redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimes, faTrash } from "@fortawesome/free-solid-svg-icons";
import {
  EditorState,
  ContentState,
  convertFromHTML,
  convertFromRaw,
  convertToRaw,
} from "draft-js";
import { Editor } from "react-draft-wysiwyg";
import draftToHtml from "draftjs-to-html";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import Dropzone from "react-dropzone";
import Resizer from "react-image-file-resizer";
import {
  UncontrolledButtonDropdown,
  DropdownToggle,
  Dropdown,
  DropdownMenu,
  DropdownItem,
  Nav,
  NavItem,
  NavLink,
  Container,
  CustomInput,
} from "reactstrap";
import { Card, CardBody, CardTitle } from "reactstrap";
import {
  Form,
  Row,
  Col,
  ListGroup,
  ListGroupItem,
  FormGroup,
  Label,
  Input,
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
} from "reactstrap";
import Cropper from "react-cropper";
import "cropperjs/dist/cropper.css";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import "react-tabs/style/react-tabs.css";
import ReactResumableJs from "../../../ReactResumableJs";
import DemoImg from "../../../assets/utils/images/originals/fence-small.jpg";
let flag = 0;
class MediaModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pictures: [],
      cmsExtra: [{ key: "", value: "", style: "" }],
      inputs: ["input-0"],
      theme_color: "",
      previousColors: [],
      editorState: EditorState.createWithContent(
        ContentState.createFromBlockArray(
          convertFromHTML("this.props.cmsValue")
        )
      ),
      videos: [],
      youtubeLink: null,
      is_global: false,
      contactusemail: "",
      subscriptionemail: "",
      triggerImage: false,
      imgSrc: null,
      image: "",
      cropResult: null,
      dimensions: {
        'banner': { 'height': 600, 'width': 600 },
        'about': { 'height': 700, 'width': 700 }
      },
      current_dimensions: null,
      errorMessage: 'Please Upload large Image.',
      errorMessageFlag: false,
      minSize: 200,
    };
    this.cropImage = this.cropImage.bind(this);
  }
  cropImageDone = () => {
    this.setState({ imgSrc: null });
    this.setState({ image: null });
    this.setState({ cropResult: null });
  }
  addClick = () => {
    var tempObject = { key: "", value: "", style: "" };
    var tempNewCmsExtra = this.state.cmsExtra;
    tempNewCmsExtra.push(tempObject);
    this.setState((prevState) => ({ cmsExtra: tempNewCmsExtra }));
  };
  toInt32 = (bytes) => {
    return (bytes[0] << 24) | (bytes[1] << 16) | (bytes[2] << 8) | bytes[3];
  }
  getDimensions = (data) => {
    return {
      width: this.toInt32(data.slice(16, 20)),
      height: this.toInt32(data.slice(20, 24))
    };
  }
  base64Decode = (data) => {
    var base64Characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';
    var result = [];
    var current = 0;
    for (var i = 0, c; c = data.charAt(i); i++) {
      if (c === '=') {
        if (i !== data.length - 1 && (i !== data.length - 2 || data.charAt(i + 1) !== '=')) {
          throw new SyntaxError('Unexpected padding character.');
        }
        break;
      }
      var index = base64Characters.indexOf(c);
      if (index === -1) {
        throw new SyntaxError('Invalid Base64 character.');
      }
      current = (current << 6) | index;
      if (i % 4 === 3) {
        result.push(current >> 16, (current & 0xff00) >> 8, current & 0xff);
        current = 0;
      }
    }
    if (i % 4 === 1) {
      throw new SyntaxError('Invalid length for a Base64 string.');
    }
    if (i % 4 === 2) {
      result.push(current >> 4);
    } else if (i % 4 === 3) {
      current <<= 6;
      result.push(current >> 16, (current & 0xff00) >> 8);
    }
    return result;
  }
  getPngDimensions = (dataUri) => {
    if (dataUri.substring(0, 22) !== 'data:image/png;base64,') {
      throw new Error('Unsupported data URI format');
    }
    return this.getDimensions(this.base64Decode(dataUri.substr(22, 32)));
  }
  checkImageSize = async (src) => {
    var dimensions = this.getPngDimensions(src);
    console.log(dimensions);
    console.log(this.state.current_dimensions);
    // if(this.state.current_dimensions.height <=dimensions.height &&this.state.current_dimensions.width <=dimensions.width )
    // {
    //   await this.setState({ errorMessageFlag: false });
    //   return true;
    // }
    // else
    // {
    //   await this.setState({ errorMessageFlag: true });
    //   return false;
    // }
    return true;
  }
  cropImage = async () => {
    if (typeof this.cropper.getCroppedCanvas() === "undefined") {
      return;
    }
    this.setState({
      cropResult: this.cropper.getCroppedCanvas().toDataURL(),
    });
    var flag = await this.checkImageSize(this.cropper.getCroppedCanvas().toDataURL());
    if (!flag) {
      return false;
    }
    var array = this.state.pictures;
    array[(array.length) - 1] = this.cropper.getCroppedCanvas().toDataURL();
    this.setState({ pictures: array });
  }
  _crop(e) {
    console.log(e);
    this.cropper.cropper.setCropBoxData({ width: this.state.current_dimensions.width, height: this.state.current_dimensions.height });
    // if (
    //         e.detail.width < 400 ||
    //         e.detail.height < 400
    //     ) {
    //     return false;
    //       console.log('dfdfddf');
    //         // e.refs.cropper.cropper.setData(
    //         //     Object.assign({}, e.detail, {
    //         //         width:
    //         //             e.detail.width < 400
    //         //                 ? 400
    //         //                 : e.detail.width,
    //         //         height:
    //         //             e.detail.height < 400
    //         //                 ? 400
    //         //                 : e.detail.height
    //         //     })
    //         // );
    //     }

    //     return;
  }
  onCancel() {
    this.setState({
      pictures: [],
    });
    this.setState({ videos: [] });
  }
  handleChangeComplete = async (color) => {
    if (this.props.themeSectionBackgroundColor) {
      const { setCmsValue } = this.props;
      setCmsValue(color.hex);
    } else {
      this.setState({ theme_color: color.hex });
    }
  };
  handleChangeBackgroundComplete = async (color) => {
    var tempArray = {
      key: this.props.sectionBackgroundColor["key"],
      value: color.hex,
    };
    const { setSectionBackgroundColor } = this.props;
    setSectionBackgroundColor(tempArray);
  };
  componentWillMount() {
    this.getData();
  }
  setImageDimensions = async () => {
    var dimensions = [];
    dimensions['height'] = 500;
    dimensions['width'] = 500;
    var str = await (this.props.cmsKey);
    Object.keys(this.state.dimensions).map((object) => {
      var check = str.includes(object);
      if (check) {
        dimensions = this.state.dimensions[object];
      }
    })
    this.setState({ current_dimensions: dimensions });
    var message = 'Please Upload large Image . Minimum height is :' + dimensions.height + ' , Minimum width is : ' + dimensions.width;
    this.setState({ errorMessage: message });
    return dimensions;
  }
  onDrop = async (files) => {
    var dimensions = await this.setImageDimensions();
    Resizer.imageFileResizer(
      files[0], // the file from input
      dimensions['width'], // width
      dimensions['height'], // height
      "PNG", // compress format WEBP, JPEG, PNG
      70, // quality
      0, // rotation
      async (uri) => {
        var flag = await this.checkImageSize(uri);
        if (flag) {
          var array = this.state.pictures;
          array.push(uri);
          this.setState({ pictures: array });
          this.setState({ imgSrc: uri });
        }
      },
      "base64",
    );
  };
  SetImage = async (index) => {
    var array = this.state.pictures;
    array.push(this.props.cmsImageArray[index].image);
    this.setState({ pictures: array });
  };
  getData = async () => {
    this.setImageArray();
    this.setColorScheme();
    this.setVideoArray();
  };
  setSectionArray = async (data) => {
    const { setCmsSection } = this.props;
    await setCmsSection(data);
  };
  getColors = async () => {
    let colors = await ApiCall.getAPICall("colors/get");
    const { setCmsImageArray } = this.props;
    await setPreviousColorsValue(colors.ColorsBackup);
    this.setState({ previousColors: colors.ColorsBackup });
  };
  setImageArray = async () => {
    let images = await ApiCall.postAPICall("images");
    const { setCmsImageArray } = this.props;
    await setCmsImageArray(images.images);
  };
  setVideoArray = async () => {
    let videos = await ApiCall.postAPICall("videos");
    const { setCmsVideoArray } = this.props;
    await setCmsVideoArray(videos.videos);
  };
  setCmsModule = async (data) => {
    const { moduleCmsObject, setModuleCmsObject } = this.props;
    await setModuleCmsObject(data);
  };
  setColorScheme = async () => {
    let color = {
      color: this.state.theme_color,
    };
    let background = {
      background: this.state.theme_color,
    };
    const { themeColor, setThemeColor } = this.props;
    await setThemeColor(color);
    const { themeBackgroundColor, setThemeBackgroundColor } = this.props;
    await setThemeBackgroundColor(background);
  };
  setCmsData = async (data) => {
    const { cmsArray, setCmsArray } = this.props;
    await setCmsArray(data);
  };
  toggle = async () => {
    flag = 0;
    var empty = [];
    this.setState({ cmsExtra: empty });
    this.setState({ is_global: this.props.isGlobal });
    const { cmsModel, setCmsModel } = this.props;
    await setCmsModel(false);
    const { setCmsEnableBackground } = this.props;
    await setCmsEnableBackground(false);
    const { setCmsModelImage } = this.props;
    await setCmsModelImage(false);
    const { setCmsEnableUploadImage } = this.props;
    setCmsEnableUploadImage(false);
    const { setAddNewCmsFlag } = this.props;
    setAddNewCmsFlag(false);
    this.dynamicUi();
  };
  toggleBackground = async () => {
    const { cmsEnableBackground, setCmsEnableBackground } = this.props;
    await setCmsEnableBackground(!this.props.cmsEnableBackground);
    await this.toggle();
  };
  toggleImage = async () => {
    flag = 0;
    var empty = [];
    this.setState({ cmsExtra: empty });
    const { cmsModelImage, setCmsModelImage } = this.props;
    await setCmsModelImage(!this.props.cmsModelImage);
    const { cmsEnableUploadImage, setCmsEnableUploadImage } = this.props;
    setCmsEnableUploadImage(!this.props.cmsEnableUploadImage);
  };
  setValue = (e) => {
    let data = draftToHtml(
      convertToRaw(this.props.editorState.getCurrentContent())
    );
    const { setCmsValue } = this.props;
    setCmsValue(data);
  };
  makeGlobal = (setCmsArray) => {
    Object.keys(setCmsArray).map((val, i) => {
      var flag = val.includes("_global");
      if (flag) {
        setCmsArray[val] = this.state.theme_color;
      }
    });
    return setCmsArray;
  };
  deleteImage = async (page_id, images_backups_id, index) => {
    var url = "deleteImage";
    var post = {
      cms_pages_id: page_id,
      images_backups_id: images_backups_id,
    };
    ApiCall.postAPICall(url, post).then(res=>{
      alert('Image Successfully Deleted')
    }).catch(err=>{
      alert('Something went wrong'+err);
    });
    var images = this.props.cmsArray[this.props.cmsKey];
    if (index > -1) {
      images.splice(index, 1);
    }
    var data = this.props.cmsArray;
    data[this.props.cmsKey] = images;
    // console.log('Your data is in Media Model'+JSON.stringify(data));
    await this.setCmsData(data);
    this.setState({ triggerImage: !this.state.triggerImage });
  };
  handleMedia = async () => {
    await this.getColors();
    let tempCmsStyle = {};
    let setCmsArray = this.props.cmsArray;
    let tempCmsExtra = this.state.cmsExtra;
    tempCmsExtra.map(function (keyName, keyIndex) {
      tempCmsStyle[keyName.key] = keyName.value;
    });
    let sendDataCms = {};
    let colorCms = {};
    if (!this.state.is_global) {
      let globalKeyArray = {};
      globalKeyArray[this.props.backgroundCmsKey] = this.state.theme_color;
      colorCms["global"] = globalKeyArray;
      if (setCmsArray[this.props.backgroundCmsKey]) {
        setCmsArray[this.props.backgroundCmsKey][
          this.props.backgroundCmsKey
        ] = this.state.theme_color;
      }
    } else {
      colorCms["global"] = this.state.theme_color;
      setCmsArray = await this.makeGlobal(setCmsArray);
    }
    const { setIsGlobal } = this.props;
    setIsGlobal(this.state.is_global);
    colorCms["key"] = this.props.sectionBackgroundColor["key"];
    colorCms["value"] = this.props.sectionBackgroundColor["value"];
    sendDataCms["key"] = this.props.cmsKey;
    sendDataCms["value"] = "";
    sendDataCms["innerkey"] = this.props.cmsKey;
    sendDataCms["color"] = colorCms;
    sendDataCms["is_global"] = this.state.is_global;
    sendDataCms["new"] = false;
    sendDataCms["style"] = tempCmsStyle;
    this.setColorScheme();
    ApiCall.postAPICall("setCmsData", sendDataCms).then(res => {

    }).catch(err => {

    });

    if (setCmsArray[this.props.cmsKey + "style"]) {
      setCmsArray[this.props.cmsKey + "style"][
        this.props.cmsKey + "style"
      ] = tempCmsStyle;
    }
    var value =
      this.state.pictures.length > 0 ? this.state.pictures : this.state.videos;
    if (this.state.youtubeLink) {
      value.push(this.state.youtubeLink);
    }
    var data = {
      key: this.props.cmsKey,
      value: value,
      style: this.state.cmsExtra,
      flag: 1,
      is_video: this.state.pictures.length > 0 ? false : true,
    };
    if (this.state.youtubeLink) {
      data.is_youtube = true;
    }
    if (
      this.state.pictures.length == 0 &&
      this.state.videos.length == 0 &&
      this.state.youtubeLink == null
    ) {
      data = {
        key: this.props.cmsKey,
        style: this.state.cmsExtra,
        style_change: true,
      };
    }
    var url = "setCmsImage";
    ApiCall.postAPICall(url, data).then(res => {
      //do
    }).catch(err => {
      //error
    });
    ApiCall.postAPICall("media-key", {
      key: this.props.cmsKey,
    }).then(res => {
      setCmsArray[this.props.cmsKey] = res.media;
    }).catch(err => {

    });

    setCmsArray[
      this.props.sectionBackgroundColor["key"]
    ][this.props.sectionBackgroundColor["key"]] = this.props.sectionBackgroundColor["value"];
    setCmsArray[this.props.cmsKey][this.props.cmsKeyInner] = this.props.cmsValue;
    this.setCmsData(setCmsArray);
    this.setState({
      pictures: [],
      videos: [],
      youtubeLink: null,
    });
    this.toggle();
  };
  setColorBackup = async (hex) => {
    let nearestColorName;
    let nearestColorHex;
    await fetch("https://api.color.pizza/v1/" + hex.split("#")[1])
      .then((res) => res.json())
      .then(
        (result) => {
          // console.log(result);
          nearestColorName = result.colors[0].name;
          nearestColorHex = result.colors[0].hex;
        },
        (error) => {
          console.log(error);
        }
      );
    var color = {
      color: nearestColorName,
      value: nearestColorHex,
    };
    var response = await ApiCall.postAPICall("colors/store", color);
  };
  handleKey = (e, i) => {
    let cmsExtraTemp = this.state.cmsExtra;
    cmsExtraTemp[i]["key"] = e.target.value;
    this.setState({ cmsExtra: cmsExtraTemp });
  };
  handleValue = (e, i) => {
    let cmsExtraTemp = this.state.cmsExtra;
    cmsExtraTemp[i]["value"] = e.target.value;
    this.setState({ cmsExtra: cmsExtraTemp });
  };
  handleSetStyle = (style, i) => {
    let cmsExtraTemp = this.state.cmsExtra;
    cmsExtraTemp[i]["style"] = style.key;
    cmsExtraTemp[i]["key"] = style.key;
    this.setState({ cmsExtra: cmsExtraTemp });
  };
  createUI = () => {
    return this.state.cmsExtra.map((el, i) => (
      <div key={i} className="form-inline" id={"div" + i}>
        <Col md={3}>
          <UncontrolledButtonDropdown>
            <DropdownToggle caret className="mt-4" color="primary">
              Select
            </DropdownToggle>
            <DropdownMenu>

              {this.props.customStyle.map((object) => (
                <DropdownItem onClick={() => this.handleSetStyle(object, i)}>
                  {object.name}
                </DropdownItem>
              ))}
            </DropdownMenu>
          </UncontrolledButtonDropdown>
        </Col>
        <Col md={3}>
          <div className="mb-2 mr-sm-2 mb-sm-0 form-group">
            <label for={"examplekey" + i} className="mr-sm-2">
              Key
            </label>
            <input
              name="key[]"
              value={this.state.cmsExtra[i]["key"]}
              onChange={(e) => this.handleKey(e, i)}
              id={"examplekey" + i}
              placeholder="key"
              type="text"
              className="form-control"
            />
          </div>
        </Col>
        <Col md={3}>
          <div className="mb-2 mr-sm-2 mb-sm-0 form-group">
            <label for={"examplevalue" + i} className="mr-sm-2">
              Value
            </label>
            <input
              name="value[]"
              value={this.state.cmsExtra[i]["value"]}
              onChange={(e) => this.handleValue(e, i)}
              id={"examplevalue" + i}
              placeholder="value"
              type="text"
              className="form-control"
            />
          </div>
        </Col>
        <Col md={3} className="mt-4">
          <FontAwesomeIcon
            icon={faTrash}
            size="2x"
            color="red"
            onClick={this.removeClick.bind(this, i)}
          />
        </Col>
      </div>
    ));
  };
  removeClick = async (i) => {
    let cmsExtra = [...this.state.cmsExtra];
    cmsExtra.splice(i, 1);
    await this.setState({ cmsExtra: cmsExtra });
  };
  dynamicUi = () => {
    if (this.props.cmsArray[this.props.cmsKey + "style"]) {
      let cmsExtraTemp = [];
      let cmsObject = {};
      Object.keys(this.props.cmsArray[this.props.cmsKey + "style"]).map(
        (val, i) => {
          Object.keys(
            this.props.cmsArray[this.props.cmsKey + "style"][val]
          ).map((index, count) => {
            cmsObject = {
              key: index,
              value: this.props.cmsArray[this.props.cmsKey + "style"][val][
                index
              ],
            };
            cmsExtraTemp[count] = cmsObject;
          });
        }
      );
      this.setState({ cmsExtra: cmsExtraTemp });
    }
  };
  renderMedia = (item, i) => {
    return (
      <Col md="2" className="mb-2" key={item.id}>
        <div
          className="pull-right mr-2 cloneIcon text-danger mt-2"
          style={{
            display: this.props.cmsEnable ? "block" : "none",
          }}
          onClick={() => {
            this.deleteImage(item.cms_pages_id, item.images_backups_id, i);
          }}
        >
          <FontAwesomeIcon icon={faTimes} size="2x" />
        </div>
        <img src={item.images_backups.image.replace('/public', '')} />
      </Col>
    );
  };
  render() {
    if (this.props.cmsArray[this.props.cmsKey + "style"] && flag == 0) {
      if (this.props.cmsModel || this.props.cmsModelImage) {
        flag++;
        this.dynamicUi();
      }
    }
    return (
      <div style={{ "--global-color-var": this.state.theme_color }}>
        <Modal
          isOpen={this.props.cmsModelImage}
          fade={false}
          toggle={this.toggleImage}
          className={this.props.className}
          size="lg"
        >
          <ModalHeader toggle={this.toggleImage}>Edit Media</ModalHeader>
          <ModalBody>
            <Tabs>
              <TabList>
                <Tab>Gallery</Tab>
                <Tab>Upload</Tab>
                <Tab>Styles</Tab>
                <Tab>Section Background</Tab>
                <Tab>Background</Tab>
              </TabList>
              <TabPanel>
                {/* Media Gallery */}
                <Tabs>
                  <TabList>
                    <Tab>Images</Tab>
                    <Tab>Videos</Tab>
                  </TabList>
                  {/* Image Gallery */}
                  <TabPanel>
                    <Col md="12">
                      <div className="mb-2">
                          <Col md="12">
                        <Row>
                          {/* {this.props.cmsImageArray.length > 0
                            ? this.props.cmsImageArray.map((item, i) => (
                                <Col md="3">
                                  <img
                                    onClick={() => this.SetImage(i)}
                                    src={item.image.replace('/public','')}
                                  />
                                </Col>
                              ))
                            : <p>No Images in gallerys</p>} */}



                          {this.props.cmsKey && this.props.cmsArray && this.props.cmsModelImage && this.props.cmsArray[this.props.cmsKey] &&
                            this.props.cmsArray[this.props.cmsKey].length > 0 ? (
                            <div className="mb-2">
                              {this.props.cmsArray[this.props.cmsKey].length >
                                0 ? <p><b>Images in gallery</b></p>:null
                              }
                              <Row>
                                {this.props.cmsArray[
                                  this.props.cmsKey
                                ].map((item, i) =>
                                  item.images_backups &&
                                    item.images_backups.is_video == 0
                                    ? <Col md="3">
                                    <img
                                      onClick={() => this.SetImage(i)}
                                      src={item.images_backups.image.replace('/public', '')}
                                    />
                                  </Col>
                                    : null
                                )
                                }
                              </Row>
                            </div>
                          ) :  
                          <p className="ml-2">No images in gallery</p>
                          }
                   
                        </Row>
                        </Col>
                      </div>
                    </Col>


                    
                    <Col md="12">
                      <div className="mb-2">
                        <Col md="12">
                          {this.props.cmsKey && this.props.cmsArray && this.props.cmsModelImage && this.props.cmsArray[this.props.cmsKey] &&
                            this.props.cmsArray[this.props.cmsKey].length > 0 ? (
                            <div className="mb-2">
                              {this.props.cmsArray[this.props.cmsKey].length >
                                0 ? (
                                <p color>Added Images</p>
                              ) : null}
                              <Row>
                                {this.props.cmsArray[
                                  this.props.cmsKey
                                ].map((item, i) =>
                                  item.images_backups &&
                                    item.images_backups.is_video == 0
                                    ? this.renderMedia(item, i)
                                    : null
                                )}
                              </Row>
                            </div>
                          ) : null}
                        </Col>
                        <Col md="12">
                          <div className="mb-2">
                            {this.state.pictures.length > 0 ? (
                              <p color>Selected Images</p>
                            ) : null}
                            <Row>
                              {this.state.pictures.map((item, i) => (
                                <Col md="2">
                                  <div
                                    className="pull-right mr-2 cloneIcon text-danger mt-2"
                                    style={{
                                      display: this.props.cmsEnable
                                        ? "block"
                                        : "none",
                                    }}
                                    onClick={() => {
                                      var pictures = this.state.pictures;
                                      pictures.splice(i, 1);
                                      this.setState({ pictures });
                                    }}
                                  >
                                    <FontAwesomeIcon icon={faTimes} size="2x" />
                                  </div>
                                  <img src={item} />
                                </Col>
                              ))}
                            </Row>
                          </div>
                        </Col>
                      </div>
                    </Col>
                  </TabPanel>
                  {/* Video Gallery */}
                  <TabPanel>
                    <Col md="12">
                      <div className="mb-2">
                        <Col>
                          {this.props.cmsVideoArray.length > 0
                            ? this.props.cmsVideoArray.map((item, i) => (
                              <Row md="4">
                                <a
                                  onClick={() => {
                                    var videos = this.state.videos;
                                    videos.push(item.image);
                                    this.setState({ videos: videos });
                                  }}
                                >
                                  {item.image.includes("youtube")
                                    ? item.image
                                    : item.image
                                      .split("/video")[1]
                                      .substring(0, 30)}
                                </a>
                              </Row>
                            ))
                            : null}
                          {this.state.videos.length > 0
                            ? this.state.videos.map((item, i) => (
                              <li className="thumbnail">
                                <label id={"media_"}>
                                  <label className="video">
                                    {item.substring(0, 30)}
                                  </label>
                                </label>
                                <a
                                  onClick={() => {
                                    var array = this.state.videos;
                                    const index = i;
                                    if (index > -1) {
                                      array.splice(index, 1);
                                      this.setState({ videos: array });
                                    }
                                  }}
                                >
                                  [X]
                                  </a>
                              </li>
                            ))
                            : null}
                        </Col>
                      </div>
                    </Col>
                  </TabPanel>
                </Tabs>
              </TabPanel>
              <TabPanel>
                {/* Upload Media */}
                <Tabs>
                  <TabList>
                    <Tab>Images</Tab>
                    <Tab>Videos</Tab>
                    <Tab>Youtube</Tab>
                  </TabList>
                  {/* Upload Images */}
                  <TabPanel>
                    {this.state.errorMessageFlag ? <span className="text-danger">{this.state.errorMessage}</span> : null}
                    <div className="dropzone-wrapper dropzone-wrapper-lg mb-2">
                      {!this.state.imgSrc
                        ?
                        <Dropzone
                          onDrop={this.onDrop.bind(this)}
                          onFileDialogCancel={this.onCancel.bind(this)}
                        >
                          {({ getRootProps, getInputProps }) => (
                            <div {...getRootProps()}>
                              <input {...getInputProps()} />
                              <div className="dropzone-content">
                                <p>
                                  Try dropping some files here, or click to select
                                  files to upload.
                              </p>
                              </div>
                            </div>
                          )}
                        </Dropzone>
                        :
                        <div>
                          <Cropper
                            ref={(cropper) => {
                              this.cropper = cropper;
                            }}
                            src={this.state.imgSrc} style={{ height: 400, width: "100%" }}
                            guides={false} crop={this._crop.bind(this)} />
                          <div className="divider" />
                          <div className="text-center">
                            <div className="text-center">
                              <Button color="primary" onClick={this.cropImage}>
                                Crop Selection
                              </Button>
                            </div>
                            {this.state.cropResult ? (
                              <div>
                                <div className="divider" />
                                <div>
                                  <h6>Cropped Result</h6>
                                </div>
                                <img className="after-img rounded" src={this.state.cropResult} alt="" />
                                {!this.state.errorMessageFlag ?
                                  <div className="text-center mt-2">
                                    <Button color="primary" onClick={this.cropImageDone}>
                                      Done Crop
                                  </Button>
                                  </div>
                                  : null}
                              </div>
                            ) : null}
                          </div>
                        </div>
                      }
                    </div>
                    <Col md="12">
                      <div className="mb-2">
                        {this.state.pictures.length > 0 ? (
                          <p color>Selected Images</p>
                        ) : null}
                        <Row>
                          {this.state.pictures.map((item, i) => (
                            <Col md="2">
                              <div
                                className="pull-right mr-2 cloneIcon text-danger mt-2"
                                style={{
                                  display: this.props.cmsEnable
                                    ? "block"
                                    : "none",
                                }}
                                onClick={() => {
                                  var pictures = this.state.pictures;
                                  pictures.splice(i, 1);
                                  this.setState({ pictures });
                                }}
                              >
                                <FontAwesomeIcon icon={faTimes} size="2x" />
                              </div>
                              <img src={item} />
                            </Col>
                          ))}
                        </Row>
                      </div>
                    </Col>
                  </TabPanel>
                  {/* Upload Videos */}
                  <TabPanel>
                    <ReactResumableJs
                      uploaderID="image-upload"
                      dropTargetID="myDropTarget"
                      filetypes={[
                        "jpg",
                        "JPG",
                        "png",
                        "PNG",
                        "mp4",
                        "MP4",
                        "mkv",
                      ]}
                      maxFileSize={9999000000}
                      fileAccept="*/*"
                      fileAddedMessage="Started!"
                      completedMessage="Complete!"
                      service="http://localhost:8000/api/upload"
                      disableDragAndDrop={false}
                      onFileSuccess={(file, message) => {
                        // console.log(file);
                        // console.log(message);
                        // console.log("hey");
                        var videos = this.state.videos;
                        videos.push(JSON.parse(message));
                        this.setState({
                          videos: videos,
                        });
                        // this.props.setFiles(file, message);
                        this.inputDisable = false;
                      }}
                      onFileAdded={(file, resumable) => {
                        // console.log("file added");
                        this.inputDisable = true;
                        //resumable.upload();
                      }}
                      onFileRemoved={(file) => {
                        this.inputDisable = false;
                        this.forceUpdate();
                        console.log("file removed", file);
                      }}
                      onMaxFileSizeErrorCallback={(file, errorCount) => {
                        console.log("Error! Max file size reached: ", file);
                        console.log("errorCount: ", errorCount);
                      }}
                      fileNameServer="file"
                      tmpDir="http://localhost:8000/tmp/"
                      maxFiles={10}
                      onFileAddedError={(file, errorCount) => {
                        console.log("error file added", file, errorCount);
                      }}
                      maxFilesErrorCallback={(file, errorCount) => {
                        console.log("maxFiles", file, errorCount);
                      }}
                      disableInput={this.inputDisable}
                      startButton={true}
                      pauseButton={false}
                      cancelButton={false}
                      onStartUpload={() => { }}
                      onCancelUpload={() => {
                        this.inputDisable = false;
                      }}
                      onPauseUpload={() => {
                        this.inputDisable = false;
                      }}
                      onResumeUpload={() => {
                        this.inputDisable = true;
                      }}
                    />
                    {this.props.cmsArray && this.props.cmsModelImage
                      ? this.props.cmsKey
                        ? this.props.cmsArray[this.props.cmsKey].length > 0
                          ? this.props.cmsArray[this.props.cmsKey].map((item) =>
                            item.images_backups.is_video == 1 ? (
                              <li className="thumbnail">
                                <label id={"media_"}>
                                  <label className="video">
                                    {item.images_backups.image.substring(
                                      0,
                                      30
                                    )}
                                  </label>
                                </label>
                                <a onClick={() => { }}>[X]</a>
                              </li>
                            ) : null
                          )
                          : null
                        : null
                      : null}
                    {this.state.videos.length > 0
                      ? this.state.videos.map((item, i) => (
                        <li className="thumbnail">
                          <label id={"media_"}>
                            <label className="video">
                              {item.substring(0, 30)}
                            </label>
                          </label>
                          <a
                            onClick={() => {
                              var array = this.state.videos;
                              const index = i;
                              if (index > -1) {
                                array.splice(index, 1);
                                this.setState({ videos: array });
                              }
                            }}
                          >
                            [X]
                            </a>
                        </li>
                      ))
                      : null}
                  </TabPanel>
                  {/* Youtube link */}
                  <TabPanel>
                    <Col md={12}>
                      <div className="mb-2 mr-sm-2 mb-sm-0 form-group">
                        <label for={"youtube-link"} className="mr-sm-2">
                          Youtube Link
                        </label>
                        <input
                          name="youtube-link"
                          value={this.state.youtubeLink}
                          onChange={(e) =>
                            this.setState({ youtubeLink: e.target.value })
                          }
                          id={"youtube-link"}
                          placeholder="Paste your youtube link here"
                          type="text"
                          className="form-control"
                        />
                      </div>
                    </Col>
                  </TabPanel>
                </Tabs>
              </TabPanel>
              <TabPanel>
                <Container fluid>
                  <Row>
                    <form onSubmit={this.handleSubmit}>
                      <div className="pb-3 pt-3">
                        <Button
                          className="pull-right"
                          color="primary"
                          onClick={this.addClick.bind(this)}
                        >
                          Add More
                        </Button>
                      </div>
                      <div className="styleSheetDiv">{this.createUI()}</div>
                    </form>
                  </Row>
                </Container>
              </TabPanel>
              <TabPanel>
                <Container fluid>
                  <Row>
                    <Col lg="6">
                      <SketchPicker
                        color={this.props.sectionBackgroundColor["value"]}
                        onChangeComplete={this.handleChangeBackgroundComplete}
                      />
                    </Col>

                    <Col lg="6">
                      <Card className="main-card mb-3">
                        <CardBody>
                          <CardTitle>PREVIOUSLY USED</CardTitle>
                          <UncontrolledButtonDropdown>
                            <DropdownToggle
                              caret
                              className="mb-2 mr-2"
                              color="primary"
                              onClick={() => this.getColors()}
                            >
                              Select Color
                            </DropdownToggle>
                            <DropdownMenu>
                              <Nav vertical>
                                {this.state.previousColors.map((item) => (
                                  <NavItem>
                                    <NavLink
                                      href="#"
                                      style={{ color: item.value }}
                                      onClick={() =>
                                        this.handleChangeBackgroundComplete({
                                          hex: item.value,
                                        })
                                      }
                                    >
                                      {item.color}
                                    </NavLink>
                                  </NavItem>
                                ))}
                              </Nav>
                            </DropdownMenu>
                          </UncontrolledButtonDropdown>
                        </CardBody>
                      </Card>
                    </Col>
                  </Row>
                </Container>
              </TabPanel>
              <TabPanel>
                <Container fluid>
                  <Row>
                    <Col lg="6">
                      <SketchPicker
                        color={this.state.theme_color}
                        onChangeComplete={this.handleChangeComplete}
                      />
                    </Col>

                    <Col lg="6">
                      <Card className="main-card mb-3">
                        <CardBody>
                          <CardTitle>PREVIOUSLY USED</CardTitle>
                          <UncontrolledButtonDropdown>
                            <DropdownToggle
                              caret
                              className="mb-2 mr-2"
                              color="primary"
                              onClick={() => this.getColors()}
                            >
                              Select Color
                            </DropdownToggle>
                            <DropdownMenu>
                              <Nav vertical>
                                {this.state.previousColors.map((item) => (
                                  <NavItem>
                                    <NavLink
                                      href="#"
                                      style={{ color: item.value }}
                                      onClick={() =>
                                        this.handleChangeComplete({
                                          hex: item.value,
                                        })
                                      }
                                    >
                                      {item.color}
                                    </NavLink>
                                  </NavItem>
                                ))}
                              </Nav>
                            </DropdownMenu>
                          </UncontrolledButtonDropdown>
                        </CardBody>
                      </Card>
                    </Col>
                  </Row>
                </Container>
              </TabPanel>
            </Tabs>
          </ModalBody>
          <ModalFooter>
            <Button color="link" onClick={this.toggleImage}>
              Cancel
            </Button>
            <Button color="primary" onClick={this.handleMedia}>
              Save
            </Button>{" "}
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}
const mapStateToProps = (state) => ({
  cmsArray: state.ThemeOptions.cmsArray,
  cmsModel: state.ThemeOptions.cmsModel,
  cmsKey: state.ThemeOptions.cmsKey,
  cmsValue: state.ThemeOptions.cmsValue,
  themeColor: state.ThemeOptions.themeColor,
  themeBackgroundColor: state.ThemeOptions.themeBackgroundColor,
  editorState: state.ThemeOptions.editorState,
  moduleCmsObject: state.ThemeOptions.moduleCmsObject,
  cmsEnableBackground: state.ThemeOptions.cmsEnableBackground,
  cmsEnable: state.ThemeOptions.cmsEnable,
  cmsEnableUploadImage: state.ThemeOptions.cmsEnableUploadImage,
  cmsModelImage: state.ThemeOptions.cmsModelImage,
  themeSectionBackgroundColor: state.ThemeOptions.themeSectionBackgroundColor,
  sectionBackgroundColor: state.ThemeOptions.sectionBackgroundColor,
  cmsImageArray: state.ThemeOptions.cmsImageArray,
  previousColors: state.ThemeOptions.previousColors,
  cmsVideoArray: state.ThemeOptions.cmsVideoArray,
  cmsSection: state.ThemeOptions.cmsSection,
  backgroundCmsKey: state.ThemeOptions.backgroundCmsKey,
  sectionId: state.ThemeOptions.sectionId,
  contact_us_email: state.ThemeOptions.contact_us_email,
  subscription_email: state.ThemeOptions.subscription_email,
  isGlobal: state.ThemeOptions.isGlobal,
  customStyle: state.ThemeOptions.customStyle,
});
const mapDispatchToProps = (dispatch) => ({
  setCmsArray: (enable) => dispatch(setCmsArray(enable)),
  setCmsModel: (enable) => dispatch(setCmsModel(enable)),
  setCmsKey: (enable) => dispatch(setCmsKey(enable)),
  setCmsValue: (enable) => dispatch(setCmsValue(enable)),
  setThemeColor: (enable) => dispatch(setThemeColor(enable)),
  setThemeBackgroundColor: (enable) =>
    dispatch(setThemeBackgroundColor(enable)),
  setEditorState: (enable) => dispatch(setEditorState(enable)),
  setModuleCmsObject: (enable) => dispatch(setModuleCmsObject(enable)),
  setCmsEnableBackground: (enable) => dispatch(setCmsEnableBackground(enable)),
  setCmsEnable: (enable) => dispatch(setCmsEnable(enable)),
  setCmsEnableUploadImage: (enable) =>
    dispatch(setCmsEnableUploadImage(enable)),
  setCmsModelImage: (enable) => dispatch(setCmsModelImage(enable)),
  setThemeSectionBackgroundColor: (enable) =>
    dispatch(setThemeSectionBackgroundColor(enable)),
  setSectionBackgroundColor: (enable) =>
    dispatch(setSectionBackgroundColor(enable)),
  setCmsImageArray: (enable) => dispatch(setCmsImageArray(enable)),
  setPreviousColorsValue: (enable) => dispatch(setPreviousColorsValue(enable)),
  setCmsVideoArray: (enable) => dispatch(setCmsVideoArray(enable)),
  setCmsSection: (enable) => dispatch(setCmsSection(enable)),
  setBackgroundCmsKey: (enable) => dispatch(setBackgroundCmsKey(enable)),
  setContactUsEmail: (enable) => dispatch(setContactUsEmail(enable)),
  setSubscriptionEmail: (enable) => dispatch(setSubscriptionEmail(enable)),
  setIsGlobal: (enable) => dispatch(setIsGlobal(enable)),
  setAddNewCmsFlag: (enable) => dispatch(setAddNewCmsFlag(enable)),
});
export default connect(mapStateToProps, mapDispatchToProps)(MediaModal);
// <Screenshot />