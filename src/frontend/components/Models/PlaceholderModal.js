import React, { Component } from "react";
import "../../../index.scss";
import ApiCall from "../../../admin/Partial/Function/ApiCall";
import { useDispatch } from "react-redux";
import {
  setCmsArray,
  setCmsModel,
  setCmsKey,
  setCmsValue,
  setThemeBackgroundColor,
  setThemeColor,
  setEditorState,
  setModuleCmsObject,
  setCmsEnable,
  setCmsEnableBackground,
  setCmsEnableUploadImage,
  setCmsModelImage,
  setPlaceHolderModel,
  setThemeSectionBackgroundColor,
  setSectionBackgroundColor,
  setCmsImageArray,
  setPreviousColorsValue,
  setCmsVideoArray,
  setCmsSection,
  setBackgroundCmsKey,
  setContactUsEmail,
  setSubscriptionEmail,
  setIsGlobal,
  setCustomStyle,
  setShowIcon,
  setLogoIcon,
} from "../../../reducers/ThemeOptions";
import { connect } from "react-redux";
import {
  UncontrolledButtonDropdown,
  DropdownToggle,
  Dropdown,
  DropdownMenu,
  DropdownItem,
  Nav,
  NavItem,
  NavLink,
  Container,
  CustomInput,
} from "reactstrap";
import { Card, CardBody, CardTitle } from "reactstrap";
import {
  Form,
  Row,
  Col,
  ListGroup,
  ListGroupItem,
  FormGroup,
  Label,
  Input,
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
} from "reactstrap";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import "react-tabs/style/react-tabs.css";
class PlaceholderModal extends Component {
  constructor(props) {
    super(props);
  }
  handleKey = (e) => {
    let value = e.target.value;
    const { cmsValue, setCmsValue } = this.props;
    setCmsValue(value);
  };
  togglePlaceHolderModel=(e)=>
  {
    const { placeHolderModel, setPlaceHolderModel } = this.props;
    setPlaceHolderModel(!this.props.placeHolderModel);
  }
  handlePlaceHolderModel=async(e)=>
  {
    var data = {
      key: this.props.cmsKey,
      value: this.props.cmsValue,
      sectionId:this.props.sectionId,
    };
    var url="setCmsData/placeholder";
    if(typeof(this.props.cmsArray[this.props.cmsKey]) != 'undefined')
    {
      var cmsArray=this.props.cmsArray;
      cmsArray[this.props.cmsKey][this.props.cmsKey]=this.props.cmsValue;
      await this.setCmsData(cmsArray);
    }
    await ApiCall.postAPICall(url, data);
    this.togglePlaceHolderModel();
  }
  setCmsData = async (data) => {
    const { cmsArray, setCmsArray } = this.props;
    await setCmsArray(data);
  };
  render() {
    return (
      <div>
       <Modal
          isOpen={this.props.placeHolderModel}
          fade={false}
        >
          <ModalHeader>PlaceHolder</ModalHeader>
            <ModalBody>
              <Col md="12">
                <div className="mb-2">
                  <Row>
                    <Col md="4">
                      <input
                        name="key[]"
                        value={this.props.cmsValue}
                        onChange={(e) => this.handleKey(e)}
                        placeholder="key"
                        type="text"
                        className="form-control"
                      />
                    </Col>      
                  </Row>
                </div>
              </Col>
              </ModalBody>
              <ModalFooter>
                <Button color="link" onClick={this.togglePlaceHolderModel}>
                  Cancel
                </Button>
                <Button color="primary" onClick={this.handlePlaceHolderModel}>
                  Save
                </Button>{" "}
              </ModalFooter>
        </Modal>
      </div>
    );
  }
}
const mapStateToProps = (state) => ({
  cmsArray: state.ThemeOptions.cmsArray,
  cmsModel: state.ThemeOptions.cmsModel,
  cmsKey: state.ThemeOptions.cmsKey,
  cmsValue: state.ThemeOptions.cmsValue,
  themeColor: state.ThemeOptions.themeColor,
  themeBackgroundColor: state.ThemeOptions.themeBackgroundColor,
  editorState: state.ThemeOptions.editorState,
  moduleCmsObject: state.ThemeOptions.moduleCmsObject,
  cmsEnableBackground: state.ThemeOptions.cmsEnableBackground,
  cmsEnable: state.ThemeOptions.cmsEnable,
  cmsEnableUploadImage: state.ThemeOptions.cmsEnableUploadImage,
  cmsModelImage: state.ThemeOptions.cmsModelImage,
  placeHolderModel: state.ThemeOptions.placeHolderModel,
  themeSectionBackgroundColor: state.ThemeOptions.themeSectionBackgroundColor,
  sectionBackgroundColor: state.ThemeOptions.sectionBackgroundColor,
  cmsImageArray: state.ThemeOptions.cmsImageArray,
  previousColors: state.ThemeOptions.previousColors,
  cmsVideoArray: state.ThemeOptions.cmsVideoArray,
  cmsSection: state.ThemeOptions.cmsSection,
  backgroundCmsKey: state.ThemeOptions.backgroundCmsKey,
  sectionId: state.ThemeOptions.sectionId,
  contact_us_email: state.ThemeOptions.contact_us_email,
  subscription_email: state.ThemeOptions.subscription_email,
  isGlobal: state.ThemeOptions.isGlobal,
  customStyle: state.ThemeOptions.customStyle,
  showIcon: state.ThemeOptions.showIcon,
  logoIcon: state.ThemeOptions.logoIcon,
});
const mapDispatchToProps = (dispatch) => ({
  setCmsArray: (enable) => dispatch(setCmsArray(enable)),
  setCmsModel: (enable) => dispatch(setCmsModel(enable)),
  setCmsKey: (enable) => dispatch(setCmsKey(enable)),
  setCmsValue: (enable) => dispatch(setCmsValue(enable)),
  setThemeColor: (enable) => dispatch(setThemeColor(enable)),
  setThemeBackgroundColor: (enable) =>
    dispatch(setThemeBackgroundColor(enable)),
  setEditorState: (enable) => dispatch(setEditorState(enable)),
  setModuleCmsObject: (enable) => dispatch(setModuleCmsObject(enable)),
  setCmsEnableBackground: (enable) => dispatch(setCmsEnableBackground(enable)),
  setCmsEnable: (enable) => dispatch(setCmsEnable(enable)),
  setCmsEnableUploadImage: (enable) =>
    dispatch(setCmsEnableUploadImage(enable)),
  setCmsModelImage: (enable) => dispatch(setCmsModelImage(enable)),
  setPlaceHolderModel: (enable) => dispatch(setPlaceHolderModel(enable)),
  setShowIcon: (enable) => dispatch(setShowIcon(enable)),
  setLogoIcon: (enable) => dispatch(setLogoIcon(enable)),
  setThemeSectionBackgroundColor: (enable) =>
    dispatch(setThemeSectionBackgroundColor(enable)),
  setSectionBackgroundColor: (enable) =>
    dispatch(setSectionBackgroundColor(enable)),
  setCmsImageArray: (enable) => dispatch(setCmsImageArray(enable)),
  setPreviousColorsValue: (enable) => dispatch(setPreviousColorsValue(enable)),
  setCmsVideoArray: (enable) => dispatch(setCmsVideoArray(enable)),
  setCmsSection: (enable) => dispatch(setCmsSection(enable)),
  setBackgroundCmsKey: (enable) => dispatch(setBackgroundCmsKey(enable)),
  setContactUsEmail: (enable) => dispatch(setContactUsEmail(enable)),
  setSubscriptionEmail: (enable) => dispatch(setSubscriptionEmail(enable)),
  setIsGlobal: (enable) => dispatch(setIsGlobal(enable)),
  setCustomStyle: (enable) => dispatch(setCustomStyle(enable)),
});
export default connect(mapStateToProps, mapDispatchToProps) (PlaceholderModal);
// <Screenshot />
