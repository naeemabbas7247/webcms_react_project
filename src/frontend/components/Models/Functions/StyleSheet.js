import React, { Component } from 'react';
import {
  setInlineStyle,
  setInlineStyleEnable,
  setRichTextArrayStyleSheet
} from "../../../../reducers/ThemeOptions";
import { connect } from "react-redux";
import ApiCall from "../../../../admin/Partial/Function/ApiCall";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  Row,
  Col,
  Label,
  Input,
  Button
} from "reactstrap";
import {
  faTrash,
  faCheck,
  faPlus,
} from "@fortawesome/free-solid-svg-icons";
import {
  UncontrolledButtonDropdown,
  DropdownToggle,
  Dropdown,
  DropdownMenu,
  DropdownItem,
  Nav,
  NavItem,
  NavLink,
  Container,
  CustomInput,
} from "reactstrap";
class StyleSheet extends Component {
  constructor(props) {
    super(props);
    this.state = {
      styleSheet: [{ name:null,fontWeight:null,fontSize:null,fontColor:null,fontFamily:null,lineHeight:null, }],
    }
  }
  handleSetStyleSheet=async(key,value,index)=>
  {
    let styleSheetTemp = this.props.richTextArrayStyleSheet;
    styleSheetTemp[index][key] = value;
    this.setState({ styleSheet: styleSheetTemp });
  }
  handlestyle = (e, i) => {
    this.handleSetStyleSheet('name',e.target.value,i)
  };
  handleSetStyle=async(key,value,i)=>
  {
    this.handleSetStyleSheet(key,value,i);
  }
  removeRow = async (i) => {
    let styleSheet = [...this.props.richTextArrayStyleSheet];
    styleSheet.splice(i, 1);
    var data={
      model:'RichtextStyle',
      id:this.props.richTextArrayStyleSheet[i]['id'],
      notAudit:false,

    };
    await ApiCall.postAPICall('delete',data);
    this.props.getStyleSheet();
    await this.setState({ styleSheet: styleSheet });
  };
  saveStyle=async(i)=>
  {
    await ApiCall.postAPICall('richtextstyle',this.props.richTextArrayStyleSheet[i]);
    this.props.getStyleSheet();
  }
  addRow = () => {
    var tempObject = { name:null,fontWeight:null,fontSize:null,fontColor:null,fontFamily:null,lineHeight:null, };
    var tempNewStyleSheet = this.props.richTextArrayStyleSheet;
    tempNewStyleSheet.push(tempObject);
    this.setState((prevState) => ({ styleSheet: tempNewStyleSheet }));
  };
  styleSheetUI = () => {
    return this.props.richTextArrayStyleSheet ?this.props.richTextArrayStyleSheet.map((el, i) => (
      <div key={i} className="card form-inline mt-2" id={"div" + i}>
        <Col md={12}>
          <Row>
            <Col md={10}>
              <div className="mt-2 mr-sm-2 mb-sm-0 form-group">
                <label for={"style" + i} className="mr-sm-2">
                  Name
                </label>
                <input
                  name="style[]"
                  value={this.props.richTextArrayStyleSheet[i]['name']}
                  onChange={(e) => this.handlestyle(e, i)}
                  id={"style" + i}
                  placeholder="style"
                  type="text"
                  className="form-control"
                />
              </div>
            </Col>
          </Row>
        </Col>
        <div className="card-body">
          <Row>
            <Col md={4}>
              <div className="mt-2 mr-sm-2 mb-sm-0 form-group">
                <label for={"fontWeight" + i} className="mr-sm-2">
                  Font Weight
                </label>
                <UncontrolledButtonDropdown>
                  <DropdownToggle caret  color="primary">
                    {this.props.richTextArrayStyleSheet[i]['fontWeight']}
                  </DropdownToggle>
                  <DropdownMenu>
                    <DropdownItem onClick={() => this.handleSetStyle('fontWeight','bold', i)}>
                      BOLD
                    </DropdownItem>
                    <DropdownItem onClick={() => this.handleSetStyle('fontWeight','normal', i)}>
                      NORMAL
                    </DropdownItem>
                    <DropdownItem onClick={() => this.handleSetStyle('fontWeight','900', i)}>
                      900
                    </DropdownItem>
                  </DropdownMenu>
                </UncontrolledButtonDropdown>
              </div>
            </Col>
            <Col md={4}>
              <div className="mt-2 mr-sm-2 mb-sm-0 form-group">
                <label for={"fontSize" + i} className="mr-sm-2">
                  Font Size
                </label>
                <UncontrolledButtonDropdown>
                  <DropdownToggle caret  color="primary">
                    {this.props.richTextArrayStyleSheet[i]['fontSize']}
                  </DropdownToggle>
                  <DropdownMenu>
                    <DropdownItem onClick={() => this.handleSetStyle('fontSize','18px', i)}>
                      18
                    </DropdownItem>
                    <DropdownItem onClick={() => this.handleSetStyle('fontSize','20px', i)}>
                      20
                    </DropdownItem>
                    <DropdownItem onClick={() => this.handleSetStyle('fontSize','22px', i)}>
                      22
                    </DropdownItem>
                  </DropdownMenu>
                </UncontrolledButtonDropdown>
              </div>
            </Col>
            <Col md={4}>
              <div className="mt-2 mr-sm-2 mb-sm-0 form-group">
                <label for={"fontColor" + i} className="mr-sm-2">
                  Font Color
                </label>
                <UncontrolledButtonDropdown>
                  <DropdownToggle caret  color="primary">
                    {this.props.richTextArrayStyleSheet[i]['fontColor']}
                  </DropdownToggle>
                  <DropdownMenu>
                    <DropdownItem onClick={() => this.handleSetStyle('fontColor','Red', i)}>
                      Red
                    </DropdownItem>
                    <DropdownItem onClick={() => this.handleSetStyle('fontColor','Blue', i)}>
                      Blue
                    </DropdownItem>
                    <DropdownItem onClick={() => this.handleSetStyle('fontColor','Green', i)}>
                      Green
                    </DropdownItem>
                  </DropdownMenu>
                </UncontrolledButtonDropdown>
              </div>
            </Col>
            <Col md={4}>
              <div className="mt-2 mr-sm-2 mb-sm-0 form-group">
                <label for={"fontFamily" + i} className="mr-sm-2">
                  Font Family
                </label>
                <UncontrolledButtonDropdown>
                  <DropdownToggle caret  color="primary">
                    {this.props.richTextArrayStyleSheet[i]['fontFamily']}
                  </DropdownToggle>
                  <DropdownMenu>
                    <DropdownItem onClick={() => this.handleSetStyle('fontFamily','Arial', i)}>
                      Arial
                    </DropdownItem>
                    <DropdownItem onClick={() => this.handleSetStyle('fontFamily','Times New Roman', i)}>
                      Times New Roman
                    </DropdownItem>
                    <DropdownItem onClick={() => this.handleSetStyle('fontFamily','Helvetica', i)}>
                      Helvetica
                    </DropdownItem>
                  </DropdownMenu>
                </UncontrolledButtonDropdown>
              </div>
            </Col>
            <Col md={4}>
              <div className="mt-2 mr-sm-2 mb-sm-0 form-group">
                <label for={"lineHeight" + i} className="mr-sm-2">
                  Line Height
                </label>
                <UncontrolledButtonDropdown>
                  <DropdownToggle caret  color="primary">
                    {this.props.richTextArrayStyleSheet[i]['lineHeight']}
                  </DropdownToggle>
                  <DropdownMenu>
                    <DropdownItem onClick={() => this.handleSetStyle('lineHeight','1', i)}>
                      1
                    </DropdownItem>
                    <DropdownItem onClick={() => this.handleSetStyle('lineHeight','2', i)}>
                      2
                    </DropdownItem>
                    <DropdownItem onClick={() => this.handleSetStyle('lineHeight','3', i)}>
                      3
                    </DropdownItem>
                  </DropdownMenu>
                </UncontrolledButtonDropdown>
              </div>
            </Col>
            <Col md={4}>
              <Col md={2} className="pull-right mt-4">
                <FontAwesomeIcon
                  icon={faTrash}
                  size="1x"
                  color="red"
                  onClick={this.removeRow.bind(this, i)}
                />
              </Col>
              <Col md={2} className="pull-right mt-4">
                <FontAwesomeIcon
                  icon={faCheck}
                  size="1x"
                  color="green"
                  onClick={this.saveStyle.bind(this, i)}
                />
              </Col>
            </Col>
          </Row>
        </div>
      </div>
    )): null;
  };
  render() {
    return (
      <div>
        <Row>
          <Col md={12} className="mt-1">
            <div className ="pull-right">
              <FontAwesomeIcon
                icon={faPlus}
                size="1x"
                color="green"
                onClick={this.addRow.bind(this)}
              />
            </div>
          </Col>
        </Row>
        <Container>
          <Row>
            {this.styleSheetUI()}
          </Row>
        </Container>
      </div>
    );
  }
}const mapStateToProps = (state) => ({
  inlineStyle: state.ThemeOptions.inlineStyle,
  inlineStyleEnable: state.ThemeOptions.inlineStyleEnable,
  richTextArrayStyleSheet: state.ThemeOptions.richTextArrayStyleSheet,
});
const mapDispatchToProps = (dispatch) => ({
  setInlineStyle: (enable) => dispatch(setInlineStyle(enable)),
  setInlineStyleEnable: (enable) => dispatch(setInlineStyleEnable(enable)),
  setRichTextArrayStyleSheet: (enable) => dispatch(setRichTextArrayStyleSheet(enable)),
});
export default connect(mapStateToProps, mapDispatchToProps)(StyleSheet);
