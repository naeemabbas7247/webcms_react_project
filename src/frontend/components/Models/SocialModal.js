import React, { Component } from "react";
import "../../../index.scss";
import ApiCall from "../../../admin/Partial/Function/ApiCall";
import { SketchPicker } from "react-color";
import ImageUploader from "react-images-upload";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faClone, faTrash, faPlus, faArrowUp, faArrowDown, faPencilAlt } from "@fortawesome/free-solid-svg-icons";
import {
  setCmsArray,
  setCmsModel,
  setCmsKey,
  setCmsValue,
  setThemeBackgroundColor,
  setThemeColor,
  setEditorState,
  setModuleCmsObject,
  setCmsEnable,
  setCmsEnableBackground,
  setCmsEnableUploadImage,
  setCmsModelImage,
  setThemeSectionBackgroundColor,
  setSectionBackgroundColor,
  setCmsImageArray,
  setPreviousColorsValue,
  setCmsVideoArray,
  setCmsSection,
  setBackgroundCmsKey,
  setContactUsEmail,
  setSubscriptionEmail,
  setIsGlobal,
  setCustomStyle,
  setSocialModel,
  setSectionArrayModel,
  setCurrentSection,
} from "../../../reducers/ThemeOptions";
import { connect } from "react-redux";
import {
  EditorState,
  ContentState,
  convertFromHTML,
  convertFromRaw,
  convertToRaw,
} from "draft-js";
import { Editor } from "react-draft-wysiwyg";
import draftToHtml from "draftjs-to-html";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import Dropzone from "react-dropzone";
import Resizer from "react-image-file-resizer";
import {
  UncontrolledButtonDropdown,
  DropdownToggle,
  Dropdown,
  DropdownMenu,
  DropdownItem,
  Nav,
  NavItem,
  NavLink,
  Container,
  CustomInput,
} from "reactstrap";
import { Card, CardBody, CardTitle } from "reactstrap";
import {
  Row,
  Col,
  ListGroup,
  ListGroupItem,
  FormGroup,
  Label,
  Input,
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
} from "reactstrap";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import "react-tabs/style/react-tabs.css";
let flag = 0;
class SocialModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      social_link:null,
      social_icon:null,
      social_icon_flag:false,
    }
  }
  toggle = async () => {
    const { setSocialModel } = this.props;
    setSocialModel(!this.props.socialModel);
  };
  submit = async () => {
    // console.log(this.props.cmsArray[this.props.cmsKey+'_icon'][this.props.cmsKey+'_icon']);
    var data = {
      key: this.props.cmsKey+'_icon',
      value: this.props.cmsArray[this.props.cmsKey+'_icon'][this.props.cmsKey+'_icon'],
      new : false,
      innerkey : this.props.cmsKey+'_icon',
      is_global_text : false,
    };
    var url = "setCmsData";
    await ApiCall.postAPICall(url, data);
    var data = {
      key: this.props.cmsKey,
      value: this.props.cmsArray[this.props.cmsKey][this.props.cmsKey],
      new : false,
      innerkey : this.props.cmsKey,
      is_global_text : false,
    };
    var url = "setCmsData";
    await ApiCall.postAPICall(url, data);
    const { setSocialModel } = this.props;
    setSocialModel(false);
    this.props.getData();
    let setCmsArray = this.props.cmsArray;
    setCmsArray[this.props.cmsKey] = this.props.cmsArray[this.props.cmsKey];
    this.setCmsData(setCmsArray);
  };
  setCmsData = async (data) => {
    const { cmsArray, setCmsArray } = this.props;
    await setCmsArray(data);
  };
  handleIcon=async(value)=>
  {
    this.setState({ social_icon:value });
    this.setState({ social_icon_flag: true })
    var cms_value = this.props.cmsArray;
    var key=this.props.cmsKey+'_icon';
    cms_value[key][key] = value;
    await this.setCmsData(cms_value);
  }
   iconOptions = [
      { value: "zmdi zmdi-facebook",      name : <i className= "zmdi zmdi-facebook"></i>     },
      { value: "zmdi zmdi-twitter",     name : <i className= "zmdi zmdi-twitter"></i>    },
      { value: "zmdi zmdi-google", name : <i className= "zmdi zmdi-google"></i>},
      { value: "zmdi zmdi-linkedin",    name : <i className= "zmdi zmdi-linkedin"></i>   },
      { value: "zmdi zmdi-pinterest",     name : <i className= "zmdi zmdi-pinterest"></i>    },
      { value: "zmdi zmdi-youtube",  name : <i className= "zmdi zmdi-youtube"></i> }
  ]
  render() {
    return (
      <div>
        <Modal
          isOpen={this.props.socialModel}
          fade={false}
          toggle={
            this.props.cmsEnableBackground ? this.toggleBackground : this.toggle
          }
          className={this.props.className}
        >
          <ModalHeader toggle={this.toggle}>CMS</ModalHeader>
            <div className="form-inline">
              <Col md={12}>
                <FormGroup>
                  <label className="mr-sm-2">
                    Pase your link here
                  </label>
                  <input name="social_link" defaultValue={this.props.socialModel ? this.props.cmsArray[this.props.cmsKey] ? this.props.cmsArray[this.props.cmsKey][this.props.cmsKey] : null : null} onChange={async(e) => {
                    var value = this.props.cmsArray;
                    value[this.props.cmsKey][this.props.cmsKey] = e.target.value;
                    await this.setCmsData(value);
                  } }
                    placeholder="Paste your link here"
                    type="text"
                    className=" "
                  />
                </FormGroup>
              </Col>
              <Col md={12} className="mt-2">
                <FormGroup>
                  <div className=" mb-2">
                    <UncontrolledButtonDropdown>
                      <DropdownToggle caret className="" color="primary">
                       Icon
                      </DropdownToggle>
                       <DropdownMenu>
                       {this.iconOptions.map(item => (
                        <DropdownItem  
                          onClick={() =>{this.handleIcon(item.value)}} 
                          >{item.name}
                        </DropdownItem>
                        )) 
                      }
                       </DropdownMenu>
                    </UncontrolledButtonDropdown>
                  </div>
                  <input name="social_icon" value={this.props.cmsArray[this.props.cmsKey+'_icon'] ? this.props.cmsArray[this.props.cmsKey+'_icon'][this.props.cmsKey+'_icon'] : null } onChange={async(e) => {
                    var value = this.props.cmsArray;
                    if(typeof(value[this.props.cmsKey+'_icon'])!='undefined')
                    {
                      value[this.props.cmsKey+'_icon'][this.props.cmsKey+'_icon'] = e.target.value;
                      await this.setCmsData(value);

                    }
                  } }
                    type="text"
                    className=" "
                  />
                </FormGroup>
              </Col>
            </div>
          <ModalBody>
          </ModalBody>
          <ModalFooter>
             <Button
              color="link"
              onClick={this.toggle}
            >
              Cancel
            </Button>
            <Button color="primary" onClick={this.submit}>
              Save
            </Button>{" "}
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}
const mapStateToProps = (state) => ({
  cmsArray: state.ThemeOptions.cmsArray,
  cmsModel: state.ThemeOptions.cmsModel,
  cmsKey: state.ThemeOptions.cmsKey,
  cmsValue: state.ThemeOptions.cmsValue,
  themeColor: state.ThemeOptions.themeColor,
  themeBackgroundColor: state.ThemeOptions.themeBackgroundColor,
  editorState: state.ThemeOptions.editorState,
  moduleCmsObject: state.ThemeOptions.moduleCmsObject,
  cmsEnableBackground: state.ThemeOptions.cmsEnableBackground,
  cmsEnable: state.ThemeOptions.cmsEnable,
  cmsEnableUploadImage: state.ThemeOptions.cmsEnableUploadImage,
  cmsModelImage: state.ThemeOptions.cmsModelImage,
  themeSectionBackgroundColor: state.ThemeOptions.themeSectionBackgroundColor,
  sectionBackgroundColor: state.ThemeOptions.sectionBackgroundColor,
  cmsImageArray: state.ThemeOptions.cmsImageArray,
  previousColors: state.ThemeOptions.previousColors,
  cmsVideoArray: state.ThemeOptions.cmsVideoArray,
  cmsSection: state.ThemeOptions.cmsSection,
  backgroundCmsKey: state.ThemeOptions.backgroundCmsKey,
  sectionId: state.ThemeOptions.sectionId,
  contact_us_email: state.ThemeOptions.contact_us_email,
  subscription_email: state.ThemeOptions.subscription_email,
  isGlobal: state.ThemeOptions.isGlobal,
  customStyle: state.ThemeOptions.customStyle,
  socialModel: state.ThemeOptions.socialModel,
  sectionArrayModel: state.ThemeOptions.sectionArrayModel,
  currentSection: state.ThemeOptions.currentSection,
});
const mapDispatchToProps = (dispatch) => ({
  setCmsArray: (enable) => dispatch(setCmsArray(enable)),
  setCmsModel: (enable) => dispatch(setCmsModel(enable)),
  setCmsKey: (enable) => dispatch(setCmsKey(enable)),
  setCmsValue: (enable) => dispatch(setCmsValue(enable)),
  setThemeColor: (enable) => dispatch(setThemeColor(enable)),
  setThemeBackgroundColor: (enable) =>
    dispatch(setThemeBackgroundColor(enable)),
  setEditorState: (enable) => dispatch(setEditorState(enable)),
  setModuleCmsObject: (enable) => dispatch(setModuleCmsObject(enable)),
  setCmsEnableBackground: (enable) => dispatch(setCmsEnableBackground(enable)),
  setCmsEnable: (enable) => dispatch(setCmsEnable(enable)),
  setCmsEnableUploadImage: (enable) =>
    dispatch(setCmsEnableUploadImage(enable)),
  setCmsModelImage: (enable) => dispatch(setCmsModelImage(enable)),
  setThemeSectionBackgroundColor: (enable) =>
    dispatch(setThemeSectionBackgroundColor(enable)),
  setSectionBackgroundColor: (enable) =>
    dispatch(setSectionBackgroundColor(enable)),
  setCmsImageArray: (enable) => dispatch(setCmsImageArray(enable)),
  setPreviousColorsValue: (enable) => dispatch(setPreviousColorsValue(enable)),
  setCmsVideoArray: (enable) => dispatch(setCmsVideoArray(enable)),
  setCmsSection: (enable) => dispatch(setCmsSection(enable)),
  setBackgroundCmsKey: (enable) => dispatch(setBackgroundCmsKey(enable)),
  setContactUsEmail: (enable) => dispatch(setContactUsEmail(enable)),
  setSubscriptionEmail: (enable) => dispatch(setSubscriptionEmail(enable)),
  setIsGlobal: (enable) => dispatch(setIsGlobal(enable)),
  setCustomStyle: (enable) => dispatch(setCustomStyle(enable)),
  setSocialModel: (enable) => dispatch(setSocialModel(enable)),
  setSectionArrayModel: (enable) => dispatch(setSectionArrayModel(enable)),
  setCurrentSection: (enable) => dispatch(setCurrentSection(enable)),
});
export default connect(mapStateToProps, mapDispatchToProps)(SocialModal);
// <Screenshot />