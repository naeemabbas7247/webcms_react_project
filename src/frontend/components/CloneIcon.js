import React, { Component } from "react";
import { useDispatch } from "react-redux";
import {
  setCmsArray,
  setCmsModel,
  setCmsKey,
  setCmsValue,
  setThemeBackgroundColor,
  setThemeColor,
  setEditorState,
  setCmsEnable,
  setCmsEnableUploadImage,
  setCmsModelImage,
  setAddModel,
  setCurrentSection,
  setAddModelData,
  setCmsSection,
  setEditSectionModel,
  setEditSectionModelEnable,
  setEditSectionModelArray,
  setIsHorizontal,
  setSelectedEdit
} from "../../reducers/ThemeOptions";
import ApiCall from "../../admin/Partial/Function/ApiCall";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faClone, faTrash, faPlus, faArrowUp, faArrowDown, faPencilAlt, faCheckCircle } from "@fortawesome/free-solid-svg-icons";
import { connect } from "react-redux";
import "./CloneIcon.css";
import { Loader, Types } from "react-loaders";
import LoadingOverlay from "react-loading-overlay";
class CloneIcon extends Component {
  constructor(){
    super();
    this.state = {loader:false, arrowUp: 'Shift Section Up', arrowDown:'Shift Section Down', trash:'Delete Section',
    pencil:'Edit Section', clone:'Copy Section ', plus:'Add New Section',text_plus:'Add New Part' }
  }
  cloneKey = async (key) => {
    this.handelIsHorizontal();
    var url = "sections/duplicate";
    this.refresh(url, key);
  };
  handelIsHorizontal=async()=>
  {
    if(this.props.class_set && this.props.class_set != "vertical")
    {
      const { setIsHorizontal } = this.props;
      setIsHorizontal(true);  
    }
    else
    {
      const { setIsHorizontal } = this.props;
      setIsHorizontal(false);  
    }
  }
  cloneAddKey=async(key)=>
  {
    this.handelIsHorizontal();
    const { setAddModelData } = this.props;
    setAddModelData(key);
    const { setSelectedEdit } = this.props;
    var obj = {
      key: key,
      class: this.props.class_set ? this.props.class_set : "vertical"
    }
    setSelectedEdit(obj);
  };
  cloneKeyDelete = async (key) => {
    var url = "sections/delete";
    this.refresh(url, key);
  };
   orderChange = async (key,flag) => {
    var url = "swap";
    this.refresh(url, {key,flag});
  }
  cloneAdd = async (key) => {
    var url = "clone";
    this.handelIsHorizontal();
    this.refresh(url, key);
  }
  addSection=async(key)=>
  {
    this.setSectionModel(key);
  }
  editSection=async(key)=>
  {
    this.handelIsHorizontal();
    var temp_array=[];
    var section_name=key.split('_')[0];
    Object.keys(this.props.cmsSection).map((val, i) => {
      if(this.props.cmsSection[val]['module_name'] == section_name )
      {
        temp_array.push(this.props.cmsSection[val]);
      }
    });
    const { setEditSectionModelArray } = this.props;
    setEditSectionModelArray(temp_array);
    const { setEditSectionModel } = this.props;
    setEditSectionModel(section_name);
    const { setEditSectionModelEnable } = this.props;
    setEditSectionModelEnable(!this.props.editSectionModelEnable);
    const { setSelectedEdit } = this.props;
    var obj = {
      key: key,
      class: this.props.class_set ? this.props.class_set : "vertical"
    }
    setSelectedEdit(obj);
    this.setSectionModel(key);
  }
  setSectionModel=async(key)=>
  {
    const { setAddModel } = this.props;
    setAddModel(!this.props.addModel);
    const { setCurrentSection } = this.props;
    setCurrentSection(key);
  }
  startLoader=()=>
  {
    this.setState({loader: !this.state.loader})

  }
  refresh = async (url, key) => {
    this.startLoader();
    var data = {
      key: key,
      is_horizontal:this.props.isHorizontal,
    };
    if(this.props.module){
      data['module'] = this.props.module;
    }
    await ApiCall.postAPICall(url, data);
    this.props.getData();
    this.props.getColors();
    this.startLoader();
  };
  render() {
    let key = this.props[0];
    let style={
      display: this.props.cmsEnable ? "block" : "none"
    };
    let backgroundstyle={};
    var is_selected = false;
    if(this.props.editSectionModelEnable && this.props.selectedEdit.key == key && this.props.class_set && this.props.selectedEdit.class == this.props.class_set)
    {
      backgroundstyle={
        color: 'green'
      };
      is_selected = true;
    }
    return (
      <div>
        {this.state.loader
            ?
            <div className="icons-box-section pull-right" style={{height:10,marginRight:10}}>
              <LoadingOverlay tag="div" active={true}
                styles={{
                  overlay: (base) => ({
                    ...base,
                    background: "#fff",
                    opacity: 0.5,
                    marginRight:10
                  }),
                }}
                spinner={<Loader active type={'ball-clip-rotate-multiple'} />}>
              </LoadingOverlay>
            </div>
            :
            <div>
              <div className="icons-box-section pull-right">
              {!this.props.section_flag ?
                <div>
                  <div
                    className="pull-right mr-2 cloneIcon"
                    style={style}
                    onClick={() => this.cloneKey(key)}
                  >
                    <div className="Hov c1">
                    <FontAwesomeIcon icon={faClone} size="2x"  />
                    <span className="tooltiptext c1">{this.state.clone} </span> 
                    </div>
                  </div>
                  <div
                    className="pull-right mr-2 cloneIcon"
                    style={style}
                    onClick={() => this.cloneKeyDelete(key)}
                  >
                    <div className="Hov c1">
                    <FontAwesomeIcon icon={faTrash} size="2x"  />
                    <span className="tooltiptext c1">{this.state.trash} </span> 
                    </div>
                  </div>
                  <div
                    className="pull-right mr-2 cloneIcon"
                    style={style}
                    onClick={() => this.orderChange(key,1)}
                  >
                    <div className="Hov c1">
                    <FontAwesomeIcon icon={faArrowUp} size="2x"  />
                    <span className="tooltiptext c1">{this.state.arrowUp} </span> 
                    </div>
                  </div>
                  <div
                    className="pull-right mr-2 cloneIcon"
                    style={style}
                    onClick={() => this.orderChange(key,0)}
                  >
                   <div className="Hov c1">
                    <FontAwesomeIcon icon={faArrowDown} size="2x"  />
                    <span className="tooltiptext c1">{this.state.arrowDown} </span> 
                    </div> 
                  </div>
                  <div
                    className="pull-right mr-2 cloneIcon"
                    style={style}
                    onClick={() => this.editSection(key)}
                  >
                    
                    <div className="Hov c1">
                    <FontAwesomeIcon icon={faPencilAlt} size="2x"  />
                    <span className="tooltiptext c1">{this.state.pencil} </span> 
                    </div>
                    
                  </div>
                  <div
                    className="pull-right mr-2 cloneIcon"
                    style={style}
                    onClick={() => this.addSection(key)}
                  >
                    <div className="Hov c1">
                      <FontAwesomeIcon icon={faPlus} size="2x"  />
                      <span className="tooltiptext c1">{this.state.plus} </span> 
                    </div>
                  </div>
                </div>
                :
                is_selected ? 
                <div>
                  <div
                    className="pull-right mr-2 cloneIcon"
                    style={style}
                  >
                    
                    <FontAwesomeIcon style={backgroundstyle} icon={faCheckCircle} size="2x" className="click" />
                    
                  </div>
                </div> : 
                <div>
                  <div
                    className="pull-right mr-2 cloneIcon"
                    style={style}
                    onClick={() => this.cloneAddKey(key)}
                  > 
                  </div>
                </div>
              }
              </div>
              <div className=" pull-right mt-10">
                {this.props.addable ? 
                  <div
                    className="icons-box-section pull-right mr-2 cloneIcon"
                    style={style}
                    onClick={() => this.cloneAdd(key)}
                  >
                    <div className="Hov">
                    <FontAwesomeIcon icon={faPlus} size="2x"  />
                    <span className="tooltiptext c1">{this.state.text_plus} </span> 
                    </div>
                  </div> : null}
                </div>
            </div>
          }
      </div>
    );
  }
}
const mapStateToProps = (state) => ({
  cmsArray: state.ThemeOptions.cmsArray,
  cmsModel: state.ThemeOptions.cmsModel,
  cmsKey: state.ThemeOptions.cmsKey,
  cmsValue: state.ThemeOptions.cmsValue,
  themeColor: state.ThemeOptions.themeColor,
  themeBackgroundColor: state.ThemeOptions.themeBackgroundColor,
  editorState: state.ThemeOptions.editorState,
  cmsEnable: state.ThemeOptions.cmsEnable,
  cmsEnableUploadImage: state.ThemeOptions.cmsEnableUploadImage,
  cmsModelImage: state.ThemeOptions.cmsModelImage,
  addModel: state.ThemeOptions.addModel,
  currentSection: state.ThemeOptions.currentSection,
  addModelData: state.ThemeOptions.addModelData,
  cmsSection: state.ThemeOptions.cmsSection,
  editSectionModel: state.ThemeOptions.editSectionModel,
  editSectionModelEnable: state.ThemeOptions.editSectionModelEnable,
  editSectionModelArray: state.ThemeOptions.editSectionModelArray,
  isHorizontal: state.ThemeOptions.isHorizontal,
  selectedEdit: state.ThemeOptions.selectedEdit,
});
const mapDispatchToProps = (dispatch) => ({
  setCmsArray: (enable) => dispatch(setCmsArray(enable)),
  setCmsModel: (enable) => dispatch(setCmsModel(enable)),
  setCmsKey: (enable) => dispatch(setCmsKey(enable)),
  setCmsValue: (enable) => dispatch(setCmsValue(enable)),
  setThemeColor: (enable) => dispatch(setThemeColor(enable)),
  setThemeBackgroundColor: (enable) =>
    dispatch(setThemeBackgroundColor(enable)),
  setEditorState: (enable) => dispatch(setEditorState(enable)),
  setCmsEnable: (enable) => dispatch(setCmsEnable(enable)),
  setCmsModelImage: (enable) => dispatch(setCmsModelImage(enable)),
  setCmsEnableUploadImage: (enable) =>
    dispatch(setCmsEnableUploadImage(enable)),
  setCmsSection: (enable) => dispatch(setCmsSection(enable)),
  setAddModel: (enable) => dispatch(setAddModel(enable)),
  setCurrentSection: (enable) => dispatch(setCurrentSection(enable)),
  setAddModelData: (enable) => dispatch(setAddModelData(enable)),
  setEditSectionModel: (enable) => dispatch(setEditSectionModel(enable)),
  setEditSectionModelEnable: (enable) => dispatch(setEditSectionModelEnable(enable)),
  setEditSectionModelArray: (enable) => dispatch(setEditSectionModelArray(enable)),
  setIsHorizontal: (enable) => dispatch(setIsHorizontal(enable)),
  setSelectedEdit: (enable) => dispatch(setSelectedEdit(enable)),
});
export default connect(mapStateToProps, mapDispatchToProps)(CloneIcon);




