import React, { Component } from "react";
import { useDispatch } from "react-redux";
import {
  setCmsArray,
  setCmsModel,
  setCmsKey,
  setCmsValue,
  setThemeBackgroundColor,
  setThemeColor,
  setEditorState,
  setCmsEnable,
  setCmsEnableUploadImage,
  setCmsModelImage,
  setIconModel,
  setSectionBackgroundColor,
  setBackgroundCmsKey,
  setShowIcon,
  setLogoIcon,
} from "../../reducers/ThemeOptions";
import ReactPlayer from "react-player";
import { connect } from "react-redux";
import Icon from "./Icon.js";
import CloneIcon from "./CloneIcon.js";
import CmsSet from "./Functions/CmsSet.js";
import {
  EditorState,
  ContentState,
  convertFromHTML,
  convertFromRaw,
} from "draft-js";
import { Editor } from "react-draft-wysiwyg";
import htmlToDraft from "html-to-draftjs";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import "react-tabs/style/react-tabs.css";
import IconModal from "./Models/IconModal";
import ReactResumableJs from "../../ReactResumableJs";
import {
  Carousel,
  CarouselItem,
  CarouselControl,
  CarouselIndicators,
  CarouselCaption,
} from "reactstrap";
class whyUs extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeIndex: 0,
      animating: false,
      imageLogo: "",
      imageLogo2: "",
    };
  }
  SetCms = (key, section_id, background_key = null) => {
    if (this.props.cmsEnable) {
      var tempArray = {
        key: section_id + "banner_background",
        value: this.props.cmsArray[section_id + "banner_background"],
      };
      const { setSectionBackgroundColor } = this.props;
      setSectionBackgroundColor(tempArray);
      const contentBlock = htmlToDraft(this.props.cmsArray[section_id + key]);
      if (contentBlock) {
        const contentState = ContentState.createFromBlockArray(
          contentBlock.contentBlocks
        );
        const editorState = EditorState.createWithContent(contentState);
        const { setEditorState } = this.props;
        setEditorState(editorState);
      }
      if (background_key) {
        const { setBackgroundCmsKey } = this.props;
        setBackgroundCmsKey(section_id + background_key);
      }
      const { cmsKey, setCmsKey } = this.props;
      setCmsKey(section_id + key);
      const { cmsValue, setCmsValue } = this.props;
      setCmsValue(this.props.cmsArray[section_id + key]);
      const { cmsModel, setCmsModel } = this.props;
      setCmsModel(!this.props.cmsModel);
    }

  };
  SetImage = async (key, section_id) => {
    if (this.props.cmsEnable) {
      var tempArray = {
        key: section_id + "banner_background",
        value: this.props.cmsArray[section_id + "banner_background"],
      };
      const { setSectionBackgroundColor } = this.props;
      setSectionBackgroundColor(tempArray);

      const { cmsModelImage, setCmsModelImage } = this.props;
      setCmsModelImage(!this.props.cmsModelImage);

      const { cmsEnableUploadImage, setCmsEnableUploadImage } = this.props;
      setCmsEnableUploadImage(!this.props.cmsEnableUploadImage);
      const { cmsKey, setCmsKey } = this.props;
      const { cmsValue, setCmsValue } = this.props;
      setCmsValue(this.props.cmsArray[key]);
      setCmsKey(key);
    }
  };

  SetIcon = async (key, section_id) => {
    if (this.props.cmsEnable) {
      var tempArray = {
        key: section_id + "banner_background",
        value: this.props.cmsArray[section_id + "banner_background"],
      };
      this.setState({
        imageLogo: this.props.cmsArray[section_id + "whyUs_icon"] && this.props.cmsArray[section_id + "whyUs_icon"].length > 0 ? this.props.cmsArray[section_id + "whyUs_icon"][0][
          "images_backups"
        ]["image"] : null
      });
      this.setState({
        imageLogo2: this.props.cmsArray[section_id + "whyUs_icon"] && this.props.cmsArray[section_id + "whyUs_icon"].length > 0 ? this.props.cmsArray[section_id + "whyUs_icon"][0][
          "images_backups"
        ]["image"] : null,
      });
      const { setSectionBackgroundColor } = this.props;
      setSectionBackgroundColor(tempArray);

      const { iconModel, setIconModel } = this.props;
      setIconModel(!this.props.iconModel);

      const { showIcon, setShowIcon } = this.props;
      setShowIcon(this.state.imageLogo);

      const { logoIcon, setLogoIcon } = this.props;
      setLogoIcon(this.state.imageLogo2);

      const { cmsEnableUploadImage, setCmsEnableUploadImage } = this.props;
      setCmsEnableUploadImage(!this.props.cmsEnableUploadImage);
      const { cmsKey, setCmsKey } = this.props;
      const { cmsValue, setCmsValue } = this.props;
      setCmsValue(this.props.cmsArray[key]);
      setCmsKey(key);
    }
  };
  whyUsImg(section_id) {
    if (this.props.cmsArray[section_id + "whyUs_img"]) {
      if (this.props.cmsArray[section_id + "whyUs_img"].length > 0) {
        if (
          this.props.cmsArray[section_id + "whyUs_img"][0]["images_backups"][
            "image"
          ].includes("http")
        ) {
          if (this.props.cmsArray[section_id + "whyUs_img"][0].length == 1) {
            return (
              <div style={{ position: "relative", paddingTop: "56.25%" }}>
                <ReactPlayer
                  onClick={() =>
                    this.SetImage(section_id + "whyUs_img", section_id)
                  }
                  url={
                    this.props.cmsArray[section_id + "whyUs_img"][
                      "images_backups"
                    ]["image"]
                  }
                  style={{
                    position: "absolute",
                    top: 0,
                    left: 0,
                    padding: 12,
                  }}
                  controls
                  width="100%"
                  height="100%"
                />
              </div>
            );
          } else {
            const slides = this.props.cmsArray[section_id + "whyUs_img"].map(
              (item) => {
                return (
                  <CarouselItem
                    onExiting={() => this.setState({ animating: true })}
                    onExited={() => this.setState({ animating: false })}
                    key={item.id}
                  >
                    <div style={{ position: "relative", paddingTop: "56.25%" }}>
                      <ReactPlayer
                        onClick={() =>
                          this.SetImage(section_id + "whyUs_img", section_id)
                        }
                        url={item.images_backups.image}
                        style={{
                          position: "absolute",
                          top: 0,
                          left: 0,
                          padding: 12,
                        }}
                        controls
                        width="100%"
                        height="100%"
                      />
                    </div>
                  </CarouselItem>
                );
              }
            );
            return (
              <Carousel
                onClick={() =>
                  this.SetImage(section_id + "whyUs_img", section_id)
                }
                activeIndex={this.state.activeIndex}
                next={this.next}
                previous={this.previous}
              >
                <CarouselIndicators
                  items={this.props.cmsArray[section_id + "whyUs_img"]}
                  activeIndex={this.state.activeIndex}
                  onClickHandler={this.goToIndex}
                />
                {slides}
                <CarouselControl
                  direction="prev"
                  directionText="Previous"
                  onClickHandler={() => this.previous()}
                />
                <CarouselControl
                  direction="next"
                  directionText="Next"
                  onClickHandler={() => this.next()}
                />
              </Carousel>
            );
          }
        } else {
          if (this.props.cmsArray[section_id + "whyUs_img"][0].length == 1) {
            return (
              <img
                style={[
                  this.props.cmsArray[section_id + "whyUs_imgstyle"],
                  { position: "relative", height: "100%", width: "100%" },
                ]}
                onClick={() =>
                  this.SetImage(section_id + "whyUs_img", section_id)
                }
                className="image-1"
                src={
                  this.props.cmsArray[section_id + "whyUs_img"][0][
                    "images_backups"
                  ]["image"]
                }
                alt="App Landing"
              />
            );
          } else {
            const slides = this.props.cmsArray[section_id + "whyUs_img"].map(
              (item) => {
                return (
                  <CarouselItem
                    onExiting={() => this.setState({ animating: true })}
                    onExited={() => this.setState({ animating: false })}
                    key={item.id}
                  >
                    <img
                      src={item.images_backups.image}
                      alt={item.altText}
                      onClick={() =>
                        this.SetImage(section_id + "whyUs_img", section_id)
                      }
                      style={{
                        ...this.props.cmsArray[section_id + "whyUs_imgstyle"],
                        position: "relative",
                        height: "100%",
                        width: "100%",
                      }}
                    />
                  </CarouselItem>
                );
              }
            );
            return (
              <Carousel
                onClick={() =>
                  this.SetImage(section_id + "whyUs_img", section_id)
                }
                activeIndex={this.state.activeIndex}
                next={this.next}
                previous={this.previous}
              >
                <CarouselIndicators
                  items={this.props.cmsArray[section_id + "whyUs_img"]}
                  activeIndex={this.state.activeIndex}
                  onClickHandler={this.goToIndex}
                />
                {slides}
                <CarouselControl
                  direction="prev"
                  directionText="Previous"
                  onClickHandler={() => this.previous()}
                />
                <CarouselControl
                  direction="next"
                  directionText="Next"
                  onClickHandler={() => this.next()}
                />
              </Carousel>
            );
          }
        }
      } else {
        return (
          <img
            style={{ position: "relative", height: "100%", width: "100%" }}
            onClick={() => this.SetImage(section_id + "whyUs_img", section_id)}
            className="image-1"
            src={"../../assets/images/app/mobile-2.png"}
            alt="App Landing"
          />
        );
      }
    }
    else {
      return (
        <img
          style={{ position: "relative", height: "100%", width: "100%" }}
          onClick={() => this.SetImage(section_id + "whyUs_img", section_id)}
          className="image-1"
          src={"../../assets/images/app/mobile-2.png"}
          alt="App Landing"
        />
      );
    }
  }
  next = () => {
    if (this.state.animating) return;
    const nextIndex =
      this.state.activeIndex ===
      this.props.cmsArray[this.props[0] + "_" + "whyUs_img"].length - 1
        ? 0
        : this.state.activeIndex + 1;
    this.setState({ activeIndex: nextIndex });
  };

  previous = () => {
    if (this.state.animating) return;
    const nextIndex =
      this.state.activeIndex === 0
        ? this.props.cmsArray[this.props[0] + "_" + "whyUs_img"].length - 1
        : this.state.activeIndex - 1;
    this.setState({ activeIndex: nextIndex });
  };

  whyUsIcon(section_id) {
    return (
      <div>
       {this.props.cmsArray[section_id + "whyUs_icon"] && this.props.cmsArray[section_id + "whyUs_icon"].length > 0  ? this.props.cmsArray[section_id + "whyUs_icon"][0]['cms_page']['is_icon'] == 0 ?  
        <img
        style={this.props.cmsArray[section_id + "whyUs_iconstyle"]}
        onClick={() =>this.SetIcon(section_id + "whyUs_icon", section_id)}
        className="image-1"
        src={this.props.cmsArray[section_id + "whyUs_icon"] ? this.props.cmsArray[section_id + "whyUs_icon"][0]["images_backups"]["image"] : ""}
        controls
        width='5%'
        height='5%'
        /> : 
        <i 
        style={this.props.cmsArray[section_id + "whyUs_iconstyle"]}
        onClick={() =>this.SetIcon(section_id + "whyUs_icon", section_id)}
        className={this.props.cmsArray[section_id + "whyUs_icon"][0]["images_backups"]["image"]} ></i>
        : 
        <img
        style={{position:"relative",
        height:"3%",
        width:"3%"}}
        onClick={() =>
        this.SetIcon(section_id + "whyUs_icon", section_id)
        }
        className="image-1"
        src={
        "../../assets/images/app/mobile-2.png"
        }
        alt="App Landing" 
        />
        }
      </div>
    );
  }
  componentDidUpdate(prevProps) {
    var section_id = this.props[0] + "_";
    var oldArray = prevProps.cmsArray[section_id + "whyUs_img"];
    var newArray = this.props.cmsArray[section_id + "whyUs_img"];
    var newArray2 = this.props.cmsArray[section_id + "whyUs_icon"];
    var check = JSON.stringify(oldArray) != JSON.stringify(newArray);
    if (check) {
      this.whyUsImg(section_id);
    }
  }

  goToIndex = (newIndex) => {
    if (this.state.animating) return;
    this.setState({ activeIndex: newIndex });
  };
  render() {
    let key_id = this.props[0];
    let section_id = key_id + "_";
    var sectionBackgroundColorArray = {
      key: section_id + "banner_background",
      value: this.props.cmsArray[section_id + "banner_background"]?this.props.cmsArray[section_id + "banner_background"][section_id + "banner_background"]:null,
    };
    return (
      <div
        className={`app-whyUs ${this.props.horizontalwhyUs}`}
        style={{
          backgroundColor: this.props.cmsArray[section_id + "banner_background"]
            ? this.props.cmsArray[section_id + "banner_background"][
                section_id + "banner_background"
              ]
            : "orange",
          paddingTop: 24,
          paddingBottom: 24,
        }}
        id={section_id}
      >
        <CloneIcon
          {...[key_id]}
          current_section={this.props.current_section}
          class_set={this.props.class_set}
          section_flag={this.props.section_flag}
          getData={this.props.getData ? this.props.getData : null}
          getColors={this.props.getColors ? this.props.getColors : null}
        />
        <div className="container">
          <div className="row">
            <div className="col-lg-12">
              <div className="section-title text-center mb--40">
                <CmsSet
                  sectionBackgroundArray={sectionBackgroundColorArray}
                  styleProp={
                    this.props.cmsArray[section_id + "whyUs_headingstyle"]
                  }
                  htmlProp={this.props.cmsArray[section_id + "whyUs_heading"]}
                  sectionKey={section_id}
                  sectionId={"whyUs_heading"}
                  getData={this.props.getData ? this.props.getData : null}
                />
                {this.whyUsIcon(section_id)}
                <CmsSet
                  sectionBackgroundArray={sectionBackgroundColorArray}
                  styleProp={this.props.cmsArray[section_id + "whyUs_pstyle"]}
                  htmlProp={this.props.cmsArray[section_id + "whyUs_p"]}
                  sectionKey={section_id}
                  sectionId={"whyUs_p"}
                  getData={this.props.getData ? this.props.getData : null}
                />
              </div>
            </div>
          </div>
          <div className="row align-items-center">
            <div className="col-lg-6 mt--40">
              <div className="whyUs-content">
                <div className="title">
                <CmsSet
                  sectionBackgroundArray={sectionBackgroundColorArray}
                  styleProp={
                    this.props.cmsArray[section_id + "whyUs_tile_headingstyle"]
                  }
                  htmlProp={this.props.cmsArray[section_id + "whyUs_tile_heading"]}
                  sectionKey={section_id}
                  sectionId={"whyUs_tile_heading"}
                  getData={this.props.getData ? this.props.getData : null}
                />
                </div>
                <CmsSet
                  sectionBackgroundArray={sectionBackgroundColorArray}
                  styleProp={
                    this.props.cmsArray[section_id + "whyUs_tilestyle"]
                  }
                  htmlProp={this.props.cmsArray[section_id + "whyUs_tile_p"]}
                  sectionKey={section_id}
                  sectionId={"whyUs_tile_p"}
                  getData={this.props.getData ? this.props.getData : null}
                />
                <div className="whyUs-buttons">
                  <CmsSet
                    sectionBackgroundArray={sectionBackgroundColorArray}
                    styleProp={{
                      ...this.props.cmsArray[
                        section_id + "whyUs_btn_1style"
                      ],
                      background: this.props.cmsArray[
                        section_id + "whyUs_btn_1_global"
                      ]?this.props.cmsArray[
                        section_id + "whyUs_btn_1_global"
                      ][
                        section_id + "whyUs_btn_1_global"
                      ]:null,
                    }}
                    htmlProp={this.props.cmsArray[section_id + "whyUs_btn_1"]}
                    sectionKey={section_id}
                    globalBtn={true}
                    backgroundKey={'whyUs_btn_1_global'}
                    backgroundStyle={
                      this.props.cmsArray[
                        section_id + "whyUs_btn_1_global"
                      ]?this.props.cmsArray[
                        section_id + "whyUs_btn_1_global"
                      ][
                        section_id + "whyUs_btn_1_global"
                      ]:null
                    }
                    isIcon={true}
                    sectionId={"whyUs_btn_1"}
                    getData={this.props.getData ? this.props.getData : null}
                  />
                  <CmsSet
                    sectionBackgroundArray={sectionBackgroundColorArray}
                    styleProp={
                      this.props.cmsArray[section_id + "whyUs_btn_2style"]
                    }
                    htmlProp={this.props.cmsArray[section_id + "whyUs_btn_2"]}
                    sectionKey={section_id}
                    Btn={true}
                    isIcon={true}
                    sectionId={"whyUs_btn_2"}
                    getData={this.props.getData ? this.props.getData : null}
                  />
                </div>
              </div>
            </div>
            <div className="col-lg-5 offset-lg-1 mt--40">
              <div className="whyUs-thumbnail mr--35">
                {this.whyUsImg(section_id)}
                {/* <img style={this.props.cmsArray[section_id+'whyUs_imgstyle']} onClick={() => this.SetImage('whyUs_img', section_id)} className="image-1" src={this.props.cmsArray[section_id+'whyUs_img']} alt="App Landing"/> */}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  cmsArray: state.ThemeOptions.cmsArray,
  cmsModel: state.ThemeOptions.cmsModel,
  cmsKey: state.ThemeOptions.cmsKey,
  cmsValue: state.ThemeOptions.cmsValue,
  themeColor: state.ThemeOptions.themeColor,
  themeBackgroundColor: state.ThemeOptions.themeBackgroundColor,
  editorState: state.ThemeOptions.editorState,
  cmsEnable: state.ThemeOptions.cmsEnable,
  cmsEnableUploadImage: state.ThemeOptions.cmsEnableUploadImage,
  cmsModelImage: state.ThemeOptions.cmsModelImage,
  iconModel: state.ThemeOptions.iconModel,
  showIcon: state.ThemeOptions.showIcon,
  logoIcon: state.ThemeOptions.logoIcon,
  sectionBackgroundColor: state.ThemeOptions.sectionBackgroundColor,
  backgroundCmsKey: state.ThemeOptions.backgroundCmsKey,
});
const mapDispatchToProps = (dispatch) => ({
  setCmsArray: (enable) => dispatch(setCmsArray(enable)),
  setCmsModel: (enable) => dispatch(setCmsModel(enable)),
  setCmsKey: (enable) => dispatch(setCmsKey(enable)),
  setCmsValue: (enable) => dispatch(setCmsValue(enable)),
  setThemeColor: (enable) => dispatch(setThemeColor(enable)),
  setThemeBackgroundColor: (enable) =>
    dispatch(setThemeBackgroundColor(enable)),
  setEditorState: (enable) => dispatch(setEditorState(enable)),
  setCmsEnable: (enable) => dispatch(setCmsEnable(enable)),
  setCmsModelImage: (enable) => dispatch(setCmsModelImage(enable)),
  setIconModel: (enable) => dispatch(setIconModel(enable)),
  setShowIcon: (enable) => dispatch(setShowIcon(enable)),
  setLogoIcon: (enable) => dispatch(setLogoIcon(enable)),
  setCmsEnableUploadImage: (enable) =>
    dispatch(setCmsEnableUploadImage(enable)),
  setSectionBackgroundColor: (enable) =>
    dispatch(setSectionBackgroundColor(enable)),
  setBackgroundCmsKey: (enable) => dispatch(setBackgroundCmsKey(enable)),
});
export default connect(mapStateToProps, mapDispatchToProps)(whyUs);
