import React, { Component } from "react";
import { useDispatch } from "react-redux";
import { withRouter } from "react-router-dom";
import Login from ".../../admin/Login/Login/";
import { Link } from "react-router-dom";
import { browserHistory } from "react-router";
import {
  setCmsArray,
  setCmsModel,
  setCmsKey,
  setCmsValue,
  setThemeBackgroundColor,
  setThemeColor,
  setEditorState,
  setCmsEnable,
  setCmsEnableUploadImage,
  setCmsModelImage,
  setCmsEnableBackground,
  setThemeSectionBackgroundColor,
  setSectionBackgroundColor,
  setCmsImageArray,
  setCmsSection,
  setSectionId,
  setBackgroundCmsKey,
  setIconModel,
  setShowIcon,
  setLogoIcon,
} from "../../reducers/ThemeOptions";
import ReactPlayer from "react-player";
import { connect } from "react-redux";
import {
  EditorState,
  ContentState,
  convertFromHTML,
  convertFromRaw,
} from "draft-js";
import { Editor } from "react-draft-wysiwyg";
import htmlToDraft from "html-to-draftjs";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import "react-tabs/style/react-tabs.css";
import { faCogs } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { stat } from "fs-extra";
import {
  UncontrolledButtonDropdown,
  DropdownToggle,
  Dropdown,
  DropdownMenu,
  DropdownItem,
  Nav,
  NavItem,
  NavLink,
  Container,
  CustomInput,
} from "reactstrap";

import {
  Carousel,
  CarouselItem,
  CarouselControl,
  CarouselIndicators,
  CarouselCaption,
} from "reactstrap";
import CmsSet from "./Functions/CmsSet";

class Navbar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      enable: false,
      background_btn: false,
      cms_class: "check_cms button-default button-red",
      activeIndex: 0,
      animating: false,
      imageLogo: "",
      imageLogo2: "",
    };
  }

  SetCms = (
    key,
    section_id,
    background_key,
    sectionBackgroundArray,
    new_key
  ) => {
    if (this.props.cmsEnable) {
      const { setSectionBackgroundColor } = this.props;

      setSectionBackgroundColor(sectionBackgroundArray);
      const { setSectionId } = this.props;
      setSectionId("navbar");
      const contentBlock = "hi";
      if (contentBlock) {
        const contentState = ContentState.createFromBlockArray(
          contentBlock.contentBlocks
        );
        const editorState = EditorState.createWithContent(contentState);
        const { setEditorState } = this.props;
        setEditorState(editorState);
      }
      if (background_key) {
        const { setBackgroundCmsKey } = this.props;
        setBackgroundCmsKey(section_id + background_key);
      }
      const { cmsKey, setCmsKey } = this.props;
      setCmsKey(section_id + key);
      const { setCmsKeyInner } = this.props;
      setCmsKeyInner(new_key);
      const { cmsValue, setCmsValue } = this.props;
      setCmsValue(this.props.cmsArray[section_id + key][new_key]);
      const { cmsModel, setCmsModel } = this.props;
      setCmsModel(!this.props.cmsModel);
    }
  };
  SetIcon = async (key, section_id) => {
    if (this.props.cmsEnable) {
      var tempArray = {
        key: section_id + "banner_background",
        value: this.props.cmsArray[section_id + "banner_background"],
      };

      const { setSectionBackgroundColor } = this.props;
      setSectionBackgroundColor(tempArray);

      const { iconModel, setIconModel } = this.props;
      setIconModel(!this.props.iconModel);

      const { showIcon, setShowIcon } = this.props;
      setShowIcon(this.state.imageLogo);

      const { logoIcon, setLogoIcon } = this.props;
      setLogoIcon(this.state.imageLogo2);

      const { cmsKey, setCmsKey } = this.props;
      const { cmsValue, setCmsValue } = this.props;
      setCmsValue(this.props.cmsArray[key]);
      setCmsKey(key);
    }
  };

  navbarIcon(section_id) {
    return (
      <div>
        {this.props.cmsArray[section_id + "navbar_icon"] ? (
          this.props.cmsArray[section_id + "navbar_icon"][0]?.cms_page?.is_icon == 0 ? (
            <img
              style={this.props.cmsArray[section_id + "navbar_iconstyle"]}
              onClick={() =>
                this.SetIcon(section_id + "navbar_icon", section_id)
              }
              className="image-1"
              src={
                this.props.cmsArray[section_id + "navbar_icon"]
                  ? this.props.cmsArray[section_id + "navbar_icon"][0][
                      "images_backups"
                    ]["image"]
                  : ""
              }
              controls
              width="18%"
              height="100%"
            />
          ) : this.props.cmsArray[section_id + "navbar_icon"][0]?.cms_page?.is_icon == 1 ?(
            <i
              style={this.props.cmsArray[section_id + "navbar_iconstyle"]}
              onClick={() =>
                this.SetIcon(section_id + "navbar_icon", section_id)
              }
              className={
                this.props.cmsArray[section_id + "navbar_icon"][0]?.images_backups?.image
              }
            ></i>
          ) : (
            <img
              style={{ position: "relative", height: "100%", width: "18%" }}
              onClick={() => this.SetIcon(section_id + "navbar_icon", section_id)}
              className="image-1"
              src={"../../assets/images/app/mobile-2.png"}
              alt="App Landing"
            />
          )
        ) : (
          <img
            style={{ position: "relative", height: "100%", width: "18%" }}
            onClick={() => this.SetIcon(section_id + "navbar_icon", section_id)}
            className="image-1"
            src={"../../assets/images/app/mobile-2.png"}
            alt="App Landing"
          />
        )}
      </div>
    );
  }
  componentDidMount =async()=>
  {
    var flag=await localStorage.getItem('cmsEnable');
    if(flag === 'true')
    {
      this.modelEnable();
    }

  }
  componentDidUpdate(prevProps) {
    var section_id = this.props[0] + "_";
    var newArray2 = this.props.cmsArray[section_id + "navbar_icon"];
  }

  goToIndex = (newIndex) => {
    // console.log(newIndex);
    if (this.state.animating) return;
    this.setState({ activeIndex: newIndex });
  };

  SetImage = async (key, section_id) => {
    if (this.props.cmsEnable) {
      var tempArray = {
        key: section_id + "navbar_background",
        value: this.props.cmsArray[section_id + "navbar_background"],
      };
      const { setSectionBackgroundColor } = this.props;
      setSectionBackgroundColor(tempArray);
      const { cmsModelImage, setCmsModelImage } = this.props;
      setCmsModelImage(!this.props.cmsModelImage);
      const { cmsEnableUploadImage, setCmsEnableUploadImage } = this.props;
      setCmsEnableUploadImage(!this.props.cmsEnableUploadImage);
      const { cmsKey, setCmsKey } = this.props;
      const { cmsValue, setCmsValue } = this.props;
      setCmsValue(this.props.cmsArray[key]);
      setCmsKey(key);
    }
  };
  CmsFunctionBackground = async (key, section_id) => {
    const { setThemeSectionBackgroundColor } = this.props;
    await setThemeSectionBackgroundColor(true);
    const { cmsEnableBackground, setCmsEnableBackground } = this.props;
    var flag=!this.props.cmsEnableBackground;
    await setCmsEnableBackground(flag);
    await this.SetCms(key, section_id);
  };
  modelEnable = async () => {
    var flag=!this.props.cmsEnable;
    await localStorage.setItem('cmsEnable',flag);
    const { cmsEnable, setCmsEnable } = this.props;
    await setCmsEnable(flag);
    console.log(localStorage.getItem('cmsEnable'));
  };
  modelToggle = async () => {
    const { cmsModel, setCmsModel } = this.props;
    await setCmsModel(!this.props.cmsModel);
  };

  nextPath(path) {
    this.props.history.push(path);
  }

  handleLogOut = async (e) => {
    await localStorage.removeItem("isLogin");
    browserHistory.push("/#/admin/login");
    window.location.reload();
  };
  handleLogIn = async (e) => {
    browserHistory.push("/#/admin/login");
    window.location.reload();
  };

  headerSection(value) {
    // console.log(value);
    if (value["is_header"]) {
      return true;
    } else {
      return false;
    }
    // console.log(value);
    // props.cmsSection[val]['is_header']
  }

  CmsFunction = async () => {
    await this.modelEnable();
    if (this.props.cmsEnable) {
      this.setState({ cms_class: "check_cms button-default button-blue" });
    } else {
      this.setState({ cms_class: "check_cms button-default button-red" });
    }
    this.setState({ background_btn: !this.state.background_btn });
  };
  render() {
    let section_id = this.props[0] + "_";
    // console.log(this.props.cmsArray[section_id + "navbar_icon"][0]['cms_page']['is_icon'])

    var sectionBackgroundColorArray = {
      key: section_id + "navbar_background",
      value: this.props.cmsArray[section_id + "navbar_background"]
        ? this.props.cmsArray[section_id + "navbar_background"][
            section_id + "navbar_background"
          ]
        : "black",
    };
    return (
      <div>
        <div
          className={this.props.cmsSection[0]['is_sticker'] ? "app-header header--transparent sticker" : "app-header header--transparent"}
          style={{
            backgroundColor: this.props.cmsArray[
              section_id + "navbar_background"
            ]
              ? this.props.cmsArray[section_id + "navbar_background"][
                  section_id + "navbar_background"
                ]
              : "black",
          }}
          id="main-menu"
        >
          <div className="container">
            <div className="row align-items-center">
              <div className="col-lg-4 col-sm-5 col-5">
                <div className="logo">{this.navbarIcon(section_id)}</div>
              </div>
              <div className="col-lg-8 d-none d-lg-block">
                <div className="mainmenu-wrapper">
                  <nav>
                    <ul className="main-menu">
                      {this.props.cmsSection && this.props.cmsSection.length > 0
                        ? Object.keys(this.props.cmsSection).map((val, i) =>
                            this.headerSection(this.props.cmsSection[val]) ? (
                              <li>
                                <CmsSet
                                  sectionBackgroundArray={
                                    sectionBackgroundColorArray
                                  }
                                  htmlProp={
                                    this.props.cmsArray[
                                      this.props.cmsSection[val]["key"]
                                    ]
                                  }
                                  sectionKey={section_id}
                                  sectionId=""
                                  href={
                                    "#" +
                                    this.props.cmsSection[val]["key"] +
                                    "_"
                                  }
                                  isNav={true}
                                  getData={
                                    this.props.getData
                                      ? this.props.getData
                                      : null
                                  }
                                />
                              </li>
                            ) : (
                              ""
                            )
                          )
                        : ""}
                    </ul>
                  </nav>

                  {localStorage.getItem("isLogin") ? (
                    <UncontrolledButtonDropdown>
                      <DropdownToggle caret color="primary" tag="a">
                        {this.props.cmsEnable ? (
                          <b style={{ color: "blue" }}> Admin Actions </b>
                        ) : (
                          <b style={{ color: "white" }}> Admin Actions </b>
                        )}
                      </DropdownToggle>
                      <DropdownMenu>
                        <DropdownItem onClick={() => this.CmsFunction()}>
                          {this.props.cmsEnable ? (
                            <p style={{ color: "blue" }}> Edit CMS </p>
                          ) : (
                            <p style={{ color: "red" }}> CMS </p>
                          )}
                        </DropdownItem>
                        <DropdownItem onClick={this.handleLogOut}>
                          Logout
                        </DropdownItem>
                      </DropdownMenu>
                    </UncontrolledButtonDropdown>
                  ) : (
                    <button
                      onClick={this.handleLogIn}
                      type="button"
                      className="button-default button-olive"
                    >
                      Login
                    </button>
                  )}
                </div>
              </div>
              <div className="col-sm-7 col-7 d-block d-lg-none">
                <div className="mobile-menu mean-container">
                  <div className="mean-bar">
                    <a
                      className="meanmenu-reveal meanclose cross cross_show"
                      style={this.props.themeColor}
                    >
                      X
                    </a>
                    <a className="cross_hide">
                      <span></span>
                      <span></span>
                      <span></span>
                    </a>
                    <nav className="mobile-mean-nav mean-nav">
                      <ul>
                        {this.props.cmsSection &&
                        this.props.cmsSection.length > 0
                          ? Object.keys(this.props.cmsSection).map((val, i) =>
                              this.headerSection(this.props.cmsSection[val]) ? (
                                <li>
                                  <CmsSet
                                    sectionBackgroundArray={
                                      sectionBackgroundColorArray
                                    }
                                    htmlProp={
                                      this.props.cmsArray[
                                        this.props.cmsSection[val]["key"]
                                      ]
                                    }
                                    sectionKey={this.props[0]}
                                    href={
                                      "#" +
                                      this.props.cmsSection[val]["key"] +
                                      "_"
                                    }
                                    isNav={true}
                                    getData={
                                      this.props.getData
                                        ? this.props.getData
                                        : null
                                    }
                                  />
                                 
                                </li>
                              ) : (
                                ""
                              )
                            )
                          : ""}
                      </ul>
                    </nav>
                    <div
                      className={
                        this.props.cmsEnable
                          ? "row check_cms button-default button-blue"
                          : "row check_cms button-default button-red"
                      }
                    >
                      {localStorage.getItem("isLogin") ? (
                        <UncontrolledButtonDropdown>
                          <DropdownToggle caret color="white" tag="a">
                            {this.props.cmsEnable ? (
                              <b style={{ color: "white" }}> Settings </b>
                            ) : (
                              <b style={{ color: "white" }}> Settings </b>
                            )}
                          </DropdownToggle>
                          <DropdownMenu>
                            <DropdownItem onClick={() => this.CmsFunction()}>
                              {this.props.cmsEnable ? (
                                <p style={{ color: "blue" }}> CMS </p>
                              ) : (
                                <p style={{ color: "red" }}> CMS </p>
                              )}
                            </DropdownItem>
                            <DropdownItem onClick={this.handleLogOut}>
                              Logout
                            </DropdownItem>
                          </DropdownMenu>
                        </UncontrolledButtonDropdown>
                      ) : (
                        <button
                          onClick={this.handleLogIn}
                          type="button"
                          className="button-default button-olive"
                        >
                          Login
                        </button>
                      )}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => ({
  cmsArray: state.ThemeOptions.cmsArray,
  cmsModel: state.ThemeOptions.cmsModel,
  cmsKey: state.ThemeOptions.cmsKey,
  cmsValue: state.ThemeOptions.cmsValue,
  themeColor: state.ThemeOptions.themeColor,
  themeBackgroundColor: state.ThemeOptions.themeBackgroundColor,
  editorState: state.ThemeOptions.editorState,
  cmsEnable: state.ThemeOptions.cmsEnable,
  cmsEnableUploadImage: state.ThemeOptions.cmsEnableUploadImage,
  cmsModelImage: state.ThemeOptions.cmsModelImage,
  cmsEnableBackground: state.ThemeOptions.cmsEnableBackground,
  themeSectionBackgroundColor: state.ThemeOptions.themeSectionBackgroundColor,
  sectionBackgroundColor: state.ThemeOptions.sectionBackgroundColor,
  cmsSection: state.ThemeOptions.cmsSection,
  cmsImageArray: state.ThemeOptions.cmsImageArray,
  sectionId: state.ThemeOptions.sectionId,
  backgroundCmsKey: state.ThemeOptions.backgroundCmsKey,
  iconModel: state.ThemeOptions.iconModel,
  showIcon: state.ThemeOptions.showIcon,
  logoIcon: state.ThemeOptions.logoIcon,
});
const mapDispatchToProps = (dispatch) => ({
  setCmsArray: (enable) => dispatch(setCmsArray(enable)),
  setCmsModel: (enable) => dispatch(setCmsModel(enable)),
  setCmsKey: (enable) => dispatch(setCmsKey(enable)),
  setCmsValue: (enable) => dispatch(setCmsValue(enable)),
  setThemeColor: (enable) => dispatch(setThemeColor(enable)),
  setThemeBackgroundColor: (enable) =>
    dispatch(setThemeBackgroundColor(enable)),
  setEditorState: (enable) => dispatch(setEditorState(enable)),
  setCmsEnable: (enable) => dispatch(setCmsEnable(enable)),
  setCmsEnableUploadImage: (enable) =>
    dispatch(setCmsEnableUploadImage(enable)),
  setCmsModelImage: (enable) => dispatch(setCmsModelImage(enable)),
  setCmsEnableBackground: (enable) => dispatch(setCmsEnableBackground(enable)),
  setThemeSectionBackgroundColor: (enable) =>
    dispatch(setThemeSectionBackgroundColor(enable)),
  setSectionBackgroundColor: (enable) =>
    dispatch(setSectionBackgroundColor(enable)),
  setCmsImageArray: (enable) => dispatch(setCmsImageArray(enable)),
  setCmsSection: (enable) => dispatch(setCmsSection(enable)),
  setSectionId: (enable) => dispatch(setSectionId(enable)),
  setBackgroundCmsKey: (enable) => dispatch(setBackgroundCmsKey(enable)),
  setIconModel: (enable) => dispatch(setIconModel(enable)),
  setShowIcon: (enable) => dispatch(setShowIcon(enable)),
  setLogoIcon: (enable) => dispatch(setLogoIcon(enable)),
});
export default connect(mapStateToProps, mapDispatchToProps)(Navbar);
