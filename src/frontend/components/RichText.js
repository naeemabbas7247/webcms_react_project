import React, { Component } from "react";
import { useDispatch } from "react-redux";
import "../Placeholders.css"
import {
  setCmsArray,
  setCmsModel,
  setCmsKey,
  setCmsValue,
  setThemeBackgroundColor,
  setThemeColor,
  setEditorState,
  setCmsEnable,
  setCmsEnableUploadImage,
  setCmsModelImage,
  setAddModel,
  setCurrentSection,
  setAddModelData,
  setCmsSection,
  setEditSectionModel,
  setEditSectionModelEnable,
  setEditSectionModelArray,
  setSectionBackgroundColor,
  setAddNewCmsFlag,
  setCmsKeyInner,
  setBackgroundCmsKey,
  setSectionId,
  setPlaceholderValue,
  setDynamicPlaceholderValue,
  setTypeId,
  setFontSize,
  setInlineStyle,
  setRichTextArray
} from "../../../reducers/ThemeOptions";
import ApiCall from "../../../admin/Partial/Function/ApiCall";
import { connect } from "react-redux";
import {
  UncontrolledButtonDropdown,
  DropdownToggle,
  Dropdown,
  DropdownMenu,
  DropdownItem,
} from "reactstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faItalic,faBold,faUnderline} from "@fortawesome/free-solid-svg-icons";
let text=null;
class RichText extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fontSizeArray: [6,8,12,16,18,24,30,40,60,80],
      selectFont:16,
      text:null,
      style:{},
      status:false,
      is_blod:false,
      is_italic:false,
      open:false,
      is_underline:false,
      font:{
        'fontSize':16
      }
    }
  }
  handleChange=async(event)=>{
    console.log(this.props);
    text=event.target.value;
    this.handleSetValue(text);
  }
  handleSetValue=async(text)=>
  {
    const { setCmsValue } = this.props;
    await setCmsValue(text);
    this.handleSetCmsValue();
  }
  handleBold=async()=>
  {
    await this.setState({ is_blod: !this.state.is_blod });
    var style={fontWeight: this.state.is_blod ? 'bold':'unset'};
    this.handleSetStyle(style);
  }
  handleItalic=async()=>
  {
    await this.setState({ is_italic: !this.state.is_italic });
    var style={fontStyle:this.state.is_italic ? 'italic':'unset'};
    this.handleSetStyle(style);
  }
  handleUnderline=async()=>
  {
    await this.setState({ is_underline: !this.state.is_underline });
    var style={textDecorationLine:this.state.is_underline ? 'underline':'unset'};
    this.handleSetStyle(style);
  }
  handleFontSize=async(font)=>
  {
    var style={'fontSize':font};
    this.setState({font: style });
    this.setState({ selectFont: font });
    this.handleSetFont(font);
  }
  handleSetFont=async(font)=>
  { 
    const { setFontSize } = this.props;
    setFontSize(font);
  }
  handleSetInlineStyle=async(style)=>
  { 
    this.setState({ style: style });
    const { setInlineStyle } = this.props;
    setInlineStyle(style);
  }
  handleApiCallForInlineStyle=async()=>
  {
    this.setState({ status: true });
    var url='cms/inlineStyle/get';
    var data={
      key:this.props.cmsKeyInner
    };
    let styleData=await ApiCall.postAPICall(url, data);
    var style=JSON.parse(styleData.inline_style.inline_style);
    this.handleSetInlineStyle(style);
    this.handleFontSize(styleData.inline_style.font_size?styleData.inline_style.font_size:16);
  }
  componentDidUpdate(prevProps){
    if(this.props.cmsModel && !this.state.status)
    {
      this.handleApiCallForInlineStyle();
    }
  }
  handleSetStyle=(newStyle)=>
  {
    var style=this.state.style;
    style = {...style, ...newStyle }
    this.handleSetInlineStyle(style);
  }
  handleSetCmsValue=()=>
  {
    if(typeof(this.props.cmsValue) == "string"){
      return (this.props.cmsValue.replace(/<\/?span[^>]*>/g,""));
    } else {
      return "";
    }
   
  }
  openPlaceholderDropdown = () => this.setState({open: !this.state.open})
  addPlaceholder = (placeholder) => {  
    return placeholder;
  }
  listItem = this.props.placeholderValue.map(item => (
    <div>     
    <li 
      onClick={this.addPlaceholder.bind(this, ' %'+item.name+'% ')}  
      className="rdw-dropdownoption-default placeholder-li"
    >
      {item.value}</li>
    </div>
  ))
  listItem2 = this.props.dynamicPlaceholderValue.map(item => (
    <div>
    <li 
      onClick={this.addPlaceholder.bind(this, ' %'+item.value+'% ')}
      className="rdw-dropdownoption-default placeholder-li"
    >
      {item.value}</li>
    </div>
  )) 
  render() {
    return (
      <div>
        <div className="richtext-toolbar">
          <button type="button" className="richtext-btn" onClick={this.handleBold}>
            <FontAwesomeIcon  icon={faBold} size="1x" />
          </button>
          <button type="button" className="richtext-btn" onClick={this.handleItalic}>
            <FontAwesomeIcon  icon={faItalic} size="1x" />
          </button>
          <button type="button" className="richtext-btn" onClick={this.handleUnderline}>
            <FontAwesomeIcon icon={faUnderline} size="1x"/>
          </button>
          <UncontrolledButtonDropdown>
            <DropdownToggle caret className="richtext-dropdown" color=" ">
              {this.state.selectFont}
            </DropdownToggle>
            <DropdownMenu className="richtext-dropdown-select">
            {Object.keys(this.state.fontSizeArray).map((val, i) => (
              <DropdownItem onClick={() =>this.handleFontSize(this.state.fontSizeArray[i])}>
                {this.state.fontSizeArray[i]}
              </DropdownItem>
            ))}
            </DropdownMenu>
          </UncontrolledButtonDropdown>
          <div onClick={this.openPlaceholderDropdown} className="rdw-block-wrapper" aria-label="rdw-block-control">
        <div className="rdw-dropdown-wrapper rdw-block-dropdown" aria-label="rdw-dropdown">
          <div className="rdw-dropdown-selectedtext" title="Placeholders">
            <span>Placeholder</span> 
            <div className={`rdw-dropdown-caretto${this.state.open? "close": "open"}`}></div>
          </div>
          <ul className={`rdw-dropdown-optionwrapper ${this.state.open? "": "placeholder-ul"}`}>
          <b>Static</b>{this.listItem} 
          <b>Dynamic</b>{this.listItem2}
          </ul>
        </div>
      </div>
        </div>
        <div>
          <textarea className="richtext"  onKeyUp={this.handleChange} style={{...this.state.style,...this.state.font}}>
            {this.handleSetCmsValue()} 
          </textarea>
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => ({
  cmsArray: state.ThemeOptions.cmsArray,
  cmsModel: state.ThemeOptions.cmsModel,
  cmsKey: state.ThemeOptions.cmsKey,
  cmsValue: state.ThemeOptions.cmsValue,
  themeColor: state.ThemeOptions.themeColor,
  themeBackgroundColor: state.ThemeOptions.themeBackgroundColor,
  editorState: state.ThemeOptions.editorState,
  cmsEnable: state.ThemeOptions.cmsEnable,
  cmsEnableUploadImage: state.ThemeOptions.cmsEnableUploadImage,
  cmsModelImage: state.ThemeOptions.cmsModelImage,
  addModel: state.ThemeOptions.addModel,
  currentSection: state.ThemeOptions.currentSection,
  addModelData: state.ThemeOptions.addModelData,
  cmsSection: state.ThemeOptions.cmsSection,
  editSectionModel: state.ThemeOptions.editSectionModel,
  editSectionModelEnable: state.ThemeOptions.editSectionModelEnable,
  editSectionModelArray: state.ThemeOptions.editSectionModelArray,
  sectionBackgroundColor:state.ThemeOptions.sectionBackgroundColor,
  addNewCmsFlag:state.ThemeOptions.addNewCmsFlag,
  cmsKeyInner:state.ThemeOptions.cmsKeyInner,
  placeholderValue: state.ThemeOptions.placeholderValue,
  typeId: state.ThemeOptions.typeId,
  fontSize: state.ThemeOptions.fontSize,
  inlineStyle: state.ThemeOptions.inlineStyle,
  dynamicPlaceholderValue: state.ThemeOptions.dynamicPlaceholderValue,
  richTextArray: state.ThemeOptions.richTextArray,
});
const mapDispatchToProps = (dispatch) => ({
  setCmsArray: (enable) => dispatch(setCmsArray(enable)),
  setCmsModel: (enable) => dispatch(setCmsModel(enable)),
  setCmsKey: (enable) => dispatch(setCmsKey(enable)),
  setCmsValue: (enable) => dispatch(setCmsValue(enable)),
  setThemeColor: (enable) => dispatch(setThemeColor(enable)),
  setThemeBackgroundColor: (enable) =>
    dispatch(setThemeBackgroundColor(enable)),
  setEditorState: (enable) => dispatch(setEditorState(enable)),
  setCmsEnable: (enable) => dispatch(setCmsEnable(enable)),
  setCmsModelImage: (enable) => dispatch(setCmsModelImage(enable)),
  setCmsEnableUploadImage: (enable) =>
    dispatch(setCmsEnableUploadImage(enable)),
  setCmsSection: (enable) => dispatch(setCmsSection(enable)),
  setAddModel: (enable) => dispatch(setAddModel(enable)),
  setCurrentSection: (enable) => dispatch(setCurrentSection(enable)),
  setAddModelData: (enable) => dispatch(setAddModelData(enable)),
  setEditSectionModel: (enable) => dispatch(setEditSectionModel(enable)),
  setEditSectionModelEnable: (enable) => dispatch(setEditSectionModelEnable(enable)),
  setEditSectionModelArray: (enable) => dispatch(setEditSectionModelArray(enable)),
  setSectionBackgroundColor: enable => dispatch(setSectionBackgroundColor(enable)),
  setAddNewCmsFlag: enable => dispatch(setAddNewCmsFlag(enable)),
  setCmsKeyInner: enable => dispatch(setCmsKeyInner(enable)),
  setBackgroundCmsKey: enable => dispatch(setBackgroundCmsKey(enable)),
  setSectionId: enable => dispatch(setSectionId(enable)),
  setPlaceholderValue: (enable) => dispatch(setPlaceholderValue(enable)),
  setTypeId: (enable) => dispatch(setTypeId(enable)),
  setFontSize: (enable) => dispatch(setFontSize(enable)),
  setInlineStyle: (enable) => dispatch(setInlineStyle(enable)),
  setDynamicPlaceholderValue: (enable) => dispatch(setDynamicPlaceholderValue(enable)),
  setRichTextArray: (enable) => dispatch(setRichTextArray(enable)),
});
export default connect(mapStateToProps, mapDispatchToProps)(RichText);
