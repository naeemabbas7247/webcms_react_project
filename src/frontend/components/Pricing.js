import React, { Component } from "react";
import {
  setCmsArray,
  setCmsModel,
  setCmsKey,
  setCmsValue,
  setThemeBackgroundColor,
  setThemeColor,
  setEditorState,
  setCmsEnable,
  setModuleCmsObject,
  setCmsEnableBackground,
  setSectionBackgroundColor,
  setThemeSectionBackgroundColor,
  setIconModel,
  setShowIcon,
  setLogoIcon,
} from "../../reducers/ThemeOptions";
import CloneIcon from "./CloneIcon.js";
import CmsSet from "./Functions/CmsSet.js";
import Icon from "./Icon.js";
import { connect } from "react-redux";
import {
  EditorState,
  ContentState,
  convertFromHTML,
  convertFromRaw,
} from "draft-js";
import { Editor } from "react-draft-wysiwyg";
import htmlToDraft from "html-to-draftjs";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimes } from "@fortawesome/free-solid-svg-icons";
class Pricing extends Component {
  constructor(props) {
    super(props);
    this.state = {
      module_key: "pricing",
      imageLogo: "",
      imageLogo2: "",
      count: 1,
      addMessage: "add-text-",
      removeMessage: "remove-text-",
    };
  }
  SetCms = (key, section_id) => {
    if (this.props.cmsEnable) {
      var tempArray = {
        key: section_id + "pricing_background",
        value: this.props.cmsArray[section_id + "pricing_background"],
      };
      const { setSectionBackgroundColor } = this.props;
      setSectionBackgroundColor(tempArray);
      const contentBlock = htmlToDraft(this.props.cmsArray[section_id + key]);
      if (contentBlock) {
        const contentState = ContentState.createFromBlockArray(
          contentBlock.contentBlocks
        );
        const editorState = EditorState.createWithContent(contentState);
        const { setEditorState } = this.props;
        setEditorState(editorState);
      }
      const { cmsKey, setCmsKey } = this.props;
      setCmsKey(section_id + key);
      const { cmsValue, setCmsValue } = this.props;
      setCmsValue(this.props.cmsArray[section_id + key]);
      const { cmsModel, setCmsModel } = this.props;
      setCmsModel(!this.props.cmsModel);
    }
  };
  SetImage = async (key, section_id) => {
    if (this.props.cmsEnable) {
      var tempArray = {
        key: section_id + "pricing_background",
        value: this.props.cmsArray[section_id + "pricing_background"],
      };
      const { setSectionBackgroundColor } = this.props;
      setSectionBackgroundColor(tempArray);
      const { cmsModelImage, setCmsModelImage } = this.props;
      setCmsModelImage(!this.props.cmsModelImage);
      const { cmsEnableUploadImage, setCmsEnableUploadImage } = this.props;
      setCmsEnableUploadImage(!this.props.cmsEnableUploadImage);
      const { cmsKey, setCmsKey } = this.props;
      const { cmsValue, setCmsValue } = this.props;
      setCmsValue(this.props.cmsArray[section_id + key]);
      setCmsKey(section_id + key);
    }
  };
  SetIcon = async (key, section_id) => {
    if (this.props.cmsEnable) {
      var tempArray = {
        key: section_id + "pricing_background",
        value: this.props.cmsArray[section_id + "pricing_background"],
      };

      const { setSectionBackgroundColor } = this.props;
      setSectionBackgroundColor(tempArray);

      const { iconModel, setIconModel } = this.props;
      setIconModel(!this.props.iconModel);

      const { cmsKey, setCmsKey } = this.props;
      const { cmsValue, setCmsValue } = this.props;
      setCmsValue(this.props.cmsArray[key]);
      setCmsKey(key);
    }
  };
  pricingIcon(section_id) {
    return (
      <div>
        {this.props.cmsArray[section_id + "pricing_icon"] ? (
          this.props.cmsArray[section_id + "pricing_icon"][0]?.cms_page
            ?.is_icon == 0 ? (
            <img
              style={this.props.cmsArray[section_id + "pricing_iconstyle"]}
              onClick={() =>
                this.SetIcon(section_id + "pricing_icon", section_id)
              }
              className="image-1"
              src={
                this.props.cmsArray[section_id + "pricing_icon"]
                  ? this.props.cmsArray[section_id + "pricing_icon"][0]
                      ?.images_backups?.image
                  : ""
              }
              controls
              width="5%"
              height="5%"
            />
          ) : this.props.cmsArray[section_id + "pricing_icon"][0]?.cms_page
          ?.is_icon == 1 ? (
            <i
              style={this.props.cmsArray[section_id + "pricing_iconstyle"]}
              onClick={() =>
                this.SetIcon(section_id + "pricing_icon", section_id)
              }
              className={
                this.props.cmsArray[section_id + "pricing_icon"][0]
                  ?.images_backups?.image
              }
            ></i>
          ) : (
            <img
              style={{ position: "relative", height: "3%", width: "3%" }}
              onClick={() =>
                this.SetIcon(section_id + "pricing_icon", section_id)
              }
              className="image-1"
              src={"../../assets/images/app/mobile-2.png"}
              alt="App Landing"
            />
          )
        ) : (
          <img
            style={{ position: "relative", height: "3%", width: "3%" }}
            onClick={() =>
              this.SetIcon(section_id + "pricing_icon", section_id)
            }
            className="image-1"
            src={"../../assets/images/app/mobile-2.png"}
            alt="App Landing"
          />
        )}
      </div>
    );
  }
  componentDidUpdate(prevProps) {
    var section_id = this.props[0] + "_";
    var newArray2 = this.props.cmsArray[section_id + "pricing_icon"];
  }
  render() {
    let key_id = this.props[0];
    let section_id = key_id + "_";
    let count = 0;
    var sectionBackgroundColorArray = {
      key: section_id + "pricing_background",
      value: this.props.cmsArray[section_id + "pricing_background"]
        ? this.props.cmsArray[section_id + "pricing_background"][
            section_id + "pricing_background"
          ]
        : null,
    };
    return (
      <div
        className={`pricing-table-area pt--40 pt_sm--100 ${this.props.horizontalpricing}`}
        id={section_id}
        style={{
          backgroundColor: this.props.cmsArray[
            section_id + "pricing_background"
          ]
            ? this.props.cmsArray[section_id + "pricing_background"][
                section_id + "pricing_background"
              ]
            : "red",
        }}
      >
        <CloneIcon
          current_section={this.props.current_section}
          class_set={this.props.class_set}
          section_flag={this.props.section_flag}
          {...[key_id]}
          getData={this.props.getData ? this.props.getData : null}
          getColors={this.props.getColors ? this.props.getColors : null}
          module={this.props.module}
          addable={true}
        />
        <div className="container">
          <div className="row">
            <div className="col-lg-12">
              <div className="section-title text-center mb--40">
                <CmsSet
                  sectionBackgroundArray={sectionBackgroundColorArray}
                  styleProp={
                    this.props.cmsArray[section_id + "pricing_headingstyle"]
                  }
                  htmlProp={this.props.cmsArray[section_id + "pricing_heading"]}
                  sectionKey={section_id}
                  sectionId={"pricing_heading"}
                  getData={this.props.getData ? this.props.getData : null}
                  addMessage={this.state.addMessage}
                  removeMessage={this.state.removeMessage}
                />
                {this.pricingIcon(section_id)}
                <CmsSet
                  sectionBackgroundArray={sectionBackgroundColorArray}
                  styleProp={
                    this.props.cmsArray[section_id + "pricing_pstylestyle"]
                  }
                  htmlProp={this.props.cmsArray[section_id + "pricing_p"]}
                  sectionKey={section_id}
                  sectionId={"pricing_p"}
                  getData={this.props.getData ? this.props.getData : null}
                  addMessage={this.state.addMessage}
                  removeMessage={this.state.removeMessage}
                />
              </div>
            </div>
          </div>
          <div className="row mt--30">
            {Object.keys(this.props.moduleCmsObject).length != 0
              ? Object.keys(
                  this.props.moduleCmsObject[this.state.module_key]
                ).map((val, i) => (
                  <div
                    className="col-lg-4 col-md-6 col-12 pricing-column mt--40"
                    id={i % 2}
                    key={++count}
                    style={{
                      display: this.props.cmsArray[
                        section_id +
                          this.state.module_key +
                          "_title_" +
                          (i + this.state.count)
                      ]
                        ? "block"
                        : "none",
                    }}
                  >
                    <div
                      className="pull-right mr-2 cloneIcon text-danger mt-2"
                      style={{
                        display: this.props.cmsEnable ? "block" : "none",
                      }}
                      onClick={() =>
                        this.props.sectionDelete(key_id, i + this.state.count)
                      }
                    >
                      <FontAwesomeIcon icon={faTimes} size="2x" />
                    </div>
                    <div className="single-price-package">
                      <div className="price-title">
                        <h3>
                          <CmsSet
                            sectionBackgroundArray={sectionBackgroundColorArray}
                            styleProp={
                              this.props.cmsArray[
                                section_id +
                                  this.state.module_key +
                                  "_title_" +
                                  (i + this.state.count) +
                                  "style"
                              ]
                            }
                            htmlProp={
                              this.props.cmsArray[
                                section_id +
                                  this.state.module_key +
                                  "_title_" +
                                  (i + this.state.count)
                              ]
                            }
                            sectionKey={section_id}
                            backgroundKey={
                              "pricing_" + (i + this.state.count) + "_global"
                            }
                            sectionId={
                              this.state.module_key +
                              "_title_" +
                              (i + this.state.count)
                            }
                            getData={
                              this.props.getData ? this.props.getData : null
                            }
                            addMessage={this.state.addMessage}
                            removeMessage={this.state.removeMessage}
                          />
                        </h3>
                        <div className="price mt-2">
                          <span className="text-top d-inline-block">
                            <CmsSet
                              sectionBackgroundArray={
                                sectionBackgroundColorArray
                              }
                              styleProp={
                                this.props.cmsArray[
                                  section_id +
                                    this.state.module_key +
                                    "_icon_" +
                                    (i + this.state.count) +
                                    "style"
                                ]
                              }
                              htmlProp={
                                this.props.cmsArray[
                                  section_id +
                                    this.state.module_key +
                                    "_icon_" +
                                    (i + this.state.count)
                                ]
                              }
                              sectionKey={section_id}
                              backgroundKey={
                                "pricing_" + (i + this.state.count) + "_global"
                              }
                              sectionId={
                                this.state.module_key +
                                "_icon_" +
                                (i + this.state.count)
                              }
                              getData={
                                this.props.getData ? this.props.getData : null
                              }
                              addMessage={this.state.addMessage}
                              removeMessage={this.state.removeMessage}
                            />
                          </span>
                          <span className="text-large d-inline-block">
                            <CmsSet
                              sectionBackgroundArray={
                                sectionBackgroundColorArray
                              }
                              styleProp={
                                this.props.cmsArray[
                                  section_id +
                                    this.state.module_key +
                                    "_money_" +
                                    (i + this.state.count) +
                                    "style"
                                ]
                              }
                              htmlProp={
                                this.props.cmsArray[
                                  section_id +
                                    this.state.module_key +
                                    "_money_" +
                                    (i + this.state.count)
                                ]
                              }
                              sectionKey={section_id}
                              backgroundKey={
                                "pricing_" + (i + this.state.count) + "_global"
                              }
                              sectionId={
                                this.state.module_key +
                                "_money_" +
                                (i + this.state.count)
                              }
                              getData={
                                this.props.getData ? this.props.getData : null
                              }
                              addMessage={this.state.addMessage}
                              removeMessage={this.state.removeMessage}
                            />
                          </span>
                          <span className="text-bottom d-inline-block">
                            <CmsSet
                              sectionBackgroundArray={
                                sectionBackgroundColorArray
                              }
                              styleProp={
                                this.props.cmsArray[
                                  section_id +
                                    this.state.module_key +
                                    "_type_" +
                                    (i + this.state.count) +
                                    "style"
                                ]
                              }
                              htmlProp={
                                this.props.cmsArray[
                                  section_id +
                                    this.state.module_key +
                                    "_type_" +
                                    (i + this.state.count)
                                ]
                              }
                              sectionKey={section_id}
                              backgroundKey={
                                "pricing_" + (i + this.state.count) + "_global"
                              }
                              sectionId={
                                this.state.module_key +
                                "_type_" +
                                (i + this.state.count)
                              }
                              getData={
                                this.props.getData ? this.props.getData : null
                              }
                              addMessage={this.state.addMessage}
                              removeMessage={this.state.removeMessage}
                            />
                          </span>
                        </div>
                      </div>
                      <div className="price-list">
                        <CmsSet
                          sectionBackgroundArray={sectionBackgroundColorArray}
                          styleProp={
                            this.props.cmsArray[
                              section_id +
                                this.state.module_key +
                                "_body_" +
                                (i + this.state.count) +
                                "style"
                            ]
                          }
                          htmlProp={
                            this.props.cmsArray[
                              section_id +
                                this.state.module_key +
                                "_body_" +
                                (i + this.state.count)
                            ]
                          }
                          sectionKey={section_id}
                          backgroundKey={
                            "pricing_" + (i + this.state.count) + "_global"
                          }
                          sectionId={
                            this.state.module_key +
                            "_body_" +
                            (i + this.state.count)
                          }
                          getData={
                            this.props.getData ? this.props.getData : null
                          }
                          addMessage={this.state.addMessage}
                          removeMessage={this.state.removeMessage}
                        />
                        <div className="price-btn">
                          <button className="button button-default">
                            <CmsSet
                              sectionBackgroundArray={
                                sectionBackgroundColorArray
                              }
                              styleProp={
                                this.props.cmsArray[
                                  section_id +
                                    this.state.module_key +
                                    "_btn_" +
                                    (i + this.state.count) +
                                    "style"
                                ]
                              }
                              htmlProp={
                                this.props.cmsArray[
                                  section_id +
                                    this.state.module_key +
                                    "_btn_" +
                                    (i + this.state.count)
                                ]
                              }
                              sectionKey={section_id}
                              backgroundKey={
                                "pricing_" + (i + this.state.count) + "_global"
                              }
                              sectionId={
                                this.state.module_key +
                                "_btn_" +
                                (i + this.state.count)
                              }
                              getData={
                                this.props.getData ? this.props.getData : null
                              }
                              addMessage={this.state.addMessage}
                              removeMessage={this.state.removeMessage}
                            />
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                ))
              : ""}
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => ({
  cmsArray: state.ThemeOptions.cmsArray,
  cmsModel: state.ThemeOptions.cmsModel,
  cmsKey: state.ThemeOptions.cmsKey,
  cmsValue: state.ThemeOptions.cmsValue,
  themeColor: state.ThemeOptions.themeColor,
  themeBackgroundColor: state.ThemeOptions.themeBackgroundColor,
  editorState: state.ThemeOptions.editorState,
  cmsEnable: state.ThemeOptions.cmsEnable,
  moduleCmsObject: state.ThemeOptions.moduleCmsObject,
  sectionBackgroundColor: state.ThemeOptions.sectionBackgroundColor,
  themeSectionBackgroundColor: state.ThemeOptions.themeSectionBackgroundColor,
  iconModel: state.ThemeOptions.iconModel,
  showIcon: state.ThemeOptions.showIcon,
  logoIcon: state.ThemeOptions.logoIcon,
});
const mapDispatchToProps = (dispatch) => ({
  setCmsArray: (enable) => dispatch(setCmsArray(enable)),
  setCmsModel: (enable) => dispatch(setCmsModel(enable)),
  setCmsKey: (enable) => dispatch(setCmsKey(enable)),
  setCmsValue: (enable) => dispatch(setCmsValue(enable)),
  setThemeColor: (enable) => dispatch(setThemeColor(enable)),
  setThemeBackgroundColor: (enable) =>
    dispatch(setThemeBackgroundColor(enable)),
  setEditorState: (enable) => dispatch(setEditorState(enable)),
  setCmsEnable: (enable) => dispatch(setCmsEnable(enable)),
  setModuleCmsObject: (enable) => dispatch(setModuleCmsObject(enable)),
  setSectionBackgroundColor: (enable) =>
    dispatch(setSectionBackgroundColor(enable)),
  setThemeSectionBackgroundColor: (enable) =>
    dispatch(setThemeSectionBackgroundColor(enable)),
  setIconModel: (enable) => dispatch(setIconModel(enable)),
  setShowIcon: (enable) => dispatch(setShowIcon(enable)),
  setLogoIcon: (enable) => dispatch(setLogoIcon(enable)),
});
export default connect(mapStateToProps, mapDispatchToProps)(Pricing);
