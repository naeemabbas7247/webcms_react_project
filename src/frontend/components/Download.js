import React, { Component } from "react";
import ReactDOM from "react-dom";
import {
  setCmsArray,
  setCmsModel,
  setCmsKey,
  setCmsValue,
  setThemeBackgroundColor,
  setThemeColor,
  setEditorState,
  setCmsEnable,
  setSectionBackgroundColor,
  setCmsEnableBackground,
  setThemeSectionBackgroundColor,
  setCmsEnableUploadImage,
  setCmsModelImage,
  setIconModel,
  setShowIcon,
  setLogoIcon,
} from "../../reducers/ThemeOptions";
import Icon from "./Icon.js";
import { connect } from "react-redux";
import CmsSet from "./Functions/CmsSet.js";
import CloneIcon from "./CloneIcon.js";
import {
  EditorState,
  ContentState,
  convertFromHTML,
  convertFromRaw,
} from "draft-js";
import { Editor } from "react-draft-wysiwyg";
import htmlToDraft from "html-to-draftjs";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import { faCogs } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
class Download extends Component {
  constructor(props) {
    super(props);
    this.state = {
      imageLogo: "",
      imageLogo2: "",
      addMessage: "add-text-",
      removeMessage: "remove-text-",
      addClone: "clone-it-",
      arrowUpMessage: "move-to-top",
    };
  }
  SetCms = (key, section_id) => {
    if (this.props.cmsEnable) {
      var tempArray = {
        key: section_id + "download_background",
        value: this.props.cmsArray[section_id + "download_background"],
      };
      const { setSectionBackgroundColor } = this.props;
      setSectionBackgroundColor(tempArray);
      const contentBlock = htmlToDraft(this.props.cmsArray[section_id + key]);
      if (contentBlock) {
        const contentState = ContentState.createFromBlockArray(
          contentBlock.contentBlocks
        );
        const editorState = EditorState.createWithContent(contentState);
        const { setEditorState } = this.props;
        setEditorState(editorState);
      }
      const { cmsKey, setCmsKey } = this.props;
      setCmsKey(section_id + key);
      const { cmsValue, setCmsValue } = this.props;
      setCmsValue(this.props.cmsArray[section_id + key]);
      const { cmsModel, setCmsModel } = this.props;
      setCmsModel(!this.props.cmsModel);
    }
  };
  SetImage = async (key, section_id) => {
    if (this.props.cmsEnable) {
      var tempArray = {
        key: section_id + "download_background",
        value: this.props.cmsArray[section_id + "download_background"],
      };
      const { setSectionBackgroundColor } = this.props;
      setSectionBackgroundColor(tempArray);
      const { cmsModelImage, setCmsModelImage } = this.props;
      setCmsModelImage(!this.props.cmsModelImage);

      const { cmsEnableUploadImage, setCmsEnableUploadImage } = this.props;
      setCmsEnableUploadImage(!this.props.cmsEnableUploadImage);
      const { cmsKey, setCmsKey } = this.props;
      const { cmsValue, setCmsValue } = this.props;
      setCmsValue(this.props.cmsArray[section_id + key]);
      setCmsKey(section_id + key);
    }
  };
  SetIcon = async (key, section_id) => {
    if (this.props.cmsEnable) {
      var tempArray = {
        key: section_id + "banner_background",
        value: this.props.cmsArray[section_id + "banner_background"],
      };
      const { setSectionBackgroundColor } = this.props;
      setSectionBackgroundColor(tempArray);

      const { iconModel, setIconModel } = this.props;
      setIconModel(!this.props.iconModel);

      const { cmsEnableUploadImage, setCmsEnableUploadImage } = this.props;
      setCmsEnableUploadImage(!this.props.cmsEnableUploadImage);
      const { cmsKey, setCmsKey } = this.props;
      const { cmsValue, setCmsValue } = this.props;
      setCmsValue(this.props.cmsArray[key]);
      setCmsKey(key);
    }
  };
  downloadIcon(section_id) {
    return (
      <div>
        {this.props.cmsArray[section_id + "download_icon"] ? (
          this.props.cmsArray[section_id + "download_icon"][0]?.cms_page
            ?.is_icon == 0 ? (
            <img
              style={this.props.cmsArray[section_id + "download_iconstyle"]}
              onClick={() =>
                this.SetIcon(section_id + "download_icon", section_id)
              }
              className="image-1"
              src={
                this.props.cmsArray[section_id + "download_icon"]
                  ? this.props.cmsArray[section_id + "download_icon"][0]
                      ?.images_backups?.image
                  : ""
              }
              controls
              width="5%"
              height="5%"
            />
          ) : this.props.cmsArray[section_id + "download_icon"][0]?.cms_page
              ?.is_icon == 1 ? (
            <i
              style={this.props.cmsArray[section_id + "download_iconstyle"]}
              onClick={() =>
                this.SetIcon(section_id + "download_icon", section_id)
              }
              className={
                this.props.cmsArray[section_id + "download_icon"][0]
                  ?.images_backups?.image
              }
            ></i>
          ) : (
            <img
              style={{ position: "relative", height: "3%", width: "3%" }}
              onClick={() =>
                this.SetIcon(section_id + "download_icon", section_id)
              }
              className="image-1"
              src={"../../assets/images/app/mobile-2.png"}
              alt="App Landing"
            />
          )
        ) : (
          <img
            style={{ position: "relative", height: "3%", width: "3%" }}
            onClick={() =>
              this.SetIcon(section_id + "download_icon", section_id)
            }
            className="image-1"
            src={"../../assets/images/app/mobile-2.png"}
            alt="App Landing"
          />
        )}
      </div>
    );
  }
  componentDidUpdate(prevProps) {
    var section_id = this.props[0] + "_";
    var newArray2 = this.props.cmsArray[section_id + "download_icon"];
  }

  render() {
    let key_id = this.props[0];
    let section_id = key_id + "_";
    var sectionBackgroundColorArray = {
      key: section_id + "download_background",
      value: this.props.cmsArray[section_id + "download_background"]
        ? this.props.cmsArray[section_id + "download_background"][
            section_id + "download_background"
          ]
        : null,
    };
    return (
      <div className={`download-area ${this.props.class_set}`}>
        <CloneIcon
          class_set={this.props.class_set}
          current_section={this.props.current_section}
          section_flag={this.props.section_flag}
          {...[key_id]}
          getData={this.props.getData ? this.props.getData : null}
          getColors={this.props.getColors ? this.props.getColors : null}
          arrowUpMessage={this.state.arrowUpMessage}
        />
        <div
          className="bg-overlay"
          style={{
            backgroundColor: this.props.cmsArray[
              section_id + "download_background"
            ]
              ? this.props.cmsArray[section_id + "download_background"][
                  section_id + "download_background"
                ]
              : "",
          }}
        ></div>
        <div className="container">
          <div className="row">
            <div className="col-lg-12">
              <div className="section-title text-center mb--55 pt--200">
                <div className="text-white">
                  <CmsSet
                    sectionBackgroundArray={sectionBackgroundColorArray}
                    styleProp={
                      this.props.cmsArray[section_id + "download_headingstyle"]
                    }
                    htmlProp={
                      this.props.cmsArray[section_id + "download_heading"]
                    }
                    sectionKey={section_id}
                    sectionId={"download_heading"}
                    getData={this.props.getData ? this.props.getData : null}
                    addMessage={this.state.addMessage}
                    removeMessage={this.state.removeMessage}
                  />
                </div>
                {this.downloadIcon(section_id)}
                <div className="text-white">
                  <CmsSet
                    sectionBackgroundArray={sectionBackgroundColorArray}
                    styleProp={
                      this.props.cmsArray[section_id + "download_pstyle"]
                    }
                    htmlProp={this.props.cmsArray[section_id + "download_p"]}
                    sectionKey={section_id}
                    sectionId={"download_p"}
                    getData={this.props.getData ? this.props.getData : null}
                    addMessage={this.state.addMessage}
                    removeMessage={this.state.removeMessage}
                  />
                </div>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-12">
              <div className="download-buttons pb--50">
                <button type="submit" className="download-btn">
                  <i className="zmdi zmdi-apple"></i>
                  <CmsSet
                    sectionBackgroundArray={sectionBackgroundColorArray}
                    styleProp={
                      this.props.cmsArray[section_id + "download_btn_1style"]
                    }
                    htmlProp={
                      this.props.cmsArray[section_id + "download_btn_1"]
                    }
                    sectionKey={section_id}
                    sectionId={"download_btn_1"}
                    getData={this.props.getData ? this.props.getData : null}
                    addMessage={this.state.addMessage}
                    removeMessage={this.state.removeMessage}
                  />
                </button>
                <button type="submit" className="download-btn">
                  <i className="zmdi zmdi-google-play"></i>
                  <CmsSet
                    sectionBackgroundArray={sectionBackgroundColorArray}
                    styleProp={
                      this.props.cmsArray[section_id + "download_btn_2style"]
                    }
                    htmlProp={
                      this.props.cmsArray[section_id + "download_btn_2"]
                    }
                    sectionKey={section_id}
                    sectionId={"download_btn_2"}
                    getData={this.props.getData ? this.props.getData : null}
                    addMessage={this.state.addMessage}
                    removeMessage={this.state.removeMessage}
                  />
                </button>
                <button type="submit" className="download-btn">
                  <i className="zmdi zmdi-windows"></i>
                  <CmsSet
                    sectionBackgroundArray={sectionBackgroundColorArray}
                    styleProp={
                      this.props.cmsArray[section_id + "download_btn_3style"]
                    }
                    htmlProp={
                      this.props.cmsArray[section_id + "download_btn_3"]
                    }
                    sectionKey={section_id}
                    sectionId={"download_btn_3"}
                    getData={this.props.getData ? this.props.getData : null}
                    addMessage={this.state.addMessage}
                    removeMessage={this.state.removeMessage}
                  />
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => ({
  cmsArray: state.ThemeOptions.cmsArray,
  cmsModel: state.ThemeOptions.cmsModel,
  cmsKey: state.ThemeOptions.cmsKey,
  cmsValue: state.ThemeOptions.cmsValue,
  themeColor: state.ThemeOptions.themeColor,
  themeBackgroundColor: state.ThemeOptions.themeBackgroundColor,
  editorState: state.ThemeOptions.editorState,
  cmsEnable: state.ThemeOptions.cmsEnable,
  cmsEnableBackground: state.ThemeOptions.cmsEnableBackground,
  sectionBackgroundColor: state.ThemeOptions.sectionBackgroundColor,
  themeSectionBackgroundColor: state.ThemeOptions.themeSectionBackgroundColor,
  cmsEnableUploadImage: state.ThemeOptions.cmsEnableUploadImage,
  iconModel: state.ThemeOptions.iconModel,
  cmsModelImage: state.ThemeOptions.cmsModelImage,
  showIcon: state.ThemeOptions.showIcon,
  logoIcon: state.ThemeOptions.logoIcon,
});
const mapDispatchToProps = (dispatch) => ({
  setCmsArray: (enable) => dispatch(setCmsArray(enable)),
  setCmsModel: (enable) => dispatch(setCmsModel(enable)),
  setCmsKey: (enable) => dispatch(setCmsKey(enable)),
  setCmsValue: (enable) => dispatch(setCmsValue(enable)),
  setThemeColor: (enable) => dispatch(setThemeColor(enable)),
  setThemeBackgroundColor: (enable) =>
    dispatch(setThemeBackgroundColor(enable)),
  setEditorState: (enable) => dispatch(setEditorState(enable)),
  setCmsEnable: (enable) => dispatch(setCmsEnable(enable)),
  setCmsEnableBackground: (enable) => dispatch(setCmsEnableBackground(enable)),
  setSectionBackgroundColor: (enable) =>
    dispatch(setSectionBackgroundColor(enable)),
  setThemeSectionBackgroundColor: (enable) =>
    dispatch(setThemeSectionBackgroundColor(enable)),
  setIconModel: (enable) => dispatch(setIconModel(enable)),
  setCmsModelImage: (enable) => dispatch(setCmsModelImage(enable)),
  setCmsEnableUploadImage: (enable) =>
    dispatch(setCmsEnableUploadImage(enable)),
  setShowIcon: (enable) => dispatch(setShowIcon(enable)),
  setLogoIcon: (enable) => dispatch(setLogoIcon(enable)),
});
export default connect(mapStateToProps, mapDispatchToProps)(Download);
