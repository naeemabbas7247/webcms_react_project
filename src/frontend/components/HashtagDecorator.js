import * as React from "react";
import { EditorState,
  ContentState,
  convertFromHTML,
  convertFromRaw,
  convertToRaw,
  CompositeDecorator,
} from "draft-js";
import { Editor } from "react-draft-wysiwyg";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";

const regexStratergy = (block: ContentBlock, callback: (start: number, end: number) => void) => {
  const text = block.getText();
  let result;
  let regex = /(^|\s)#\w+/g;
  while ((result = regex.exec(text)) != null) {
    let start = result.index;
    let end = start + result[0].length;
    callback(start, end);
  }
};

const regexComponent = props => <span style={{ backgroundColor: "lightgreen" }}>{props.children}</span>;

const compositeDecorator = new CompositeDecorator([
  {
    strategy: regexStratergy,
    component: regexComponent
  }
]);

export class HashtagDecorator extends React.Component<null, { editorState: EditorState }> {
  state = { editorState: EditorState.createEmpty(compositeDecorator) };

  editorStateChanged = (newEditorState: EditorState) => this.setState({ editorState: newEditorState });

  render() {
    return (
      <div>
        <div className="editor">
          <Editor 
          editorState={this.state.editorState} 
          onChange={this.editorStateChanged} 
          />
        </div>
      </div>
    );
  }
}