import React, { Component } from 'react';
import { useDispatch } from 'react-redux';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import { EditorState, Modifier, RichUtils,CompositeDecorator,AtomicBlockUtils,Entity } from 'draft-js';
import "./Placeholders.css"
import {
  setPlaceholderValue,
  setDynamicPlaceholderValue,
  setEditorState,
  setCmsValue,
} from "../../reducers/ThemeOptions";
const cDate = new Date();

class Placeholders extends Component {
  static propTypes = {
    onChange: PropTypes.func,
    editorState: PropTypes.object,
  }
  constructor(props) {
    var today = new Date(),
    time = today.getHours() + ':' + today.getMinutes() + ':' + today.getSeconds();
    super(props);
    this.state = {
    open: false, 
    date: new Date()
   }
  }
  callMe() {
    setInterval(() => {
      this.setState({ date: new Date() });
    }, 1000);
  }
  openPlaceholderDropdown = () => this.setState({open: !this.state.open})
  addPlaceholder = (placeholder) => {
    // const editorState = PropTypes.object;
    // const onChange = PropTypes.func;
    // const contentState = Modifier.replaceText(
    // editorState.getCurrentContent(),
    // editorState.getSelection(),
    // placeholder,
    // editorState.getCurrentInlineStyle(),
    // );
    // console.log(this.props.placeholderPosition);
    var cmsValue=this.props.cmsValue;
    var selectivePart=cmsValue.replace(/<\/?[^>]+(>|$)/g, "");
    selectivePart=selectivePart.replaceAll('-@-', '');
    // console.log(cmsValue);
    // console.log(selectivePart);
    var selectedText=window.getSelection().toString();
    // console.log(selectedText);
    var end=this.props.getTextPosition();
    var index=this.props.getTextIndex(selectivePart,end,selectedText);
    var tempArray=[];
    tempArray=cmsValue.split(" ");
    selectedText=selectedText.split(" ");
    // console.log(tempArray);
    // console.log(selectedText);
    // console.log(index);
    let flag=tempArray[index].includes(placeholder);
    if(placeholder.trim() === tempArray[index].trim())
    {
      tempArray[index]='';
      // tempArray[index]= tempArray[index] + placeholder;
    }
    else
    {
      tempArray[index]= tempArray[index] + placeholder;
    }
    const arrFiltered = tempArray.filter(el => {
      return el != null && el != '';
    });
    tempArray=arrFiltered.join(' ');
    // cmsValue=cmsValue.replace(/<\/?p[^>]*>/g,"");
    // cmsValue=cmsValue.slice(0,this.props.placeholderPosition)+ placeholder+cmsValue.slice(this.props.placeholderPosition);
    const { setCmsValue } = this.props;
    setCmsValue(tempArray);
    this.props.handleSetCmsValue(cmsValue);
    // onChange(EditorState.push(editorState, contentState, 'insert-characters'));
  }
  listItem2 = this.props.dynamicPlaceholderValue.map(item => (
    <div>
      <li 
        onClick={this.addPlaceholder.bind(this, ' {'+item.name+'} ')}
        className="rdw-dropdownoption-default placeholder-li"
      >
        {item.name}
      </li>
    </div>
  ))
  listItem = this.props.placeholderValue.map(item => (
    <div>
      <li 
        onClick={this.addPlaceholder.bind(this, ' {'+item.name+'} ')}  
        className="rdw-dropdownoption-default placeholder-li"
      >
      {item.name}</li>
    </div>
  ))
  render() {
    return ( 
      <div onClick={this.openPlaceholderDropdown} className="rdw-block-wrapper" aria-label="rdw-block-control">
        <div className="rdw-dropdown-wrapper rdw-block-dropdown" aria-label="rdw-dropdown">
          <div className="rdw-dropdown-selectedtext" title="Placeholders">
            <span>Placeholder</span> 
            <div className={`rdw-dropdown-caretto${this.state.open? "close": "open"}`}></div>
          </div>
          <ul className={`rdw-dropdown-optionwrapper ${this.state.open? "": "placeholder-ul"}`}>
          <b>Static</b>{this.listItem} 
          <b>Dynamic</b>{this.listItem2}
          </ul>
        </div>
      </div>  
    );
  }
}
const mapStateToProps = (state) => ({
  placeholderValue: state.ThemeOptions.placeholderValue,
  dynamicPlaceholderValue: state.ThemeOptions.dynamicPlaceholderValue,
  editorState: state.ThemeOptions.editorState,
  placeholderPosition:state.ThemeOptions.placeholderPosition,
  cmsValue:state.ThemeOptions.cmsValue,
});
const mapDispatchToProps = (dispatch) => ({
  setPlaceholderValue: (enable) => dispatch(setPlaceholderValue(enable)),
  setDynamicPlaceholderValue: (enable) => dispatch(setDynamicPlaceholderValue(enable)),
  setEditorState: (enable) => dispatch(setEditorState(enable)),
  setCmsValue: (enable) => dispatch(setCmsValue(enable)),
});
export default connect(mapStateToProps, mapDispatchToProps)(Placeholders);