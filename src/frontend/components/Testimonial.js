import React from "react";
import Slider from "react-slick"
import {testimonial, testimonial2} from "./script";
import { useDispatch } from 'react-redux';
import {
    setCmsArray,
    setCmsModel,
    setCmsKey,
    setCmsValue,
    setThemeBackgroundColor,
    setThemeColor,
    setEditorState,
    setCmsEnable
} from '../../reducers/ThemeOptions';
import Icon from './Icon.js';
import {connect} from 'react-redux';
import { EditorState, ContentState, convertFromHTML,convertFromRaw } from 'draft-js'
import { Editor } from 'react-draft-wysiwyg';
import htmlToDraft from 'html-to-draftjs';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
class Testimonial extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          nav1: null,
          nav2: null
        };
    }
    componentDidMount() {
        this.setState({
          nav1: this.testimonial,
          nav2: this.testimonial2
        });
    }
    SetCms=(key) => {
        if(this.props.cmsEnable)
        {
            const contentBlock = htmlToDraft(this.props.cmsArray[key]);
            if (contentBlock) {
                const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks);
                const editorState = EditorState.createWithContent(contentState);
                const {setEditorState } = this.props;
                setEditorState(editorState);
            }
            const { cmsKey, setCmsKey } = this.props;
            setCmsKey(key);
            const { cmsValue, setCmsValue } = this.props;
            setCmsValue(this.props.cmsArray[key]);
            const { cmsModel, setCmsModel } = this.props;
            setCmsModel(!this.props.cmsModel);
        }
    }
    render(){
        return(
            <div className="testimonial-wrapper pt--120 text-center" id="reviews">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-12">
                            <div className="tectimonial-activation">
                                <div className="section-title text-center mb--80">
                                    <span style={this.props.cmsArray['review_headingstyle']} onClick={() => this.SetCms('review_heading')} dangerouslySetInnerHTML={{__html: this.props.cmsArray['review_heading']}}/>
                                    <Icon/>
                                    <span style={this.props.cmsArray['review_pstyle']} onClick={() => this.SetCms('review_p')} dangerouslySetInnerHTML={{__html: this.props.cmsArray['review_p']}}/>
                                </div>
                                <div className="row">
                                    <div className="col-lg-8 offset-lg-2 col-xl-6 offset-xl-3">
                                        <Slider {...testimonial} asNavFor={this.state.nav2} ref={slider => (this.testimonial = slider)} className="testimonial-image-slider text-center">
                                            <div className="sin-testiImage">
                                                <img src="/assets/images/client/1.png" alt="testimonial 1" />
                                            </div>
                                            <div className="sin-testiImage">
                                                <img src="/assets/images/client/1.png" alt="testimonial 2" />
                                            </div>
                                            <div className="sin-testiImage">
                                                <img src="/assets/images/client/2.png" alt="testimonial 3" />
                                            </div>
                                            <div className="sin-testiImage">
                                                <img src="/assets/images/client/3.png" alt="testimonial 2" />
                                            </div>
                                            <div className="sin-testiImage">
                                                <img src="/assets/images/client/2.png" alt="testimonial 3" />
                                            </div>
                                        </Slider>
                                    </div>
                                </div>
                                <Slider {...testimonial2} asNavFor={this.state.nav1} ref={slider => (this.testimonial2 = slider)} className="testimonial-text-slider text-center">
                                    <div className="sin-testiText">
                                        M S Nawaz 
                                        <div className="client-rating">
                                            <i className="zmdi zmdi-star color"></i>
                                            <i className="zmdi zmdi-star color"></i>
                                            <i className="zmdi zmdi-star color"></i>
                                            <i className="zmdi zmdi-star color"></i>
                                            <i className="zmdi zmdi-star-half color"></i>
                                        </div>
                                        There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.
                                    </div>
                                    <div className="sin-testiText">
                                        Chowchilla Madera
                                        <div className="client-rating">
                                            <i className="zmdi zmdi-star color"></i>
                                            <i className="zmdi zmdi-star color"></i>
                                            <i className="zmdi zmdi-star color"></i>
                                            <i className="zmdi zmdi-star color"></i>
                                            <i className="zmdi zmdi-star-half color"></i>
                                        </div>
                                        Nam nec tellus a odio tincidunt This lorem is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean nisi sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum gravida.
                                    </div>
                                    <div className="sin-testiText">
                                        Kattie Luis
                                        <div className="client-rating">
                                            <i className="zmdi zmdi-star color"></i>
                                            <i className="zmdi zmdi-star color"></i>
                                            <i className="zmdi zmdi-star color"></i>
                                            <i className="zmdi zmdi-star color"></i>
                                            <i className="zmdi zmdi-star-half color"></i>
                                        </div>
                                        Nam nec tellus a odio tincidunt This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean tincidunt sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum Photoshop.
                                    </div>
                                    <div className="sin-testiText">
                                        Kattie Luis
                                        <div className="client-rating">
                                            <i className="zmdi zmdi-star color"></i>
                                            <i className="zmdi zmdi-star color"></i>
                                            <i className="zmdi zmdi-star color"></i>
                                            <i className="zmdi zmdi-star color"></i>
                                            <i className="zmdi zmdi-star-half color"></i>
                                        </div>
                                        Nam nec tellus a odio tincidunt This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean tincidunt sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum Photoshop.
                                    </div>
                                    <div className="sin-testiText">
                                        Kattie Luis
                                        <div className="client-rating">
                                            <i className="zmdi zmdi-star color"></i>
                                            <i className="zmdi zmdi-star color"></i>
                                            <i className="zmdi zmdi-star color"></i>
                                            <i className="zmdi zmdi-star color"></i>
                                            <i className="zmdi zmdi-star-half color"></i>
                                        </div>
                                        Nam nec tellus a odio tincidunt This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean tincidunt sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum Photoshop.
                                    </div>
                                </Slider>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
const mapStateToProps = state => ({
    cmsArray:state.ThemeOptions.cmsArray,
    cmsModel:state.ThemeOptions.cmsModel,
    cmsKey:state.ThemeOptions.cmsKey,
    cmsValue:state.ThemeOptions.cmsValue,
    themeColor:state.ThemeOptions.themeColor,
    themeBackgroundColor:state.ThemeOptions.themeBackgroundColor,
    editorState:state.ThemeOptions.editorState,
    cmsEnable:state.ThemeOptions.cmsEnable,
});
const mapDispatchToProps = dispatch => ({
    setCmsArray: enable => dispatch(setCmsArray(enable)),
    setCmsModel: enable => dispatch(setCmsModel(enable)),
    setCmsKey: enable => dispatch(setCmsKey(enable)),
    setCmsValue: enable => dispatch(setCmsValue(enable)),
    setThemeColor: enable => dispatch(setThemeColor(enable)),
    setThemeBackgroundColor: enable => dispatch(setThemeBackgroundColor(enable)),
    setEditorState: enable => dispatch(setEditorState(enable)),
    setCmsEnable: enable => dispatch(setCmsEnable(enable)),
});
export default connect(mapStateToProps, mapDispatchToProps)(Testimonial);







