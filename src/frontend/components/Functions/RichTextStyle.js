import React, { Component } from 'react';
import {
  setInlineStyle,
  setInlineStyleEnable,
  setRichTextArrayStyleSheet
} from "../../../reducers/ThemeOptions";
import { connect } from "react-redux";
class RichTextStyle extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open:false,
    }
  }
  handleInlineStyle=async(data)=>
  {
    // var style='style="color:'+data['fontColor']+';font-size:'+data['fontSize']+';font-family:'+data['fontFamily']+';font-Weight:'+data['fontWeight']+';line-height:'+data['lineHeight']+';"';
    const {setInlineStyle} = this.props;
    await setInlineStyle(data['name'].replace(/\s/g, ''));
    const {setInlineStyleEnable} = this.props;
    await setInlineStyleEnable(true);
  }
  options = Object.keys(this.props.richTextArrayStyleSheet).map(item => (
    <div>
      <li 
        onClick={this.handleInlineStyle.bind(this,this.props.richTextArrayStyleSheet[item])}
        className="rdw-dropdownoption-default placeholder-li"
      >
        {this.props.richTextArrayStyleSheet[item]['name']}
      </li>
    </div>
  ))
  openStyleDropdown = () => this.setState({open: !this.state.open})
  render() {
    return (
      <div onClick={this.openStyleDropdown} className="rdw-block-wrapper" aria-label="rdw-block-control">
        <div className="rdw-dropdown-wrapper rdw-block-dropdown" aria-label="rdw-dropdown">
          <div className="rdw-dropdown-selectedtext" title="Placeholders">
            <span>Style</span> 
            <div className={`rdw-dropdown-caretto${this.state.open? "close": "open"}`}></div>
          </div>
          <ul className={`rdw-dropdown-optionwrapper ${this.state.open? "": "placeholder-ul"}`}>
            {this.options}
          </ul>
        </div>
      </div>  
    );
  }
}const mapStateToProps = (state) => ({
  inlineStyle: state.ThemeOptions.inlineStyle,
  inlineStyleEnable: state.ThemeOptions.inlineStyleEnable,
  richTextArrayStyleSheet: state.ThemeOptions.richTextArrayStyleSheet,
});
const mapDispatchToProps = (dispatch) => ({
  setInlineStyle: (enable) => dispatch(setInlineStyle(enable)),
  setInlineStyleEnable: (enable) => dispatch(setInlineStyleEnable(enable)),
  setRichTextArrayStyleSheet: (enable) => dispatch(setRichTextArrayStyleSheet(enable)),
});
export default connect(mapStateToProps, mapDispatchToProps)(RichTextStyle);