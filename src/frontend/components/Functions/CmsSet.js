import React, { Component } from "react";
import { useDispatch } from "react-redux";
import {
  setCmsArray,
  setCmsModel,
  setCmsKey,
  setCmsValue,
  setThemeBackgroundColor,
  setThemeColor,
  setEditorState,
  setCmsEnable,
  setCmsEnableUploadImage,
  setCmsModelImage,
  setAddModel,
  setCurrentSection,
  setAddModelData,
  setCmsSection,
  setEditSectionModel,
  setEditSectionModelEnable,
  setEditSectionModelArray,
  setSectionBackgroundColor,
  setAddNewCmsFlag,
  setCmsKeyInner,
  setBackgroundCmsKey,
  setSectionId,
  setPlaceholderValue,
  setDynamicPlaceholderValue,
  setTypeId,
  setEnableTextClone,
  setEnableTextModel,
  setRichTextArrayStyleSheet
} from "../../../reducers/ThemeOptions";
import ApiCall from "../../../admin/Partial/Function/ApiCall";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { EditorState, ContentState, convertFromHTML,convertFromRaw } from 'draft-js'
import { Editor } from 'react-draft-wysiwyg';
import htmlToDraft from 'html-to-draftjs';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import { faClone, faTrash, faPlus, faArrowUp, faArrowDown, faPencilAlt,faMinus } from "@fortawesome/free-solid-svg-icons";
import { connect } from "react-redux";
import "./CmsSet.css"
class CmsSet extends Component {
  constructor(){
    super();
    this.state = {date: new Date(), removeMessage: 'Remove Text', plusMessage:'Add New Text',cloneMessage:'Copy Text' };
  }
  callMe() {
    setInterval(() => {
      this.setState({ date: new Date() });
    }, 1000);
  }
  SetCms=(key,section_id,background_key,sectionBackgroundArray,new_key,type=null,flag,addFlag=null,textFlag=null) => {
    if(this.props.cmsEnable)
    {
      const {setSectionBackgroundColor } = this.props;
      setSectionBackgroundColor( sectionBackgroundArray); 
      var contentBlock;
      const { cmsValue, setCmsValue } = this.props;
      const { cmsKey, setCmsKey } = this.props;
      const { setSectionId } = this.props;
      setSectionId(new_key);
      if(this.props.isNav){
        contentBlock = htmlToDraft(this.props.cmsArray[new_key][new_key]);
        setCmsValue(this.props.cmsArray[new_key][new_key]);
        setCmsKey(section_id);
      } else if(type != "bullet" || type != "list"){
        contentBlock = htmlToDraft(typeof(this.props.cmsArray[section_id+key][new_key]) == "string" ? this.props.cmsArray[section_id+key][new_key] : '');
        addFlag?setCmsValue(''):setCmsValue(this.props.cmsArray[section_id+key][new_key]);
        setCmsKey(section_id+key);
      } else {
        addFlag?setCmsValue(''):setCmsValue(this.props.cmsArray[section_id+key][new_key]);
        setCmsKey(section_id+key);
      }
      if (contentBlock) {
        const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks);
        const editorState = EditorState.createWithContent(contentState);
        const {setEditorState } = this.props;
        setEditorState(editorState);
      }
      if(background_key)
      {
        const {setBackgroundCmsKey } = this.props;
        setBackgroundCmsKey(section_id+background_key);
      }
      const {setCmsKeyInner } = this.props;
      setCmsKeyInner(new_key);
      if(flag)
      {
        const { cmsModel, setCmsModel } = this.props;
        setCmsModel(!this.props.cmsModel);
      }
      const {setEnableTextModel } = this.props;
      if(textFlag && !this.props.backgroundKeyFlag)
      {
        setEnableTextModel(true);

      }
    }
    else
    {
      ApiCall.postAPICall(this.props.postRequest, this.props.postData);
    }
  }
  setNav = (key,section_id,background_key,sectionBackgroundArray,new_key) => {
    if(this.props.cmsEnable)
    {
      const { setSectionId } = this.props;
      setSectionId("navbar");
      const {setSectionBackgroundColor } = this.props;
      setSectionBackgroundColor( sectionBackgroundArray); 
      var contentBlock;
      const { cmsValue, setCmsValue } = this.props;
      const { cmsKey, setCmsKey } = this.props;
      if(this.props.isNav){
        contentBlock = htmlToDraft(this.props.cmsArray[new_key][new_key]);
        setCmsValue(this.props.cmsArray[new_key][new_key]);
        setCmsKey(section_id);
      } else {
        contentBlock = htmlToDraft(this.props.cmsArray[section_id+key][new_key]);
        setCmsValue(this.props.cmsArray[section_id+key][new_key]);
        setCmsKey(section_id+key);
      }
      if (contentBlock) {
        const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks);
        const editorState = EditorState.createWithContent(contentState);
        const {setEditorState } = this.props;
        setEditorState(editorState);
      }
      if(background_key)
      {
        const {setBackgroundCmsKey } = this.props;
        setBackgroundCmsKey(section_id+background_key);
      }
      const {setCmsKeyInner } = this.props;
      setCmsKeyInner(new_key);
      const { cmsModel, setCmsModel } = this.props;
      setCmsModel(!this.props.cmsModel);
    }
  }
  flagEnable=async(key,section_id,background_key,sectionBackgroundArray,new_key)=>
  {
    const {setAddNewCmsFlag } = this.props;
    await setAddNewCmsFlag(true);
    this.SetCms(key,section_id,background_key,sectionBackgroundArray,new_key,null,1,1);
  }
  textEnableClone=async(key,section_id,background_key,sectionBackgroundArray,new_key)=>
  {
    const {setEnableTextClone } = this.props;
    await setEnableTextClone(true);
    const {setAddNewCmsFlag } = this.props;
    await setAddNewCmsFlag(true);
    this.SetCms(key,section_id,background_key,sectionBackgroundArray,new_key,null,0);
  }
  deleteText=async(key,innerKey)=>
  {
    var url="cms/text/delete";
    var data={'key':innerKey};
    ApiCall.postAPICall(url, data);
    this.props.getData();
  }
  parseVal(prop){
    Object.keys(this.props.richTextArrayStyleSheet).map(item => {
      let temp=this.props.richTextArrayStyleSheet[item];
      let name=temp['name'];
      name=name.replace(/\s/g, '');
      name='class="'+name+'"';
      let style=' style="color:'+temp['fontColor']+';font-size:'+temp['fontSize']+';font-family:'+temp['fontFamily']+';font-Weight:'+temp['fontWeight']+';line-height:'+temp['lineHeight']+';"';
      prop=prop.replaceAll(name, style);
    });
    prop=prop.replaceAll('-@-', '');
    var alter;
    if(prop){
      var newprop = this.get(prop,"{","}");
      if(newprop.length>0)
      {
        for (var i = 0; i < newprop.length; i++)
        {
          alter = this.props.placeholderValue.filter(d => d.name == newprop[i])
          if(alter[0])
          {
            prop = prop.replace(newprop[i], alter[0].value);
            prop =  prop.replace(/\{/g, ' ');
            prop =  prop.replace(/\}/g, ' ');
            alter = prop;
          }
          else
          {
            return prop;
          }
        }   
      }
      else {
        alter = prop
      }
      return alter;
    }
    else {
      return prop;
    }
  }
  getFromBetween =  (sub1,sub2) => {
    if(this.string.indexOf(sub1) < 0 || this.string.indexOf(sub2) < 0) return false;
    var SP = this.string.indexOf(sub1)+sub1.length;
    var string1 = this.string.substr(0,SP);
    var string2 = this.string.substr(SP);
    var TP = string1.length + string2.indexOf(sub2);
    return this.string.substring(SP,TP);
  }
  removeFromBetween =  (sub1,sub2) =>{
    if(this.string.indexOf(sub1) < 0 || this.string.indexOf(sub2) < 0) return false;
    var removal = sub1+this.getFromBetween(sub1,sub2)+sub2;
    this.string = this.string.replace(removal,"");
  }
  getAllResults = (sub1,sub2) => {
    if(this.string.indexOf(sub1) < 0 || this.string.indexOf(sub2) < 0) return;
    var result = this.getFromBetween(sub1,sub2);
    this.results.push(result);
    this.removeFromBetween(sub1,sub2);
    if(this.string.indexOf(sub1) > -1 && this.string.indexOf(sub2) > -1) {
      this.getAllResults(sub1,sub2);
    }
    else return;
  };
  get =  (string,sub1,sub2) => {
    this.results = [];
    this.string = string;
    this.getAllResults(sub1,sub2);
    return this.results;
  }
  render() {
    var sectionBackgroundArray=this.props.sectionBackgroundArray;
    var styleProp=this.props.styleProp;
    var htmlProp=this.props.htmlProp;
    var sectionKey=this.props.sectionKey;
    var sectionId=this.props.sectionId;
    var backgroundKey = this.props.backgroundKey;
    let style={
      display: this.props.cmsEnable ? "inline-block" : "none"
    };
    return (
      <div style={{'display':'inline-block', flexDirection:"column"}}>
        {htmlProp !=null && Object.entries(htmlProp).length > 0 ?
          Object.keys(htmlProp).map((val, i) => (
            <div id={val+sectionId} className="custom-height">
              {i==0 ?
                !this.props.globalBtn && !this.props.isIcon && !this.props.Btn ?
                this.props.isNav ? "" :
                  <div className={this.props.cmsEnable ? "pull-right icons-box" : "pull-right"}>
                    <div
                      className="cloneIcon pull-right"
                      style={style}
                      onClick={() => this.flagEnable(sectionId, sectionKey,this.props.backgroundKey,sectionBackgroundArray,val)}
                    > 
                      <div className="Hov c1">
                        <FontAwesomeIcon   icon={faPlus} size="2x" color="#343a40"/>
                          <span className="tooltiptext c1">{this.state.plusMessage}</span>
                      </div>
                    </div>
                    <div
                      className="cloneIcon pull-right pr-2"
                      style={style}
                      onClick={() => this.textEnableClone(sectionId, sectionKey,this.props.backgroundKey,sectionBackgroundArray,val)}
                    >
                      <div className="Hov c1">
                        <FontAwesomeIcon icon={faClone} size="2x" color="#343a40"/>
                          <span className="tooltiptext c1">{this.state.cloneMessage}</span>
                      </div>
                    </div>
                  </div>
                  :'' 
              :''}
              <div>
              {this.props.isIcon ?
                  this.props.globalBtn ?
                    <button
                      type="button"
                      className={!this.props.submitBtnFlag ?"button-default button-olive d-inline-block":'btn-style'}
                      style={{...this.props.styleProp? this.props.styleProp[val+'style'] :null,
                        'background':this.props.backgroundStyle?this.props.backgroundStyle:null,
                        'display':'inline-block'
                      }}
                      onClick={() =>
                       this.SetCms(sectionId, sectionKey,this.props.backgroundKey,sectionBackgroundArray,val,null,1)}
                      dangerouslySetInnerHTML={{__html: htmlProp[val]}}
                    ></button>
                  :
                    this.props.Btn ? 
                      <a
                        style={{...this.props.styleProp? this.props.styleProp[val+'style'] :null,'display':'inline-block'}}
                        className="button-default button-white d-inline-block"
                        role="button"
                        onClick={() =>
                        this.SetCms(sectionId, sectionKey,this.props.backgroundKey,sectionBackgroundArray,val,null,1)}
                        dangerouslySetInnerHTML={{__html: htmlProp[val]}}
                      ></a>
                    :
                      <i style={{...this.props.styleProp? this.props.styleProp[val+'style'] :null,'display':'inline-block','padding':(!this.props.flagPadding?'5vh':'unset')}} onClick={() => this.SetCms(sectionId, sectionKey,this.props.backgroundKey,sectionBackgroundArray,val,null,1)} className={`${htmlProp[val]}`}></i>
                :    
                this.props.isNav ? 
                  <a
                    onClick={() =>
                    this.setNav(null, sectionKey,this.props.backgroundKey,sectionBackgroundArray,val,null,1)}
                    dangerouslySetInnerHTML={{__html: (htmlProp[val]).replace(/<[^>]*>?/gm, '')}}
                    className="scrollShift hometab"
                    href={
                    this.props.href
                    }
                  /> :
                  htmlProp[val]['type'] && htmlProp[val]['type'] == "bullet"
                  ?
                  <div className="conatct-info" onClick={() => this.SetCms(sectionId, sectionKey,this.props.backgroundKey,sectionBackgroundArray,val,htmlProp[val]['type'],1)}>
                    <div className="single-contact-info">
                      <div
                        className={htmlProp[val]['class'] ? htmlProp[val]['class'] : "contact-icon"}   style={{...this.props.styleProp? this.props.styleProp[val+'style'] :null,
                        'background':this.props.backgroundStyle?this.props.backgroundStyle:null,
                        "overflow": "hidden"
                      }}
                      >
                        {new RegExp("([a-zA-Z0-9]+://)?([a-zA-Z0-9_]+:[a-zA-Z0-9_]+@)?([a-zA-Z0-9.-]+\\.[A-Za-z]{2,4})(:[0-9]+)?(/.*)?").test(htmlProp[val]['content']['image']) 
                        ? 
                          <img src={htmlProp[val]['content']['image']} style={{marginLeft: "auto", marginRight:"auto", display:"block"}}/> 
                        : 
                          <i className={htmlProp[val]['content']['image']}></i>
                        }
                      </div>
                      <div className="contact-text">
                        {htmlProp[val]['content'] ? htmlProp[val]['content']['texts'].map(item=>(
                          <div dangerouslySetInnerHTML={{__html:item}}>
                          </div>
                        )):null}
                      </div>
                    </div>
                  </div>
                  :
                  htmlProp[val]['type'] && htmlProp[val]['type'] == "list" 
                  ? 
                  
                  htmlProp[val]['content'] ? htmlProp[val]['content']['texts'].map((item, i)=>(
                  <div className="conatct-info" onClick={() => this.SetCms(sectionId, sectionKey,this.props.backgroundKey,sectionBackgroundArray,val,htmlProp[val]['type'],1)}>
                    <div className="single-contact-info">
                      <div
                        className={htmlProp[val]['class'] ? htmlProp[val]['class'] : "contact-icon"}   style={{...this.props.styleProp? this.props.styleProp[val+'style'] :null,
                        'background':this.props.backgroundStyle?this.props.backgroundStyle:null,
                        "overflow": "hidden",
                        position: "relative", 
                        textAlign: "center"
                      }}
                      >
                        <span style={{position: "absolute", top:"50%", left: 0, right: 0, marginTop:"-9px"}}>{parseInt(htmlProp[val]['starting_number'])+i}</span>
                      </div>
                      <div className="contact-text">
                          <p dangerouslySetInnerHTML={{__html:item}} />
                      </div>
                    </div>
                  </div> 
                  )) : null
                  : 
                  this.props.type == "grey_button" ? 
                    <button className="button" style={{padding:'3%'}}>
                      <span 
                        style={{...this.props.styleProp ? this.props.styleProp[val+'style'] :null,'display':'inline-block'}} 
                        onClick={() => this.SetCms(sectionId, sectionKey,this.props.backgroundKey,sectionBackgroundArray,val,null,1,null,1)}
                        dangerouslySetInnerHTML = {{__html:this.parseVal(htmlProp[val]).replace('%time%',this.state.date.toLocaleTimeString()).replace('%date%',this.state.date.toLocaleDateString()).replace('time',this.state.date.toLocaleTimeString()).replace('date',this.state.date.toLocaleDateString()) }}
                      />
                    </button> 
                  : 
                  <span 
                    style={{...this.props.styleProp ? this.props.styleProp[val+'style'] :null,'display':'inline-block'}} 
                    onClick={() => this.SetCms(sectionId, sectionKey,this.props.backgroundKey,sectionBackgroundArray,val,null,1,null,1)}
                    dangerouslySetInnerHTML = {{__html:this.parseVal(htmlProp[val]).replace('%time%',this.state.date.toLocaleTimeString()).replace('%date%',this.state.date.toLocaleDateString()).replace('time',this.state.date.toLocaleTimeString()).replace('date',this.state.date.toLocaleDateString()) }}
                  />
                } 
                </div>            
              {i!=0 ?
                <div
                  className="cloneIcon pr-5 pull-right"
                  style={style}
                  onClick={() => this.deleteText(sectionKey+sectionId,val)}
                >
                  <div className="Hov c1">
                    <FontAwesomeIcon icon={faMinus} size="2x" />
                    <span className="tooltiptext c1">{this.state.removeMessage}</span>
                  </div>
                </div>
              :null}
            </div>
          ))
          :''
        }
      </div>
    );
  }
}
const mapStateToProps = (state) => ({
  cmsArray: state.ThemeOptions.cmsArray,
  cmsModel: state.ThemeOptions.cmsModel,
  cmsKey: state.ThemeOptions.cmsKey,
  cmsValue: state.ThemeOptions.cmsValue,
  themeColor: state.ThemeOptions.themeColor,
  themeBackgroundColor: state.ThemeOptions.themeBackgroundColor,
  editorState: state.ThemeOptions.editorState,
  cmsEnable: state.ThemeOptions.cmsEnable,
  cmsEnableUploadImage: state.ThemeOptions.cmsEnableUploadImage,
  cmsModelImage: state.ThemeOptions.cmsModelImage,
  addModel: state.ThemeOptions.addModel,
  currentSection: state.ThemeOptions.currentSection,
  addModelData: state.ThemeOptions.addModelData,
  cmsSection: state.ThemeOptions.cmsSection,
  editSectionModel: state.ThemeOptions.editSectionModel,
  editSectionModelEnable: state.ThemeOptions.editSectionModelEnable,
  editSectionModelArray: state.ThemeOptions.editSectionModelArray,
  sectionBackgroundColor:state.ThemeOptions.sectionBackgroundColor,
  addNewCmsFlag:state.ThemeOptions.addNewCmsFlag,
  cmsKeyInner:state.ThemeOptions.cmsKeyInner,
  placeholderValue: state.ThemeOptions.placeholderValue,
  dynamicPlaceholderValue: state.ThemeOptions.dynamicPlaceholderValue,
  typeId: state.ThemeOptions.typeId,
  enableTextClone:state.ThemeOptions.enableTextClone,
  enableTextModel:state.ThemeOptions.enableTextModel,
  richTextArrayStyleSheet: state.ThemeOptions.richTextArrayStyleSheet,
});
const mapDispatchToProps = (dispatch) => ({
  setCmsArray: (enable) => dispatch(setCmsArray(enable)),
  setCmsModel: (enable) => dispatch(setCmsModel(enable)),
  setCmsKey: (enable) => dispatch(setCmsKey(enable)),
  setCmsValue: (enable) => dispatch(setCmsValue(enable)),
  setThemeColor: (enable) => dispatch(setThemeColor(enable)),
  setThemeBackgroundColor: (enable) => dispatch(setThemeBackgroundColor(enable)),
  setEditorState: (enable) => dispatch(setEditorState(enable)),
  setCmsEnable: (enable) => dispatch(setCmsEnable(enable)),
  setCmsModelImage: (enable) => dispatch(setCmsModelImage(enable)),
  setCmsEnableUploadImage: (enable) => dispatch(setCmsEnableUploadImage(enable)),
  setCmsSection: (enable) => dispatch(setCmsSection(enable)),
  setAddModel: (enable) => dispatch(setAddModel(enable)),
  setCurrentSection: (enable) => dispatch(setCurrentSection(enable)),
  setAddModelData: (enable) => dispatch(setAddModelData(enable)),
  setEditSectionModel: (enable) => dispatch(setEditSectionModel(enable)),
  setEditSectionModelEnable: (enable) => dispatch(setEditSectionModelEnable(enable)),
  setEditSectionModelArray: (enable) => dispatch(setEditSectionModelArray(enable)),
  setSectionBackgroundColor: enable => dispatch(setSectionBackgroundColor(enable)),
  setAddNewCmsFlag: enable => dispatch(setAddNewCmsFlag(enable)),
  setCmsKeyInner: enable => dispatch(setCmsKeyInner(enable)),
  setBackgroundCmsKey: enable => dispatch(setBackgroundCmsKey(enable)),
  setSectionId: enable => dispatch(setSectionId(enable)),
  setPlaceholderValue: (enable) => dispatch(setPlaceholderValue(enable)),
  setDynamicPlaceholderValue: (enable) => dispatch(setDynamicPlaceholderValue(enable)),
  setTypeId: (enable) => dispatch(setTypeId(enable)),
  setEnableTextClone: (enable) => dispatch(setEnableTextClone(enable)),
  setEnableTextModel: (enable) => dispatch(setEnableTextModel(enable)),
  setRichTextArrayStyleSheet: (enable) => dispatch(setRichTextArrayStyleSheet(enable)),
});
export default connect(mapStateToProps, mapDispatchToProps)(CmsSet);
