import React, { Component } from "react";
import { useDispatch } from "react-redux";
import "../Placeholders.css"
import {
  setCmsArray,
  setCmsModel,
  setCmsKey,
  setCmsValue,
  setThemeBackgroundColor,
  setThemeColor,
  setEditorState,
  setCmsEnable,
  setCmsEnableUploadImage,
  setCmsModelImage,
  setAddModel,
  setCurrentSection,
  setAddModelData,
  setCmsSection,
  setEditSectionModel,
  setEditSectionModelEnable,
  setEditSectionModelArray,
  setSectionBackgroundColor,
  setAddNewCmsFlag,
  setCmsKeyInner,
  setBackgroundCmsKey,
  setSectionId,
  setPlaceholderValue,
  setDynamicPlaceholderValue,
  setTypeId,
  setFontSize,
  setInlineStyle,
  setPlaceholderPosition,
  setShowPlaceholder,
  setFontFamily,
  setRichTextArray,
  setRichTextArrayStyle,
  setRichTextFontSize,
  setRichTextFontFamily,
  setRichTextPositionEnd,
  setInlineStyleEnable,
  setRichTextArrayStyleSheet,
  setEnableRichTextEditor,
} from "../../../reducers/ThemeOptions";
import ApiCall from "../../../admin/Partial/Function/ApiCall";
import { connect } from "react-redux";
import {
  UncontrolledButtonDropdown,
  DropdownToggle,
  Dropdown,
  DropdownMenu,
  DropdownItem,
} from "reactstrap";
import {
  Row,
  Col,
  ListGroup,
  ListGroupItem,
  FormGroup,
  Label,
  Input,
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
} from "reactstrap";
import RichTextStyle from "./RichTextStyle";
import StyleSheet from "../Models/Functions/StyleSheet";
import Placeholders from "../Placeholders.js";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faItalic,faBold,faUnderline} from "@fortawesome/free-solid-svg-icons";
import ContentEditable from 'react-contenteditable';
let text=null;
let position=null;
var styleArray=[];
class RichText extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fontSizeArray: [6,8,12,16,18,24,30,40,60,80],
      fontFamilyArray: ['Lato','Raleway'],
      selectFont:16,
      selectFontFamily:'Lato',
      text:null,
      style:{},
      status:false,
      is_blod:false,
      is_italic:false,
      open:false,
      is_underline:false,
      font:{
        'fontSize':16
      },
      fontFamily:{
        'fontFamily':'Lato'
      },
      placeholdervalue : "",
      divID:null,
      toogleStyle:false,
      toogleRichText:true,
      enableEdiotor:false,
      tempArray:[],
    }
  }
  toogle=async()=>
  {
    console.log(this.props.richTextArray);
    this.setState({toogleStyle:!this.state.toogleStyle});
    if(!this.state.toogleStyle)
    {
      let value=this.props.flag? this.props.richTextArray[this.props.index] : this.props.cmsValue;
      value=value.replace(/<\/?span[^>]*>/g,"");
      // let value=this.props.cmsValue;
      Object.keys(styleArray).map(item => {
        value=value.replaceAll(styleArray[item], item);
      });
      if(this.props.flag)
      {
        var array=this.props.richTextArray;
        array[this.props.index]=value;
        const { setRichTextArray } = this.props;
        await setRichTextArray(array);
      }
      else
      {
        this.handleSetValue(value);
      }
    }
    this.setState({toogleRichText:!this.state.toogleRichText});
  }
  clearTags=async(event)=>{
    let value=this.props.cmsValue;
    value=value.replace(/<\/?span[^>]*>/g,"");
    value=value.replace(/<\/?[^>]+(>|$)/g, "");
    value=value.replaceAll('-@-', '');
    this.handleSetValue(value);
  }
  handlePosition=async(event)=>{
    position=event.target.selectionStart;
    this.handleSetPosition(position);
    this.handleSetPositionEnd(event.target.selectionEnd);  
    text=event.target.value;
    this.handleSetValue(text);
  }
  handleChange= (event)=>{
    console.log(this.props.richTextArray);
    var tempArray=this.state.tempArray;
    if(!this.state.enableEdiotor)
    {
      var data=this.props.flag ? typeof(this.props.richTextArray[this.props.index]) != "undefined"
                ? this.setRichTextvalue(this.props.richTextArray[this.props.index]):null 
              :(typeof(this.props.cmsValue)=="string"
                ? this.setRichTextvalue(this.props.cmsValue) : "");
      if(this.props.flag)
      {
        tempArray[this.props.index]=data;
      } 
      else
      {
        tempArray[0]=data;
      }
      this.setState({enableEdiotor:true});
    }
    if(this.state.enableEdiotor)
    {
      if(this.props.flag)
      {
        tempArray[this.props.index]=event.target.value;
      } 
      else
      {
        tempArray[0]=event.target.value;
      }

    }
    this.setState({tempArray:tempArray});
    this.handleLoader();
    text=event.target.value;
    this.handleSetValue(text);
    setTimeout(() => {
      this.handleLoader();
    }, 2000);
  }
  handleSetPositionEnd=async(position)=>
  {
    const {setRichTextPositionEnd} = this.props;
    await setRichTextPositionEnd(position);
  }
  handleSetPosition=async(position)=>
  {
    const {setPlaceholderPosition} = this.props;
    await setPlaceholderPosition(position);
  }
  handleSetValue=async(text)=>
  {
    console.log(text);
    if(this.props.flag)
    {
      var array=this.props.richTextArray;
      array[this.props.index]=text;
      const { setRichTextArray } = this.props;
      await setRichTextArray(array);
    }
    else
    {
      const { setCmsValue } = this.props;
      await setCmsValue(text);
      await this.handleSetCmsValue();
    }
  }
  handleStyleSelectivePart=async(class_name)=>
  {
    var str=this.props.flag?this.props.richTextArray[this.props.index]:this.props.cmsValue;
    str=str.replace(/<\/?span[^>]*>/g,"");
    str=await this.handlePositionPointer(str,class_name,this.props.placeholderPosition,this.props.richTextPositionEnd);
    if(this.props.flag)
    {
      var array=this.props.richTextArray;
      array[this.props.index]=str;
      const { setRichTextArray } = this.props;
      await this.props.setRichTextArray(array);
      // console.log(this.props.richTextArray)
    }
    else
    {
      // const { setCmsValue } = this.props;
      // await setCmsValue(str);
      // this.handleSetCmsValue();
    }
  }
  handleSetIndividualChar=async(str,class_name)=>
  {
    let copy_str=str;
    let original_text=window.getSelection().toString();
    var flag=str.includes(class_name);
    str=str.replace(/<\/?p[^>]*>/g,"");
    str=str.replaceAll('-@-', '');
    if(original_text.includes(str))
    {
      var emptyArray=[];
      if(!flag)
      {
         str='<pclass="'+class_name+'">'+str+'</p>';
        // for (var i = 0; i < str.length; i++) {
        //   emptyArray.push('<pclass="'+class_name+'">'+str.charAt(i)+'</p>');
        // }
        // -@-
        // str=emptyArray.join('-@-');
      }

    }
    else
    {
      // if(str.length > original_text.length && str.includes(original_text))
      // {
      //   // var copy_str_length=copy_str.length;
      //   copy_str=copy_str.split('-@-');
      //   if(copy_str.length == 1)
      //   {
      //     copy_str=copy_str[0].split('');
      //   }
      //   let index=this.getDifference(original_text,str);
      //   var emptyArray=[];
      //   for (var i = 0; i < original_text.length; i++) {
      //     if(typeof(copy_str[index+i]) != 'undefined')
      //     {
      //       if(copy_str[index+i].includes(class_name))
      //       {
      //         copy_str[index+i]=original_text.charAt(i);
      //       }
      //       else
      //       {
      //         let temp='<pclass="'+class_name+'">'+original_text.charAt(i)+'</p>';
      //         if(original_text.charAt(i) == copy_str[index+i].replace(/<\/?p[^>]*>/g,""))
      //         {
      //           copy_str[index+i]=temp;
      //         }
      //       }
      //     }
      //   }
      //   str=copy_str.join('-@-');
      // }
    }
    return str;
  }

  getDifference=(a, b)=>
  {
    var length=b.split(a)[0];
    return length.length ? length.length : 0;
    // var i = 0;
    // var j = 0;
    // var result = "";

    // while (j < b.length)
    // {
    //  if (a[i] != b[j] || i == a.length)
    //      result += b[j];
    //  else
    //      i++;
    //  j++;
    // }
    // return result;
  }
  handlePositionPointer=async(str,class_name,position,position_end)=>
  {
    var selectedText=window.getSelection().toString();
    var selection = window.getSelection();
    var range = selection.getRangeAt(0);
    var start = range.startOffset;
    var string=this.props.flag? this.state.tempArray[this.props.index] : this.state.tempArray[0];
    // var end = range.endOffset;
    var end =this.getTextPosition();
    const {setPlaceholderPosition} = this.props;
    await setPlaceholderPosition(end);
    var selectivePart=string.replace(/<\/?[^>]+(>|$)/g, "");
    selectivePart=selectivePart.replaceAll('-@-', '');
    var index=this.getTextIndex(selectivePart,end,selectedText);
    var tempArray=[];
    var check_string=string.replace(/<\/?[^>]+(>|$)/g, "");
    check_string=check_string.replaceAll('-@-', '');
    check_string=check_string.replaceAll('&nbsp;',' ');
    var flag=0;
    if(check_string.trim().length == selectedText.trim().length)
    {
      flag=1;
    }
    if(check_string == selectedText)
    {
      string=check_string;
    }
    if(!flag)
    {
      tempArray=string.split(" ");
      console.log(tempArray);
      selectedText=selectedText.split(" ");
      for (var j = 0; j < selectedText.length; j++) {
        var local;
        if(selectedText.length == 1)
        {
          local=await index; 
        }
        else
        {
          local=await (index-selectedText.length)+j+1;
        }
        // (index-((selectedText.length)-j))+1;
        let emptyArray=[];
        tempArray[local]=await this.handleSetIndividualChar(tempArray[local],class_name);
      }
      tempArray=tempArray.join(' ');
      string=tempArray;
    }
    else
    {
      string=string.replace(/<\/?span[^>]*>/g,"");
      string=string.replace(/<\/?[^>]+(>|$)/g, "");
      string=string.replaceAll('&nbsp;', ' ');
      string='<pclass="'+class_name+'">'+string+'</p>';
    }
    console.log(string);
    if(this.props.flag)
    {
      var array=this.props.richTextArray;
      array[this.props.index]=string;
      const { setRichTextArray } = this.props;
      await setRichTextArray(array);
      console.log(this.props.richTextArray);
      console.log(array);
    }
    else
    {
      const { setCmsValue } = this.props;
      await setCmsValue(string);
      await this.handleSetCmsValue();
    }

    // const { setCmsValue } = this.props;
    // await setCmsValue(string);
    // await this.handleSetCmsValue();
    // var s1 = str.substr(0, position);
    // var s2 = str.substr(0 ,position_end);
    // var s3 = str.substr(position_end);
    // s2=s2.split(s1)[1];
    // let flag=s2.includes(class_name);
    // s2=s2.replace(/(<([^>]+)>)/gi, "");
    // if(!flag)
    // {
    //   let tempArray=s2.split(" ");
    //   let emptyArray=[];
    //   for (var i = 0; i < tempArray.length; i++) {
    //     emptyArray[i]=await this.handleSetIndividualChar(tempArray[i],class_name);
    //   }
    //   s2=emptyArray.join(' ');

    // }
    // var newText = s1 +s2+s3;
    // return newText;

  }
  handleBold=async()=>
  {
    if(!this.props.richTextPositionEnd)
    {
      await this.setState({ is_blod: !this.state.is_blod });
      var style={fontWeight: this.state.is_blod ? 'bold':'unset'};
      this.handleSetStyle(style);
    }
    else
    {
      this.handleStyleSelectivePart();
    }
  }
  handleSetInlineStyle=async(style)=>
  {
    // if(this.props.flag && style?.length !=0)
    // {
    //   var arrayStyle=this.props.richTextArrayStyle;
    //   var styleArray={};
    //   styleArray['style']=style;
    //   if (typeof(arrayStyle[this.props.index]) != "undefined")
    //   {
    //     styleArray['style'] = {...styleArray['style'], ...arrayStyle[this.props.index]['style'] };
    //   }
    //   arrayStyle[this.props.index]=styleArray;
    //   const { setRichTextArrayStyle } = this.props;
    //   await setRichTextArrayStyle(arrayStyle);
    // }
    // else
    // {
    //   this.setState({ style: style });
    //   const { setInlineStyle } = this.props;
    //   setInlineStyle(style);
    // }
  }
  handleInlineStyle=async()=>
  {
    const {setInlineStyleEnable} = this.props;
    await setInlineStyleEnable(false);
    this.handleStyleSelectivePart(this.props.inlineStyle);
  }
  componentWillMount() {
    if(!this.state.enableEdiotor)
    {
      var tempArray=this.state.tempArray;
      var data=this.props.flag ? typeof(this.props.richTextArray[this.props.index]) != "undefined"
                ? this.setRichTextvalue(this.props.richTextArray[this.props.index]):null 
              :(typeof(this.props.cmsValue)=="string"
                ? this.setRichTextvalue(this.props.cmsValue) : "");
      if(this.props.flag)
      {
        tempArray=this.props.richTextArray;
      } 
      else
      {
        tempArray[0]=data;
      }
      this.setState({tempArray:tempArray});
      this.setState({enableEdiotor:true});
    }
  }
  componentDidUpdate(prevProps){
    if(this.props.inlineStyleEnable)
    {
      this.handleInlineStyle();
    }
    if(this.props.cmsModel)
    {
      if(!this.state.enableEdiotor)
      {
        var tempArray=this.state.tempArray;
        var data=this.props.flag ? typeof(this.props.richTextArray[this.props.index]) != "undefined"
                  ? this.setRichTextvalue(this.props.richTextArray[this.props.index]):null 
                :(typeof(this.props.cmsValue)=="string"
                  ? this.setRichTextvalue(this.props.cmsValue) : "");
        if(this.props.flag)
        {
          tempArray=this.props.richTextArray;
        } 
        else
        {
          tempArray[0]=data;
        }
        this.setState({tempArray:tempArray});
      }
    }
    
  }
  handleSetStyle=(newStyle)=>
  {
    var style=this.state.style;
    style = {...style, ...newStyle }
    this.handleSetInlineStyle(style);
  }
  handleSetCmsValue=()=>
  {
    if(typeof(this.props.cmsValue) == "string"){
      return (
        this.props.cmsValue.replace(/<\/?span[^>]*>/g,"")
        );
    } else {
      return "";
    }
  }
  handleLoader=async()=>
  {
    const { setEnableRichTextEditor } = this.props;
    await setEnableRichTextEditor(!this.props.enableRichTextEditor);
  }
  setRichTextvalue=(value)=>
  {
    value=value.replace(/<\/?span[^>]*>/g,"");
     Object.keys(this.props.richTextArrayStyleSheet).map(item => {
      let temp=this.props.richTextArrayStyleSheet[item];
      let name=temp['name'];
      name=name.replace(/\s/g, '');
      name='class="'+name+'"';
      let style=' style="color:'+temp['fontColor']+';font-size:'+temp['fontSize']+';font-family:'+temp['fontFamily']+';font-Weight:'+temp['fontWeight']+';line-height:'+temp['lineHeight']+';"';
      styleArray[name]=style;
      value=value.replaceAll(name, style);
    });
    // value=value.replace(/<\/?[^>]+(>|$)/g, "");
    value=value.replaceAll('-@-', '');
    var alter;
    if(value){
      var newvalue = this.get(value,"{","}");
      if(newvalue.length>0)
      {
        for (var i = 0; i < newvalue.length; i++)
        {
          alter = this.props.placeholderValue.filter(d => d.name == newvalue[i])
          if(alter[0])
          {
            value = value.replace(newvalue[i], alter[0].value);
            value =  value.replace(/\{/g, ' ');
            value =  value.replace(/\}/g, ' ');
            alter = value;
          }
          else
          {
            return value;
          }
        }  
        
      }
      else {
        alter = value
      }
      return alter;
    }
    else {
      return value;
    }
    return (value);
  }
  getFromBetween =  (sub1,sub2) => {
    if(this.string.indexOf(sub1) < 0 || this.string.indexOf(sub2) < 0) return false;
    var SP = this.string.indexOf(sub1)+sub1.length;
    var string1 = this.string.substr(0,SP);
    var string2 = this.string.substr(SP);
    var TP = string1.length + string2.indexOf(sub2);
    return this.string.substring(SP,TP);
  }
  removeFromBetween =  (sub1,sub2) =>{
    if(this.string.indexOf(sub1) < 0 || this.string.indexOf(sub2) < 0) return false;
    var removal = sub1+this.getFromBetween(sub1,sub2)+sub2;
    this.string = this.string.replace(removal,"");
  }
  getAllResults = (sub1,sub2) => {
    if(this.string.indexOf(sub1) < 0 || this.string.indexOf(sub2) < 0) return;
    var result = this.getFromBetween(sub1,sub2);
    this.results.push(result);
    this.removeFromBetween(sub1,sub2);
    if(this.string.indexOf(sub1) > -1 && this.string.indexOf(sub2) > -1) {
      this.getAllResults(sub1,sub2);
    }
    else return;
  };
  get =  (string,sub1,sub2) => {
    this.results = [];
    this.string = string;
    this.getAllResults(sub1,sub2);
    return this.results;
  }
  setRichTextvalueRmoveTag=(value)=>
  {
    value=value.replace(/<\/?span[^>]*>/g,"");
    value=value.replace(/<\/?[^>]+(>|$)/g, "");
    value=value.replaceAll('-@-', '');
    return (value);
  }
  displayText=(data)=>
  {
    console.log(data);
    Object.keys(this.props.richTextArrayStyleSheet).map(item => {
      let temp=this.props.richTextArrayStyleSheet[item];
      let name=temp['name'];
      name=name.replace(/\s/g, '');
      name='class="'+name+'"';
      let style=' style="color:'+temp['fontColor']+';font-size:'+temp['fontSize']+';font-family:'+temp['fontFamily']+';font-Weight:'+temp['fontWeight']+';line-height:'+temp['lineHeight']+';"';
      data=data.replaceAll(name, style);
    });
    data=data.replaceAll('-@-', '');
    return data;
  }
  getTextPosition=()=>
  {
    if(this.props.flag)
    {
      var id='contentbox'+this.props.index;
    }
    else
    {
      var id=this.state.divID;
    }
    var element = document.getElementById(this.state.divID);
    console.log(element);
    var caretOffset = 0;
    if (typeof window.getSelection != "undefined") {
        var range = window.getSelection().getRangeAt(0);
        var preCaretRange = range.cloneRange();
        preCaretRange.selectNodeContents(element);
        preCaretRange.setEnd(range.endContainer, range.endOffset);
        caretOffset = preCaretRange.toString().length;
    }
    else if (typeof document.selection != "undefined" && document.selection.type != "Control")
    {
        var textRange = document.selection.createRange();
        var preCaretTextRange = document.body.createTextRange();
        preCaretTextRange.moveToElementText(element);
        preCaretTextRange.setEndPoint("EndToEnd", textRange);
        caretOffset = preCaretTextRange.text.length;
    }
    return caretOffset;
  }
  getTextIndex=(data,end,part)=>
  {
    data=data.substr(0,end);
    return (data.split(" ").length - 1);
  }
  getRichTextData=async(id)=>
  {
    console.log(id);
    this.setState({divID:id});
  }
  divID=()=>
  {
    return (this.props.flag? typeof(this.props.richTextArray[this.props.index]) != "undefined" ? 'contentbox'+this.props.index:null :(typeof(this.props.cmsValue)=="string"? 'contentbox' : ""));
  }
  printData=()=>
  {
    return this.props.flag? this.state.tempArray[this.props.index] : this.state.tempArray[0]; 
  }
  render() {
    console.log(this.props.richTextArray);
    console.log(this.props.richTextArray[this.props.index]);
    console.log(this.state.tempArray);
    return (
      <div style={{width:'100%'}}>
       
          {!this.props.flag 
            ?
            <div>
              <Button className="pull-right mb-2" color="primary" onClick={() =>this.toogle()}>
                {!this.state.toogleStyle ? 'Apply Style' : 'Back to Editor'}
              </Button>{" "}
              {this.state.toogleStyle ?
                <div> 
                  <div className="richtext-toolbar">
                    <button type="button" className="richtext-btn">
                      <RichTextStyle/>
                    </button>
                    <button type="button" className="richtext-btn">
                      <Placeholders getTextPosition={this.getTextPosition} handleSetCmsValue={this.handleSetCmsValue} getTextIndex={this.getTextIndex}/>
                    </button>
                    <Button className="richtext-btn pull-right mr-5 mb-2" onClick={() =>this.clearTags()}>
                      Clear
                  </Button>
                  </div>
                  <div className="richtext-toolbar" style={{height:'50vh'}}>
                    <p id={this.divID()} onClick={e=>{this.getRichTextData(this.divID())}} dangerouslySetInnerHTML={{__html:this.displayText(this.props.flag? typeof(this.props.richTextArray[this.props.index]) != "undefined" ? this.props.richTextArray[this.props.index]:null :(typeof(this.props.cmsValue)=="string"? this.props.cmsValue : ""))}}/>
                  </div>
                  <div className="richtext-toolbar">
                    <p>
                    {(this.props.flag? typeof(this.props.richTextArray[this.props.index]) != "undefined" ? this.props.richTextArray[this.props.index]:null :(typeof(this.props.cmsValue)=="string"? this.props.cmsValue : ""))}
                    </p>
                  </div>
                </div>
                : null
              }
            </div>
            :null
            }
          
        <div>
          {
            this.state.toogleRichText 
            ?
              <div>
                <div id="custom_richtext">
                  <ContentEditable
                    id='custom_richtext'
                    html={this.printData()} // innerHTML of the editable div
                    disabled={false}
                    onChange={this.handleChange}
                    tagName='article'
                  /> 
                </div>
              </div>
            :null
          }
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => ({
  cmsArray: state.ThemeOptions.cmsArray,
  cmsModel: state.ThemeOptions.cmsModel,
  cmsKey: state.ThemeOptions.cmsKey,
  cmsValue: state.ThemeOptions.cmsValue,
  themeColor: state.ThemeOptions.themeColor,
  themeBackgroundColor: state.ThemeOptions.themeBackgroundColor,
  editorState: state.ThemeOptions.editorState,
  cmsEnable: state.ThemeOptions.cmsEnable,
  cmsEnableUploadImage: state.ThemeOptions.cmsEnableUploadImage,
  cmsModelImage: state.ThemeOptions.cmsModelImage,
  addModel: state.ThemeOptions.addModel,
  currentSection: state.ThemeOptions.currentSection,
  addModelData: state.ThemeOptions.addModelData,
  cmsSection: state.ThemeOptions.cmsSection,
  editSectionModel: state.ThemeOptions.editSectionModel,
  editSectionModelEnable: state.ThemeOptions.editSectionModelEnable,
  editSectionModelArray: state.ThemeOptions.editSectionModelArray,
  sectionBackgroundColor:state.ThemeOptions.sectionBackgroundColor,
  addNewCmsFlag:state.ThemeOptions.addNewCmsFlag,
  cmsKeyInner:state.ThemeOptions.cmsKeyInner,
  placeholderValue: state.ThemeOptions.placeholderValue,
  typeId: state.ThemeOptions.typeId,
  fontSize: state.ThemeOptions.fontSize,
  inlineStyle: state.ThemeOptions.inlineStyle,
  dynamicPlaceholderValue: state.ThemeOptions.dynamicPlaceholderValue,
  placeholderPosition: state.ThemeOptions.placeholderPosition,
  showPlaceholder: state.ThemeOptions.showPlaceholder,
  fontFamily:state.ThemeOptions.fontFamily,
  richTextArray: state.ThemeOptions.richTextArray,
  richTextArrayStyle: state.ThemeOptions.richTextArrayStyle,
  richTextFontSize: state.ThemeOptions.richTextFontSize,
  richTextFontFamily: state.ThemeOptions.richTextFontFamily,
  richTextPositionEnd:state.ThemeOptions.richTextPositionEnd,
  inlineStyleEnable: state.ThemeOptions.inlineStyleEnable,
  richTextArrayStyleSheet: state.ThemeOptions.richTextArrayStyleSheet,
  enableRichTextEditor: state.ThemeOptions.enableRichTextEditor,
});
const mapDispatchToProps = (dispatch) => ({
  setCmsArray: (enable) => dispatch(setCmsArray(enable)),
  setCmsModel: (enable) => dispatch(setCmsModel(enable)),
  setCmsKey: (enable) => dispatch(setCmsKey(enable)),
  setCmsValue: (enable) => dispatch(setCmsValue(enable)),
  setThemeColor: (enable) => dispatch(setThemeColor(enable)),
  setThemeBackgroundColor: (enable) =>
    dispatch(setThemeBackgroundColor(enable)),
  setEditorState: (enable) => dispatch(setEditorState(enable)),
  setCmsEnable: (enable) => dispatch(setCmsEnable(enable)),
  setCmsModelImage: (enable) => dispatch(setCmsModelImage(enable)),
  setCmsEnableUploadImage: (enable) =>
    dispatch(setCmsEnableUploadImage(enable)),
  setCmsSection: (enable) => dispatch(setCmsSection(enable)),
  setAddModel: (enable) => dispatch(setAddModel(enable)),
  setCurrentSection: (enable) => dispatch(setCurrentSection(enable)),
  setAddModelData: (enable) => dispatch(setAddModelData(enable)),
  setEditSectionModel: (enable) => dispatch(setEditSectionModel(enable)),
  setEditSectionModelEnable: (enable) => dispatch(setEditSectionModelEnable(enable)),
  setEditSectionModelArray: (enable) => dispatch(setEditSectionModelArray(enable)),
  setSectionBackgroundColor: enable => dispatch(setSectionBackgroundColor(enable)),
  setAddNewCmsFlag: enable => dispatch(setAddNewCmsFlag(enable)),
  setCmsKeyInner: enable => dispatch(setCmsKeyInner(enable)),
  setBackgroundCmsKey: enable => dispatch(setBackgroundCmsKey(enable)),
  setSectionId: enable => dispatch(setSectionId(enable)),
  setPlaceholderValue: (enable) => dispatch(setPlaceholderValue(enable)),
  setTypeId: (enable) => dispatch(setTypeId(enable)),
  setFontSize: (enable) => dispatch(setFontSize(enable)),
  setInlineStyle: (enable) => dispatch(setInlineStyle(enable)),
  setDynamicPlaceholderValue: (enable) => dispatch(setDynamicPlaceholderValue(enable)),
  setPlaceholderPosition: (enable) => dispatch(setPlaceholderPosition(enable)),
  setShowPlaceholder: (enable) => dispatch(setShowPlaceholder(enable)),
  setFontFamily: (enable) => dispatch(setFontFamily(enable)),
  setRichTextArray: (enable) => dispatch(setRichTextArray(enable)),
  setRichTextArrayStyle: (enable) => dispatch(setRichTextArrayStyle(enable)),
  setRichTextFontSize: (enable) => dispatch(setRichTextFontSize(enable)),
  setRichTextFontFamily: (enable) => dispatch(setRichTextFontFamily(enable)),
  setRichTextPositionEnd: (enable) => dispatch(setRichTextPositionEnd(enable)),
  setInlineStyleEnable: (enable) => dispatch(setInlineStyleEnable(enable)),
  setRichTextArrayStyleSheet: (enable) => dispatch(setRichTextArrayStyleSheet(enable)),
  setEnableRichTextEditor: (enable) => dispatch(setEnableRichTextEditor(enable)),
  
});
export default connect(mapStateToProps, mapDispatchToProps)(RichText);
