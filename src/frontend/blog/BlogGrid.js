import React, { Component } from "react";
import Breadcaump from '../components/Breadcaump';
import BlogPost from './components/BlogPost';
import Footer from '../components/Footer';


class BlogGrid extends Component{
    render(){
        return(
            <div>

                {/* Breadcaump Area */}
                <Breadcaump />

                <BlogPost />
                {/* Footer */}
                <Footer horizontal="horizontal" />

            </div>
        )
    }
}
export default BlogGrid;

