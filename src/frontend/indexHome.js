// React Required
import React, { Component } from 'react';
import ReactDOM from 'react-dom';

// Create Import File
import './index.scss';
import {withRouter} from 'react-router-dom';
import Demo from './page-demo/Demo';
import HomeOlive from './HomeOlive';
import HomeHorizontal from './HomeHorizontal';
import BlogGrid from './blog/BlogGrid';

import BlogTwoColumn from './blog/BlogTwoColumn';
import BlogDetails from './blog/BlogDetails';
import CRMDashboard from ".../../admin/Dashboards/CRM/";
import Login from ".../../admin/Login/";



import {BrowserRouter, Switch, Route} from 'react-router-dom';
import * as serviceWorker from './serviceWorker';

class Root extends Component{

    render(){
        return(
            <Route path={`${this.props.match.url}/`} component={HomeOlive} />    
        )
    }
}

export default Root;
serviceWorker.register();


