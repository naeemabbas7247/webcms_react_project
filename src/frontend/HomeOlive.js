import React, { Component } from "react";
import ReactDOM from "react-dom";
import Banner from "./components/Banner";
import About from "./components/About";
import WhyUs from "./components/whyUs";
import Service from "./components/Service";
import Feature from "./components/Feature";
import Download from "./components/Download";
import Pricing from "./components/Pricing";
import Testimonial from "./components/Testimonial";
import Subscription from "./components/Subscription";
import TextModal from "./components/Models/TextModal";
import AddModal from "./components/Models/AddModal";
import SocialModal from "./components/Models/SocialModal";
import MediaModal from "./components/Models/MediaModal";
import IconModal from "./components/Models/IconModal";
import PlaceholderModal from "./components/Models/PlaceholderModal";
import Screenshot from "./components/Screenshot";
import Navbar from "./components/Navbar";
import Blog from "./components/Blog";
import Footer from "./components/Footer";
import RichText from "./components/Functions/RichText";
import "../index.scss";
import ApiCall from "../admin/Partial/Function/ApiCall";
import { useDispatch } from "react-redux";
import { SketchPicker } from "react-color";
import ImageUploader from "react-images-upload";
import namedColors from "color-name-list";
import {
  setCmsArray,
  setCmsModel,
  setCmsKey,
  setCmsValue,
  setThemeBackgroundColor,
  setThemeColor,
  setEditorState,
  setModuleCmsObject,
  setCmsEnable,
  setCmsEnableBackground,
  setCmsEnableUploadImage,
  setCmsModelImage,
  setIconModel,
  setThemeSectionBackgroundColor,
  setSectionBackgroundColor,
  setCmsImageArray,
  setPreviousColorsValue,
  setCmsVideoArray,
  setCmsSection,
  setBackgroundCmsKey,
  setContactUsEmail,
  setSubscriptionEmail,
  setIsGlobal,
  setSectionArrayModel,
  setCustomStyle,
  setTypesArray,
  setPlaceHolderModel,
  setSectionId
} from "../reducers/ThemeOptions";
import { connect } from "react-redux";
import {
  EditorState,
  ContentState,
  convertFromHTML,
  convertFromRaw,
  convertToRaw,
} from "draft-js";
import { Editor } from "react-draft-wysiwyg";
import draftToHtml from "draftjs-to-html";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import Dropzone from "react-dropzone";
import Resizer from "react-image-file-resizer";
import {
  UncontrolledButtonDropdown,
  DropdownToggle,
  Dropdown,
  DropdownMenu,
  DropdownItem,
  Nav,
  NavItem,
  NavLink,
  Container,
  CustomInput,
} from "reactstrap";
import { Card, CardBody, CardTitle } from "reactstrap";
import {
  Form,
  Row,
  Col,
  ListGroup,
  ListGroupItem,
  FormGroup,
  Label,
  Input,
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
} from "reactstrap";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import "react-tabs/style/react-tabs.css";
import ReactResumableJs from "../ReactResumableJs";
import Loader from "react-loaders";
import ContactUs from "./components/ContactUs";
let flag = 0;
class HomeOlive extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pictures: [],
      cmsExtra: [{ key: "", value: "" }],
      inputs: ["input-0"],
      theme_color: "red",
      previousColors: [],
      editorState: EditorState.createWithContent(
        ContentState.createFromBlockArray(
          convertFromHTML("this.props.cmsValue")
        )
      ),
      videos: [],
      youtubeLink: null,
      is_global: false,
      contactusemail: "",
      subscriptionemail: "",
    };
  }
  handleChangeComplete = async (color) => {
    if (this.props.themeSectionBackgroundColor) {
      const { setCmsValue } = this.props;
      setCmsValue(color.hex);
    } else {
      this.setState({ theme_color: color.hex });
    }
  };
  setCstyle = async (data) => {
    const { setCustomStyle } = this.props;
    await setCustomStyle(data);
  };
  handleChangeBackgroundComplete = async (color) => {
    var tempArray = {
      key: this.props.sectionBackgroundColor["key"],
      value: color.hex,
    };
    const { setSectionBackgroundColor } = this.props;
    setSectionBackgroundColor(tempArray);
  };
  componentWillMount() {
    this.getData();
    this.getColors();
  }
  getData = async () => {
    let sectionArray = await ApiCall.getAPICall("sections");
    this.setSectionArray(sectionArray.sections);
    let data = await ApiCall.getAPICall("getCms");
    console.log("The cmd data is here"+JSON.stringify(data.data));
    let backup = data;
    var cmsArray = this.props.cmsArray;
    cmsArray = Object.assign(cmsArray, data.data);
    console.log("The cms array is here"+cmsArray);
    await this.setCmsData(data.data);
    const { setIsGlobal } = this.props;
    setIsGlobal(backup.is_global == 0 ? false : true);
    this.setState({ is_global: backup.is_global == 0 ? false : true });
    this.setSectionDataModel(backup.sections);
    this.setState({ theme_color: backup.color.value });
    this.setCmsModule(backup.modules);
    let cstyle = await ApiCall.getAPICall("custom/style/get");
    this.types();
    this.setCstyle(cstyle.customStyle);
    this.setColorScheme();
    // let images = await ApiCall.getAPICall("getPageImages");
    // var a = this.props.cmsArray;
    // var b = await Object.assign(a, images.data);
    // await this.setCmsData(b);
    const { cmsEnable, setCmsEnable } = this.props;
    await setCmsEnable(!this.props.cmsEnable);
    await setCmsEnable(!this.props.cmsEnable);
  };
  types = async () => {
    let types = await ApiCall.getAPICall('type');
    const { setTypesArray } = this.props;
    await setTypesArray(types.types);
  };
  setSectionDataModel = async (data) => {
    const { setSectionArrayModel } = this.props;
    await setSectionArrayModel(data);
  };
  setSectionArray = async (data) => {
    const { setCmsSection } = this.props;
    await setCmsSection(data);
  };
  getColors = async () => {
    let colors = await ApiCall.getAPICall("colors/get");
    const { setCmsImageArray } = this.props;
    await setPreviousColorsValue(colors.ColorsBackup);
    this.setState({ previousColors: colors.ColorsBackup });
  };
  setCmsModule = async (data) => {
    const { moduleCmsObject, setModuleCmsObject } = this.props;
    await setModuleCmsObject(data);
  };
  setColorScheme = async () => {
    let color = {
      color: this.state.theme_color,
    };
    let background = {
      background: this.state.theme_color,
    };
    const { themeColor, setThemeColor } = this.props;
    await setThemeColor(color);
    const { themeBackgroundColor, setThemeBackgroundColor } = this.props;
    await setThemeBackgroundColor(background);
  };
  setCmsData = async (data) => {
    const { cmsArray, setCmsArray } = this.props;
    await setCmsArray(data);
  };
  sectionDelete = async (key, id) => {
    var url = "clone/delete";
    var data = {
      key: key,
      id: id,
    };
    console.log(data);
    await ApiCall.postAPICall(url, data);
    this.getData();
  };
  SetPlaceholder=async(section_id,key)=>
  {
    if (this.props.cmsEnable) {
      const { placeHolderModel, setPlaceHolderModel } = this.props;
      setPlaceHolderModel(!this.props.placeHolderModel);
      const { cmsKey, setCmsKey } = this.props;
      const { cmsValue, setCmsValue } = this.props;
      const {setSectionId } = this.props;
      setSectionId(section_id);
      if(typeof(this.props.cmsArray[section_id+'_'+key]) != 'undefined')
      {
        setCmsValue(this.props.cmsArray[section_id+'_'+key][section_id+'_'+key]);
      }
      setCmsKey(section_id+'_'+key);
    }
  }
  render() {
    return (
      <div style={{ "--global-color-var": this.state.theme_color }}>
        <SocialModal getData={this.getData} />
        <AddModal getData={this.getData} />
        <TextModal  getData={this.getData}/>
        <MediaModal />
        <IconModal />
        <PlaceholderModal/>
        <div>
          {this.props.cmsSection.length > 0
            ? Object.keys(this.props.cmsSection).map((val, i) => (
                <div>
                  {this.props.cmsSection[val]["module_name"] == "Navbar" ? (
                    <Navbar
                      bgshape="bg-shape"
                      section_flag={false}
                      getData={this.getData}
                      class_set="vertical"
                      getColors={this.getColors}
                      module={this.props.cmsSection[val]["module_name"]}
                      horizontal=""
                      {...[this.props.cmsSection[val]["key"]]}
                    />
                  ) : (
                    ""
                  )}
                  {this.props.cmsSection[val]["module_name"] == "Banner" ? (
                    <Banner
                      bgshape="bg-shape"
                      section_flag={false}
                      class_set={this.props.cmsSection[val]["is_horizontal"] ? "horizontal" :null }
                      getData={this.getData}
                      getColors={this.getColors}
                      module={this.props.cmsSection[val]["module_name"]}
                      horizontal=""
                      {...[this.props.cmsSection[val]["key"]]}
                    />
                  ) : (
                    ""
                  )}
                  
                  {this.props.cmsSection[val]["module_name"] == "About" ? (
                    <About
                      horizontalabout="vertical-about"
                      section_flag={false}
                      getData={this.getData}
                      class_set="vertical"
                      getColors={this.getColors}
                      module={this.props.cmsSection[val]["module_name"]}
                      {...[this.props.cmsSection[val]["key"]]}
                    />
                  ) : (
                    ""
                  )}
                  {this.props.cmsSection[val]["module_name"] == "whyUs" ? (
                    <WhyUs
                      horizontalwhyUs="vertical-whyUs"
                      section_flag={false}
                      getData={this.getData}
                      class_set="vertical"
                      getColors={this.getColors}
                      module={this.props.cmsSection[val]["module_name"]}
                      {...[this.props.cmsSection[val]["key"]]}
                    />
                  ) : (
                    ""
                  )}
                  {this.props.cmsSection[val]["module_name"] == "Service" ? (
                    <Service
                      horizontal="vertical-service"
                      section_flag={false}
                      getData={this.getData}
                      class_set="vertical"
                      getColors={this.getColors}
                      module={this.props.cmsSection[val]["module_name"]}
                      {...[this.props.cmsSection[val]["key"]]}
                    />
                  ) : (
                    ""
                  )}
                  {this.props.cmsSection[val]["module_name"] == "Feature" ? (
                    <Feature
                      horizontalfeature="vertical-feature"
                      section_flag={false}
                      class_set="vertical"
                      getData={this.getData}
                      getColors={this.getColors}
                      module={this.props.cmsSection[val]["module_name"]}
                      {...[this.props.cmsSection[val]["key"]]}
                    />
                  ) : (
                    ""
                  )}
                  {this.props.cmsSection[val]["module_name"] == "Download" ? (
                    <Download
                      horizontal=""
                      class_set={this.props.cmsSection[val]["is_horizontal"] ? "horizontal" : "vertical" }
                      section_flag={false}
                      getData={this.getData}
                      getColors={this.getColors}
                      module={this.props.cmsSection[val]["module_name"]}
                      {...[this.props.cmsSection[val]["key"]]}
                    />
                  ) : (
                    ""
                  )}
                  {this.props.cmsSection[val]["module_name"] == "Pricing" ? (
                    <Pricing
                      horizontalpricing="vertical-pricing"
                      sectionDelete={this.sectionDelete}
                      section_flag={false}
                      class_set="vertical"
                      getData={this.getData}
                      getColors={this.getColors}
                      module={this.props.cmsSection[val]["module_name"]}
                      {...[this.props.cmsSection[val]["key"]]}
                    />
                  ) : (
                    ""
                  )}
                  {this.props.cmsSection[val]["module_name"] ==
                  "Testimonial" ? (
                    <Testimonial
                      section_flag={false}
                      getData={this.getData}
                      getColors={this.getColors}
                      class_set="vertical"
                      module={this.props.cmsSection[val]["module_name"]}
                      {...[this.props.cmsSection[val]["key"]]}
                    />
                  ) : (
                    ""
                  )}
                  {this.props.cmsSection[val]["module_name"] == "Blog" ? (
                    <Blog
                      section_flag={false}
                      getData={this.getData}
                      sectionDelete={this.sectionDelete}
                      getColors={this.getColors}
                      class_set="vertical"
                      module={this.props.cmsSection[val]["module_name"]}
                      {...[this.props.cmsSection[val]["key"]]}
                    />
                  ) : (
                    ""
                  )}
                  {this.props.cmsSection[val]["module_name"] ==
                  "ContactUs" ? (
                    <ContactUs
                      section_flag={false}
                      class_set={this.props.cmsSection[val]["is_horizontal"] ? "horizontal" :null }
                      getData={this.getData}
                      SetPlaceholder={this.SetPlaceholder}
                      getColors={this.getColors}
                      class_set="vertical"
                      horizontal="vertical-footer"
                      module={this.props.cmsSection[val]["module_name"]}
                      {...[this.props.cmsSection[val]["key"]]}
                    />
                  ) : (
                    ""
                  )}
                  {this.props.cmsSection[val]["module_name"] ==
                  "Subscription" ? (
                    <Subscription
                      section_flag={false}
                      getData={this.getData}
                      getColors={this.getColors}
                      SetPlaceholder={this.SetPlaceholder}
                      class_set="vertical"
                      horizontal="vertical-footer"
                      module={this.props.cmsSection[val]["module_name"]}
                      {...[this.props.cmsSection[val]["key"]]}
                    />
                  ) : (
                    ""
                  )}
                  {this.props.cmsSection[val]["module_name"] == "Footer" ? (
                    <Footer
                      section_flag={false}
                      getData={this.getData}
                      getColors={this.getColors}
                      class_set="vertical"
                      horizontal="vertical-footer"
                      module={this.props.cmsSection[val]["module_name"]}
                      {...[this.props.cmsSection[val]["key"]]}
                    />
                  ) : (
                    ""
                  )}
                </div>
              ))
            : ""}
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => ({
  cmsArray: state.ThemeOptions.cmsArray,
  cmsModel: state.ThemeOptions.cmsModel,
  cmsKey: state.ThemeOptions.cmsKey,
  cmsValue: state.ThemeOptions.cmsValue,
  themeColor: state.ThemeOptions.themeColor,
  themeBackgroundColor: state.ThemeOptions.themeBackgroundColor,
  editorState: state.ThemeOptions.editorState,
  moduleCmsObject: state.ThemeOptions.moduleCmsObject,
  cmsEnableBackground: state.ThemeOptions.cmsEnableBackground,
  cmsEnable: state.ThemeOptions.cmsEnable,
  cmsEnableUploadImage: state.ThemeOptions.cmsEnableUploadImage,
  cmsModelImage: state.ThemeOptions.cmsModelImage,
  iconModel: state.ThemeOptions.iconModel,
  themeSectionBackgroundColor: state.ThemeOptions.themeSectionBackgroundColor,
  sectionBackgroundColor: state.ThemeOptions.sectionBackgroundColor,
  cmsImageArray: state.ThemeOptions.cmsImageArray,
  previousColors: state.ThemeOptions.previousColors,
  cmsVideoArray: state.ThemeOptions.cmsVideoArray,
  cmsSection: state.ThemeOptions.cmsSection,
  backgroundCmsKey: state.ThemeOptions.backgroundCmsKey,
  sectionId: state.ThemeOptions.sectionId,
  contact_us_email: state.ThemeOptions.contact_us_email,
  subscription_email: state.ThemeOptions.subscription_email,
  isGlobal: state.ThemeOptions.isGlobal,
  sectionArrayModel: state.ThemeOptions.sectionArrayModel,
  typesArray: state.ThemeOptions.typesArray,
  placeHolderModel: state.ThemeOptions.placeHolderModel,
});
const mapDispatchToProps = (dispatch) => ({
  setCmsArray: (enable) => dispatch(setCmsArray(enable)),
  setCmsModel: (enable) => dispatch(setCmsModel(enable)),
  setCmsKey: (enable) => dispatch(setCmsKey(enable)),
  setCmsValue: (enable) => dispatch(setCmsValue(enable)),
  setThemeColor: (enable) => dispatch(setThemeColor(enable)),
  setThemeBackgroundColor: (enable) =>
    dispatch(setThemeBackgroundColor(enable)),
  setEditorState: (enable) => dispatch(setEditorState(enable)),
  setModuleCmsObject: (enable) => dispatch(setModuleCmsObject(enable)),
  setCmsEnableBackground: (enable) => dispatch(setCmsEnableBackground(enable)),
  setCmsEnable: (enable) => dispatch(setCmsEnable(enable)),
  setCmsEnableUploadImage: (enable) =>
    dispatch(setCmsEnableUploadImage(enable)),
  setCmsModelImage: (enable) => dispatch(setCmsModelImage(enable)),
  setIconModel: (enable) => dispatch(setIconModel(enable)),
  setThemeSectionBackgroundColor: (enable) =>
    dispatch(setThemeSectionBackgroundColor(enable)),
  setSectionBackgroundColor: (enable) =>
    dispatch(setSectionBackgroundColor(enable)),
  setCmsImageArray: (enable) => dispatch(setCmsImageArray(enable)),
  setPreviousColorsValue: (enable) => dispatch(setPreviousColorsValue(enable)),
  setCmsVideoArray: (enable) => dispatch(setCmsVideoArray(enable)),
  setCmsSection: (enable) => dispatch(setCmsSection(enable)),
  setBackgroundCmsKey: (enable) => dispatch(setBackgroundCmsKey(enable)),
  setContactUsEmail: (enable) => dispatch(setContactUsEmail(enable)),
  setSubscriptionEmail: (enable) => dispatch(setSubscriptionEmail(enable)),
  setIsGlobal: (enable) => dispatch(setIsGlobal(enable)),
  setSectionArrayModel: (enable) => dispatch(setSectionArrayModel(enable)),
  setCustomStyle: (enable) => dispatch(setCustomStyle(enable)),
  setTypesArray: (enable) => dispatch(setTypesArray(enable)),
  setPlaceHolderModel: (enable) => dispatch(setPlaceHolderModel(enable)),
  setSectionId:(enable)=>dispatch(setSectionId(enable)),
});
export default connect(mapStateToProps, mapDispatchToProps)(HomeOlive);
// <Screenshot />