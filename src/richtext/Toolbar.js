import React from 'react'
import './Toolbar.css'
import {
  UncontrolledButtonDropdown,
  DropdownToggle,
  Dropdown,
  DropdownMenu,
  DropdownItem,
  Nav,
  NavItem,
  NavLink,
  Container,
  CustomInput,
} from "reactstrap";
export default function Toolbar() {
  function format(com, val) {
  document.execCommand(com, false, val);
  }
  function underline(){

  }

  return (
    <div className='toolbar'>
      <button onClick={e => format('bold')}>Bold</button>
      <button onClick={e => format('italic')}>Italics</button>
      <button onClick={e => underline()}>underline</button>
      <UncontrolledButtonDropdown>
        <DropdownToggle caret>
         Select
        </DropdownToggle>
         <DropdownMenu>
          <DropdownItem>1</DropdownItem>
          <DropdownItem>2</DropdownItem>
          <DropdownItem>3</DropdownItem>
          <DropdownItem>4</DropdownItem>
         </DropdownMenu>
      </UncontrolledButtonDropdown>
    </div>
  )
}