import React from 'react';
import Toolbar from './Toolbar';
import './Editor.css';
import {
  UncontrolledButtonDropdown,
  DropdownToggle,
  Dropdown,
  DropdownMenu,
  DropdownItem,
  Nav,
  NavItem,
  NavLink,
  Container,
  CustomInput,
} from "reactstrap";
export default function Editor2() {
  
  function paste(e) {
  e.preventDefault();
  const open = new RegExp('<', 'gi');
  const close = new RegExp('>', 'gi');
  const text = (e.originalEvent || e).clipboardData
   .getData('text/plain')
   .replace(open, '&lt')
   .replace(close, '&gt');
  document.execCommand('insertHTML', false, text)
}
  
  return (
    <React.Fragment>
      <Toolbar />
      <div
        id='title'
        contentEditable='true'
        data-placeholder='Title...'
        className='title'
      ></div>
      
      <div
        className='editor'
        id='editor'
        contentEditable='true'
        data-placeholder='Body...'
        onPaste={(e) => paste(e)}
      ></div>
    </React.Fragment>
  );
}