import React, { Fragment, Component } from "react";
import Slider from "react-slick";
import {connect} from 'react-redux';
import bg3 from "../../../assets/utils/images/originals/citynights.jpg";
import { Col, Row, Button, Form, FormGroup, Label, Input } from "reactstrap";
import MyGlobleSetting from '../../MyGlobleSetting';
import axios from 'axios';
import { Loader, Types } from "react-loaders";
import LoadingOverlay from "react-loading-overlay";
import SweetAlert from "sweetalert-react";
import {
  setEnableActive,
  setEnableShow,
} from '../../../reducers/ThemeOptions';
class Register extends Component {
  constructor(props)
  {
    super(props);
    this.state=MyGlobleSetting.state;
    this.state.error_name='';
    this.state.error_confirmpassword='';
    this.state.error_email='';
    this.state.error_password='';
    this.state.show=false;
  }
  toggleEnableSubmit = () => {
    let { enableActive, setEnableActive } = this.props;
    setEnableActive(!enableActive);
  };
  handleName =(e)=>{
    this.setState({
      username: e.target.value
    });
    this.handleCheckField('username');
  };
  toggleEnableShow = () => {
    let { enableShow, setEnableShow } = this.props;
    setEnableShow(!enableShow);
  };
  handleEmail=(e)=>{
    this.setState({
      email: e.target.value
    })
    this.handleCheckField('email');
  };
  handlePassword=(e)=>{
    this.setState({
      password: e.target.value
    });
    this.handleCheckField('password');
    this.handleCheckPassword(e.target.value,this.state.confirm_password);
  };
  handleConfirmPassword=(e)=>
  {
    this.setState({
      confirm_password: e.target.value
    });
    this.handleCheckPassword(this.state.password,e.target.value);
  };
  handleCheckField=(key)=>
  {
    let errorKey='error_'+key;
    this.setState({[errorKey]: '' })
  };
  handleCheckPassword=(password,confirm_password)=>{
    if (typeof password !== "undefined" && typeof confirm_password !== "undefined" && password != confirm_password ) { 
      this.setState({error_confirmpassword : "Passwords don't match" });
      this.setState({error_password : "Passwords don't match" });
    }
    else{
      this.setState({error_confirmpassword : '' });
      this.setState({error_password : '' });
    } 
  };
  handleSubmit=(e)=>{
    e.preventDefault();
    const data = {
      username: this.state.username,
      email: this.state.email,
      password: this.state.password,
    }
    let uri = this.state.url + '/register';
    this.toggleEnableSubmit();
    axios.post(uri, data).then((response) => {
      if(response.data.status)
      {
        this.setState({isLogin:true});
        localStorage.setItem('user', response.data.success.user);
        localStorage.setItem('isLogin', true);
        this.props.history.push("/dashboard");
      }
      else
      {
        const errors = response.data.error;
        Object.keys(errors).map(keyName=> {
          this.setState({[keyName]: errors[keyName] })
        });
      }
      this.toggleEnableSubmit();

    })
    .catch((error)=>
    {
      this.toggleEnableSubmit();
      this.toggleEnableShow();

    });
  }
  render() {
    let settings = {
      dots: true,
      infinite: true,
      speed: 500,
      arrows: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      fade: true,
      initialSlide: 0,
      autoplay: true,
      adaptiveHeight: true,
    };
    let {
      enableActive,
      enableShow
    } = this.props;
    return (
      <Fragment>
        <SweetAlert confirmButtonColor="" confirmButtonText="Close" title={this.state.unexpectedError} show={enableShow} type="error" onConfirm={() => this.toggleEnableShow()}/>
        <div className="h-100">
          <Row className="h-100 no-gutters">
            <Col lg="7" md="12" className="h-100 d-md-flex d-sm-block bg-white justify-content-center align-items-center">
              <Col lg="9" md="10" sm="12" className="mx-auto app-login-box">
                <div className="app-logo" />
                <h4>
                  <div>Welcome,</div>
                  <span>
                    It only takes a{" "}
                    <span className="text-success">few seconds</span> to create
                    your account
                  </span>
                </h4>
                <div>
                  <Form onSubmit={this.handleSubmit}>
                    <Row form>
                      <Col md={6}>
                        <FormGroup>
                          <Label for="exampleEmail">
                            <span className="text-danger">*</span> Email
                          </Label>
                          <Input onChange={this.handleEmail} type="email" name="email" id="exampleEmail" placeholder="Email here..."/>
                          {<span className="text-danger">{this.state.error_email}</span>}
                        </FormGroup>
                      </Col>
                      <Col md={6}>
                        <FormGroup>
                          <Label for="exampleName">Name</Label>
                          <Input onChange={this.handleName} type="text" name="text" id="exampleName" placeholder="Name here..."/>
                          {<span className="text-danger">{this.state.error_name}</span>}
                        </FormGroup>
                      </Col>
                      <Col md={6}>
                        <FormGroup>
                          <Label for="examplePassword">
                            <span className="text-danger">*</span> Password
                          </Label>
                          <Input onChange={this.handlePassword}  type="password" name="password" id="examplePassword" placeholder="Password here..."/>
                          {<span className="text-danger">{this.state.error_password}</span>}
                        </FormGroup>
                      </Col>
                      <Col md={6}>
                        <FormGroup>
                          <Label for="examplePasswordRep">
                            <span className="text-danger">*</span> Repeat
                            Password
                          </Label>
                          <Input type="password" onChange={this.handleConfirmPassword} name="passwordrep" id="examplePasswordRep" placeholder="Repeat Password here..."/>
                          {<span className="text-danger">{this.state.error_confirmpassword}</span>}
                        </FormGroup>
                      </Col>
                    </Row>
                    <FormGroup className="mt-3" check>
                      <Input type="checkbox" name="check" id="exampleCheck" />
                      <Label for="exampleCheck" check>
                        Accept our{" "}
                        <a href="https://colorlib.com/" onClick={(e) => e.preventDefault()}>
                          Terms and Conditions
                        </a>
                        .
                      </Label>
                    </FormGroup>
                    <div className="mt-4 d-flex align-items-center">
                      <h5 className="mb-0">
                        Already have an account?{" "}
                        <a href="#/admin/login" className="text-primary">
                          Sign in
                        </a>
                      </h5>
                      <div className="ml-auto">
                      {!enableActive ?
                        <Button color="primary" className="btn-wide btn-pill btn-shadow btn-hover-shine" size="lg">
                          Create Account
                        </Button>
                        : <LoadingOverlay tag="div" active={enableActive}
                          styles={{
                            overlay: (base) => ({
                              ...base,
                              background: "#fff",
                              opacity: 0.5,
                            }),
                          }}
                          spinner={<Loader active type={this.state.loaderType} />}>
                        </LoadingOverlay>
                      }
                      </div>
                    </div>
                  </Form>
                </div>
              </Col>
            </Col>
            <Col lg="5" className="d-lg-flex d-xs-none">
              <div className="slider-light">
                <Slider {...settings}>
                  <div className="h-100 d-flex justify-content-center align-items-center bg-premium-dark">
                    <div className="slide-img-bg"
                      style={{
                        backgroundImage: "url(" + bg3 + ")",
                      }}/>
                    <div className="slider-content">
                      <h3>Scalable, Modular, Consistent</h3>
                      <p>
                        Easily exclude the components you don't require.
                        Lightweight, consistent Bootstrap based styles across
                        all elements and components
                      </p>
                    </div>
                  </div>
                </Slider>
              </div>
            </Col>
          </Row>
        </div>
      </Fragment>
    );
  }
}
const mapStateToProps = state => ({
  enableActive: state.ThemeOptions.enableActive,
  enableShow: state.ThemeOptions.enableShow,
});

const mapDispatchToProps = dispatch => ({
  setEnableActive: enable => dispatch(setEnableActive(enable)),
  setEnableShow: enable => dispatch(setEnableShow(enable))
});

export default connect(mapStateToProps, mapDispatchToProps)(Register);
