import React, { Fragment } from "react";
import { Route } from "react-router-dom";

// USER PAGES

import Login from "./Login/";
import LoginBoxed from "./LoginBoxed/";

import Register from "./Register/";
import RegisterBoxed from "./RegisterBoxed/";

import ForgotPassword from "./ForgotPassword/";
import ForgotPasswordBoxed from "./ForgotPasswordBoxed/";

class UserPages extends React.Component {
  constructor(props){
    super(props);
    if(localStorage.getItem('isLogin'))
    {
      window.location.href = "/#/dashboard";
    }
  }
  render() {
    return (
      <Fragment>
        <div className="app-container">
          {/* User Pages */}

          <Route path={`${this.props.match.url}/login`} component={Login} />
          <Route path={`${this.props.match.url}/login-boxed`} component={LoginBoxed} />
          <Route path={`${this.props.match.url}/register`} component={Register} />
          <Route path={`${this.props.match.url}/register-boxed`} component={RegisterBoxed} />
          <Route path={`${this.props.match.url}/forgot-password`} component={ForgotPassword} />
          <Route path={`${this.props.match.url}/forgot-password-boxed`} component={ForgotPasswordBoxed}/>
        </div>
      </Fragment>
    );
  }
}

export default UserPages;
