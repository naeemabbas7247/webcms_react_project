import React, { Fragment,Component, } from "react";
import Slider from "react-slick";
import {connect} from 'react-redux';
import bg1 from "../../../assets/utils/images/originals/city.jpg";
import bg2 from "../../../assets/utils/images/originals/citydark.jpg";
import bg3 from "../../../assets/utils/images/originals/citynights.jpg";
import { Col, Row, Form,Button, FormGroup, Label, Input } from "reactstrap";
import MyGlobleSetting from '../../MyGlobleSetting';
import axios from 'axios';
import { Loader, Types } from "react-loaders";
import LoadingOverlay from "react-loading-overlay";
import SweetAlert from "sweetalert-react";
import {
    setEnableActive,
    setEnableShow,
} from '../../../reducers/ThemeOptions';

class Login extends Component {
    constructor(props){
      super(props);
      this.state=MyGlobleSetting.state;
      this.state.error_email='';
      this.state.error_password='';
    }
    toggleEnableSubmit = () => {
      let { enableActive, setEnableActive } = this.props;
      setEnableActive(!enableActive);
    };
    toggleEnableShow = () => {
      let { enableShow, setEnableShow } = this.props;
      setEnableShow(!enableShow);
    };
    handleEmail = (e)=>{
      this.setState({
        email: e.target.value
      });
    };
    handlePassword = (e)=>{
      this.setState({
        password:e.target.value
      });
    };
    handleSubmit=(e)=>{
    e.preventDefault();
    const data = {
      email: this.state.email,
      password: this.state.password,
    }
    let uri = this.state.url + '/login';
    this.toggleEnableSubmit();
    axios.post(uri, data).then((response) => {
      if(response.data.status)
      {
        this.setState({isLogin:true});
        localStorage.setItem('user', response.data.success.user);
        localStorage.setItem('isLogin', true);
        this.props.history.push("/dashboard");
      }
      else
      {
        const errors = response.data.error;
        this.setState({error_email: errors });
      }
      this.toggleEnableSubmit();
    })
    .catch((error)=>
    {
      this.toggleEnableShow();
      // this.setState({show:true});
      this.toggleEnableSubmit();
    })
    ;
  }
  render() {
    let settings = {
      dots: true,
      infinite: true,
      speed: 500,
      arrows: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      fade: true,
      initialSlide: 0,
      autoplay: true,
      adaptiveHeight: true,
    };
    let {
      enableActive,
      enableShow,
    } = this.props; 
    return (
      <Fragment>
        <SweetAlert confirmButtonColor="" confirmButtonText="Close" title={this.state.unexpectedError} show={enableShow} type="error" onConfirm={() => this.toggleEnableShow()}/>
        <div className="h-100">
          <Row className="h-100 no-gutters">
            <Col lg="4" className="d-none d-lg-block">
              <div className="slider-light">
                <Slider {...settings}>
                  <div className="h-100 d-flex justify-content-center align-items-center bg-plum-plate">
                    <div className="slide-img-bg"
                      style={{
                        backgroundImage: "url(" + bg1 + ")",
                      }}/>
                    <div className="slider-content">
                      <h3>Perfect Balance</h3>
                      <p>
                        ArchitectUI is like a dream. Some think it's too good to
                        be true! Extensive collection of unified React Boostrap
                        Components and Elements.
                      </p>
                    </div>
                  </div>
                  <div className="h-100 d-flex justify-content-center align-items-center bg-premium-dark">
                    <div className="slide-img-bg"
                      style={{
                        backgroundImage: "url(" + bg3 + ")",
                      }}/>
                    <div className="slider-content">
                      <h3>Scalable, Modular, Consistent</h3>
                      <p>
                        Easily exclude the components you don't require.
                        Lightweight, consistent Bootstrap based styles across
                        all elements and components
                      </p>
                    </div>
                  </div>
                  <div className="h-100 d-flex justify-content-center align-items-center bg-sunny-morning">
                    <div className="slide-img-bg opacity-6"
                      style={{
                        backgroundImage: "url(" + bg2 + ")",
                      }}/>
                    <div className="slider-content">
                      <h3>Complex, but lightweight</h3>
                      <p>
                        We've included a lot of components that cover almost all
                        use cases for any type of application.
                      </p>
                    </div>
                  </div>
                </Slider>
              </div>
            </Col>
            <Col lg="8" md="12" className="h-100 d-flex bg-white justify-content-center align-items-center">
              <Col lg="9" md="10" sm="12" className="mx-auto app-login-box">
                <div className="app-logo" />
                <h4 className="mb-0">
                  <div>Welcome back,</div>
                  <span>Please sign in to your account.</span>
                </h4>
                <h6 className="mt-3">
                  No account?{" "}
                  <a href="#/admin/register" className="btn-lg btn btn-link" >
                    Sign up now
                  </a>
                </h6>
                <Row className="divider" />
                <div>
                  <Form onSubmit={this.handleSubmit}>
                    <Row form>
                      <Col md={6}>
                        <FormGroup>
                          <Label for="exampleEmail">Email</Label>
                          <Input type="email" onChange={this.handleEmail} name="email" id="exampleEmail" placeholder="Email here..."/>
                          {<span className="text-danger">{this.state.error_email}</span>}
                        </FormGroup>
                      </Col>
                      <Col md={6}>
                        <FormGroup>
                          <Label for="examplePassword">Password</Label>
                          <Input type="password" onChange={this.handlePassword} name="password" id="examplePassword" placeholder="Password here..."/>
                        </FormGroup>
                      </Col>
                    </Row>
                    <FormGroup check>
                      <Input type="checkbox" name="check" id="exampleCheck" />
                      <Label for="exampleCheck" check>
                        Keep me logged in
                      </Label>
                    </FormGroup>
                    <Row className="divider" />
                    <div className="d-flex align-items-center">
                      <div className="ml-auto">
                        {!enableActive ?
                          <Fragment>
                            <a href="#/admin/forgot-password" className="btn-lg btn btn-link" >
                            Recover Password</a>
                            <Button color="primary" size="lg">
                              Login to Dashboard
                            </Button>
                          </Fragment> 
                        : <LoadingOverlay tag="div" active={enableActive}
                          styles={{
                            overlay: (base) => ({
                              ...base,
                              background: "#fff",
                              opacity: 0.5,
                            }),
                          }}
                          spinner={<Loader active type={this.state.loaderType} />}>
                        </LoadingOverlay>
                      }
                      </div>
                    </div>
                  </Form>
                </div>
              </Col>
            </Col>
          </Row>
        </div>
      </Fragment>
    );
  }
}
const mapStateToProps = state => ({
  enableActive: state.ThemeOptions.enableActive,
  enableShow: state.ThemeOptions.enableShow,
});

const mapDispatchToProps = dispatch => ({
  setEnableActive: enable => dispatch(setEnableActive(enable)),
  setEnableShow: enable => dispatch(setEnableShow(enable))
});
export default connect(mapStateToProps, mapDispatchToProps)(Login);

