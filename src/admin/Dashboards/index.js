import React, { Fragment } from "react";
import { Route } from "react-router-dom";
import CRMDashboard from "./CRM/";
// Layout

import AppHeader from "../Layout/AppHeader/";
import AppSidebar from "../Layout/AppSidebar/";
import AppFooter from "../Layout/AppFooter/";

// Theme Options
import ThemeOptions from "../Layout/ThemeOptions/";

export default class Dashboards extends React.Component{
  constructor(props){
    super(props);
  }
  render() { 
    const match=this.props.match;
    return (
      <Fragment>
        <ThemeOptions />
        <AppHeader {...this.props}/>
        <div className="app-main">
          <AppSidebar />
          <div className="app-main__outer">
            <div className="app-main__inner">
              <Route path={`${match.url}`} component={CRMDashboard} />
            </div>
            <AppFooter />
          </div>
        </div>
      </Fragment>
    );
  }
}
