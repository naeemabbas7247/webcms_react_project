import React, { Component, Fragment } from "react";
import CSSTransitionGroup from "react-transition-group/CSSTransitionGroup";
import CRMDashboard1 from "./Examples/Variation1";
export default class CRMDashboard extends Component {
  constructor(props){
    super(props);
  }
  render() {
    return (
      <Fragment>
        <CSSTransitionGroup component="div" transitionName="TabsAnimation" transitionAppear={true}
          transitionAppearTimeout={0} transitionEnter={false} transitionLeave={false}>
            <CRMDashboard1 {...this.props} />
        </CSSTransitionGroup>
      </Fragment>
    );
  }
}
