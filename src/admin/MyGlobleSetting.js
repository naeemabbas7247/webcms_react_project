import {Component} from "react";
class MyGlobleSetting extends Component {
  	constructor(props) {
  		super(props);
		this.state = {
			// url:'https://api.test.xweb4u.com/api',
			url:'http://127.0.0.1:8000/api',
			loaderType:'ball-clip-rotate-multiple',
			unexpectedError:'Network Error',
		};
  	}
}
export default (new MyGlobleSetting);