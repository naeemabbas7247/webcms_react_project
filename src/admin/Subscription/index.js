import React, { Fragment } from "react";
import { Route } from "react-router-dom";

// Tables
import Subscription from "./Subscription";
// Layout
import AppHeader from "../Layout/AppHeader/";
import AppSidebar from "../Layout/AppSidebar/";
import AppFooter from "../Layout/AppFooter/";
// Theme Options
import ThemeOptions from "../Layout/ThemeOptions/";

class Subscriptions extends React.Component {
  render() {
    let url=window.location.href.split(this.props.match.url)[1];
    return (
      <Fragment>
        <ThemeOptions />
        <AppHeader />
        <div className="app-main">
          {(url == '/listing') ? <AppSidebar /> : '' }
          <div className="app-main__outer">
            <div className="app-main__inner">
              {/* Tables */}
              <Route path={`${this.props.match.url}/listing`} component={Subscription} />
            </div>
            <AppFooter />
          </div>
        </div>
      </Fragment>
    );
  }
}

export default Subscriptions;
