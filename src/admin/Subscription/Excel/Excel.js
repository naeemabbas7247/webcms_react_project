import React, {Fragment,Component } from "react";
import ReactExport from "react-export-excel";
import {
  Button,
  Col
} from "reactstrap";
import {
  setSubscriptionExcel,
} from '../../../reducers/ThemeOptions';
import {connect} from 'react-redux';
const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;
class Excel extends React.Component {
    render() {
        return (
          <Fragment>
            <ExcelFile element=
            {<Button  className="mb-2 mr-2 btn-icon btn-shadow btn-outline-2x" outline color="warning">
              <i className="lnr-screen btn-icon-wrapper"> </i>
              Excel
            </Button>}>
                <ExcelSheet  data={this.props.subscriptionExcel} name="Audit">
                  <ExcelColumn label="Email" value="email"/>
                  <ExcelColumn label="Date" value="created_at"/>
                </ExcelSheet>
              </ExcelFile>
          </Fragment>
        );
    }
}
const mapStateToProps = state => ({
  subscriptionExcel: state.ThemeOptions.subscriptionExcel, 
});

const mapDispatchToProps = dispatch => ({
  setSubscriptionExcel: enable => dispatch(setSubscriptionExcel(enable)),

});

export default connect(mapStateToProps, mapDispatchToProps)(Excel);
