import React, { Component, Fragment } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import MetisMenu from "react-metismenu";
import { setEnableMobileMenu } from "../../../reducers/ThemeOptions";
import {
  RoleNav
} from "./RoleItems";
import ApiCall from "../../Partial/Function/ApiCall";
class NavRole extends Component {
  state = {};
  constructor(props){
    super(props);
    this.state=
    {
      roleSideBar:[],
    }
  }
  toggleMobileSidebar = () => {
    let { enableMobileMenu, setEnableMobileMenu } = this.props;
    setEnableMobileMenu(!enableMobileMenu);
  };
  async componentWillMount()
  {
    await this.getData();
  }
  getData=async()=>
  {
    let content_array=[];    
    let id=window.location.href.split(this.props.match.url)[1];
    id=id.match(/\d+/)[0];
    let data=await ApiCall.postAPICall('roles','');
    Object.keys(data.permissionsModule).map(keyName=> {
      var module_name=data.permissionsModule[keyName]['module_name'];
      var path=module_name+"/"+id;
      var url='#/role/permission/'+path;
      var obj ={
        label : module_name,
        to: url,
      };
      content_array.push(obj);
    });
    RoleNav[0]['content']=content_array;
    this.setState({roleSideBar:RoleNav});
  };
  render() {
    return (
      <Fragment>
        <h5 className="app-sidebar__heading">Menu</h5>
        <MetisMenu content={this.state.roleSideBar} onSelected={this.toggleMobileSidebar} activeLinkFromLocation
          className="vertical-nav-menu" iconNamePrefix="" classNameStateIcon="pe-7s-angle-down"/>
      </Fragment>
    );
  }

  isPathActive(path) {
    return this.props.location.pathname.startsWith(path);
  }
}
const mapStateToProps = (state) => ({
  enableMobileMenu: state.ThemeOptions.enableMobileMenu,
});

const mapDispatchToProps = (dispatch) => ({
  setEnableMobileMenu: (enable) => dispatch(setEnableMobileMenu(enable)),
});
export default connect(mapStateToProps, mapDispatchToProps)(withRouter(NavRole));
