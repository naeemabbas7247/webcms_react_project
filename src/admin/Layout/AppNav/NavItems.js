export const MainNav = [
  {
    icon: "pe-7s-browser",
    label: "Users Master",
    content: [
      {
        label: "users",
        to: "#/users/listing",
      },
      {
        label: "role",
        to: "#/role/listing",
      }
    ],    
  },
   {
    icon: "pe-7s-browser",
    label: "Audit",
    content: [
      {
        label: "Audit",
        to: "#/audits/listing",
      }
    ],    
  },
  ,
   {
    icon: "pe-7s-browser",
    label: "Contact",
    content: [
      {
        label: "Contact",
        to: "#/contact/listing",
      }
    ],    
  },
   ,
   {
    icon: "pe-7s-browser",
    label: "Subscription",
    content: [
      {
        label: "Subscription",
        to: "#/subscription/listing",
      }
    ],    
  },
];