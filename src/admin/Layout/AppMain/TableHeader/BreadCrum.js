import React, { Component, Fragment } from "react";

import { Breadcrumb, BreadcrumbItem } from "reactstrap";

import { faHome } from "@fortawesome/free-solid-svg-icons";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default class TitleComponent3 extends Component {
  render() {
    let {
      currentpath
    } = this.props;
    return (
      <Fragment>
        <Breadcrumb>
          <BreadcrumbItem>
            <a href="/#/dashboard">
              <FontAwesomeIcon icon={faHome} />
            </a>
          </BreadcrumbItem>
          <BreadcrumbItem>
            <a href="/#/dashboard">
              Dashboard
            </a>
          </BreadcrumbItem>
          <BreadcrumbItem active>{currentpath}</BreadcrumbItem>
        </Breadcrumb>
      </Fragment>
    );
  }
}
