import React, { Component, Fragment } from "react";

import {
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  Nav,
  NavItem,
  NavLink,
  Button,
  UncontrolledTooltip,
} from "reactstrap";

import { faStar, faBusinessTime } from "@fortawesome/free-solid-svg-icons";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import { toast, Slide } from "react-toastify";
import AddModal from "../../../Partial/Modal";
export default class AddButton extends Component {
  toggle(name) {
    this.setState({
      [name]: !this.state[name],
      progress: 0.5,
    });

  }
  render() {
    return (
      <Fragment>
        <AddModal {...this.props} />
      </Fragment>
    );
  }
}
