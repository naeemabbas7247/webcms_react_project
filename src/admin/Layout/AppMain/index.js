import { Route, Redirect } from "react-router-dom";
import React, { Suspense, lazy, Fragment } from "react";
import Loader from "react-loaders";

import { ToastContainer } from "react-toastify";
// Create Import File
const index = lazy(() => import("../../../frontend/indexHome"));
const Login = lazy(() => import("../../Login"));
const Dashboards = lazy(() => import("../../Dashboards"));
const Users = lazy(() => import("../../Users"));
const Role = lazy(() => import("../../Role"));
const Audits = lazy(() => import("../../Audits"));
const Contact = lazy(() => import("../../Contact"));
const Subscription = lazy(() => import("../../Subscription"));
const Video = lazy(() => import("../../Video"));
const Permissions = lazy(() => import("../../Permissions"));
const AppMain = () => {

    return (
        <Fragment>

            { /*home */}
            <Suspense fallback={
                <div className="loader-container">
                    <div className="loader-container-inner">
                        <div className="text-center">
                            <Loader type="ball-pulse-rise"/>
                        </div>
                        <h6 className="mt-5">
                            Hello Welcome to our site
                            <small>Because this is a demonstration we load at once all the Role examples. This wouldn't happen in a real live app!</small>
                        </h6>
                    </div>
                </div>
            }>
            <Route path="/home" component={index}/>
            </Suspense>
            { /*Video */}
            <Suspense fallback={
                <div className="loader-container">
                    <div className="loader-container-inner">
                        <div className="text-center">
                            <Loader type="ball-pulse-rise"/>
                        </div>
                        <h6 className="mt-5">
                            Please wait while we load all the Video examples
                            <small>Because this is a demonstration we load at once all the Role examples. This wouldn't happen in a real live app!</small>
                        </h6>
                    </div>
                </div>
            }>
            <Route path="/video" component={Video}/>
            </Suspense>
            { /*Audits */}
            <Suspense fallback={
                <div className="loader-container">
                    <div className="loader-container-inner">
                        <div className="text-center">
                            <Loader type="ball-pulse-rise"/>
                        </div>
                        <h6 className="mt-5">
                            Please wait while we load all the Audits examples
                            <small>Because this is a demonstration we load at once all the Role examples. This wouldn't happen in a real live app!</small>
                        </h6>
                    </div>
                </div>
            }>
                <Route path="/audits" component={Audits}/>
            </Suspense>
            { /*Contact */}
            <Suspense fallback={
                <div className="loader-container">
                    <div className="loader-container-inner">
                        <div className="text-center">
                            <Loader type="ball-pulse-rise"/>
                        </div>
                        <h6 className="mt-5">
                            Please wait while we load all the Contact examples
                            <small>Because this is a demonstration we load at once all the Role examples. This wouldn't happen in a real live app!</small>
                        </h6>
                    </div>
                </div>
            }>
                <Route path="/contact" component={Contact}/>
            </Suspense>
            { /*Subscription */}
            <Suspense fallback={
                <div className="loader-container">
                    <div className="loader-container-inner">
                        <div className="text-center">
                            <Loader type="ball-pulse-rise"/>
                        </div>
                        <h6 className="mt-5">
                            Please wait while we load all the Subscription examples
                            <small>Because this is a demonstration we load at once all the Role examples. This wouldn't happen in a real live app!</small>
                        </h6>
                    </div>
                </div>
            }>
                <Route path="/subscription" component={Subscription}/>
            </Suspense>
            
            { /*Permissions */}
            <Suspense fallback={
                <div className="loader-container">
                    <div className="loader-container-inner">
                        <div className="text-center">
                            <Loader type="ball-pulse-rise"/>
                        </div>
                        <h6 className="mt-5">
                            Please wait while we load all the Permissions examples
                            <small>Because this is a demonstration we load at once all the Role examples. This wouldn't happen in a real live app!</small>
                        </h6>
                    </div>
                </div>
            }>
                <Route path="/permissions" component={Permissions}/>
            </Suspense>

            {/* Role */}

            <Suspense fallback={
                <div className="loader-container">
                    <div className="loader-container-inner">
                        <div className="text-center">
                            <Loader type="ball-pulse-rise"/>
                        </div>
                        <h6 className="mt-5">
                            Please wait while we load all the Role examples
                            <small>Because this is a demonstration we load at once all the Role examples. This wouldn't happen in a real live app!</small>
                        </h6>
                    </div>
                </div>
            }>
                <Route path="/role" component={Role}/>
            </Suspense>

           

            {/* Pages */}

            <Suspense fallback={
                <div className="loader-container">
                    <div className="loader-container-inner">
                        <div className="text-center">
                            <Loader type="line-scale-party"/>
                        </div>
                        <h6 className="mt-3">
                            Please wait while we load all the Pages examples
                            <small>Because this is a demonstration we load at once all the Pages examples. This wouldn't happen in a real live app!</small>
                        </h6>
                    </div>
                </div>
            }>
                <Route path="/admin" component={Login}/>
            </Suspense>

        
            {/* Dashboards */}

            <Suspense fallback={
                <div className="loader-container">
                    <div className="loader-container-inner">
                        <div className="text-center">
                            <Loader type="ball-grid-cy"/>
                        </div>
                        <h6 className="mt-3">
                            Please wait while we load all the Dashboards examples
                            <small>Because this is a demonstration, we load at once all the Dashboards examples. This wouldn't happen in a real live app!</small>
                        </h6>
                    </div>
                </div>
            }>
                <Route path="/dashboard" component={Dashboards}/>
            </Suspense>

            <Suspense fallback={
                <div className="loader-container">
                    <div className="loader-container-inner">
                        <div className="text-center">
                            <Loader type="ball-pulse-rise"/>
                        </div>
                        <h6 className="mt-5">
                            Please wait while we load all the Users examples
                            <small>Because this is a demonstration we load at once all the Users examples. This wouldn't happen in a real live app!</small>
                        </h6>
                    </div>
                </div>
            }>
                <Route path="/users" component={Users}/>
            </Suspense>
            <Route exact path="/" render={() => (
                <Redirect to="/home"/>
            )}/>
            <ToastContainer/>
        </Fragment>
    )
};

export default AppMain;
