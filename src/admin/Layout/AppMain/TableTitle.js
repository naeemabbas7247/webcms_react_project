import React, { Component,Fragment} from "react";
import { connect } from "react-redux";
import cx from "classnames";
import BreadCrum from "./TableHeader/BreadCrum";


class PageTitle extends Component {
  randomize(myArray) {
    return myArray[Math.floor(Math.random() * myArray.length)];
  }

  render() {
    let {
      currentpath,
      enablePageTitleIcon,
      enablePageTitleSubheading,
      heading,
      icon,
      dataTableAddButton,
      subheading,
    } = this.props;

    return (
      <Fragment>
      <div className="app-page-title">
        <div className="page-title-wrapper">
          <div className="page-title-heading">
            <div className={cx("page-title-icon", {
                "d-none": !enablePageTitleIcon,
              })}>
              <i className={icon} />
            </div>
            <div>
              {heading}
              <div className={cx("page-title-subheading", {
                  "d-none": !enablePageTitleSubheading,
                })}>
                {subheading}
              </div>
            </div>
          </div>
          <div className="page-title-actions">
            <BreadCrum {...this.props} />
          </div>
        </div>
        
      </div>
      </Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  enablePageTitleIcon: state.ThemeOptions.enablePageTitleIcon,
  enablePageTitleSubheading: state.ThemeOptions.enablePageTitleSubheading,
});

const mapDispatchToProps = (dispatch) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(PageTitle);
