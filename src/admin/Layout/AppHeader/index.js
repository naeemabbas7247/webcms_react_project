import React, { Fragment } from "react";
import cx from "classnames";
import { connect } from "react-redux";
import CSSTransitionGroup from "react-transition-group/CSSTransitionGroup";
import HeaderLogo from "../AppLogo";
import UserBox from "./Components/UserBox";
import SweetAlert from "sweetalert-react";
import {browserHistory} from 'react-router';
class Header extends React.Component {
   constructor(props) {
    super(props);
    this.state = {
      notLogin: "Your Are Not Login"
    };
    if(!localStorage.getItem('isLogin'))
    {
      // window.location.href = "http://www.w3schools.com";
      console.log(this.props);
      browserHistory.push("/#/admin/login");
      window.location.reload();
    }
  }
  render() {
    let {
      headerBackgroundColor,
      enableMobileMenuSmall,
      enableHeaderShadow,
    } = this.props;
    return (
      <Fragment>
        <CSSTransitionGroup component="div"
          className={cx("app-header", headerBackgroundColor, {
            "header-shadow": enableHeaderShadow,
          })}
          transitionName="HeaderAnimation" transitionAppear={true} transitionAppearTimeout={1500}
          transitionEnter={false} transitionLeave={false}>
          <HeaderLogo />
          <div className={cx("app-header__content", {
              "header-mobile-open": enableMobileMenuSmall,
            })}>
            <div className="app-header-right">
              <UserBox  {...this.props}/>
            </div>
          </div>
        </CSSTransitionGroup>
      </Fragment>
    );
  }
}
const mapStateToProps = (state) => ({
  enableHeaderShadow: state.ThemeOptions.enableHeaderShadow,
  closedSmallerSidebar: state.ThemeOptions.closedSmallerSidebar,
  headerBackgroundColor: state.ThemeOptions.headerBackgroundColor,
  enableMobileMenuSmall: state.ThemeOptions.enableMobileMenuSmall,
});
const mapDispatchToProps = (dispatch) => ({});
export default connect(mapStateToProps, mapDispatchToProps)(Header);
