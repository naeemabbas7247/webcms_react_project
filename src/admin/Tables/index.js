import React, { Fragment } from "react";
import { Route } from "react-router-dom";

// Tables
import GridTables from "./GridTables";

// Layout

import AppHeader from "../Layout/AppHeader/";
import AppSidebar from "../Layout/AppSidebar/";
import AppFooter from "../Layout/AppFooter/";

// Theme Options

import ThemeOptions from "../../Layout/ThemeOptions/";

const Tables = ({ match }) => (
  <Fragment>
    <ThemeOptions />
    <AppHeader />
    <div className="app-main">
      <AppSidebar />
      <div className="app-main__outer">
        <div className="app-main__inner">
          {/* Tables */}
          <Route path={`${match.url}`} component={GridTables} />
        </div>
        <AppFooter />
      </div>
    </div>
  </Fragment>
);

export default Tables;
