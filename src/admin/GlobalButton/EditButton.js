import React, {Fragment,Component } from "react";
import CSSTransitionGroup from "react-transition-group/CSSTransitionGroup";
import {
  Col,
  Button,
  ModalHeader,
  ModalBody,
  ModalFooter,
} from "reactstrap";
import {connect} from 'react-redux';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Row, Card, CardBody, CardTitle, Container } from "reactstrap";
import {
  faEdit,
  faTrash
} from "@fortawesome/free-solid-svg-icons";
class EditButton extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <Fragment>
        <FontAwesomeIcon icon={faEdit} size="1x" />
      </Fragment>
    );
  }
}
export default EditButton;
