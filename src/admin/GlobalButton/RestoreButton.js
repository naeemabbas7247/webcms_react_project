import React, {Fragment,Component } from "react";
import {
  Button,
  Col
} from "reactstrap";
import {connect} from 'react-redux';
import { Row, Card, CardBody, CardTitle, Container } from "reactstrap";

class RestoreButton extends Component {
  render() {
    return (
      <Fragment>
	      <Button className="mb-2 mr-2 btn-icon btn-shadow btn-outline-2x" outline color="info">
          <i className="lnr-paperclip btn-icon-wrapper"> </i>
          Restore
        </Button>
      </Fragment>
    );
  }
}

export default RestoreButton;