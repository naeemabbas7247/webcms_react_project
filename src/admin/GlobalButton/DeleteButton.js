import React, {Fragment,Component } from "react";
import {
  Button,
  Col
} from "reactstrap";
import {connect} from 'react-redux';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Form,Row, Card, CardBody, CardTitle, Container } from "reactstrap";
import {
  faEdit,
  faTrash
} from "@fortawesome/free-solid-svg-icons";
class DeleteButton extends Component {
    constructor(props){
      super(props);
    }
  render() {
    return (
      <Fragment>
         <div className="text-danger">
           <FontAwesomeIcon  icon={faTrash} size="1x" />
          </div>
      </Fragment>
    );
  }
}
export default DeleteButton;
