import React, {Fragment,Component } from "react";
import {
  Button,
  Col
} from "reactstrap";
import {connect} from 'react-redux';
import { Row, Card, CardBody, CardTitle, Container } from "reactstrap";
import SweetAlert from "sweetalert-react";
class SelectedActiveButton extends Component {
   constructor(props){
      super(props);
  }
  render() {
    let {
      enableActiveTab,
    } = this.props;
    return (
      <Fragment>
        <Button  className="mb-2 mr-2 btn-icon btn-shadow btn-outline-2x" outline color="success">
          <i className="lnr-user btn-icon-wrapper"> </i>
          Select Active
        </Button>
      </Fragment>
    );
  }
}

export default SelectedActiveButton;