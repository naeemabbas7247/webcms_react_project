import React, {Fragment,Component } from "react";
import {
  Button,
  Col
} from "reactstrap";
import {connect} from 'react-redux';
import { Row, Card, CardBody, CardTitle, Container } from "reactstrap";

class ShowActiveButton extends Component {
  render() {
    return (
      <Fragment>
        <Button onClick={this.toggleTab}  className="mb-2 mr-2 btn-icon btn-shadow btn-outline-2x" outline color="primary">
            <i className="lnr-store btn-icon-wrapper"> </i>
            Show Active
        </Button>
      </Fragment>
    );
  }
}

export default ShowActiveButton;