import React, {Fragment,Component } from "react";
import {
  Button,
  Col
} from "reactstrap";
import {connect} from 'react-redux';
import { Row, Card, CardBody, CardTitle, Container } from "reactstrap";
class SelectedDeleteButton extends Component {
  constructor(props){
      super(props);
  }
  render() {
    return (
      	<Fragment>
          <Button className="mb-2 mr-2 btn-icon btn-shadow btn-outline-2x" outline color="danger">
            <i className="lnr-smartphone btn-icon-wrapper"> </i>
            SelectedDeleteButton
          </Button>
      </Fragment>
    );
  }
}

export default SelectedDeleteButton;