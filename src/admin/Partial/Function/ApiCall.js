import MyGlobleSetting from '../../MyGlobleSetting';
import axios from 'axios';
import configureStore from "../../../config/configureStore";
const base_url=MyGlobleSetting.state.url;
let activeData=[];
let deletedData=[];
const setDataTable = dataTable => ({
    type: "SET_DATATABLE",
    dataTable
});
const store = configureStore();
var ApiCall={
	async getAPICall(url)
	{
		try {
	      let response = await fetch(base_url + '/'+url, {
	        method: 'GET',
	        headers: new Headers({
	          'Content-Type': 'application/json',
	        }),
	      });
	      return response.json();
	    } catch (error) {
	      console.log(error);
	    }
	},
	async postAPICall(url,data)
	{
		// alert('Your url data is'+JSON.stringify(url));
		try {
	      let response = await fetch(base_url + '/'+url, {
	        method: 'POST',
	        headers: new Headers({
	          'Content-Type': 'application/json',
	        }),
	        body: JSON.stringify(data),
	      });

	      return response.json();
	    } catch (error) {
	      console.log(error);
	    }
	},

}
export default ApiCall;
