import React, {Fragment,Component } from "react";
import {
  Button,
  Col
} from "reactstrap";
import {connect} from 'react-redux';
import {
  setEnableActiveTab,
  setEnableDISACTIVETab,
} from '../../../reducers/ThemeOptions';
import { Row, Card, CardBody, CardTitle, Container } from "reactstrap";

class ExcelButton extends Component {
  toggleTab = () => {
    const { enableActiveTab, setEnableActiveTab,enableDISACTIVETab,setEnableDISACTIVETab } = this.props;
    setEnableActiveTab(!enableActiveTab);
    setEnableDISACTIVETab(!enableDISACTIVETab);
  };
  render() {
    return (
      <Fragment>
        <Button  className="mb-2 mr-2 btn-icon btn-shadow btn-outline-2x" outline color="warning">
            <i className="lnr-screen btn-icon-wrapper"> </i>
          Excel Export
        </Button>
      </Fragment>
    );
  }
}
const mapStateToProps = state => ({
  enableActiveTab: state.ThemeOptions.enableActiveTab,
  enableDISACTIVETab: state.ThemeOptions.enableDISACTIVETab,
});

const mapDispatchToProps = dispatch => ({
  setEnableActiveTab: enable => dispatch(setEnableActiveTab(enable)),
  setEnableDISACTIVETab: enable => dispatch(setEnableDISACTIVETab(enable)),
});
export default connect(mapStateToProps, mapDispatchToProps)(ExcelButton);
