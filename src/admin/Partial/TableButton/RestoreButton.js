import React, {Fragment,Component } from "react";
import {
  Button,
  Col
} from "reactstrap";
import {connect} from 'react-redux';
import {
  setEnableActiveTab,
  setEnableDISACTIVETab,
} from '../../../reducers/ThemeOptions';
import { Row, Card, CardBody, CardTitle, Container } from "reactstrap";

class RestoreButton extends Component {
  toggleTab = () => {
    const { enableActiveTab, setEnableActiveTab,enableDISACTIVETab,setEnableDISACTIVETab } = this.props;
    setEnableActiveTab(!enableActiveTab);
    setEnableDISACTIVETab(!enableDISACTIVETab);
  };
  render() {
    return (
      <Fragment>
	      <Button onClick={this.toggleTab} className="mb-2 mr-2 btn-icon btn-shadow btn-outline-2x" outline color="info">
          <i className="lnr-paperclip btn-icon-wrapper"> </i>
          Restore
        </Button>
      </Fragment>
    );
  }
}
const mapStateToProps = state => ({
  enableActiveTab: state.ThemeOptions.enableActiveTab,
  enableDISACTIVETab: state.ThemeOptions.enableDISACTIVETab,
});

const mapDispatchToProps = dispatch => ({
  setEnableActiveTab: enable => dispatch(setEnableActiveTab(enable)),
  setEnableDISACTIVETab: enable => dispatch(setEnableDISACTIVETab(enable)),
});
export default connect(mapStateToProps, mapDispatchToProps)(RestoreButton);
