import React, {Fragment,Component } from "react";
import {
  Button,
  Col
} from "reactstrap";
import {connect} from 'react-redux';
import {
    setPaddingTwo,
} from '../../../reducers/ThemeOptions';
import {
  CustomInput
} from "reactstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Row, Card, CardBody, CardTitle, Container } from "reactstrap";
import {
  faEdit,
  faTrash
} from "@fortawesome/free-solid-svg-icons";
class CheckBoxButton extends Component {
  render() {
    return (
      <CustomInput onChange={(e) => {this.handleCheckBoxClick(this.props.id)}} type="checkbox" id={this.props.id} label=""/>
    );
  }
}
const mapStateToProps = state => ({
  // checkBox: state.ThemeOptions.checkBox,
});

const mapDispatchToProps = dispatch => ({
  // setCheckBox: enable => dispatch(setCheckBox(enable)),
});
export default connect(mapStateToProps, mapDispatchToProps)(CheckBoxButton);
