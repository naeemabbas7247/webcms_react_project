import React, {Fragment,Component } from "react";
import {
  Button,
  Col
} from "reactstrap";
import {connect} from 'react-redux';
import {
  setEnableActiveTab,
  setEnableDISACTIVETab,
  setUserArray,
  setUserExcel
} from '../../../reducers/ThemeOptions';
import ApiCall from "../Function/ApiCall";
import { Row, Card, CardBody, CardTitle, Container } from "reactstrap";

class ShowActiveButton extends Component {
  toggleTab = async() => {
    const { enableActiveTab, setEnableActiveTab,enableDISACTIVETab,setEnableDISACTIVETab } = this.props;
    setEnableActiveTab(!enableActiveTab);
    setEnableDISACTIVETab(!enableDISACTIVETab);
    let getData=await ApiCall.getAPICall('users');
    var emptyUserArray=[];
    setUserArray(emptyUserArray);
    const {setUserExcel } = this.props;
    setUserExcel(getData.active);
  };
  render() {
    return (
      <Fragment>
        <Button onClick={this.toggleTab}  className="mb-2 mr-2 btn-icon btn-shadow btn-outline-2x" outline color="primary">
            <i className="lnr-store btn-icon-wrapper"> </i>
            Show Active
        </Button>
      </Fragment>
    );
  }
}
const mapStateToProps = state => ({
  enableActiveTab: state.ThemeOptions.enableActiveTab,
  enableDISACTIVETab: state.ThemeOptions.enableDISACTIVETab,
  userExcel: state.ThemeOptions.userExcel, 
  userArray: state.ThemeOptions.userArray,
});

const mapDispatchToProps = dispatch => ({
  setEnableActiveTab: enable => dispatch(setEnableActiveTab(enable)),
  setEnableDISACTIVETab: enable => dispatch(setEnableDISACTIVETab(enable)),
  setUserExcel: enable => dispatch(setUserExcel(enable)),
  setUserArray: enable => dispatch(setUserArray(enable)),
});
export default connect(mapStateToProps, mapDispatchToProps)(ShowActiveButton);