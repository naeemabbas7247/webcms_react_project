import React, {Fragment,Component } from "react";
import {
  Button,
  Col
} from "reactstrap";
import {connect} from 'react-redux';
import {
    setPaddingTwo,
} from '../../../reducers/ThemeOptions';

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Row, Card, CardBody, CardTitle, Container } from "reactstrap";
import {
  faEdit,
  faTrash
} from "@fortawesome/free-solid-svg-icons";
class EditButton extends Component {
  render() {
    return (
      <Fragment>
        <div className="text-primary">
           <FontAwesomeIcon icon={faEdit} size="2x" />
          </div>
      </Fragment>
    );
  }
}
const mapStateToProps = state => ({
  paddingTwo: state.ThemeOptions.paddingTwo,
});

const mapDispatchToProps = dispatch => ({
  setPaddingTwo: enable => dispatch(setPaddingTwo(enable)),
});
export default connect(mapStateToProps, mapDispatchToProps)(EditButton);
