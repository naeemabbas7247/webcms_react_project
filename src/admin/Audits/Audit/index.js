import React, { Fragment } from "react";
import {
  Row,
  Col,
  Card,
  CardBody,
  UncontrolledButtonDropdown,
  DropdownItem,
  DropdownMenu,
  Form,
  DropdownToggle,
  Container,
  CustomInput,
  Button,
  span,
  FormGroup,
  Label,
  Input,
  CardHeader,
  Collapse,
} from "reactstrap";
import { useDispatch } from 'react-redux';
import {
  setAuditArrayID,
  setAuditListArray,
  setAuditExcel,
  setAuditSearch,
} from '../../../reducers/ThemeOptions';
import {browserHistory} from 'react-router';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {connect} from 'react-redux';
import paginationFactory from 'react-bootstrap-table2-paginator';
import BootstrapTable from "react-bootstrap-table-next";
import filterFactory, { textFilter } from "react-bootstrap-table2-filter";
import ReactTable from "react-table";
import TableTitle from "../../Layout/AppMain/TableTitle";
import SelectedActiveButton from "../../GlobalButton/SelectedActiveButton";
import ShowActiveButton from "../../GlobalButton/ShowActiveButton";
import RestoreButton from "../../GlobalButton/RestoreButton";
import EditButton from "../../GlobalButton/EditButton";
import DeleteButton from "../../GlobalButton/DeleteButton";
import SelectedDeleteButton from "../../GlobalButton/SelectedDeleteButton";
import ActiveButton from "../../GlobalButton/ActiveButton";
import CheckBoxButton from "../../Partial/TableButton/CheckBoxButton";
import ApiCall from "../../Partial/Function/ApiCall";
import Excel from "../Excel/Excel";
import SweetAlert from "sweetalert-react";
var newArray = [];
var excelArray=[];
var countActive=0;
class Audit extends React.Component {
  constructor(props){
    super(props);
    this.state={
      dataTableAddButton:"",
      heading:"Audit",
      currentpath:"audits",
      exceldata:[],
      enableDisactiveTabAudit:false,
      enableActiveTabAudit:true,
      confirm_modal:false,
      permission_module:"_audit",
      permission_array:{
        'view':false,
        'create':false,
        'edit':false,
        'delete':false,
        'delete_selected':false,
        'restore':false,
        'selected_active':false,
        'hard_delete':false,
        'show_active':false,
        'active':false,
        'excel':false,
      },
      id:'',
      accordion: [],
      globalSearch:'',
      activeData:[],
      deletedData:[],
      controller:'AuditController',
      model:'Audits',
      user_id:localStorage.getItem('user'),
      confirm_modal_selected_delete:false,
      confirm_modal_selected_active:false,
      confirm_modal_soft:false,
      confirm_modal_active:false,
    };
  }
  toggleEnableModalShowActive = (id) => {
    this.setState({confirm_modal_active: true});
    this.setState({id: id});
  };
  toggleEnableModalShowSoft = (id) => {
    this.setState({confirm_modal_soft: true});
    this.setState({id: id});
  };
  toggleEnableShow = (id) => {
    this.setState({confirm_modal: true});
    this.setState({id: id});
  };
   toggleDataTabel = async(data)=>{
    this.setState({permission_array: data.permissions});
    const { auditListArray, setAuditListArray } = this.props;
    await setAuditListArray(data);
    this.setState({activeData: data.active});
    this.setState({deletedData: data.deleted});
    this.setExcelData(); 
  };
  setExcelData=async()=>{
    const {setAuditExcel } = this.props;
    await setAuditExcel(this.props.auditListArray.active);
  };
  async getData(){
    const user={
      user_id:this.state.user_id,
      permission_module:this.state.permission_module,
      permission_array:this.state.permission_array,
    };
    let data=await ApiCall.postAPICall('audits',user);
    var toogleArray=[];
    var accordion=this.state.accordion;
    if(this.state.enableActiveTabAudit)
    {
      toogleArray=data.active;
    }
    else
    {
      toogleArray=data.deleted;
    }
    for (var i = 0; i < toogleArray.length; i++) {
      accordion[i]= false;
      this.setState({accordion});
    }
    countActive=0;
    this.toggleDataTabel(data);
  };
  handleDataDelete = (id) => {
    this.setState({confirm_modal: false});
    const data = {
      id: this.state.id,
      model: this.state.model,
      controller: this.state.controller,
      user_id:this.state.user_id,
    };
    ApiCall.postAPICall('delete',data);
    this.getData();
  };
  handleDataActive = () => {
    this.setState({confirm_modal_active: false});
    const data = {
      id: this.state.id,
      model: this.state.model,
      controller: this.state.controller,
      user_id:this.state.user_id,
    };
    ApiCall.postAPICall('active',data);
    this.getData();
  };
  handleDataSoftDelete = () => {
    this.setState({confirm_modal_soft: false});
    const data = {
      id: this.state.id,
      model: this.state.model,
      controller: this.state.controller,
      user_id:this.state.user_id,
    };
    ApiCall.postAPICall('soft/delete',data);
    this.getData();
  };
  handleSearch=async(e)=>
  {
    this.setState({
      globalSearch: e.target.value
    });
    const {setAuditSearch } = this.props;
    await setAuditSearch(e.target.value);
    let searchArray;
    if(this.state.enableActiveTabAudit)
    {
      searchArray=this.props.auditListArray.active;
    }
    else
    {
      searchArray=this.props.auditListArray.deleted;
    }
    let searchIndex=[];
    Object.keys(searchArray).map(keyName=> {
      let index=Object.values(searchArray[keyName]);
      var term = this.props.aduitSearch; 
      var search = new RegExp(term , 'i');
      let b = index.filter(item => search.test(item));
      if(b.length > 0)
      {
        searchIndex.push(searchArray[keyName]);
      }
    });
    if(searchIndex.length == 0)
    {
      searchIndex=searchArray;
    }
    if(this.state.enableActiveTabAudit)
    {
      this.setState({activeData: searchIndex});
    }
    else
    {
      this.setState({deletedData: searchIndex});
    }

  };
  handleDataRefresh =async()=>
  {
    this.setState({
      enableDisactiveTabAudit:!this.state.enableDisactiveTabAudit
    });
    this.setState({
      enableActiveTabAudit:!this.state.enableActiveTabAudit
    });
    this.setState({
      globalSearch:''
    });
    this.getData();
  }
  toggleEnableShowSelectedDelete = () => {
    this.setState({confirm_modal_selected_delete: true})
  };
  toggleEnableShowSelectedActive = () => {
    this.setState({confirm_modal_selected_active: true})
  };
  componentDidMount(){
    this.getData();
  }
  search(nameKey){
    let customArray=[]
    let myArray=this.props.auditListArray.active;
    for (var i=0; i < myArray.length; i++) {
      if (myArray[i].id === nameKey) {
        return myArray[i];
      }
    }
  }
  remove(nameKey){
    let myArray=[];
    for (var i=0; i < excelArray.length; i++) {
      if (excelArray[i].id !== nameKey) {
        myArray.push(excelArray[i]);
      }
    }
    return myArray;
  }

  handleCheckBoxClick(id,event)
  {
    if(event)
    {
      var resultObject = this.search(id);
      excelArray.push(resultObject);
      const {setAuditExcel } = this.props;
      setAuditExcel(excelArray);
      newArray.push(id);
      const { auditArrayID, setAuditArrayID } = this.props;
      setAuditArrayID(newArray);
    }
    else
    {
      excelArray=this.remove(id);
      const {setAuditExcel } = this.props;
      setAuditExcel(excelArray);
      if(excelArray.length == 0)
      {
        this.setExcelData();
      }
      newArray.splice( newArray.indexOf(id),1);
      setAuditArrayID(newArray); 
    }
  }
  async selectedDeleteFunction()
  {
    this.setState({confirm_modal_selected_delete: false});
    const data = {
      idArray: this.props.auditArrayID,
      model: this.state.model,
      controller: this.state.controller,
    };
    if(this.state.enableActiveTabAudit)
    {
      await ApiCall.postAPICall('delete',data);
    }
    else
    {
      await ApiCall.postAPICall('soft/delete',data);
    }
    this.getData();
  };
  async selectedActiveFunction()
  {
    this.setState({confirm_modal_selected_active: false});
    const data = {
      idArray: this.props.auditArrayID,
      model: this.state.model,
    };
    await ApiCall.postAPICall('active',data);
    this.getData();
  };
  render() {
    const defaultSorted = [
      {
        dataField: "name",
        order: "desc",
      },
    ];
    const columns = [
      {
        dataField: "checkbox",
        text: "",
        sort: true,
        formatter: (cellContent, row) => {
          return (
            <CustomInput onChange={(e) => {this.handleCheckBoxClick(row.id,e.target.checked)}} type="checkbox" id={row.id} label=""/>
          );
        },
      },
      {
        dataField: "actions",
        isDummyField: true,
        align: "center",
        text: "",
        formatter: (cellContent, row) => {
          return (
            <Fragment>
              {this.state.permission_array.delete ?
                <span className="d-inline-block ml-2" onClick={() => this.toggleEnableShow(row.id)} >
                  <DeleteButton />
                </span>
                :''
              }
            </Fragment>
          );
        },
      },
      {
        dataField: "user_name",
        text: "User Name",
        sort: true,
      },
      {
        dataField: "actions",
        isDummyField: true,
        align: "center",
        text: "Actions",
        formatter: (cellContent, row) => {
          let key=countActive++;
          function toggleAccordion(tab)
          {
            var flag=document.getElementById('collapse'+tab).style.display;
            var arrayCollapse=document.getElementsByClassName('collapse');
            for (var i = arrayCollapse.length - 1; i >= 0; i--) {
              arrayCollapse[i].style.display = "none";
            }
            if(flag !='block')
            {
              document.getElementById('collapse'+tab).style.display = "block";
            }
            else
            {
              document.getElementById('collapse'+tab).style.display = "none";
            }
          }
          return (
            <Fragment>
              <div id="accordion" className="accordion-wrapper mb-3">
                  <Card>
                    <CardHeader id={'heading'+key}>
                      <Button block color="link" className="text-left m-0 p-0" onClick={() => toggleAccordion(key)}
                        aria-expanded={this.state.accordion[key]} aria-controls={'collapse'+key}>
                        <h5 className="m-0 p-0 text-capitalize">{row['action']}</h5>
                      </Button>
                    </CardHeader>
                    <div className="grid-menu grid-menu-3col">
                      <Collapse isOpen={this.state.accordion[key]} data-parent="#accordion" id={'collapse'+key}>
                        <table>
                          <thead>
                            <tr>
                              <th>Field</th>
                              <th>Old</th>
                              <th>New</th>
                            </tr>
                          </thead>
                          <tbody>
                          {Object.keys(row.audit_fields).map(keyName=>
                            <tr>
                              <td className="text-capitalize">{(row.audit_fields[keyName]['cms_value'])}</td>
                              <td className="text-capitalize">{(row.audit_fields[keyName]['old_value'])}</td>
                              <td className="text-capitalize">{(row.audit_fields[keyName]['new_value'])}</td>
                            </tr>
                          )}
                          </tbody>
                        </table>
                      </Collapse>
                    </div>
                  </Card> 
              </div>
            </Fragment>
          );
        },
      },
      {
        dataField: "created_at",
        text: "Date",
        sort: true,
      },
      {
        dataField:'tab_name',
        text:'Tab Name',
        sort:true,
      },
      
    ];
    const columnsDeleted = [
      {
        dataField: "checkbox",
        text: "",
        sort: true,
        formatter: (cellContent, row) => {
          return (
            <CustomInput onChange={(e) => {this.handleCheckBoxClick(row.id,e.target.checked)}} type="checkbox" id={row.id} label=""/>
          );
        },
      },
      {
        dataField: "actions",
        isDummyField: true,
        align: "center",
        text: "",
        formatter: (cellContent, row) => {
          return (
            <Fragment>
              {
                this.state.permission_array.active ?
                <span className="d-inline-block" onClick={() => this.toggleEnableModalShowActive(row.id)} >
                  <ActiveButton/>
                </span>
                :''
              }
              {
                this.state.permission_array.hard_delete ?
                <span className="d-inline-block ml-2" onClick={() => this.toggleEnableShow(row.id)} >
                  <DeleteButton />
                </span>
                :''
              }
            </Fragment>
          );
        },
      },
      {
        dataField: "user_name",
        text: "User Name",
        sort: true,
      },
      {
        dataField: "actions",
        isDummyField: true,
        align: "center",
        text: "Actions",
        formatter: (cellContent, row) => {
          let key=countActive++;
          function toggleAccordion(tab)
          {
            var flag=document.getElementById('collapse'+tab).style.display;
            var arrayCollapse=document.getElementsByClassName('collapse');
            for (var i = arrayCollapse.length - 1; i >= 0; i--) {
              arrayCollapse[i].style.display = "none";
            }
            if(flag !='block')
            {
              document.getElementById('collapse'+tab).style.display = "block";
            }
            else
            {
              document.getElementById('collapse'+tab).style.display = "none";
            }
          }
          return (
            <Fragment>
              <div id="accordion" className="accordion-wrapper mb-3">
                  <Card>
                    <CardHeader id={'heading'+key}>
                      <Button block color="link" className="text-left m-0 p-0" onClick={() => toggleAccordion(key)}
                        aria-expanded={this.state.accordion[key]} aria-controls={'collapse'+key}>
                        <h5 className="m-0 p-0 text-capitalize">{row['action']}</h5>
                      </Button>
                    </CardHeader>
                    <div className="grid-menu grid-menu-3col">
                      <Collapse isOpen={this.state.accordion[key]} data-parent="#accordion" id={'collapse'+key}>
                        <table>
                          <thead>
                            <tr>
                              <th>Field</th>
                              <th>Old</th>
                              <th>New</th>
                            </tr>
                          </thead>
                          <tbody>
                            {Object.keys(row.audit_fields).map(keyName=>
                              <tr>
                                <td className="text-capitalize">{(row.audit_fields[keyName]['cms_value'])}</td>
                                <td className="text-capitalize">{(row.audit_fields[keyName]['old_value'])}</td>
                                <td className="text-capitalize">{(row.audit_fields[keyName]['new_value'])}</td>
                              </tr>
                            )}
                          </tbody>
                        </table>
                      </Collapse>
                    </div>
                  </Card> 
              </div>
            </Fragment>
          );
        },
      },
      {
        dataField: "created_at",
        text: "Date",
        sort: true,
      },
      {
        dataField:'tab_name',
        text:'Tab Name',
        sort:true,
      }
    ];
    let {
      enableActiveTabAudit,
      enableDisactiveTabAudit,
    } = this.state;
    return (
      <Fragment>
        <SweetAlert showCancel cancelButtonColor="" confirmButtonColor="" cancelButtonText="Cancel" confirmButtonText="yes" title="Are you sure?" show={this.state.confirm_modal_selected_active} type="error" onCancel={() => this.setState({confirm_modal_selected_active: false})} onConfirm={() => this.selectedActiveFunction()}/>
        <SweetAlert showCancel cancelButtonColor="" confirmButtonColor="" cancelButtonText="Cancel" confirmButtonText="yes" title="Are you sure?" show={this.state.confirm_modal_selected_delete} type="error" onCancel={() => this.setState({confirm_modal_selected_delete: false})} onConfirm={() => this.selectedDeleteFunction()}/>
        <SweetAlert showCancel cancelButtonColor="" confirmButtonColor="" cancelButtonText="Cancel" confirmButtonText="yes" title="Are you sure?" show={this.state.confirm_modal} type="error" onCancel={() => this.setState({confirm_modal: false})} onConfirm={() => this.handleDataDelete()}/>
        <SweetAlert showCancel cancelButtonColor="" confirmButtonColor="" cancelButtonText="Cancel" confirmButtonText="yes" title="Are you sure?" show={this.state.confirm_modal_soft} type="error" onCancel={() => this.setState({confirm_modal_soft: false})} onConfirm={() => this.handleDataSoftDelete()}/>
        <SweetAlert showCancel cancelButtonColor="" confirmButtonColor="" cancelButtonText="Cancel" confirmButtonText="yes" title="Are you sure?" show={this.state.confirm_modal_active} type="error" onCancel={() => this.setState({confirm_modal_active: false})} onConfirm={() => this.handleDataActive()}/>
        
        <TableTitle
          heading={this.state.heading}
          subheading=" "
          currentpath={this.state.currentpath}
          dataTableAddButton={this.state.dataTableAddButton}
          icon="pe-7s-users icon-gradient bg-mixed-hopes"
        />
        {
          this.state.permission_array.view ?
          <Row>
            <Col md="12">
              <Card className="main-card mb-3">
                <CardBody>
                  <Container fluid>
                    <Row>
                      <Col lg="12">
                        <Card className="main-card mb-3">
                          <CardBody>
                            <div className="pull-right">
                              <FormGroup>
                                <Input type="text" name="search"  onChange={e => this.handleSearch(e)} id="search" placeholder="Search"/>
                              </FormGroup>
                            </div>
                            {
                              this.state.permission_array.delete_selected ?
                              <span onClick={() => this.toggleEnableShowSelectedDelete()} >
                                <SelectedDeleteButton/>
                              </span>
                              :''
                            }
                            {
                              this.state.permission_array.selected_active ?
                              !enableActiveTabAudit ?
                              <span onClick={(e) => {this.toggleEnableShowSelectedActive()}} >
                                <SelectedActiveButton/>
                              </span> 
                              : " "
                              :''
                            }
                            {
                              this.state.permission_array.show_active ?
                              !enableActiveTabAudit ? <span onClick={() => this.handleDataRefresh()}><ShowActiveButton/></span> : " "
                              :''
                            }
                            {
                              this.state.permission_array.restore ?
                              !enableDisactiveTabAudit ?  <span onClick={() => this.handleDataRefresh()}> <RestoreButton/> </span>: " "
                              :''
                            }
                            {
                              this.state.permission_array.excel ?
                              !enableDisactiveTabAudit ? <Excel {...this.state}/>: ""
                              :''
                            }
                          </CardBody>
                        </Card>
                      </Col>
                    </Row>
                  </Container>
                  {this.props.auditListArray.active 
                  ? <div className="table-responsive">
                    {enableActiveTabAudit ?
                      <BootstrapTable
                      bootstrap4
                      keyField="id"
                      pagination={ paginationFactory() } 
                      data={this.state.activeData} 
                      columns={columns}
                      // filter={filterFactory()}
                      defaultSorted={defaultSorted}
                    />
                    :
                     <BootstrapTable
                      bootstrap4
                      keyField="id"
                      pagination={ paginationFactory()}
                      data={this.state.deletedData} 
                      columns={columnsDeleted}
                      // filter={filterFactory()}
                      defaultSorted={defaultSorted}
                    />}
                  </div>
                  :'loading..!'
                  }                
                </CardBody>
              </Card>
            </Col>
          </Row>
          :''
        }
      </Fragment>
    );
  }
};
const mapStateToProps = state => ({
  auditArrayID: state.ThemeOptions.auditArrayID,
  auditListArray: state.ThemeOptions.auditListArray,
  auditExcel: state.ThemeOptions.auditExcel, 
  aduitSearch:state.ThemeOptions.aduitSearch,
});

const mapDispatchToProps = dispatch => ({
  setAuditArrayID: enable => dispatch(setAuditArrayID(enable)),
  setAuditListArray: enable => dispatch(setAuditListArray(enable)),
  setAuditExcel: enable => dispatch(setAuditExcel(enable)),
  setAuditSearch: enable => dispatch(setAuditSearch(enable)),

});

export default connect(mapStateToProps, mapDispatchToProps)(Audit);

