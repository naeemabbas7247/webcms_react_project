import React, {Fragment,Component } from "react";
import {
  Button,
  Col
} from "reactstrap";
import {connect} from 'react-redux';
import {
  setEnableActiveRoleTab,
  setEnableDISACTIVERoleTab,
  setUserArray,
  setUserExcel
} from '../../../reducers/ThemeOptions';
import ApiCall from "../../Partial/Function/ApiCall";
import { Row, Card, CardBody, CardTitle, Container } from "reactstrap";

class ShowActiveButton extends Component {
  toggleTab = async() => {
    document.getElementById("search").value=" ";
    const { enableActiveRoleTab, setEnableActiveRoleTab,enableDISACTIVERoleTab,setEnableDISACTIVERoleTab } = this.props;
    setEnableActiveRoleTab(!enableActiveRoleTab);
    setEnableDISACTIVERoleTab(!enableDISACTIVERoleTab);
    let getData=await ApiCall.postAPICall('users',"");
    var emptyUserArray=[];
    setUserArray(emptyUserArray);
    const {setUserExcel } = this.props;
    setUserExcel(getData.active);
  };
  render() {
    return (
      <Fragment>
        <Button onClick={this.toggleTab}  className="mb-2 mr-2 btn-icon btn-shadow btn-outline-2x" outline color="primary">
            <i className="lnr-store btn-icon-wrapper"> </i>
            Show Active
        </Button>
      </Fragment>
    );
  }
}
const mapStateToProps = state => ({
  enableActiveRoleTab: state.ThemeOptions.enableActiveRoleTab,
  enableDISACTIVERoleTab: state.ThemeOptions.enableDISACTIVERoleTab,
  userExcel: state.ThemeOptions.userExcel, 
  userArray: state.ThemeOptions.userArray,
});

const mapDispatchToProps = dispatch => ({
  setEnableActiveRoleTab: enable => dispatch(setEnableActiveRoleTab(enable)),
  setEnableDISACTIVERoleTab: enable => dispatch(setEnableDISACTIVERoleTab(enable)),
  setUserExcel: enable => dispatch(setUserExcel(enable)),
  setUserArray: enable => dispatch(setUserArray(enable)),
});
export default connect(mapStateToProps, mapDispatchToProps)(ShowActiveButton);