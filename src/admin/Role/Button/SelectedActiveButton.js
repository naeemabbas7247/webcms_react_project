import React, {Fragment,Component } from "react";
import {
  Button,
  Col
} from "reactstrap";
import {connect} from 'react-redux';
import {
  setEnableActiveRoleTab,
  setEnableDISACTIVERoleTab,
  setCheckboxRoleArray,
  setRoleArray,
  setRoleExcel,
} from '../../../reducers/ThemeOptions';
import ApiCall from "../../Partial/Function/ApiCall";
import { Row, Card, CardBody, CardTitle, Container } from "reactstrap";
import SweetAlert from "sweetalert-react";
class SelectedActiveButton extends Component {
   constructor(props){
      super(props);
      this.state={
        confirm_modal:false
    }
  }
  toggleEnableShow = () => {
      this.setState({confirm_modal: true})
    };
  async selectedActiveFunction()
  {
    this.setState({confirm_modal: false});
    const data = {
      idArray: this.props.checkboxRoleArray,
      model: 'CustomRole',
    };
    await ApiCall.postAPICall('active',data);
    let getData=await ApiCall.postAPICall('roles','');
    const { roleArray, setRoleArray } = this.props;
    setRoleArray(getData.roles);
    var checkboxRoleArray=[];
    setCheckboxRoleArray(checkboxRoleArray);
    const {setRoleExcel } = this.props;
    setRoleExcel(getData.roles.active);
  };
  render() {
    let {
      enableActiveTab,
    } = this.props;
    return (
      <Fragment>
        <SweetAlert showCancel cancelButtonColor="" confirmButtonColor="" cancelButtonText="Cancel" confirmButtonText="yes" title="Are you sure?" show={this.state.confirm_modal} type="error" onCancel={() => this.setState({confirm_modal: false})} onConfirm={() => this.selectedActiveFunction()}/>
        <Button onClick={(e) => {this.toggleEnableShow()}} className="mb-2 mr-2 btn-icon btn-shadow btn-outline-2x" outline color="success">
            <i className="lnr-user btn-icon-wrapper"> </i>
          Select Active
        </Button>
      </Fragment>
    );
  }
}
const mapStateToProps = state => ({
  enableActiveRoleTab: state.ThemeOptions.enableActiveTab,
  enableDISACTIVERoleTab: state.ThemeOptions.enableDISACTIVERoleTab,
  checkboxRoleArray: state.ThemeOptions.checkboxRoleArray,
  roleArray: state.ThemeOptions.roleArray,
  userExcel: state.ThemeOptions.userExcel, 

});

const mapDispatchToProps = dispatch => ({
  setEnableActiveRoleTab: enable => dispatch(setEnableActiveRoleTab(enable)),
  setEnableDISACTIVERoleTab: enable => dispatch(setEnableDISACTIVERoleTab(enable)),
  setCheckboxRoleArray: enable => dispatch(setCheckboxRoleArray(enable)),
  setRoleArray: enable => dispatch(setRoleArray(enable)),
  setRoleExcel: enable => dispatch(setRoleExcel(enable)),
  
});
export default connect(mapStateToProps, mapDispatchToProps)(SelectedActiveButton);