import React, { Component, Fragment } from "react";
import Rodal from "rodal";
import CSSTransitionGroup from "react-transition-group/CSSTransitionGroup";
import {
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  Nav,
  NavItem,
  NavLink,
  Button,
  UncontrolledTooltip,
} from "reactstrap";
import {
  ModalHeader,
  ModalBody,
  ModalFooter,
  Form,
  FormGroup,
  Label,
  Input,
  FormText,
  Row,
  Col,
  Card,
  CardBody,
  CardTitle,
  Container,
} from "reactstrap";
import { faStar, faBusinessTime } from "@fortawesome/free-solid-svg-icons";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import { toast, Slide } from "react-toastify";
import {connect} from 'react-redux';
import {
  setUserEditArray,
  setUserArray,
  setEnableDISACTIVETab,
  setEnableActive,
  setUserlistArray,
  setUserVisible,
  setUserIsEdit,
} from '../../../reducers/ThemeOptions';
import ModalBodyCustom from "../Modal/ModalBodyCustom";
import CancelButton from "../Button/CancelButton";
import SaveButton from "../Button/SaveButton";
import ApiCall from "../../Partial/Function/ApiCall";
class AddButton extends Component {
  constructor(props) {
    super(props);
    this.state = {
      // visible: false,
      animation: "zoom",
    };
  }
  show(animation) {
    const { userVisible, setUserVisible } = this.props;
    setUserVisible(true);
    this.setState({
      animation,
      // visible: true,
    });
  }
  hide() {
    const { userVisible, setUserVisible } = this.props;
    setUserVisible(false);
    const {setUserIsEdit } = this.props
    setUserIsEdit(false);
    // this.setState({ visible: false });
  }
 
  render() {
    let {
      dataTableAddButton
    } = this.props;
    let types = [
      "zoom",
    ];
    let buttons = types.map((value, index) => {
      let style = {
        animationDelay: index * 100 + "ms",
        WebkitAnimationDelay: index * 100 + "ms",
      };
      return (
        <Button key={index} className="mb-2 mr-2" color="primary" onClick={this.show.bind(this, value)} style={style}>
          {dataTableAddButton}
        </Button>
      );
    });
    return (
      <Fragment>
        <CSSTransitionGroup component="div" transitionName="TabsAnimation" transitionAppear={true}
          transitionAppearTimeout={0} transitionEnter={false} transitionLeave={false}>
          <Row className="text-center">
            <Col md="12">
              {buttons}
            </Col>
          </Row>
          <Rodal visible={this.props.userVisible && !this.props.userIsEdit} onClose={this.hide.bind(this)} animation={this.state.animation} showMask={false}>
            <ModalBodyCustom {...this.props}/>
            <ModalFooter>
              <Button color="link" onClick={this.hide.bind(this)}>
                <CancelButton/>
              </Button>
            </ModalFooter>
          </Rodal>
        </CSSTransitionGroup>
      </Fragment>
    );
  }
}
const mapStateToProps = state => ({
  enableActiveTab: state.ThemeOptions.enableActiveTab,
  enableDISACTIVETab: state.ThemeOptions.enableDISACTIVETab,
  userArray: state.ThemeOptions.userArray,
  userEditArray: state.ThemeOptions.userEditArray,
  userlistArray: state.ThemeOptions.userlistArray,
  userIsEdit: state.ThemeOptions.userIsEdit,
  userVisible: state.ThemeOptions.userVisible,
});

const mapDispatchToProps = dispatch => ({
  setEnableDISACTIVETab: enable => dispatch(setEnableDISACTIVETab(enable)),
  setEnableActive: enable => dispatch(setEnableActive(enable)),
  setUserArray: enable => dispatch(setUserArray(enable)),
  setUserEditArray: enable => dispatch(setUserEditArray(enable)),
  setUserlistArray: enable => dispatch(setUserlistArray(enable)),
  setUserIsEdit: enable => dispatch(setUserIsEdit(enable)),
  setUserVisible: enable => dispatch(setUserVisible(enable)),
});

export default connect(mapStateToProps, mapDispatchToProps)(AddButton);
