import React, {Fragment,Component } from "react";
import {
  Button,
  Col
} from "reactstrap";
import {connect} from 'react-redux';
import {
  setRoleArray,
  setEnableActiveRoleTab,
  setRoleExcel,
  setCheckboxRoleArray,
} from '../../../reducers/ThemeOptions';
import SweetAlert from "sweetalert-react";
import ApiCall from "../../Partial/Function/ApiCall";
import { Row, Card, CardBody, CardTitle, Container } from "reactstrap";
class SelectedDeleteButton extends Component {
  constructor(props){
      super(props);
      this.state={
        confirm_modal:false
    }
  }
  toggleEnableShow = () => {
      this.setState({confirm_modal: true})
    };
  async selectedDeleteFunction()
  {
    this.setState({confirm_modal: false});
    const data = {
      idArray: this.props.checkboxRoleArray,
      model: 'CustomRole',
    };
    if(this.props.enableActiveRoleTab)
    {
      await ApiCall.postAPICall('delete',data);
    }
    else
    {
      await ApiCall.postAPICall('soft/delete',data);
    }
    let getData=await ApiCall.postAPICall('roles','')
    const { roleArray, setRoleArray } = this.props;
    setRoleArray(getData.roles);
    const { checkboxRoleArray, setCheckboxRoleArray } = this.props;
    var emptyRoleArray=[];
    setCheckboxRoleArray(emptyRoleArray);
    const {setRoleExcel } = this.props;
    setRoleExcel(emptyRoleArray);
    console.log(this.props);
  };
  render() {
	let {
      roleArray,
    } = this.props;
    return (
      	<Fragment>
          <SweetAlert showCancel cancelButtonColor="" confirmButtonColor="" cancelButtonText="Cancel" confirmButtonText="yes" title="Are you sure?" show={this.state.confirm_modal} type="error" onCancel={() => this.setState({confirm_modal: false})} onConfirm={() => this.selectedDeleteFunction()}/>
          <Button onClick={(e) => {this.toggleEnableShow()}} className="mb-2 mr-2 btn-icon btn-shadow btn-outline-2x" outline color="danger">
            <i className="lnr-smartphone btn-icon-wrapper"> </i>
            SelectedDeleteButton
          </Button>
      </Fragment>
    );
  }
}
const mapStateToProps = state => ({
  roleArray: state.ThemeOptions.roleArray,
  enableActiveRoleTab: state.ThemeOptions.enableActiveRoleTab,
  checkboxRoleArray: state.ThemeOptions.checkboxRoleArray,
  roleExcel: state.ThemeOptions.roleExcel, 
  
});

const mapDispatchToProps = dispatch => ({
  setRoleArray: enable => dispatch(setRoleArray(enable)),
  setEnableActiveRoleTab: enable => dispatch(setEnableActiveRoleTab(enable)),
  setCheckboxRoleArray: enable => dispatch(setCheckboxRoleArray(enable)),
  setRoleExcel: enable => dispatch(setRoleExcel(enable)),
});
export default connect(mapStateToProps, mapDispatchToProps)(SelectedDeleteButton);