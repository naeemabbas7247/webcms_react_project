import React, {Fragment,Component } from "react";
import {
  Button,
  Col
} from "reactstrap";
import {connect} from 'react-redux';
import {
    setDataTable,
} from '../../../reducers/ThemeOptions';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Form,Row, Card, CardBody, CardTitle, Container } from "reactstrap";
import {
  faEdit,
  faTrash,
  faRetweet,
} from "@fortawesome/free-solid-svg-icons";
class ActiveButton extends Component {
    constructor(props){
      super(props);
    }
  render() {
    return (
      <Fragment>
         <div className="text-info">
           <FontAwesomeIcon  icon={faRetweet} size="2x" />
          </div>
      </Fragment>
    );
  }
}
const mapStateToProps = state => ({
  dataTable: state.ThemeOptions.dataTable,
});

const mapDispatchToProps = dispatch => ({
  setDataTable: enable => dispatch(setDataTable(enable)),
});
export default connect(mapStateToProps, mapDispatchToProps)(ActiveButton);
