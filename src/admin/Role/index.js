import React, { Fragment } from "react";
import { Route } from "react-router-dom";

// Tables
import Role from "./Role";
import Detail from "./Detail";
import Permission from "./Permission";
// Layout

import AppHeader from "../Layout/AppHeader/";
import AppSidebar from "../Layout/AppSidebar/";
import AppSidebarRole from "../Layout/AppSidebarRole/";
import AppFooter from "../Layout/AppFooter/";

// Theme Options

import ThemeOptions from "../Layout/ThemeOptions/";

class Roles extends React.Component {
  render() {
    let url=window.location.href.split(this.props.match.url)[1];
    return (
      <Fragment>
        <ThemeOptions />
        <AppHeader />
        <div className="app-main">
          {(url == '/listing') ? <AppSidebar /> : <AppSidebarRole/> }
          <div className="app-main__outer">
            <div className="app-main__inner">
              {/* Tables */}
              <Route path={`${this.props.match.url}/listing`} component={Role} />
              <Route path={`${this.props.match.url}/detail/:param1`} component={Detail} />
              <Route path={`${this.props.match.url}/permission/:param1/:param2`} component={Permission} />
            </div>
            <AppFooter />
          </div>
        </div>
      </Fragment>
    );
  }
}

export default Roles;
