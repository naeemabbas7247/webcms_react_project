import React, { Fragment } from "react";
import {
  Row,
  Col,
  Card,
  CardBody,
  UncontrolledButtonDropdown,
  DropdownItem,
  DropdownMenu,
  Form,
  DropdownToggle,
  Container,
  CustomInput,
  Button,
  span,
  FormGroup,
  Label,
  Input,
  CardTitle,
} from "reactstrap";
import { useDispatch } from 'react-redux';
import {
  toast,
} from "react-toastify";

import {
  setRoleEditArray,
  setRoleIsEdit,
} from '../../../reducers/ThemeOptions';
import { Loader, Types } from "react-loaders";
import LoadingOverlay from "react-loading-overlay";
import {connect} from 'react-redux';
import paginationFactory from 'react-bootstrap-table2-paginator';
import BootstrapTable from "react-bootstrap-table-next";
import filterFactory, { textFilter } from "react-bootstrap-table2-filter";
import ReactTable from "react-table";
import TableTitle from "../../Layout/AppMain/TableTitle";
import ApiCall from "../../Partial/Function/ApiCall";
import MyGlobleSetting from '../../MyGlobleSetting';
import SweetAlert from "sweetalert-react";
import {browserHistory} from 'react-router';
var newArray = [];
class Detail extends React.Component {
  constructor(props){
    super(props);
    this.state=MyGlobleSetting.state;
    this.state.tempArray=[];
    this.state.dataTableAddButton="Add role";
    this.state.heading="Role";
    this.state.currentpath="Roles";
    this.state.user_id=localStorage.getItem('user');
    this.state.model='CustomRole';
    this.state.id='';
    this.state.submitLoader=false;
    this.state.username='';
  }
  async componentDidMount(){
    localStorage.setItem('sidebar', 'role');
    let id=this.props.match.params.param1;
    const data = {
      id: id,
      model: this.state.model,
    };
    let editData=await ApiCall.postAPICall('edit',data);
    const { roleEditArray, setRoleEditArray } = this.props;
    await setRoleEditArray(editData.data);
    console.log(editData);
    this.setData(editData.data);
  }
  async setData(data){
    this.setState({name: data['name'] });
    this.setState({id: data['id'] });
  };
  FormSubmit = async(e)=>{
    console.log(this.state);
    this.setState({submitLoader: true});
    e.preventDefault();
    let url='role/update/'+this.state.id;    
    const data = {
      name: this.state.name,
      model: this.state.model,
      user_id:this.state.user_id,
    };
    let saveCall=await ApiCall.postAPICall(url,data);
    console.log(saveCall);
    if(typeof saveCall.error !== 'undefined') 
    {
      const errors = saveCall.error;
      Object.keys(errors).map(keyName=> {
        this.setState({[keyName]: errors[keyName] })
      });
    }
    else
    {
      toast(saveCall.message);
      this.setState({'error_name':"" });
      // this.setState({'name':"" });
    }
    this.setState({submitLoader: false});
  };
  render() {
    return (
      <Fragment>
        <SweetAlert showCancel cancelButtonColor="" confirmButtonColor="" cancelButtonText="Cancel" confirmButtonText="yes" title="Are you sure?" show={this.state.confirm_modal} type="error" onCancel={() => this.setState({confirm_modal: false})} onConfirm={() => this.handleDataDelete()}/>
        <TableTitle
          heading={this.state.heading}
          subheading=" "
          currentpath={this.state.currentpath}
          dataTableAddButton={this.state.dataTableAddButton}
          icon="pe-7s-users icon-gradient bg-mixed-hopes"
        />
        <Container fluid>
            <Card className="main-card mb-3">
              <CardBody>
                <Form onSubmit={this.FormSubmit}>
                  <Row form>
                    <Col md={6}>
                      <FormGroup>
                        <Label for="exampleRoleName">Name</Label>
                        <Input type="text" name="name" id="exampleRoleName" onChange={e => this.setState({name: e.target.value})} value={this.state.name}/>
                        {<span className="text-danger">{this.state.error_name}</span>}
                      </FormGroup>
                    </Col>
                  </Row>
                  {!this.state.submitLoader ?
                    <Button color="primary" className="mt-1">
                      Update
                    </Button>
                    :
                    <LoadingOverlay tag="div" active={this.state.submitLoader}
                      styles={{
                        overlay: (base) => ({
                          ...base,
                          background: "#fff",
                          opacity: 0.5,
                        }),
                      }}
                      spinner={<Loader active type={this.state.loaderType} />}>
                    </LoadingOverlay>
                    }
                </Form>
              </CardBody>
            </Card>
          </Container>
      </Fragment>
    );
  }
};
const mapStateToProps = state => ({
  roleEditArray: state.ThemeOptions.roleEditArray,
  roleIsEdit: state.ThemeOptions.roleIsEdit,

});

const mapDispatchToProps = dispatch => ({
  setRoleEditArray: enable => dispatch(setRoleEditArray(enable)),
  setRoleIsEdit: enable => dispatch(setRoleIsEdit(enable)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Detail);

