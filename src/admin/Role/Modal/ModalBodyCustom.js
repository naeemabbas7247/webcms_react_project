import React, { Component, Fragment } from "react";
import {
  ModalHeader,
  ModalBody,
  Button,
  Form,
  FormGroup,
  Label,
  Input,
  FormText,
  Row,
  Col,
  Card,
  CardBody,
  Container,
  CardFooter,
  CardHeader,
  CardTitle,
  Collapse,
  Fade,
  CustomInput
} from "reactstrap";
import {connect} from 'react-redux';
import {
  setPermissionArray,
  setRoleEditArray,
  setRoleArray,
  setRoleIsEdit,
  setUserVisible,
  setRoleArrayIndex,
} from '../../../reducers/ThemeOptions';
import ApiCall from "../../Partial/Function/ApiCall";
import { Loader, Types } from "react-loaders";
import LoadingOverlay from "react-loading-overlay";
let permission_array=[];
class ModalBodyCustom extends Component {
  constructor(props){
    super(props);
    this.toggleAccordion = this.toggleAccordion.bind(this);
    this.state = {
      id:'',
      flag:false,
      username : "",
      submitLoader:false,
      error_username:'',
      error_email:'',
      error_password:'',
      accordion: [],
      permission_array:[],
      roles:{}
    }
  }
  componentDidMount(){
    setTimeout(() => {
      this.setData();
    }, 3000);
  }

  handleCheckBoxPermision=(id,event)=>
  {
    if(event)
    {
      var element = document.getElementById('div'+id);
      element.classList.add('green');
      element.classList.remove('red');
      permission_array.push(id);
      this.props.roles['role_edit'+id] = true;
    }
    else
    {
      var element = document.getElementById('div'+id);
      element.classList.add('red');
      element.classList.remove('green');
      permission_array.splice(permission_array.indexOf(id),1);
      this.props.roles['role_edit'+id] = false;
    }
    const { roleArrayIndex, setRoleArrayIndex } = this.props;
    setRoleArrayIndex(this.props.roles);

  }
  setData=()=>
  {
    var accordion=this.state.accordion;
    var permissionArray=this.props.permissionArray;
    for (var i = 0; i < permissionArray.length; i++) {
      accordion[i]= false;
      this.setState({accordion});
    }
  }
  toggleAccordion(tab) {
    console.log(permission_array);
    var accordion = this.state;
    accordion[tab] = !this.state.accordion[tab];
    this.setState({
      accordion
    });
  }
  onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
  }
  RoleFormSubmit =async(e)=>{
    permission_array=permission_array.filter(this.onlyUnique);
    this.setState({submitLoader: true});
    e.preventDefault();
    let role={};
    let url='';
    if(this.props.roleIsEdit)
    {
      url='role/update/'+this.props.roleEditArray.id;
      role =
      {
        name:this.state.name,
        permissions: permission_array,
      }
    }
    else
    {
      url='role/add';
      role = {
        name: this.state.name,
        permissions: permission_array,
      };
    }    
    // console.log(role);
    let saveCall=await ApiCall.postAPICall(url,role);
    if(typeof saveCall.error !== 'undefined') 
    {
      const errors = saveCall.error;
      Object.keys(errors).map(keyName=> {
        this.setState({[keyName]: errors[keyName] })
      });
    }
    else
    {
      let getData=await ApiCall.postAPICall('roles','');
      const { roleArray, setRoleArray } = this.props;
      setRoleArray(getData.roles);
      this.hide();
    }
    this.setState({submitLoader: false});
  
  };
  hide() {
    const { userVisible, setUserVisible } = this.props;
    setUserVisible(false);
    const {setUserIsEdit } = this.props;
    setUserIsEdit(false);
    // this.setState({ visible: false });
  }
  intialPermissionArray=()=>
  {
    let tempIntialPermissionArray=this.props.roleEditArray['permissions'];
    if(tempIntialPermissionArray)
    {
      if(!this.state.name)
      {
        this.setState({name: this.props.roleEditArray.name});
        this.setState({id: this.props.roleEditArray.id});
      }

      tempIntialPermissionArray.map(function(item, i){
        permission_array.push(item);
        // var checkbox = document.getElementById('role_edit'+item);
        // var div = document.getElementById('div'+item);
        // div.classList.add('green');
        // checkbox.checked = true;
      });
      permission_array=permission_array.filter(this.onlyUnique);
    }
  }
  render() {
    console.log(this.props.roles);
    var permissionArray=this.props.permissionArray;
    if(this.props.roleIsEdit)
    {
      this.intialPermissionArray();
    }
    return (
      <Fragment>
        <ModalHeader>{this.props.heading}</ModalHeader>
          <ModalBody>
            <Row>
              <Col md="12">
                <Card className="main-card mb-3">
                  <CardBody>
                    <Form onSubmit={this.RoleFormSubmit}>
                      <FormGroup>
                        <Label for="name">Name</Label>
                        <Input type="text" name="name" value={this.props.roleIsEdit == true ? this.props.roleEditArray.name : this.state.name} onChange={e => this.setState({name: e.target.value})}  placeholder="with a placeholder"/>
                        {<span className="text-danger">{this.state.error_name}</span>}
                      </FormGroup>
                      <Row>
                        <Col md="12">
                          <div id="accordion" className="accordion-wrapper mb-3">
                          {permissionArray ?
                            permissionArray.map((module_name,key) => (
                              <Card>
                                <CardHeader id={'heading'+key}>
                                  <Button block color="link" className="text-left m-0 p-0" onClick={() => this.toggleAccordion(key)}
                                    aria-expanded={this.state.accordion[key]} aria-controls={'collapse'+key}>
                                    <h5 className="m-0 p-0">{(module_name[0]) ? (module_name[0]['module_name']) : ''}</h5>
                                  </Button>
                                </CardHeader>
                                <div className="grid-menu grid-menu-3col">
                                  <Collapse isOpen={this.state.accordion[key]} data-parent="#accordion" id={'collapse'+key}>
                                    <Row className="no-gutters">
                                      { (module_name[0]) ? module_name.map(permission => (
                                        <Col xl="4" sm="6" id={'div'+permission['id']} className={this.props.roles['role_edit'+permission['id']] ? 'green' :''}>
                                          <CardBody>
                                            <CustomInput checked={this.props.roles['role_edit'+permission['id']]} onChange={(e) => {this.handleCheckBoxPermision(permission.id,e.target.checked)}} type="checkbox" id={'role_edit'+permission.id} label={permission['permission_name']}/>
                                          </CardBody>
                                        </Col>
                                      )) : ''}
                                    </Row>
                                  </Collapse>
                                </div>
                              </Card> 
                            ))
                            : ''
                          }
                          </div>
                        </Col>
                      </Row>
                      {!this.state.submitLoader ?
                      <Button color="primary" className="mt-1">
                        Submit
                      </Button>
                      :
                      <LoadingOverlay tag="div" active={this.state.submitLoader}
                        styles={{
                          overlay: (base) => ({
                            ...base,
                            background: "#fff",
                            opacity: 0.5,
                          }),
                        }}
                        spinner={<Loader active type={this.state.loaderType} />}>
                      </LoadingOverlay>
                      }
                    </Form>
                  </CardBody>
                </Card>
              </Col>
            </Row>
          </ModalBody>
      </Fragment>
    );
  }
}
const mapStateToProps = state => ({
  roleEditArray: state.ThemeOptions.roleEditArray,
  roleIsEdit: state.ThemeOptions.roleIsEdit,
  roleArray: state.ThemeOptions.roleArray,
  userVisible: state.ThemeOptions.userVisible,
  permissionArray: state.ThemeOptions.permissionArray,
  roleArrayIndex:state.ThemeOptions.roleArrayIndex,
});

const mapDispatchToProps = dispatch => ({
  setPermissionArray: enable => dispatch(setPermissionArray(enable)),
  setRoleEditArray: enable => dispatch(setRoleEditArray(enable)),
  setRoleIsEdit: enable => dispatch(setRoleIsEdit(enable)),
  setRoleArray: enable => dispatch(setRoleArray(enable)),
  setUserVisible: enable => dispatch(setUserVisible(enable)),
  setRoleArrayIndex: enable => dispatch(setRoleArrayIndex(enable)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ModalBodyCustom);
