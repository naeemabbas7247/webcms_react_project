import React, {Fragment,Component } from "react";
import ReactExport from "react-export-excel";
import {
  Button,
  Col
} from "reactstrap";
import {
  setRoleExcel,
} from '../../../reducers/ThemeOptions';
import {connect} from 'react-redux';
const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;
class Excel extends React.Component {
    render() {
      console.log(this.props.roleExcel);
        return (
          <Fragment>
            <ExcelFile element=
            {<Button  className="mb-2 mr-2 btn-icon btn-shadow btn-outline-2x" outline color="warning">
              <i className="lnr-screen btn-icon-wrapper"> </i>
              Excel
            </Button>}>
                  <ExcelSheet  data={this.props.roleExcel} name="Employees">
                      <ExcelColumn label="name" value="name"/>
                  </ExcelSheet>
              </ExcelFile>
          </Fragment>
        );
    }
}
const mapStateToProps = state => ({
  roleExcel: state.ThemeOptions.roleExcel, 
});

const mapDispatchToProps = dispatch => ({
  setRoleExcel: enable => dispatch(setRoleExcel(enable)),

});

export default connect(mapStateToProps, mapDispatchToProps)(Excel);
