import React, { Fragment } from "react";
import {
  Row,
  Col,
  Card,
  CardBody,
  UncontrolledButtonDropdown,
  DropdownItem,
  DropdownMenu,
  Form,
  DropdownToggle,
  Container,
  CustomInput,
  Button,
  span,
  FormGroup,
  Label,
  Input
} from "reactstrap";
import { useDispatch } from 'react-redux';
import {
  setPermissionArray,
  setRoleArray,
  setEnableDISACTIVERoleTab,
  setEnableActiveRoleTab,
  setCheckboxRoleArray,
  setRoleEditArray,
  setRoleIsEdit,
  setRoleExcel,
  setRoleSearch,
  setRoleArrayIndex,
} from '../../../reducers/ThemeOptions';
import {connect} from 'react-redux';
import BootstrapTable from "react-bootstrap-table-next";
import filterFactory, { textFilter } from "react-bootstrap-table2-filter";
import ReactTable from "react-table";
import TableTitle from "../../Layout/AppMain/TableTitle";
import SelectedDeleteButton from "../../GlobalButton/SelectedDeleteButton";
import RestoreButton from "../../GlobalButton/RestoreButton";
import SelectedActiveButton from "../../GlobalButton/SelectedActiveButton";
import ShowActiveButton from "../../GlobalButton/ShowActiveButton";
import EditButton from "../../GlobalButton/EditButton";
import DeleteButton from "../../GlobalButton/DeleteButton";
import ActiveButton from "../../GlobalButton/ActiveButton";
import CheckBoxButton from "../../Partial/TableButton/CheckBoxButton";
import ApiCall from "../../Partial/Function/ApiCall";
import axios from 'axios';
import AddButton from "../../GlobalButton/AddButton";
import MyGlobleSetting from '../../MyGlobleSetting';
import Excel from "../Excel/Excel";
import SweetAlert from "sweetalert-react";
var newRoleArray = [];
var excelRoleArray=[];
class Role extends React.Component
{
  constructor(props){
    super(props);
    this.state={
      dataTableAddButton:"Add role",
      heading:"Roles",
      currentpath:"role",
      permission_module:"_role",
      permission_array:{
        'view':false,
        'create':false,
        'edit':false,
        'delete':false,
        'delete_selected':false,
        'restore':false,
        'selected_active':false,
        'hard_delete':false,
        'show_active':false,
        'active':false,
        'excel':false,
      },
      exceldata:[],
      confirm_modal:false,
      id:'',
      globalSearch:'',
      activeData:[],
      deletedData:[],
      controller:'RoleController',
      model:'CustomRole',
      user_id:localStorage.getItem('user'),
      confirm_modal_selected_delete:false,
      confirm_modal_selected_active:false,
      roles:{},
    }
  }
  toggleEnableModalShowActive = (id) => {
    this.setState({confirm_modal_active: true});
    this.setState({id: id});
  };
  toggleEnableModalShowSoft = (id) => {
    this.setState({confirm_modal_soft: true});
    this.setState({id: id});
  };
  toggleEnableShow = (id) => {
    this.setState({confirm_modal: true});
    this.setState({id: id});
  };
  toggleDataTabel = async(data)=>{
    this.setState({permission_array: data.permissionsList});
    const { permissionArray, setPermissionArray } = this.props;
    await setPermissionArray(data.permissions);
    const { roleArray, setRoleArray } = this.props;
    await setRoleArray(data.roles);
    this.setState({activeData: data.roles.active});
    this.setState({deletedData: data.roles.deleted});
    this.setExcelData(); 
  };
  setExcelData=async()=>{
    const {setRoleExcel } = this.props;
    await setRoleExcel(this.props.roleArray.active);
  };
  async getData(){
    const user={
      user_id:this.state.user_id,
      permission_module:this.state.permission_module,
      permission_array:this.state.permission_array,
    };
    let data=await ApiCall.postAPICall('roles',user);
    this.toggleDataTabel(data);
  };
  handleDataDelete = (id) => {
    this.setState({confirm_modal: false});
    const data = {
      id: this.state.id,
      model: this.state.model,
      controller: this.state.controller,
      user_id:this.state.user_id,
    };
    ApiCall.postAPICall('delete',data);
    this.getData();
  };
  handleDataActive = () => {
    this.setState({confirm_modal_active: false});
    const data = {
      id: this.state.id,
      model: this.state.model,
      controller: this.state.controller,
      user_id:this.state.user_id,
    };
    ApiCall.postAPICall('active',data);
    this.getData();
  };
  permissionsDivAppend=()=>
  {
    var obj = this.state.roles
    this.props.permissionArray.map((module_name,key) => (
      module_name.map(permission =>  (
        obj['role_edit'+permission.id] = false
      ))    
    ));
    if(typeof this.props.roleEditArray['permissions'] !== 'undefined')
    { 
      this.props.roleEditArray['permissions'].map(edit_permission =>  (
        obj['role_edit'+edit_permission] = true
      ));
    }
    this.setState({
      roles:obj
    });
    const { roleArrayIndex, setRoleArrayIndex } = this.props;
    setRoleArrayIndex(this.state.roles);
  }
  handleDataEditModal = async (id)=>{
    
    if(!this.props.roleIsEdit)
    {
      const {setRoleIsEdit } = this.props
      await setRoleIsEdit(true);
      const data = {
        id: id,
        model: this.state.model,
        user_id:this.state.user_id,
      };
      let editData=await ApiCall.postAPICall('role/edit',data);
      const { roleEditArray, setRoleEditArray } = this.props;
      await setRoleEditArray(editData.data);
      // this.permissionsDivAppend();
      window.location.href = "/#/role/detail/"+id;
    }
  };
  handleDataSoftDelete = () => {
    this.setState({confirm_modal_soft: false});
    const data = {
      id: this.state.id,
      model: this.state.model,
      controller: this.state.controller,
      user_id:this.state.user_id,
    };
    ApiCall.postAPICall('soft/delete',data);
    this.getData();
  };
  handleSearch=async(e)=>
  {
    this.setState({
      globalSearch: e.target.value
    });
    const {setRoleSearch } = this.props;
    await setRoleSearch(e.target.value);
    let searchArray;
    if(this.props.enableActiveRoleTab)
    {
      searchArray=this.props.roleArray.active;
    }
    else
    {
      searchArray=this.props.roleArray.deleted;
    }
    let searchIndex=[];
    Object.keys(searchArray).map(keyName=> {
      let index=Object.values(searchArray[keyName]);
      var term = this.state.globalSearch; 
      var search = new RegExp(term , 'i');
      let b = index.filter(item => search.test(item));
      if(b.length > 0)
      {
        searchIndex.push(searchArray[keyName]);
      }
    });
    if(searchIndex.length == 0)
    {
      searchIndex=searchArray;
    }
    if(this.props.enableActiveRoleTab)
    {
      this.setState({activeData: searchIndex});
    }
    else
    {
      this.setState({deletedData: searchIndex});
    }
    // this.getData();
  };
  handleDataRefresh =async()=>
  {
    const { enableActiveRoleTab, setEnableActiveRoleTab,enableDISACTIVERoleTab,setEnableDISACTIVERoleTab } = this.props;
    setEnableActiveRoleTab(!enableActiveRoleTab);
    setEnableDISACTIVERoleTab(!enableDISACTIVERoleTab);
    this.setState({
      globalSearch:''
    });
    this.getData();
  }
  handleAddButton =async()=>{
    var url='role/add';   
    const data = {
      model: this.state.model,
      user_id:this.state.user_id,
    };
    let saveCall=await ApiCall.postAPICall(url,data);

    window.location.href = "/#/role/detail/"+saveCall.role.id;
    // const {setRoleIsEdit } = this.props;
    // setRoleIsEdit(false);
    // const { roleEditArray, setRoleEditArray } = this.props;
    // setRoleEditArray('');
    // this.permissionsDivAppend();
  };
   toggleEnableShowSelectedDelete = () => {
    this.setState({confirm_modal_selected_delete: true})
  };
  toggleEnableShowSelectedActive = () => {
    this.setState({confirm_modal_selected_active: true})
  };
  componentDidMount(){
    this.getData();
  }
  search(nameKey){
    let customArray=[]
    let myArray=this.props.roleArray.active;
    for (var i=0; i < myArray.length; i++) {
      if (myArray[i].id === nameKey) {
        return myArray[i];
      }
    }
  }
  remove(nameKey){
    let myArray=[];
    for (var i=0; i < excelRoleArray.length; i++) {
      if (excelRoleArray[i].id !== nameKey) {
        myArray.push(excelRoleArray[i]);
      }
    }
    return myArray;
  }
  handleCheckBoxRoleClick(id,event)
  {
    if(event)
    {
      var resultObject = this.search(id);
      excelRoleArray.push(resultObject);
      const {setRoleExcel } = this.props;
      setRoleExcel(excelRoleArray);
      newRoleArray.push(id);
      const { checkboxRoleArray, setCheckboxRoleArray } = this.props;
      setCheckboxRoleArray(newRoleArray);
    }
    else
    {
      excelRoleArray=this.remove(id);
      const {setRoleExcel } = this.props;
      setRoleExcel(excelRoleArray);
      if(excelRoleArray.length == 0)
      {
        this.setExcelData();
      }
      newRoleArray.splice( newRoleArray.indexOf(id),1);
      setCheckboxRoleArray(newRoleArray); 
    }
  }
  async selectedDeleteFunction()
  {
    this.setState({confirm_modal_selected_delete: false});
    const data = {
      idArray: this.props.checkboxRoleArray,
      model: this.state.model,
      controller: this.state.controller,
      user_id:this.state.user_id,
    };
    if(this.props.enableActiveRoleTab)
    {
      await ApiCall.postAPICall('delete',data);
    }
    else
    {
      await ApiCall.postAPICall('soft/delete',data);
    }
    this.getData();
  };
  async selectedActiveFunction()
  {
    this.setState({confirm_modal_selected_active: false});
    const data = {
      idArray: this.props.checkboxRoleArray,
      model: this.state.model,
      controller: this.state.controller,
      user_id:this.state.user_id,
    };
    await ApiCall.postAPICall('active',data);
    this.getData();
  };
  render() {
    const defaultSorted = [
      {
        dataField: "name",
        order: "desc",
      },
    ];
    const columns = [
      {
        dataField: "checkbox",
        text: "Checkbox",
        sort: true,
        formatter: (cellContent, row) => {
          return (
            <CustomInput onChange={(e) => {this.handleCheckBoxRoleClick(row.id,e.target.checked)}} type="checkbox" id={'role'+row.id} label=""/>
            // <CheckBoxButton id={"checkbox"+row.id} />
          );
        },
      },
      {
        dataField: "name",
        text: "Name",
        sort: true,
      },
      {
        dataField: "actions",
        isDummyField: true,
        align: "center",
        text: "Actions",
        formatter: (cellContent, row) => {
          return (
            <Fragment>
              {this.state.permission_array.edit ? 
                <a className="d-inline-block" href={'/#/role/detail/'+row.id}>
                  <EditButton/>
                </a>
                :''
              }
              {this.state.permission_array.delete ? 
                <span className="d-inline-block ml-2" onClick={() => this.toggleEnableShow(row.id)} >
                  <DeleteButton />
                </span>
              :''}
            </Fragment>
          );
        },
      },
    ];
    const columnsDeleted = [
      {
        dataField: "checkbox",
        text: "Checkbox",
        sort: true,
        formatter: (cellContent, row) => {
          return (
            <CustomInput onChange={(e) => {this.handleCheckBoxRoleClick(row.id,e.target.checked)}} type="checkbox" id={'role'+row.id} label=""/>
            // <CheckBoxButton id={"checkbox"+row.id} />
          );
        },
      },
      {
        dataField: "name",
        text: "Name",
        sort: true,
      },
      {
        dataField: "actions",
        isDummyField: true,
        align: "center",
        text: "Actions",
        formatter: (cellContent, row) => {
          return (
            <Fragment>
              {
                this.state.permission_array.active ?
                <span className="d-inline-block" onClick={() => this.toggleEnableModalShowActive(row.id)} >
                  <ActiveButton/>
                </span>
                :''
              }
              {
                this.state.permission_array.hard_delete ?
                <span className="d-inline-block ml-2" onClick={() => this.toggleEnableModalShowSoft(row.id)} >
                  <DeleteButton />
                </span>
                 :''
              }
            </Fragment>
          );
        },
      },
    ];
    let {
      enableActiveRoleTab,
      enableDISACTIVERoleTab,
      roleArray,
    } = this.props;
    return (
      <Fragment>
        <SweetAlert showCancel cancelButtonColor="" confirmButtonColor="" cancelButtonText="Cancel" confirmButtonText="yes" title="Are you sure?" show={this.state.confirm_modal_selected_active} type="error" onCancel={() => this.setState({confirm_modal_selected_active: false})} onConfirm={() => this.selectedActiveFunction()}/>
        <SweetAlert showCancel cancelButtonColor="" confirmButtonColor="" cancelButtonText="Cancel" confirmButtonText="yes" title="Are you sure?" show={this.state.confirm_modal_selected_delete} type="error" onCancel={() => this.setState({confirm_modal_selected_delete: false})} onConfirm={() => this.selectedDeleteFunction()}/>
        
        <SweetAlert showCancel cancelButtonColor="" confirmButtonColor="" cancelButtonText="Cancel" confirmButtonText="yes" title="Are you sure?" show={this.state.confirm_modal} type="error" onCancel={() => this.setState({confirm_modal: false})} onConfirm={() => this.handleDataDelete()}/>
        <SweetAlert showCancel cancelButtonColor="" confirmButtonColor="" cancelButtonText="Cancel" confirmButtonText="yes" title="Are you sure?" show={this.state.confirm_modal_soft} type="error" onCancel={() => this.setState({confirm_modal_soft: false})} onConfirm={() => this.handleDataSoftDelete()}/>
        <SweetAlert showCancel cancelButtonColor="" confirmButtonColor="" cancelButtonText="Cancel" confirmButtonText="yes" title="Are you sure?" show={this.state.confirm_modal_active} type="error" onCancel={() => this.setState({confirm_modal_active: false})} onConfirm={() => this.handleDataActive()}/>
        
        <TableTitle
          heading={this.state.heading}
          subheading=" "
          currentpath={this.state.currentpath}
          dataTableAddButton={this.state.dataTableAddButton}
          icon="pe-7s-users icon-gradient bg-mixed-hopes"
        />
        {
          this.state.permission_array.view ?
          <Row>
            <Col md="12">
              <Card className="main-card mb-3">
                <CardBody>
                  <Container fluid>
                    <Row>
                      <Col lg="12">
                        <Card className="main-card mb-3">
                          <CardBody>
                            <div className="pull-right">
                              <FormGroup>
                                <Input type="text" name="search"  onChange={e => this.handleSearch(e)} id="search" placeholder="Search"/>
                              </FormGroup>
                            </div>
                            {
                              this.state.permission_array.delete_selected ?
                              <span onClick={() => this.toggleEnableShowSelectedDelete()} >
                                <SelectedDeleteButton/>
                              </span>
                              :''
                            }
                            {
                              this.state.permission_array.selected_active ? !enableActiveRoleTab ? 
                              <span onClick={(e) => {this.toggleEnableShowSelectedActive()}} >
                                <SelectedActiveButton/>
                              </span> 
                              : " "
                            :''
                            }
                            {
                              this.state.permission_array.show_active ?
                              !enableActiveRoleTab ? <span onClick={() => this.handleDataRefresh()}><ShowActiveButton/></span> : " "
                              :''
                            }
                            {
                              this.state.permission_array.restore ?
                              !enableDISACTIVERoleTab ? <span onClick={() => this.handleDataRefresh()}> <RestoreButton/> </span> : " "
                              :''
                            }
                            {
                              this.state.permission_array.excel ?
                              !enableDISACTIVERoleTab ? <Excel {...this.state}/>: ""
                              :''
                            }
                            { this.state.permission_array.create ?
                              <div className="pull-right">
                                <span onClick={() => this.handleAddButton()}>
                                  <AddButton  />
                                </span>
                              </div>
                            :''
                            }
                          </CardBody>
                        </Card>
                      </Col>
                    </Row>
                  </Container>
                  {this.props.roleArray.active 
                  ? <div className="table-responsive">
                    {enableActiveRoleTab ?
                      <BootstrapTable
                      bootstrap4
                      keyField="id"
                      data={this.state.activeData} 
                      columns={columns}
                      // filter={filterFactory()}
                      defaultSorted={defaultSorted}
                    />
                    :
                     <BootstrapTable
                      bootstrap4
                      keyField="id"
                      data={this.state.deletedData} 
                      columns={columnsDeleted}
                      // filter={filterFactory()}
                      defaultSorted={defaultSorted}
                    />}
                  </div>
                  :'loading..!'
                  }                
                </CardBody>
              </Card>
            </Col>
          </Row>
          :''
        }
      </Fragment>
    );
  }
};
const mapStateToProps = state => ({
  enableActiveRoleTab: state.ThemeOptions.enableActiveRoleTab,
  enableDISACTIVERoleTab: state.ThemeOptions.enableDISACTIVERoleTab,
  roleArray: state.ThemeOptions.roleArray,
  roleEditArray: state.ThemeOptions.roleEditArray,
  roleIsEdit: state.ThemeOptions.roleIsEdit,
  roleExcel: state.ThemeOptions.roleExcel, 
  roleSearch:state.ThemeOptions.roleSearch,
  permissionArray:state.ThemeOptions.permissionArray,
  checkboxRoleArray:state.ThemeOptions.checkboxRoleArray,
  roleArrayIndex:state.ThemeOptions.roleArrayIndex,
});
const mapDispatchToProps = dispatch => ({
  setEnableDISACTIVERoleTab: enable => dispatch(setEnableDISACTIVERoleTab(enable)),
  setEnableActiveRoleTab: enable => dispatch(setEnableActiveRoleTab(enable)),
  setRoleArray: enable => dispatch(setRoleArray(enable)),
  setRoleEditArray: enable => dispatch(setRoleEditArray(enable)),
  setRoleIsEdit: enable => dispatch(setRoleIsEdit(enable)),
  setRoleExcel: enable => dispatch(setRoleExcel(enable)),
  setRoleSearch: enable => dispatch(setRoleSearch(enable)),
  setPermissionArray: enable => dispatch(setPermissionArray(enable)),
  setCheckboxRoleArray: enable => dispatch(setCheckboxRoleArray(enable)),
  setRoleArrayIndex: enable => dispatch(setRoleArrayIndex(enable)),
  
});
export default connect(mapStateToProps, mapDispatchToProps)(Role);

