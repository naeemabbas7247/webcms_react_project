import React, { Fragment } from "react";
import {
  Row,
  Col,
  Card,
  CardBody,
  UncontrolledButtonDropdown,
  DropdownItem,
  DropdownMenu,
  Form,
  DropdownToggle,
  Container,
  CustomInput,
  Button,
  span,
  FormGroup,
  Label,
  Input,
  CardTitle,
} from "reactstrap";
import { useDispatch } from 'react-redux';
import {
  toast,
} from "react-toastify";

import {
  setRoleEditArray,
  setRoleIsEdit,
  setRoleArrayIndex,
  setOldPath,
} from '../../../reducers/ThemeOptions';
import { Loader, Types } from "react-loaders";
import LoadingOverlay from "react-loading-overlay";
import {connect} from 'react-redux';
import paginationFactory from 'react-bootstrap-table2-paginator';
import BootstrapTable from "react-bootstrap-table-next";
import filterFactory, { textFilter } from "react-bootstrap-table2-filter";
import ReactTable from "react-table";
import TableTitle from "../../Layout/AppMain/TableTitle";
import ApiCall from "../../Partial/Function/ApiCall";
import MyGlobleSetting from '../../MyGlobleSetting';
import SweetAlert from "sweetalert-react";
import {browserHistory} from 'react-router';
var newArray = [];
class Detail extends React.Component {
  constructor(props){
    super(props);
    this.state=MyGlobleSetting.state;
    this.state.tempArray=[];
    this.state.dataTableAddButton="Add role";
    this.state.heading="Role";
    this.state.currentpath="Roles";
    this.state.id='';
    this.state.permissions_array=[];
    this.state.already_permissions_array=[];
    this.state.submitLoader=false;
    this.state.username='';
    this.state.accordion={};
  }
  componentDidUpdate(prevProps) {
    if(this.props.oldPath !=this.props.match.params.param1)
    {
      this.componentDidMount();
    }
  }
  async componentDidMount(){
    localStorage.setItem('sidebar', 'role');
    let module_name=this.props.match.params.param1;
    const {setOldPath } = this.props;
    setOldPath(module_name);
    this.setData({path:module_name});
    console.log(this.state.path);
    let role_id=this.props.match.params.param2;
    const data = {
      module_name: module_name,
      role_id:role_id,
    };
    let permissions=await ApiCall.postAPICall('permission/edit',data);
    console.log(permissions);
    this.setState({already_permissions_array:permissions.data.permissions});
    this.setData(permissions.permissions);
  }
  async setData(data){
    this.setState({permissions_array: data });
    data=Object.keys(data);
    var accordion={};
    data.map((key) => {  
      accordion[key]=false;
    });
    console.log(this.state.accordion);
    this.state.already_permissions_array.map((key) => {  
      accordion[key]=true;
    });
    this.setState({accordion:accordion});
    console.log(this.state.accordion);
  };
  RoleFormSubmit = async(e)=>{
    this.setState({submitLoader: true});
    e.preventDefault();
    var url='permissions/update/'+this.props.match.params.param2;
    let role =
    {
      permissions: this.state.already_permissions_array,
    }
    let saveCall=await ApiCall.postAPICall(url,role);
    console.log(saveCall);
    if(typeof saveCall.error !== 'undefined') 
    {
      const errors = saveCall.error;
      Object.keys(errors).map(keyName=> {
        this.setState({[keyName]: errors[keyName] })
      });
    }
    else
    {
      toast(saveCall.message);
      this.setState({'error_name':"" });
      // this.setState({'name':"" });
    }
    this.setState({submitLoader: false});
  };
  handleCheckBoxPermision=(id,event)=>
  {
    var accordion=this.state.accordion;
    var permissions_array=this.state.already_permissions_array;
    if(event)
    {
      var element = document.getElementById('div'+id);
      element.classList.add('green');
      element.classList.remove('red');
      permissions_array.push(id);
      accordion[id] = true;
    }
    else
    {
      var element = document.getElementById('div'+id);
      element.classList.add('red');
      element.classList.remove('green');
      var temp=[];
      for (var i = permissions_array.length - 1; i >= 0; i--) {
        if(permissions_array[i] != id)
        {
          temp.push(permissions_array[i]);
        }
      }
      permissions_array=temp;
      accordion[id] = false;
    }
    this.setState({accordion});
    this.setState({already_permissions_array:permissions_array});
    const { roleArrayIndex, setRoleArrayIndex } = this.props;
    setRoleArrayIndex(this.props.roles);

  }
  render() {
    let permissions_array=this.state.permissions_array;
    permissions_array=Object.keys(permissions_array);
    return (
      <Fragment>
        <SweetAlert showCancel cancelButtonColor="" confirmButtonColor="" cancelButtonText="Cancel" confirmButtonText="yes" title="Are you sure?" show={this.state.confirm_modal} type="error" onCancel={() => this.setState({confirm_modal: false})} onConfirm={() => this.handleDataDelete()}/>
        <TableTitle
          heading={this.state.heading}
          subheading=" "
          currentpath={this.state.currentpath}
          dataTableAddButton={this.state.dataTableAddButton}
          icon="pe-7s-users icon-gradient bg-mixed-hopes"
        />
        <Row>
          <Col md="12">
            <Card className="main-card mb-3">
              <CardBody>
                <Form onSubmit={this.RoleFormSubmit}>
                  <Row>
                    <Col md="12">
                      <div className="grid-menu grid-menu-3col">
                          <Row className="no-gutters">
                            {permissions_array.map((key) => (
                              <Col xl="4" sm="6" id={'div'+key} className={this.state.accordion[key] ? 'green' :''}>
                                <CardBody>
                                  <CustomInput checked={this.state.accordion[key]} onChange={(e) => {this.handleCheckBoxPermision(key,e.target.checked)}} type="checkbox" id={'role_edit'+key} label={this.state.permissions_array[key]}/>
                                </CardBody>
                              </Col>
                            ))}
                          </Row>
                      </div>
                    </Col>
                  </Row>
                  {!this.state.submitLoader ?
                    <Col md="3">
                      <Button color="primary" className="mt-1">
                        Submit
                      </Button>
                    </Col>
                    :
                    <LoadingOverlay tag="div" active={this.state.submitLoader}
                      styles={{
                        overlay: (base) => ({
                          ...base,
                          background: "#fff",
                          opacity: 0.5,
                        }),
                      }}
                      spinner={<Loader active type={this.state.loaderType} />}>
                    </LoadingOverlay>
                  }
                </Form>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Fragment>
    );
  }
};
const mapStateToProps = state => ({
  roleEditArray: state.ThemeOptions.roleEditArray,
  roleIsEdit: state.ThemeOptions.roleIsEdit,
  roleArrayIndex:state.ThemeOptions.roleArrayIndex,
  oldPath:state.ThemeOptions.oldPath,
});

const mapDispatchToProps = dispatch => ({
  setRoleEditArray: enable => dispatch(setRoleEditArray(enable)),
  setRoleIsEdit: enable => dispatch(setRoleIsEdit(enable)),
  setRoleArrayIndex: enable => dispatch(setRoleArrayIndex(enable)),
  setOldPath: enable => dispatch(setOldPath(enable)),
    
});

export default connect(mapStateToProps, mapDispatchToProps)(Detail);

