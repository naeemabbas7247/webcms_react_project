import React, { Fragment } from "react";
import {
  Row,
  Col,
  Card,
  CardBody,
  UncontrolledButtonDropdown,
  DropdownItem,
  DropdownMenu,
  Form,
  DropdownToggle,
  Container,
  CustomInput,
  Button,
  span,
  FormGroup,
  Label,
  Input,
  CardTitle,
} from "reactstrap";
import { useDispatch } from 'react-redux';
import {
  toast,
} from "react-toastify";

import {
  setUserEditArray,
  setUserIsEdit,
} from '../../../reducers/ThemeOptions';
import { Loader, Types } from "react-loaders";
import LoadingOverlay from "react-loading-overlay";
import {connect} from 'react-redux';
import paginationFactory from 'react-bootstrap-table2-paginator';
import BootstrapTable from "react-bootstrap-table-next";
import filterFactory, { textFilter } from "react-bootstrap-table2-filter";
import ReactTable from "react-table";
import TableTitle from "../../Layout/AppMain/TableTitle";
import ApiCall from "../../Partial/Function/ApiCall";
import MyGlobleSetting from '../../MyGlobleSetting';
import SweetAlert from "sweetalert-react";
import {browserHistory} from 'react-router';
var newArray = [];
class Detail extends React.Component {
  constructor(props){
    super(props);
    this.state=MyGlobleSetting.state;
    this.state.tempArray=[];
    this.state.dataTableAddButton="Add users";
    this.state.heading="User";
    this.state.currentpath="users";
    this.state.user_id=localStorage.getItem('user');
    this.state.id='';
    this.state.submitLoader=false;
    this.state.username='';
    this.state.role_listing='';
  }
  async componentDidMount(){
    localStorage.setItem('sidebar', 'user');
    let id=this.props.match.params.param1;
    const data = {
      id: id,
      model: 'User',
    };
    let editData=await ApiCall.postAPICall('edit',data);
    let role_listing=await ApiCall.postAPICall('roles');
    this.setState({role_listing:role_listing.roles.active});
    const { userEditArray, setUserEditArray } = this.props;
    await setUserEditArray(editData.data);
    this.setData(editData.data);
  }
  async setData(data){
    this.setState({username: data['username'] });
    this.setState({first_name: data['first_name'] });
    this.setState({surname: data['surname'] });
    this.setState({id: data['id'] });
    this.setState({email: data['email'] });
    this.setState({role: data['role'] });
  };
  FormSubmit = async(e)=>{
    console.log(this.state);
    this.setState({submitLoader: true});
    e.preventDefault();
    let url='user/update/'+this.state.id;
    let user = {
      username: this.state.username,
      first_name: this.state.first_name,
      surname: this.state.surname,
      email: this.state.email,
      password: this.state.password,
      role: this.state.role,
      user_id:this.state.user_id
    };    
    const data = {
      idArray: user,
      model: 'User',
    };
    let saveCall=await ApiCall.postAPICall(url,user);
    console.log(saveCall);
    if(typeof saveCall.error !== 'undefined') 
    {
      const errors = saveCall.error;
      Object.keys(errors).map(keyName=> {
        this.setState({[keyName]: errors[keyName] })
      });
    }
    else
    {
      toast(saveCall.message);
      this.setState({'error_email':"" });
      this.setState({'username':"" });
    }
    this.setState({submitLoader: false});
  };
  render() {
    return (
      <Fragment>
        <SweetAlert showCancel cancelButtonColor="" confirmButtonColor="" cancelButtonText="Cancel" confirmButtonText="yes" title="Are you sure?" show={this.state.confirm_modal} type="error" onCancel={() => this.setState({confirm_modal: false})} onConfirm={() => this.handleDataDelete()}/>
        <TableTitle
          heading={this.state.heading}
          subheading=" "
          currentpath={this.state.currentpath}
          dataTableAddButton={this.state.dataTableAddButton}
          icon="pe-7s-users icon-gradient bg-mixed-hopes"
        />
        <Container fluid>
            <Card className="main-card mb-3">
              <CardBody>
                <Form onSubmit={this.FormSubmit}>
                  <Row form>
                    <Col md={6}>
                      <FormGroup>
                        <Label for="exampleEmail11">Email</Label>
                        <Input type="email" name="email" value= {this.state.email} onChange={e => this.setState({email: e.target.value})} id="exampleEmail11" placeholder="with a placeholder"/>
                        {<span className="text-danger">{this.state.error_email}</span>}
                      </FormGroup>
                    </Col>
                    <Col md={6}>
                      <FormGroup>
                        <Label for="examplePassword11">Password</Label>
                        <Input type="password" name="password" id="examplePassword11" onChange={e => this.setState({password: e.target.value})} placeholder="password placeholder"/>
                        {<span className="text-danger">{this.state.error_password}</span>}
                      </FormGroup>
                    </Col>
                  </Row>
                  <Row form>
                    <Col md={6}>
                      <FormGroup>
                        <Label for="exampleUserName">Username</Label>
                        <Input type="text" name="username" value={this.state.username} onChange={e => this.setState({username: e.target.value})} id="exampleUserName" />
                        {<span className="text-danger">{this.state.error_username}</span>}
                      </FormGroup>
                    </Col>
                    <Col md={6}>
                      <FormGroup>
                        <Label for="exampleFirstName">First Name</Label>
                        <Input type="text" name="firstname" value={this.state.first_name} onChange={e => this.setState({first_name: e.target.value})} id="exampleFirstName" />
                      </FormGroup>
                    </Col>
                    <Col md={6}>
                      <FormGroup>
                        <Label for="exampleSurName">SurName</Label>
                        <Input type="text" name="SurName" value={this.state.surname} onChange={e => this.setState({surname: e.target.value})} id="exampleSurName" />
                      </FormGroup>
                    </Col>
                    <Col md={6}>
                      <FormGroup>
                        <Label for="exampleSelect">Role</Label>
                        <Input type="select" name="select" id="exampleSelect" onChange={e => this.setState({role: e.target.value})}>
                          {Object.keys(this.state.role_listing).map((key) =>( 
                            <option selected={this.state.role === this.state.role_listing[key]['id'] ? 'selected' : ''} value={this.state.role_listing[key]['id']}>{this.state.role_listing[key]['name']}</option>
                          ))}
                        </Input>
                      </FormGroup>
                    </Col>
                  </Row>
                  {!this.state.submitLoader ?
                    <Button color="primary" className="mt-1">
                      Update
                    </Button>
                    :
                    <LoadingOverlay tag="div" active={this.state.submitLoader}
                      styles={{
                        overlay: (base) => ({
                          ...base,
                          background: "#fff",
                          opacity: 0.5,
                        }),
                      }}
                      spinner={<Loader active type={this.state.loaderType} />}>
                    </LoadingOverlay>
                    }
                </Form>
              </CardBody>
            </Card>
          </Container>
      </Fragment>
    );
  }
};
const mapStateToProps = state => ({
  userEditArray: state.ThemeOptions.userEditArray,
  userIsEdit: state.ThemeOptions.userIsEdit,
});

const mapDispatchToProps = dispatch => ({
  setUserEditArray: enable => dispatch(setUserEditArray(enable)),
  setUserIsEdit: enable => dispatch(setUserIsEdit(enable)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Detail);

