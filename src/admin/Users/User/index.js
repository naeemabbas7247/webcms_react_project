import React, { Fragment } from "react";
import {
  Row,
  Col,
  Card,
  CardBody,
  UncontrolledButtonDropdown,
  DropdownItem,
  DropdownMenu,
  Form,
  DropdownToggle,
  Container,
  CustomInput,
  Button,
  span,
  FormGroup,
  Label,
  Input
} from "reactstrap";
import { useDispatch } from 'react-redux';
import {
  setUserEditArray,
  setUserArray,
  setEnableDISACTIVETab,
  setEnableActiveTab,
  setUserlistArray,
  setUserIsEdit,
  setUserExcel,
  setUserSearch,
} from '../../../reducers/ThemeOptions';
import {
  faEdit,
  faTrash
} from "@fortawesome/free-solid-svg-icons";
import {browserHistory} from 'react-router';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {connect} from 'react-redux';
import paginationFactory from 'react-bootstrap-table2-paginator';
import BootstrapTable from "react-bootstrap-table-next";
import filterFactory, { textFilter } from "react-bootstrap-table2-filter";
import ReactTable from "react-table";
import TableTitle from "../../Layout/AppMain/TableTitle";
import SelectedActiveButton from "../../GlobalButton/SelectedActiveButton";
import ShowActiveButton from "../../GlobalButton/ShowActiveButton";
import RestoreButton from "../../GlobalButton/RestoreButton";
import EditButton from "../../GlobalButton/EditButton";
import DeleteButton from "../../GlobalButton/DeleteButton";
import SelectedDeleteButton from "../../GlobalButton/SelectedDeleteButton";
import ActiveButton from "../../GlobalButton/ActiveButton";
import CheckBoxButton from "../../Partial/TableButton/CheckBoxButton";
import ApiCall from "../../Partial/Function/ApiCall";
import AddButton from "../../GlobalButton/AddButton";
import Excel from "../Excel/Excel";
import SweetAlert from "sweetalert-react";
var newArray = [];
var excelArray=[];
class User extends React.Component {
  constructor(props){
    super(props);
    this.state={
      dataTableAddButton:"Add users",
      permission_module:"_user",
      permission_array:{
        'view':false,
        'create':false,
        'edit':false,
        'delete':false,
        'delete_selected':false,
        'restore':false,
        'selected_active':false,
        'hard_delete':false,
        'show_active':false,
        'active':false,
        'excel':false,
      },
      heading:"User",
      currentpath:"users",
      exceldata:[],
      confirm_modal:false,
      id:'',
      globalSearch:'',
      activeData:[],
      deletedData:[],
      user_id:localStorage.getItem('user'),
      controller:'UserController',
      model:'User',
      user_id:localStorage.getItem('user'),
      confirm_modal_selected_delete:false,
      confirm_modal_selected_active:false,
      confirm_modal_soft:false,
      confirm_modal_active:false,
    };
  }
  toggleEnableModalShowActive = (id) => {
    this.setState({confirm_modal_active: true});
    this.setState({id: id});
  };
  toggleEnableModalShowSoft = (id) => {
    this.setState({confirm_modal_soft: true});
    this.setState({id: id});
  };
  toggleEnableShow = (id) => {
    this.setState({confirm_modal: true});
    this.setState({id: id});
  };
   toggleDataTabel = async(data)=>{
    this.setState({permission_array: data.permissions});
    const { userListArray, setUserlistArray } = this.props;
    await setUserlistArray(data);
    this.setState({activeData: data.active});
    this.setState({deletedData: data.deleted});
    this.setExcelData(); 
  };
  setExcelData=async()=>{
    const {setUserExcel } = this.props;
    await setUserExcel(this.props.userlistArray.active);
  };
  async getData(){
    const user={
      user_id:this.state.user_id,
      permission_module:this.state.permission_module,
      permission_array:this.state.permission_array,
    };
    let data=await ApiCall.postAPICall('users',user);
    this.toggleDataTabel(data);
  };
  handleDataDelete = (id) => {
    this.setState({confirm_modal: false});
    const data = {
      id: this.state.id,
      model: this.state.model,
      controller: this.state.controller,
      user_id:this.state.user_id,
    };
    ApiCall.postAPICall('delete',data);
    this.getData();
  };
  handleDataActive = () => {
    this.setState({confirm_modal_active: false});
    const data = {
      id: this.state.id,
      model: this.state.model,
      controller: this.state.controller,
      user_id:this.state.user_id,
    };
    ApiCall.postAPICall('active',data);
    this.getData();
  };
  handleDataEditModal = async (id)=>{
    if(!this.props.userIsEdit)
    {
      const {setUserIsEdit } = this.props
      await setUserIsEdit(true);
      const data = {
        id: id,
        model: this.state.model,
        user_id:this.state.user_id,
      };
      let editData=await ApiCall.postAPICall('edit',data);
      const { userEditArray, setUserEditArray } = this.props;
      await setUserEditArray(editData.data);
      window.location.href = "/#/users/detail/"+id;
    }
  };
  handleDataSoftDelete = () => {
    this.setState({confirm_modal_soft: false});
    const data = {
      id: this.state.id,
      model: this.state.model,
      controller: this.state.controller,
      user_id:this.state.user_id,
    };
    ApiCall.postAPICall('soft/delete',data);
    this.getData();
  };
  handleSearch=async(e)=>
  {
    this.setState({
      globalSearch: e.target.value
    });
    console.log(e.target.value);
    const {setUserSearch } = this.props;
    await setUserSearch(e.target.value);
    let searchArray;
    if(this.props.enableActiveTab)
    {
      searchArray=this.props.userlistArray.active;
    }
    else
    {
      searchArray=this.props.userlistArray.deleted;
    }
    let searchIndex=[];
    Object.keys(searchArray).map(keyName=> {
      let index=Object.values(searchArray[keyName]);
      var term = this.props.userSearch; 
      var search = new RegExp(term , 'i');
      let b = index.filter(item => search.test(item));
      if(b.length > 0)
      {
        searchIndex.push(searchArray[keyName]);
      }
    });
    if(searchIndex.length == 0)
    {
      searchIndex=searchArray;
    }
    if(this.props.enableActiveTab)
    {
      this.setState({activeData: searchIndex});
    }
    else
    {
      this.setState({deletedData: searchIndex});
    }

  };
  handleDataRefresh =async()=>
  {
    const { enableActiveTab, setEnableActiveTab,enableDISACTIVETab,setEnableDISACTIVETab } = this.props;
    setEnableActiveTab(!enableActiveTab);
    setEnableDISACTIVETab(!enableDISACTIVETab);
    this.setState({
      globalSearch:''
    });
    this.getData();
  }
  handleAddButton =async()=>{
    var url='user/add';   
    const data = {
      model: this.state.model,
      user_id:this.state.user_id,
    };
    let saveCall=await ApiCall.postAPICall(url,data);
    window.location.href = "/#/users/detail/"+saveCall.user.id;
  };
  toggleEnableShowSelectedDelete = () => {
    this.setState({confirm_modal_selected_delete: true})
  };
  toggleEnableShowSelectedActive = () => {
    this.setState({confirm_modal_selected_active: true})
  };
  componentDidMount(){
    this.getData();
  }
  search(nameKey){
    let customArray=[]
    let myArray=this.props.userlistArray.active;
    for (var i=0; i < myArray.length; i++) {
      if (myArray[i].id === nameKey) {
        return myArray[i];
      }
    }
  }
  remove(nameKey){
    let myArray=[];
    for (var i=0; i < excelArray.length; i++) {
      if (excelArray[i].id !== nameKey) {
        myArray.push(excelArray[i]);
      }
    }
    return myArray;
  }

  handleCheckBoxClick(id,event)
  {
    if(event)
    {
      var resultObject = this.search(id);
      excelArray.push(resultObject);
      const {setUserExcel } = this.props;
      setUserExcel(excelArray);
      newArray.push(id);
      const { userArray, setUserArray } = this.props;
      setUserArray(newArray);
    }
    else
    {
      excelArray=this.remove(id);
      const {setUserExcel } = this.props;
      setUserExcel(excelArray);
      if(excelArray.length == 0)
      {
        this.setExcelData();
      }
      newArray.splice( newArray.indexOf(id),1);
      setUserArray(newArray); 
    }
  }
  async selectedDeleteFunction()
  {
    this.setState({confirm_modal_selected_delete: false});
    const data = {
      idArray: this.props.userArray,
      model: 'User',
      controller: 'UserController',
    };
    if(this.props.enableActiveTab)
    {
      await ApiCall.postAPICall('delete',data);
    }
    else
    {
      await ApiCall.postAPICall('soft/delete',data);
    }
    this.getData();
  };
  async selectedActiveFunction()
  {
    this.setState({confirm_modal_selected_active: false});
    const data = {
      idArray: this.props.userArray,
      model: 'User',
    };
    await ApiCall.postAPICall('active',data);
    this.getData();
  };
  render() {
    const defaultSorted = [
      {
        dataField: "name",
        order: "desc",
      },
    ];
    const columns = [
      {
        dataField: "checkbox",
        text: "",
        sort: true,
        formatter: (cellContent, row) => {
          return (
            <CustomInput onChange={(e) => {this.handleCheckBoxClick(row.id,e.target.checked)}} type="checkbox" id={row.id} label=""/>
          );
        },
      },
      {
        dataField: "actions",
        isDummyField: true,
        align: "center",
        text: "",
        formatter: (cellContent, row) => {
          return (
            <Fragment>
              {this.state.permission_array.edit ? 
                <a className="d-inline-block" href={'/#/users/detail/'+row.id}>
                  <EditButton/>
                </a>
                :''}
              {this.state.permission_array.delete ? 
                <span className="d-inline-block ml-2" onClick={() => this.toggleEnableShow(row.id)} >
                  <DeleteButton />
                </span>
              :''}
            </Fragment>
          );
        },
      },
      {
        dataField: "username",
        text: "User Name",
        sort: true,
      },
      {
        dataField: "full_name",
        text: "Full Name",
        sort: true,
        formatter: (cellContent, row) => { 
          return (row.first_name+' '+ row.surname);
        },
      },
      {
        dataField: "role_name",
        align: "center",
        text: "Role",
      },
      {
        dataField: "email",
        align: "center",
        text: "Email",
      },
    ];
    const columnsDeleted = [
      {
        dataField: "checkbox",
        text: "",
        sort: true,
        formatter: (cellContent, row) => {
          return (
            <CustomInput onChange={(e) => {this.handleCheckBoxClick(row.id,e.target.checked)}} type="checkbox" id={row.id} label=""/>
            // <CheckBoxButton id={"checkbox"+row.id} />
          );
        },
      },
      {
        dataField: "actions",
        isDummyField: true,
        align: "center",
        text: "",
        formatter: (cellContent, row) => {
          return (
            <Fragment>
              {
                this.state.permission_array.active ?
                <span className="d-inline-block" onClick={() => this.toggleEnableModalShowActive(row.id)} >
                  <ActiveButton/>
                </span>
                :''
              }
              {
                this.state.permission_array.hard_delete ?
                <span className="d-inline-block ml-2" onClick={() => this.toggleEnableModalShowSoft(row.id)} >
                  <DeleteButton />
                </span>
                :''
              }
            </Fragment>
          );
        },
      },
      {
        dataField: "username",
        text: "User Name",
        sort: true,
      },
      {
        dataField: "full_name",
        text: "Full Name",
        sort: true,
        formatter: (cellContent, row) => { 
          return (row.first_name+' '+ row.surname);
        },
      },
      {
        dataField: "role_name",
        align: "center",
        text: "Role",
      },
      {
        dataField: "email",
        align: "center",
        text: "Email",
      },
    ];
    let {
      enableActiveTab,
      enableDISACTIVETab,
      userArray,
    } = this.props;
    return (
      <Fragment>
        <SweetAlert showCancel cancelButtonColor="" confirmButtonColor="" cancelButtonText="Cancel" confirmButtonText="yes" title="Are you sure?" show={this.state.confirm_modal_selected_active} type="error" onCancel={() => this.setState({confirm_modal_selected_active: false})} onConfirm={() => this.selectedActiveFunction()}/>
        <SweetAlert showCancel cancelButtonColor="" confirmButtonColor="" cancelButtonText="Cancel" confirmButtonText="yes" title="Are you sure?" show={this.state.confirm_modal_selected_delete} type="error" onCancel={() => this.setState({confirm_modal_selected_delete: false})} onConfirm={() => this.selectedDeleteFunction()}/>
        <SweetAlert showCancel cancelButtonColor="" confirmButtonColor="" cancelButtonText="Cancel" confirmButtonText="yes" title="Are you sure?" show={this.state.confirm_modal} type="error" onCancel={() => this.setState({confirm_modal: false})} onConfirm={() => this.handleDataDelete()}/>
        <SweetAlert showCancel cancelButtonColor="" confirmButtonColor="" cancelButtonText="Cancel" confirmButtonText="yes" title="Are you sure?" show={this.state.confirm_modal_soft} type="error" onCancel={() => this.setState({confirm_modal_soft: false})} onConfirm={() => this.handleDataSoftDelete()}/>
        <SweetAlert showCancel cancelButtonColor="" confirmButtonColor="" cancelButtonText="Cancel" confirmButtonText="yes" title="Are you sure?" show={this.state.confirm_modal_active} type="error" onCancel={() => this.setState({confirm_modal_active: false})} onConfirm={() => this.handleDataActive()}/>
        
        <TableTitle
          heading={this.state.heading}
          subheading=" "
          currentpath={this.state.currentpath}
          dataTableAddButton={this.state.dataTableAddButton}
          icon="pe-7s-users icon-gradient bg-mixed-hopes"
        />
        {
          this.state.permission_array.view ?
          <Row>
            <Col md="12">
              <Card className="main-card mb-3">
                <CardBody>
                  <Container fluid>
                    <Row>
                      <Col lg="12">
                        <Card className="main-card mb-3">
                          <CardBody>
                            <div className="pull-right">
                              <FormGroup>
                                <Input type="text" name="search"  onChange={e => this.handleSearch(e)} id="search" placeholder="Search"/>
                              </FormGroup>
                            </div>
                            {
                              this.state.permission_array.delete_selected ?
                              <span onClick={() => this.toggleEnableShowSelectedDelete()} >
                                <SelectedDeleteButton/>
                              </span>
                              :''
                            }
                            {
                              this.state.permission_array.selected_active ?
                              !enableActiveTab ?
                                <span onClick={(e) => {this.toggleEnableShowSelectedActive()}} >
                                  <SelectedActiveButton/>
                                </span> 
                                : " "
                              :''
                            }
                            {
                              this.state.permission_array.show_active ?
                              !enableActiveTab ? <span onClick={() => this.handleDataRefresh()}><ShowActiveButton/></span> : " "
                              :''
                            }
                            {
                              this.state.permission_array.restore ?
                              !enableDISACTIVETab ?  <span onClick={() => this.handleDataRefresh()}> <RestoreButton/> </span>: " "
                              :''
                            }
                            {
                              this.state.permission_array.excel ?
                              !enableDISACTIVETab ? <Excel {...this.state}/>: ""
                              :''
                            }
                            { this.state.permission_array.create ?
                              <div className="pull-right">
                                <span onClick={() => this.handleAddButton()}>
                                  <AddButton/>
                                </span>
                              </div>
                              :''
                            }
                          </CardBody>
                        </Card>
                      </Col>
                    </Row>
                  </Container>
                  {this.props.userlistArray.active 
                  ? <div className="table-responsive">
                    {enableActiveTab ?
                      <BootstrapTable
                      bootstrap4
                      keyField="id"
                      pagination={ paginationFactory() } 
                      data={this.state.activeData} 
                      columns={columns}
                      // filter={filterFactory()}
                      defaultSorted={defaultSorted}
                    />
                    :
                     <BootstrapTable
                      bootstrap4
                      keyField="id"
                      pagination={ paginationFactory()}
                      data={this.state.deletedData} 
                      columns={columnsDeleted}
                      // filter={filterFactory()}
                      defaultSorted={defaultSorted}
                    />}
                  </div>
                  :'loading..!'
                  }                
                </CardBody>
              </Card>
            </Col>
          </Row>
          :''
        }
      </Fragment>
    );
  }
};
const mapStateToProps = state => ({
  enableActiveTab: state.ThemeOptions.enableActiveTab,
  enableDISACTIVETab: state.ThemeOptions.enableDISACTIVETab,
  userArray: state.ThemeOptions.userArray,
  userEditArray: state.ThemeOptions.userEditArray,
  userlistArray: state.ThemeOptions.userlistArray,
  userIsEdit: state.ThemeOptions.userIsEdit,
  userExcel: state.ThemeOptions.userExcel, 
  userSearch:state.ThemeOptions.userSearch,
});

const mapDispatchToProps = dispatch => ({
  setEnableDISACTIVETab: enable => dispatch(setEnableDISACTIVETab(enable)),
  setEnableActiveTab: enable => dispatch(setEnableActiveTab(enable)),
  setUserArray: enable => dispatch(setUserArray(enable)),
  setUserEditArray: enable => dispatch(setUserEditArray(enable)),
  setUserlistArray: enable => dispatch(setUserlistArray(enable)),
  setUserIsEdit: enable => dispatch(setUserIsEdit(enable)),
  setUserExcel: enable => dispatch(setUserExcel(enable)),
  setUserSearch: enable => dispatch(setUserSearch(enable)),

});

export default connect(mapStateToProps, mapDispatchToProps)(User);

