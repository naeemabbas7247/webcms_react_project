import React, { Fragment } from "react";
import { Route } from "react-router-dom";

// Tables
import User from "./User";
import Detail from "./Detail";
// Layout
import AppHeader from "../Layout/AppHeader/";
import AppSidebar from "../Layout/AppSidebar/";
import AppSidebarUser from "../Layout/AppSidebarUser/";
import AppFooter from "../Layout/AppFooter/";
// Theme Options
import ThemeOptions from "../Layout/ThemeOptions/";

class Users extends React.Component {
  render() {
    let url=window.location.href.split(this.props.match.url)[1];
    return (
      <Fragment>
        <ThemeOptions />
        <AppHeader />
        <div className="app-main">
          {(url == '/listing') ? <AppSidebar /> : <AppSidebarUser/> }
          <div className="app-main__outer">
            <div className="app-main__inner">
              {/* Tables */}
              <Route path={`${this.props.match.url}/listing`} component={User} />
              <Route path={`${this.props.match.url}/detail/:param1`} component={Detail} />
            </div>
            <AppFooter />
          </div>
        </div>
      </Fragment>
    );
  }
}

export default Users;
