import React, {Fragment,Component } from "react";
import ReactExport from "react-export-excel";
import {
  Button,
  Col
} from "reactstrap";
import {
  setAuditExcel,
} from '../../../reducers/ThemeOptions';
import {connect} from 'react-redux';
const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;
class Excel extends React.Component {
    render() {
      console.log(this.props.auditExcel);
        return (
          <Fragment>
            <ExcelFile element=
            {<Button  className="mb-2 mr-2 btn-icon btn-shadow btn-outline-2x" outline color="warning">
              <i className="lnr-screen btn-icon-wrapper"> </i>
              Excel
            </Button>}>
                <ExcelSheet  data={this.props.auditExcel} name="Audit">
                  <ExcelColumn label="Username" value="user_name"/>
                  <ExcelColumn label="Old Value" dangerouslySetInnerHTML="{{__html: old_value}}"/>
                  <ExcelColumn label="New Value" value="new_value"/>
                  <ExcelColumn label="Date" value="created_at"/>
                </ExcelSheet>
              </ExcelFile>
          </Fragment>
        );
    }
}
const mapStateToProps = state => ({
  auditExcel: state.ThemeOptions.auditExcel, 
});

const mapDispatchToProps = dispatch => ({
  setAuditExcel: enable => dispatch(setAuditExcel(enable)),

});

export default connect(mapStateToProps, mapDispatchToProps)(Excel);
