import React, {Fragment,Component } from "react";
import ReactExport from "react-export-excel";
import {
  Button,
  Col
} from "reactstrap";
import {
  setUserExcel,
} from '../../../reducers/ThemeOptions';
import {connect} from 'react-redux';
const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;
class Excel extends React.Component {
    render() {
      console.log(this.props.userExcel);
        return (
          <Fragment>
            <ExcelFile element=
            {<Button  className="mb-2 mr-2 btn-icon btn-shadow btn-outline-2x" outline color="warning">
              <i className="lnr-screen btn-icon-wrapper"> </i>
              Excel
            </Button>}>
                  <ExcelSheet  data={this.props.userExcel} name="Employees">
                      <ExcelColumn label="Username" value="username"/>
                      <ExcelColumn label="Email" value="email"/>
                      <ExcelColumn label="First Name" value="first_name"/>
                      <ExcelColumn label="Surname" value="surname"/>
                  </ExcelSheet>
              </ExcelFile>
          </Fragment>
        );
    }
}
const mapStateToProps = state => ({
  userExcel: state.ThemeOptions.userExcel, 
});

const mapDispatchToProps = dispatch => ({
  setUserExcel: enable => dispatch(setUserExcel(enable)),

});

export default connect(mapStateToProps, mapDispatchToProps)(Excel);
