import React, { Fragment } from "react";
import {
  Row,
  Col,
  Card,
  CardBody,
  UncontrolledButtonDropdown,
  DropdownItem,
  DropdownMenu,
  Form,
  DropdownToggle,
  Container,
  CustomInput,
  Button,
  span,
  FormGroup,
  Label,
  Input,
  CardHeader,
  Collapse
} from "reactstrap";
import { useDispatch } from 'react-redux';
import {
  setPermissionArray,
  setUserEditArray,
  setUserArray,
  setEnableDISACTIVETab,
  setEnableActive,
  setUserlistArray,
  setUserIsEdit,
  setUserExcel,
  setUserSearch,
} from '../../../reducers/ThemeOptions';
import {connect} from 'react-redux';
import paginationFactory from 'react-bootstrap-table2-paginator';
import BootstrapTable from "react-bootstrap-table-next";
import filterFactory, { textFilter } from "react-bootstrap-table2-filter";
import ReactTable from "react-table";
import TableTitle from "../../Layout/AppMain/TableTitle";
import SelectedDeleteButton from "../Button/SelectedDeleteButton";
import RestoreButton from "../Button/RestoreButton";
import SelectedActiveButton from "../Button/SelectedActiveButton";
import ShowActiveButton from "../Button/ShowActiveButton";
import EditButton from "../Button/EditButton";
import DeleteButton from "../Button/DeleteButton";
import ActiveButton from "../Button/ActiveButton";
import CheckBoxButton from "../../Partial/TableButton/CheckBoxButton";
import ApiCall from "../../Partial/Function/ApiCall";
import axios from 'axios';
import AddButton from "../Button/AddButton";
import MyGlobleSetting from '../../MyGlobleSetting';
import Excel from "../Excel/Excel";
import SweetAlert from "sweetalert-react";
var newArray = [];
var excelArray=[];
class Permission extends React.Component {
  constructor(props){
    super(props);
    this.state=MyGlobleSetting.state;
    this.state.tempArray=[];
    this.state.dataTableAddButton="Add Permissions";
    this.state.heading="Permission";
    this.state.currentpath="Permissions";
  }
  toggleDataTabel = async(data)=>{
    const { permissionArray, setPermissionArray } = this.props;
    await setPermissionArray(data.permissions); 
  };
  async getData(){
    const search={
      search:this.props.userSearch
    };
    let data=await ApiCall.postAPICall('permissions',search);
    console.log(data.permissions);
    this.toggleDataTabel(data);
  };
  handleDataActive = () => {
    this.setState({confirm_modal_active: false});
    const data = {
      id: this.state.id,
      model: 'User',
    };
    ApiCall.postAPICall('active',data);
    this.getData();
  };
  handleSearch=async(e)=>
  {
    const {setUserSearch } = this.props;
    await setUserSearch(e.target.value);
    console.log(this.props.userSearch);
    this.getData();

  };
  componentDidMount(){
    this.getData();
  }
  search(nameKey){
    let customArray=[]
    let myArray=this.props.userlistArray.active;
    for (var i=0; i < myArray.length; i++) {
      if (myArray[i].id === nameKey) {
        return myArray[i];
      }
    }
  }
  render() {
    const defaultSorted = [
      {
        dataField: "name",
        order: "desc",
      },
    ];
    const columns = [
      {
        dataField: "permission_name",
        text: "Name",
        sort: true,
      },
      {
        dataField: "module_name",
        text: "Module name",
        sort: true,
        // filter: textFilter(),
      },
    ];
    var permissionArray=this.props.permissionArray;
    return (
      <Fragment>
        <SweetAlert showCancel cancelButtonColor="" confirmButtonColor="" cancelButtonText="Cancel" confirmButtonText="yes" title="Are you sure?" show={this.state.confirm_modal} type="error" onCancel={() => this.setState({confirm_modal: false})} onConfirm={() => this.handleDataDelete()}/>
        <SweetAlert showCancel cancelButtonColor="" confirmButtonColor="" cancelButtonText="Cancel" confirmButtonText="yes" title="Are you sure?" show={this.state.confirm_modal_soft} type="error" onCancel={() => this.setState({confirm_modal_soft: false})} onConfirm={() => this.handleDataSoftDelete()}/>
        <SweetAlert showCancel cancelButtonColor="" confirmButtonColor="" cancelButtonText="Cancel" confirmButtonText="yes" title="Are you sure?" show={this.state.confirm_modal_active} type="error" onCancel={() => this.setState({confirm_modal_active: false})} onConfirm={() => this.handleDataActive()}/>
        
        <TableTitle
          heading={this.state.heading}
          subheading=" "
          currentpath={this.state.currentpath}
          dataTableAddButton={this.state.dataTableAddButton}
          icon="pe-7s-users icon-gradient bg-mixed-hopes"
        />
        <Row>
          <Col md="12">
            <Card className="main-card mb-3">
              <CardBody>
                <Container fluid>
                  <Row>
                    <Col lg="12">
                      <Card className="main-card mb-3">
                        <CardBody>
                          <div className="pull-right">
                            <FormGroup>
                              <Input type="text"  name="search" defaultValue={this.props.userSearch}  onChange={e => this.handleSearch(e)} id="search" placeholder="Search"/>
                            </FormGroup>
                          </div>
                        </CardBody>
                      </Card>
                    </Col>
                  </Row>
                </Container> 
                {permissionArray 
                  ?
                   <BootstrapTable
                    bootstrap4
                    keyField="id"
                    pagination={ paginationFactory() } 
                    data={permissionArray} 
                    columns={columns}
                    // filter={filterFactory()}
                    defaultSorted={defaultSorted}
                  />
                  :'loading..!'
                }                
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Fragment>
    );
  }
};
const mapStateToProps = state => ({
  enableActiveTab: state.ThemeOptions.enableActiveTab,
  enableDISACTIVETab: state.ThemeOptions.enableDISACTIVETab,
  userArray: state.ThemeOptions.userArray,
  userEditArray: state.ThemeOptions.userEditArray,
  userlistArray: state.ThemeOptions.userlistArray,
  userIsEdit: state.ThemeOptions.userIsEdit,
  userExcel: state.ThemeOptions.userExcel, 
  userSearch:state.ThemeOptions.userSearch,
  permissionArray:state.ThemeOptions.permissionArray,
});

const mapDispatchToProps = dispatch => ({
  setEnableDISACTIVETab: enable => dispatch(setEnableDISACTIVETab(enable)),
  setEnableActive: enable => dispatch(setEnableActive(enable)),
  setUserArray: enable => dispatch(setUserArray(enable)),
  setUserEditArray: enable => dispatch(setUserEditArray(enable)),
  setUserlistArray: enable => dispatch(setUserlistArray(enable)),
  setUserIsEdit: enable => dispatch(setUserIsEdit(enable)),
  setUserExcel: enable => dispatch(setUserExcel(enable)),
  setUserSearch: enable => dispatch(setUserSearch(enable)),
  setPermissionArray: enable => dispatch(setPermissionArray(enable)),
  

});

export default connect(mapStateToProps, mapDispatchToProps)(Permission);

