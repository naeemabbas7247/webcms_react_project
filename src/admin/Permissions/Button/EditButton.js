import React, {Fragment,Component } from "react";
import CSSTransitionGroup from "react-transition-group/CSSTransitionGroup";
import {
  Col,
  Button,
  ModalHeader,
  ModalBody,
  ModalFooter,
} from "reactstrap";
import Rodal from "rodal";
import {connect} from 'react-redux';
import {
  setUserEditArray,
  setUserVisible,
  setUserIsEdit,
} from '../../../reducers/ThemeOptions';
import ModalBodyCustom from "../Modal/ModalBodyCustom";
import CancelButton from "../Button/CancelButton";
import SaveButton from "../Button/SaveButton";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Row, Card, CardBody, CardTitle, Container } from "reactstrap";
import {
  faEdit,
  faTrash
} from "@fortawesome/free-solid-svg-icons";
class EditButton extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      animation: "zoom",
    };
  }
  show(animation) {
    const { userVisible, setUserVisible } = this.props;
    setUserVisible(true);
    this.setState({
      animation,
      // visible: true,
    });
  }
  hide() {
    const { userVisible, setUserVisible } = this.props;
    setUserVisible(false);
    const {setUserIsEdit } = this.props;
    setUserIsEdit(false);
    // this.setState({ visible: false });
  }
  render() {
    let {
      dataTableAddButton
    } = this.props;
    let types = [
      "zoom",
    ];
    let buttons = types.map((value, index) => {
      let style = {
        animationDelay: index * 100 + "ms",
        WebkitAnimationDelay: index * 100 + "ms",
      };
      return (
        <Fragment>
          <div className="text-primary" onClick={this.show.bind(this, value)}>
             <FontAwesomeIcon icon={faEdit} size="2x" />
            </div>
        </Fragment>
      );
    });
    return (
      <Fragment>
        <CSSTransitionGroup component="div" transitionName="TabsAnimation" transitionAppear={true}
          transitionAppearTimeout={0} transitionEnter={false} transitionLeave={false}>
          <Row className="text-center">
            <Col md="12">
              {buttons}
            </Col>
          </Row>
          <Rodal visible={this.props.userVisible && this.props.userIsEdit} onClose={this.hide.bind(this)} animation={this.state.animation} showMask={false}>
            <ModalBodyCustom {...this.props}/>
            <ModalFooter>
              <Button color="link" onClick={this.hide.bind(this)}>
                <CancelButton/>
              </Button>
            </ModalFooter>
          </Rodal>
        </CSSTransitionGroup>
      </Fragment>
    );
  }
}
const mapStateToProps = state => ({
  userEditArray: state.ThemeOptions.userEditArray,
  userVisible: state.ThemeOptions.userVisible,
   userIsEdit: state.ThemeOptions.userIsEdit,
});

const mapDispatchToProps = dispatch => ({
  setUserEditArray: enable => dispatch(setUserEditArray(enable)),
  setUserVisible: enable => dispatch(setUserVisible(enable)),
  setUserIsEdit: enable => dispatch(setUserIsEdit(enable)),
});
export default connect(mapStateToProps, mapDispatchToProps)(EditButton);

              // <Button color="primary" onClick={this.hide.bind(this)}>
              //   <SaveButton/>
              // </Button>