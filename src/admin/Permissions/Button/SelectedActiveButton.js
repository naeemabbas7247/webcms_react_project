import React, {Fragment,Component } from "react";
import {
  Button,
  Col
} from "reactstrap";
import {connect} from 'react-redux';
import {
  setEnableActiveTab,
  setEnableDISACTIVETab,
  setUserArray,
  setUserlistArray,
  setUserExcel,
} from '../../../reducers/ThemeOptions';
import ApiCall from "../../Partial/Function/ApiCall";
import { Row, Card, CardBody, CardTitle, Container } from "reactstrap";
import SweetAlert from "sweetalert-react";
class SelectedActiveButton extends Component {
   constructor(props){
      super(props);
      this.state={
        confirm_modal:false
    }
  }
  toggleEnableShow = () => {
      this.setState({confirm_modal: true})
    };
  async selectedActiveFunction()
  {
    this.setState({confirm_modal: false});
    const data = {
      idArray: this.props.userArray,
      model: 'User',
    };
    await ApiCall.postAPICall('active',data);
    let getData=await ApiCall.getAPICall('users');
    const { userListArray, setUserlistArray } = this.props;
    setUserlistArray(getData);
    var emptyUserArray=[];
    setUserArray(emptyUserArray);
    const {setUserExcel } = this.props;
    setUserExcel(getData.active);
  };
  render() {
    let {
      enableActiveTab,
    } = this.props;
    return (
      <Fragment>
        <SweetAlert showCancel cancelButtonColor="" confirmButtonColor="" cancelButtonText="Cancel" confirmButtonText="yes" title="Are you sure?" show={this.state.confirm_modal} type="error" onCancel={() => this.setState({confirm_modal: false})} onConfirm={() => this.selectedActiveFunction()}/>
        <Button onClick={(e) => {this.toggleEnableShow()}} className="mb-2 mr-2 btn-icon btn-shadow btn-outline-2x" outline color="success">
            <i className="lnr-user btn-icon-wrapper"> </i>
          Select Active
        </Button>
      </Fragment>
    );
  }
}
const mapStateToProps = state => ({
  enableActiveTab: state.ThemeOptions.enableActiveTab,
  enableDISACTIVETab: state.ThemeOptions.enableDISACTIVETab,
  userArray: state.ThemeOptions.userArray,
  userlistArray: state.ThemeOptions.userlistArray,
  userExcel: state.ThemeOptions.userExcel, 

});

const mapDispatchToProps = dispatch => ({
  setEnableActiveTab: enable => dispatch(setEnableActiveTab(enable)),
  setEnableDISACTIVETab: enable => dispatch(setEnableDISACTIVETab(enable)),
  setUserArray: enable => dispatch(setUserArray(enable)),
  setUserlistArray: enable => dispatch(setUserlistArray(enable)),
  setUserExcel: enable => dispatch(setUserExcel(enable)),
  
});
export default connect(mapStateToProps, mapDispatchToProps)(SelectedActiveButton);