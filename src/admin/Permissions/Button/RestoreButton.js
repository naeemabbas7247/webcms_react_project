import React, {Fragment,Component } from "react";
import {
  Button,
  Col
} from "reactstrap";
import {connect} from 'react-redux';
import {
  setEnableActiveTab,
  setEnableDISACTIVETab,
  setUserArray,
  setUserExcel,
  setUserSearch,
  setUserlistArray,
} from '../../../reducers/ThemeOptions';
import ApiCall from "../../Partial/Function/ApiCall";
import { Row, Card, CardBody, CardTitle, Container } from "reactstrap";

class RestoreButton extends Component {
  toggleTab = async() => {
    document.getElementById("search").value=" ";
    const { enableActiveTab, setEnableActiveTab,enableDISACTIVETab,setEnableDISACTIVETab } = this.props;
    setEnableActiveTab(!enableActiveTab);
    setEnableDISACTIVETab(!enableDISACTIVETab);
    const {setUserSearch } = this.props;
    await setUserSearch("");
    let getData=await ApiCall.postAPICall('users',"");
    const { userListArray, setUserlistArray } = this.props;
    setUserlistArray(getData);
    var emptyUserArray=[];
    setUserArray(emptyUserArray);
    const {setUserExcel } = this.props;
    setUserExcel(getData.active);
  };
  render() {
    return (
      <Fragment>
	      <Button onClick={this.toggleTab} className="mb-2 mr-2 btn-icon btn-shadow btn-outline-2x" outline color="info">
          <i className="lnr-paperclip btn-icon-wrapper"> </i>
          Restore
        </Button>
      </Fragment>
    );
  }
}
const mapStateToProps = state => ({
  enableActiveTab: state.ThemeOptions.enableActiveTab,
  enableDISACTIVETab: state.ThemeOptions.enableDISACTIVETab,
  userExcel: state.ThemeOptions.userExcel, 
  userArray: state.ThemeOptions.userArray,
  userSearch:state.ThemeOptions.userSearch,
  userlistArray: state.ThemeOptions.userlistArray,
});

const mapDispatchToProps = dispatch => ({
  setEnableActiveTab: enable => dispatch(setEnableActiveTab(enable)),
  setEnableDISACTIVETab: enable => dispatch(setEnableDISACTIVETab(enable)),
  setUserExcel: enable => dispatch(setUserExcel(enable)),
  setUserArray: enable => dispatch(setUserArray(enable)),
  setUserSearch: enable => dispatch(setUserSearch(enable)),
  setUserlistArray: enable => dispatch(setUserlistArray(enable)),
});
export default connect(mapStateToProps, mapDispatchToProps)(RestoreButton);