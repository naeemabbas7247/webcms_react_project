import React, {Fragment,Component } from "react";
import {
  Button,
  Col
} from "reactstrap";
import {connect} from 'react-redux';
import {
  setUserArray,
  setEnableActiveTab,
  setUserlistArray,
  setUserExcel,
} from '../../../reducers/ThemeOptions';
import SweetAlert from "sweetalert-react";
import ApiCall from "../../Partial/Function/ApiCall";
import { Row, Card, CardBody, CardTitle, Container } from "reactstrap";
class SelectedDeleteButton extends Component {
  constructor(props){
      super(props);
      this.state={
        confirm_modal:false
    }
  }
  toggleEnableShow = () => {
      this.setState({confirm_modal: true})
    };
  async selectedDeleteFunction()
  {
    this.setState({confirm_modal: false});
    const data = {
      idArray: this.props.userArray,
      model: 'User',
    };
    if(this.props.enableActiveTab)
    {
      await ApiCall.postAPICall('delete',data);
    }
    else
    {
      await ApiCall.postAPICall('soft/delete',data);
    }
    let getData=await ApiCall.getAPICall('users');
    const { userListArray, setUserlistArray } = this.props;
    setUserlistArray(getData);
    const { userArray, setUserArray } = this.props;
    var emptyUserArray=[];
    setUserArray(emptyUserArray);
    const {setUserExcel } = this.props;
    setUserExcel(emptyUserArray);
  };
  render() {
	let {
      userArray,
    } = this.props;
    return (
      	<Fragment>
          <SweetAlert showCancel cancelButtonColor="" confirmButtonColor="" cancelButtonText="Cancel" confirmButtonText="yes" title="Are you sure?" show={this.state.confirm_modal} type="error" onCancel={() => this.setState({confirm_modal: false})} onConfirm={() => this.selectedDeleteFunction()}/>
          <Button onClick={(e) => {this.toggleEnableShow()}} className="mb-2 mr-2 btn-icon btn-shadow btn-outline-2x" outline color="danger">
            <i className="lnr-smartphone btn-icon-wrapper"> </i>
            SelectedDeleteButton
          </Button>
      </Fragment>
    );
  }
}
const mapStateToProps = state => ({
  userArray: state.ThemeOptions.userArray,
  enableActiveTab: state.ThemeOptions.enableActiveTab,
  userlistArray: state.ThemeOptions.userlistArray,
  userExcel: state.ThemeOptions.userExcel, 
  
});

const mapDispatchToProps = dispatch => ({
  setUserArray: enable => dispatch(setUserArray(enable)),
  setEnableActiveTab: enable => dispatch(setEnableActiveTab(enable)),
  setUserlistArray: enable => dispatch(setUserlistArray(enable)),
  setUserExcel: enable => dispatch(setUserExcel(enable)),
});
export default connect(mapStateToProps, mapDispatchToProps)(SelectedDeleteButton);