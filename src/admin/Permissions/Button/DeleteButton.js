import React, {Fragment,Component } from "react";
import {
  Button,
  Col
} from "reactstrap";
import {connect} from 'react-redux';
import {
    setDataTable,
} from '../../../reducers/ThemeOptions';

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Form,Row, Card, CardBody, CardTitle, Container } from "reactstrap";
import {
  faEdit,
  faTrash
} from "@fortawesome/free-solid-svg-icons";
class DeleteButton extends Component {
    constructor(props){
      super(props);
    }
  render() {
    return (
      <Fragment>
         <div className="text-danger">
           <FontAwesomeIcon  icon={faTrash} size="2x" />
          </div>
      </Fragment>
    );
  }
}
const mapStateToProps = state => ({
  dataTable: state.ThemeOptions.dataTable,
});

const mapDispatchToProps = dispatch => ({
  setDataTable: enable => dispatch(setDataTable(enable)),
});
export default connect(mapStateToProps, mapDispatchToProps)(DeleteButton);
