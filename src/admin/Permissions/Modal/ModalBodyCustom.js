import React, { Component, Fragment } from "react";
import {
  ModalHeader,
  ModalBody,
  Button,
  Form,
  FormGroup,
  Label,
  Input,
  FormText,
  Row,
  Col,
  Card,
  CardBody,
  CardTitle,
  Container,
} from "reactstrap";
import {connect} from 'react-redux';
import {
  setUserEditArray,
  setUserlistArray,
  setUserIsEdit,
  setUserVisible,
} from '../../../reducers/ThemeOptions';
import ApiCall from "../../Partial/Function/ApiCall";
import { Loader, Types } from "react-loaders";
import LoadingOverlay from "react-loading-overlay";
class ModalBodyCustom extends Component {
  constructor(props){
    super(props);
    this.state = {
      username : "",
      submitLoader:false,
      error_username:'',
      error_email:'',
      error_password:'',
    }
  }
 
  hide() {
    const { userVisible, setUserVisible } = this.props;
    setUserVisible(false);
    console.log(this.props);
    const {setUserIsEdit } = this.props;
    setUserIsEdit(false);
  }
  FormSubmit = async(e)=>{
  this.setState({submitLoader: true});
    e.preventDefault();
    let user={};
    let url='';
    if(this.props.userIsEdit)
    {
      url='user/update/'+this.props.userEditArray.id;
      user =this.state;
    }
    else
    {
      url='user/add';
      user = {
        username: this.state.username,
        first_name: this.state.first_name,
        surname: this.state.surname,
        email: this.state.email,
        password: this.state.password,
        role: this.state.role,
      };
    }    
    const data = {
      idArray: this.props.userArray,
      model: 'User',
    };
    let saveCall=await ApiCall.postAPICall(url,user);
    console.log(saveCall.error);
    if(typeof saveCall.error !== 'undefined') 
    {
      const errors = saveCall.error;
      Object.keys(errors).map(keyName=> {
        this.setState({[keyName]: errors[keyName] })
      });
    }
    else
    {
      let getData=await ApiCall.postAPICall('users',"");
      const { userListArray, setUserlistArray } = this.props;
      setUserlistArray(getData);
      this.hide();
      this.setState({'error_username':"" });
      this.setState({'error_email':"" });
      this.setState({'error_password':"" });
      this.setState({'username':"" });
      this.setState({'email':"" });
      this.setState({'password':"" });
      this.setState({'first_name':"" });
      this.setState({'surname':"" });
      this.setState({'role':"" });
    }
    this.setState({submitLoader: false});
  
  };

  render() {
    // console.log(this.props);
    // if(this.props.userIsEdit)
    // {
    //   await this.editFunction();
    // }
    return (
      <Fragment>
        <ModalHeader>{this.props.heading}</ModalHeader>
          <ModalBody>
            <Row>
             <Col md="12">
                <Card className="main-card mb-3">
                  <CardBody>
                    <Form onSubmit={this.FormSubmit}>
                      <FormGroup>
                        <Label for="username">Username</Label>
                        <Input type="text" name="username" defaultValue={this.props.userIsEdit ? this.props.userEditArray.username : ''} onChange={e => this.setState({username: e.target.value})} id="username" placeholder="with a placeholder"/>
                        {<span className="text-danger">{this.state.error_username}</span>}
                      </FormGroup>
                      <FormGroup>
                        <Label for="first_name">First Name</Label>
                        <Input type="text" name="first_name" id="first_name" defaultValue={this.props.userIsEdit ? this.props.userEditArray.first_name : ''} onChange={e => this.setState({first_name: e.target.value})}  placeholder="with a placeholder"/>
                      </FormGroup>
                      <FormGroup>
                        <Label for="surname">Surname</Label>
                        <Input type="text" name="surname" id="surname" defaultValue={this.props.userIsEdit ? this.props.userEditArray.surname : ''} onChange={e => this.setState({surname: e.target.value})} placeholder="with a placeholder"/>
                      </FormGroup>
                      <FormGroup>
                        <Label for="exampleEmail">Email</Label>
                        <Input type="email" name="email" id="exampleEmail" defaultValue= {this.props.userIsEdit ? this.props.userEditArray.email : ''} onChange={e => this.setState({email: e.target.value})} placeholder="with a placeholder"/>
                        {<span className="text-danger">{this.state.error_email}</span>}
                      </FormGroup>
                      <FormGroup>
                        <Label for="examplePassword">Password</Label>
                        <Input type="password" name="password" id="examplePassword" onChange={e => this.setState({password: e.target.value})} placeholder="password placeholder"/>
                        {<span className="text-danger">{this.state.error_password}</span>}
                      </FormGroup>
                      <FormGroup>
                        <Label for="exampleSelect">Role</Label>
                        <Input type="select" name="select" id="exampleSelect" onChange={e => this.setState({role: e.target.value})}>
                          <option selected={this.props.userIsEdit ? (this.props.userEditArray.role === 1) ? 'selected' : '' : ''} value="1">Admin</option>
                          <option selected={this.props.userIsEdit ? (this.props.userEditArray.role === 2) ? 'selected' : '' : ''} value="2">Customer</option>
                        </Input>
                      </FormGroup>
                      {!this.state.submitLoader ?
                      <Button color="primary" className="mt-1">
                        Submit
                      </Button>
                      :
                      <LoadingOverlay tag="div" active={this.state.submitLoader}
                        styles={{
                          overlay: (base) => ({
                            ...base,
                            background: "#fff",
                            opacity: 0.5,
                          }),
                        }}
                        spinner={<Loader active type={this.state.loaderType} />}>
                      </LoadingOverlay>
                      }
                    </Form>
                  </CardBody>
                </Card>
              </Col>
            </Row>
          </ModalBody>
      </Fragment>
    );
  }
}
const mapStateToProps = state => ({
  userEditArray: state.ThemeOptions.userEditArray,
  userIsEdit: state.ThemeOptions.userIsEdit,
  userlistArray: state.ThemeOptions.userlistArray,
  userVisible: state.ThemeOptions.userVisible,
});

const mapDispatchToProps = dispatch => ({
  setUserEditArray: enable => dispatch(setUserEditArray(enable)),
  setUserIsEdit: enable => dispatch(setUserIsEdit(enable)),
  setUserlistArray: enable => dispatch(setUserlistArray(enable)),
  setUserVisible: enable => dispatch(setUserVisible(enable)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ModalBodyCustom);


