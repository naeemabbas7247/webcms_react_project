import sideBar6 from '../assets/utils/images/sidebar/city1.jpg';
export const SET_SUBSCRIPTION_ARRAY_ID = 'THEME_OPTIONS/SET_SUBSCRIPTION_ARRAY_ID';
export const SET_SUBSCRIPTION_LIST_ARRAY = 'THEME_OPTIONS/SET_SUBSCRIPTION_LIST_ARRAY';
export const SET_SUBSCRIPTION_EXCEL = 'THEME_OPTIONS/SET_SUBSCRIPTION_EXCEL';
export const SET_SUBSCRIPTION_SEARCH = 'THEME_OPTIONS/SET_SUBSCRIPTION_SEARCH';
export const SET_Contact_ARRAY_ID = 'THEME_OPTIONS/SET_Contact_ARRAY_ID';
export const SET_Contact_LIST_ARRAY = 'THEME_OPTIONS/SET_Contact_LIST_ARRAY';
export const SET_Contact_EXCEL = 'THEME_OPTIONS/SET_Contact_EXCEL';
export const SET_Contact_SEARCH = 'THEME_OPTIONS/SET_Contact_SEARCH';
export const SET_ENABLE_ACTIVE = 'THEME_OPTIONS/SET_ENABLE_ACTIVE';
export const SET_ENABLE_SHOW = 'THEME_OPTIONS/SET_ENABLE_SHOW';
export const SET_ENABLE_TEXT_MODEL = 'THEME_OPTIONS/SET_ENABLE_TEXT_MODEL';
export const SET_NEW_BLOG_KEY = 'THEME_OPTIONS/SET_NEW_BLOG_KEY';
export const SET_ENABLE_TEXT_CLONE = 'THEME_OPTIONS/SET_ENABLE_TEXT_CLONE';
export const SET_IS_HORIZONTAL = 'THEME_OPTIONS/SET_IS_HORIZONTAL';
export const SET_FONT_SIZE = 'THEME_OPTIONS/SET_FONT_SIZE';
export const SET_FONT_FAMILY = 'THEME_OPTIONS/SET_FONT_FAMILY';
export const SET_INLINE_STYLE = 'THEME_OPTIONS/SET_INLINE_STYLE';
export const SET_INLINE_STYLE_ENABLE = 'THEME_OPTIONS/SET_INLINE_STYLE_ENABLE';
export const SET_ADD_NEW_CMS_FLAG = 'THEME_OPTIONS/SET_ADD_NEW_CMS_FLAG';
export const SET_ADD_MODEL_DATA = 'THEME_OPTIONS/SET_ADD_MODEL_DATA';
export const SET_PADDING_Two = 'THEME_OPTIONS/SET_PADDING_Two';
export const SET_BACKGROUND_CMS_KEY = 'THEME_OPTIONS/SET_BACKGROUND_CMS_KEY';
export const SET_DATATABLE = 'THEME_OPTIONS/SET_DATATABLE';
export const SET_OLD_PATH = 'THEME_OPTIONS/SET_OLD_PATH';
export const SET_CMS_MODEL = 'THEME_OPTIONS/SET_CMS_MODEL';
export const SET_TYPES_ARRAY = 'THEME_OPTIONS/SET_TYPES_ARRAY';
export const SET_RICHTEXT_ARRAY = 'THEME_OPTIONS/SET_RICHTEXT_ARRAY';
export const SET_RICHTEXT_ARRAY_STYLE = 'THEME_OPTIONS/SET_RICHTEXT_ARRAY_STYLE';
export const SET_RICHTEXT_ARRAY_STYLESHEET = 'THEME_OPTIONS/SET_RICHTEXT_ARRAY_STYLESHEET';
export const SET_RICHTEXT_FONT_SIZE = 'THEME_OPTIONS/SET_RICHTEXT_FONT_SIZE';
export const SET_RICHTEXT_FONT_FAMILY = 'THEME_OPTIONS/SET_RICHTEXT_FONT_FAMILY';
export const SET_SECTION_ARRAY_MODEL = 'THEME_OPTIONS/SET_SECTION_ARRAY_MODEL';
export const SET_EDIT_SECTION_MODEL_ARRAY = 'THEME_OPTIONS/SET_EDIT_SECTION_MODEL_ARRAY';
export const SET_EDIT_SECTION_MODEL = 'THEME_OPTIONS/SET_EDIT_SECTION_MODEL';
export const SET_SELECTED_EDIT = 'THEME_OPTIONS/SET_SELECTED_EDIT';
export const SET_EDIT_SECTION_MODEL_ENABLE = 'THEME_OPTIONS/SET_EDIT_SECTION_MODEL_ENABLE';
export const SET_ADD_MODEL = 'THEME_OPTIONS/SET_ADD_MODEL';
export const SET_SOCIAL_MODEL = 'THEME_OPTIONS/SET_SOCIAL_MODEL';
export const SET_ICON_MODEL = 'THEME_OPTIONS/SET_ICON_MODEL';
export const SET_PLACEHOLDER_MODEL = 'THEME_OPTIONS/SET_PLACEHOLDER_MODEL';
export const SET_CMS_SECTION = 'THEME_OPTIONS/SET_CMS_SECTION';
export const SET_CURRENT_SECTION = 'THEME_OPTIONS/SET_CURRENT_SECTION';
export const SET_CMS_MODEL_IMAGE = 'THEME_OPTIONS/SET_CMS_MODEL_IMAGE';
export const SET_CMS_IMAGE_ARRAY = 'THEME_OPTIONS/SET_CMS_IMAGE_ARRAY';
export const SET_CMS_VIDEO_ARRAY = 'THEME_OPTIONS/SET_CMS_VIDEO_ARRAY';
export const SET_MODULE_CMS_OBJECT = 'THEME_OPTIONS/SET_MODULE_CMS_OBJECT';
export const SET_EDITOR_STATE = 'THEME_OPTIONS/SET_EDITOR_STATE';
export const SET_THEME_COLOR = 'THEME_OPTIONS/SET_THEME_COLOR';
export const SET_CMS_ENABLE_UPLOAD_IMAGE = 'THEME_OPTIONS/SET_CMS_ENABLE_UPLOAD_IMAGE';
export const SET_THEME_BACKGROUND_COLOR = 'THEME_OPTIONS/SET_THEME_BACKGROUND_COLOR';
export const SET_THEME_SECTION_BACKGROUND_COLOR = 'THEME_OPTIONS/SET_THEME_SECTION_BACKGROUND_COLOR';
export const SET_SECTION_BACKGROUND_COLOR = 'THEME_OPTIONS/SET_SECTION_BACKGROUND_COLOR';
export const SET_CMS_ENABLE = 'THEME_OPTIONS/SET_CMS_ENABLE';
export const SET_CMS_ENABLE_BACKGROUND = 'THEME_OPTIONS/SET_CMS_ENABLE_BACKGROUND';
export const SET_CMS_KEY = 'THEME_OPTIONS/SET_CMS_KEY';
export const SET_CMS_KEY_INNER = 'THEME_OPTIONS/SET_CMS_KEY_INNER';
export const SET_CMS_VALUE = 'THEME_OPTIONS/SET_CMS_VALUE';
export const SET_ENABLE_ACTIVE_Tab = 'THEME_OPTIONS/SET_ENABLE_ACTIVE_Tab';
export const SET_ENABLE_RICHTEXT_EDITOR = 'THEME_OPTIONS/SET_ENABLE_RICHTEXT_EDITOR';
export const SET_ENABLE_ROLE_ACTIVE_TAB = 'THEME_OPTIONS/SET_ENABLE_ROLE_ACTIVE_TAB';
export const SET_ENABLE_DISACTIVE_Tab = 'THEME_OPTIONS/SET_ENABLE_DISACTIVE_Tab';
export const SET_ENABLE_DISACTIVE_ROLE_TAB = 'THEME_OPTIONS/SET_ENABLE_DISACTIVE_ROLE_TAB';
export const SET_USER_DATA = 'THEME_OPTIONS/SET_USER_DATA';
export const SET_USER_ARRAY = 'THEME_OPTIONS/SET_USER_ARRAY';
export const SET_CMS_ARRAY = 'THEME_OPTIONS/SET_CMS_ARRAY';
export const SET_Audit_ARRAY_ID = 'THEME_OPTIONS/SET_Audit_ARRAY_ID';
export const SET_PERMISSION_ARRAY = 'THEME_OPTIONS/SET_PERMISSION_ARRAY';
export const SET_ROLE_ARRAY = 'THEME_OPTIONS/SET_ROLE_ARRAY';
export const SET_ROLE_ARRAY_INDEX = 'THEME_OPTIONS/SET_ROLE_ARRAY_INDEX';

export const SET_USER_EDIT_ARRAY = 'THEME_OPTIONS/SET_USER_EDIT_ARRAY';
export const SET_ROLE_EDIT_ARRAY = 'THEME_OPTIONS/SET_ROLE_EDIT_ARRAY';

export const SET_USER_LIST_ARRAY = 'THEME_OPTIONS/SET_USER_LIST_ARRAY';
export const SET_AUDIT_LIST_ARRAY = 'THEME_OPTIONS/SET_AUDIT_LIST_ARRAY';

export const SET_USER_IS_EDIT = 'THEME_OPTIONS/SET_USER_IS_EDIT';
export const SET_ROLE_IS_EDIT = 'THEME_OPTIONS/SET_ROLE_IS_EDIT';
export const SET_USER_VISIBLE = 'THEME_OPTIONS/SET_USER_VISIBLE';
export const SET_USER_EXCEL = 'THEME_OPTIONS/SET_USER_EXCEL';
export const SET_AUDIT_EXCEL = 'THEME_OPTIONS/SET_AUDIT_EXCEL';
export const SET_USER_SEARCH = 'THEME_OPTIONS/SET_USER_SEARCH';
export const SET_ENABLE_BACKGROUND_IMAGE = 'THEME_OPTIONS/SET_ENABLE_BACKGROUND_IMAGE';

export const SET_ROLE_EXCEL = 'THEME_OPTIONS/SET_ROLE_EXCEL';
export const SET_CHECKBOX_ROLE_ARRAY = 'THEME_OPTIONS/SET_CHECKBOX_ROLE_ARRAY';
export const SET_ROLE_SEARCH = 'THEME_OPTIONS/SET_ROLE_SEARCH';
export const SET_AUDIT_SEARCH = 'THEME_OPTIONS/SET_AUDIT_SEARCH';
export const SET_ENABLE_MOBILE_MENU = 'THEME_OPTIONS/SET_ENABLE_MOBILE_MENU';
export const SET_ENABLE_MOBILE_MENU_SMALL = 'THEME_OPTIONS/SET_ENABLE_MOBILE_MENU_SMALL';
export const SET_TYPE_ID = 'THEME_OPTIONS/SET_TYPE_ID';

export const SET_ENABLE_FIXED_HEADER = 'THEME_OPTIONS/SET_ENABLE_FIXED_HEADER';
export const SET_ENABLE_HEADER_SHADOW = 'THEME_OPTIONS/SET_ENABLE_HEADER_SHADOW';
export const SET_ENABLE_SIDEBAR_SHADOW = 'THEME_OPTIONS/SET_ENABLE_SIDEBAR_SHADOW';
export const SET_ENABLE_FIXED_SIDEBAR = 'THEME_OPTIONS/SET_ENABLE_FIXED_SIDEBAR';
export const SET_ENABLE_CLOSED_SIDEBAR = 'THEME_OPTIONS/SET_ENABLE_CLOSED_SIDEBAR';
export const SET_ENABLE_FIXED_FOOTER = 'THEME_OPTIONS/SET_ENABLE_FIXED_FOOTER';

export const SET_ENABLE_PAGETITLE_ICON = 'THEME_OPTIONS/SET_ENABLE_PAGETITLE_ICON';
export const SET_ENABLE_PAGETITLE_SUBHEADING = 'THEME_OPTIONS/SET_ENABLE_PAGETITLE_SUBHEADING';
export const SET_ENABLE_PAGE_TABS_ALT = 'THEME_OPTIONS/SET_ENABLE_PAGE_TABS_ALT';

export const SET_BACKGROUND_IMAGE = 'THEME_OPTIONS/SET_BACKGROUND_IMAGE';
export const SET_BACKGROUND_COLOR = 'THEME_OPTIONS/SET_BACKGROUND_COLOR';
export const SET_COLOR_SCHEME = 'THEME_OPTIONS/SET_COLOR_SCHEME';
export const SET_BACKGROUND_IMAGE_OPACITY = 'THEME_OPTIONS/SET_BACKGROUND_IMAGE_OPACITY';

export const SET_HEADER_BACKGROUND_COLOR = 'THEME_OPTIONS/SET_HEADER_BACKGROUND_COLOR';
export const SET_PREVIOUS_COLORS_VALUE = 'THEME_OPTIONS/SET_PREVIOUS_COLORS_VALUE';
export const SET_SECTION_ID = 'THEME_OPTIONS/SET_SECTION_ID';
export const SET_CONTACT_US_EMAIL = 'THEME_OPTIONS/SET_CONTACT_US_EMAIL';
export const SET_SUBSCRIPTION_EMAIL = 'THEME_OPTIONS/SET_SUBSCRIPTION_EMAIL';

export const SET_IS_GLOBAL = 'THEME_OPTIONS/SET_IS_GLOBAL';
export const SET_CUSTOM_STYLE = 'THEME_OPTIONS/SET_CUSTOM_STYLE';
export const SET_SHOW_ICON = 'THEME_OPTIONS/SET_SHOW_ICON';
export const SET_LOGO_ICON = 'THEME_OPTIONS/SET_LOGO_ICON';
export const SET_PLACEHOLDER_VALUE = 'THEME_OPTIONS/SET_PLACEHOLDER_VALUE';
export const SET_DYNAMIC_PLACEHOLDER_VALUE = 'THEME_OPTIONS/SET_DYNAMIC_PLACEHOLDER_VALUE';
export const SET_PLACEHOLDER_POSITION = 'THEME_OPTIONS/SET_PLACEHOLDER_POSITION';
export const SET_RICHTEXT_POSITION_END = 'THEME_OPTIONS/SET_RICHTEXT_POSITION_END';
export const SET_SHOW_PLACEHOLDER = 'THEME_OPTIONS/SET_SHOW_PLACEHOLDER';
export const SET_SHOW_CONTACT = 'THEME_OPTIONS/SET_SHOW_CONTACT';

export const setUserExcel = userExcel => ({
    type: SET_USER_EXCEL,
    userExcel
});

export const setPlaceholderValue = placeholderValue => ({
    type: SET_PLACEHOLDER_VALUE,
    placeholderValue
});
export const setDynamicPlaceholderValue = dynamicPlaceholderValue => ({
    type: SET_DYNAMIC_PLACEHOLDER_VALUE,
    dynamicPlaceholderValue
});
export const setPlaceholderPosition = placeholderPosition => ({
    type: SET_PLACEHOLDER_POSITION,
    placeholderPosition
});
export const setRichTextPositionEnd = richTextPositionEnd => ({
    type: SET_RICHTEXT_POSITION_END,
    richTextPositionEnd
});
export const setShowPlaceholder = showPlaceholder => ({
    type: SET_SHOW_PLACEHOLDER,
    showPlaceholder
});
export const setShowContact = showContact => ({
    type: SET_SHOW_CONTACT,
    showContact
});

export const setSectionId = sectionId => ({
    type: SET_SECTION_ID,
    sectionId
});

export const setCustomStyle = customStyle => ({
    type: SET_CUSTOM_STYLE,
    customStyle
});

export const setShowIcon = showIcon => ({
    type: SET_SHOW_ICON,
    showIcon
});

export const setLogoIcon = logoIcon => ({
    type: SET_LOGO_ICON,
    logoIcon
});

export const setSelectedEdit = selectedEdit => ({
    type: SET_SELECTED_EDIT,
    selectedEdit
});

export const setAuditExcel = auditExcel => ({
    type: SET_AUDIT_EXCEL,
    auditExcel
});
export const setRoleExcel = roleExcel => ({
    type: SET_ROLE_EXCEL,
    roleExcel
});

export const setTypeId = typeId => ({
    type: SET_TYPE_ID,
    typeId
});

export const setUserSearch = userSearch => ({
    type: SET_USER_SEARCH,
    userSearch
});
export const setRoleSearch = roleSearch => ({
    type: SET_ROLE_SEARCH,
    roleSearch
});

export const setAuditSearch = aduitSearch => ({
    type: SET_AUDIT_SEARCH,
    aduitSearch
});

export const setEnableActiveTab = enableActiveTab => ({
    type: SET_ENABLE_ACTIVE_Tab,
    enableActiveTab
});
export const setEnableRichTextEditor = enableRichTextEditor => ({
    type: SET_ENABLE_RICHTEXT_EDITOR,
    enableRichTextEditor
});

export const setEditorState = editorState => ({
    type: SET_EDITOR_STATE,
    editorState
});
export const setModuleCmsObject = moduleCmsObject => ({
    type: SET_MODULE_CMS_OBJECT,
    moduleCmsObject
});
export const setCmsModel = cmsModel => ({
    type: SET_CMS_MODEL,
    cmsModel
});
export const setTypesArray = typesArray => ({
    type: SET_TYPES_ARRAY,
    typesArray
});
export const setRichTextArray = richTextArray => ({
    type: SET_RICHTEXT_ARRAY,
    richTextArray
});
export const setRichTextArrayStyle = richTextArrayStyle => ({
    type: SET_RICHTEXT_ARRAY_STYLE,
    richTextArrayStyle
});
export const setRichTextArrayStyleSheet = richTextArrayStyleSheet => ({
    type: SET_RICHTEXT_ARRAY_STYLESHEET,
    richTextArrayStyleSheet
});
export const setRichTextFontSize = richTextFontSize => ({
    type: SET_RICHTEXT_FONT_SIZE,
    richTextFontSize
});
export const setRichTextFontFamily = richTextFontFamily => ({
    type: SET_RICHTEXT_FONT_FAMILY,
    richTextFontFamily
});
export const setSectionArrayModel = sectionArrayModel => ({
    type: SET_SECTION_ARRAY_MODEL,
    sectionArrayModel
});
export const setEditSectionModel = editSectionModel => ({
    type: SET_EDIT_SECTION_MODEL,
    editSectionModel
});
export const setEditSectionModelArray = editSectionModelArray => ({
    type: SET_EDIT_SECTION_MODEL_ARRAY,
    editSectionModelArray
});
export const setEditSectionModelEnable = editSectionModelEnable => ({
    type: SET_EDIT_SECTION_MODEL_ENABLE,
    editSectionModelEnable
});
export const setAddModel = addModel => ({
    type: SET_ADD_MODEL,
    addModel
});
export const setSocialModel = socialModel => ({
    type: SET_SOCIAL_MODEL,
    socialModel
});
export const setIconModel = iconModel => ({
    type: SET_ICON_MODEL,
    iconModel
});
export const setPlaceHolderModel = placeHolderModel => ({
    type: SET_PLACEHOLDER_MODEL,
    placeHolderModel
});
export const setIsGlobal = isGlobal => ({
    type: SET_IS_GLOBAL,
    isGlobal,
});
export const setCmsSection = cmsSection => ({
    type: SET_CMS_SECTION,
    cmsSection
});
export const setCurrentSection = currentSection => ({
    type: SET_CURRENT_SECTION,
    currentSection
});
export const setCmsModelImage = cmsModelImage => ({
    type: SET_CMS_MODEL_IMAGE,
    cmsModelImage
});
export const setCmsImageArray = cmsImageArray => ({
    type: SET_CMS_IMAGE_ARRAY,
    cmsImageArray
});
export const setCmsVideoArray = cmsVideoArray => ({
    type: SET_CMS_VIDEO_ARRAY,
    cmsVideoArray
});
export const setCmsKey = cmsKey => ({
    type: SET_CMS_KEY,
    cmsKey
});
export const setCmsKeyInner = cmsKeyInner => ({
    type: SET_CMS_KEY_INNER,
    cmsKeyInner
});
export const setCmsEnable = cmsEnable => ({
    type: SET_CMS_ENABLE,
    cmsEnable
});
export const setCmsEnableBackground = cmsEnableBackground => ({
    type: SET_CMS_ENABLE_BACKGROUND,
    cmsEnableBackground
});
export const setThemeColor = themeColor=>({
    type: SET_THEME_COLOR,
    themeColor
});
export const setThemeBackgroundColor = themeBackgroundColor => ({
    type: SET_THEME_BACKGROUND_COLOR,
    themeBackgroundColor
});
export const setThemeSectionBackgroundColor = themeSectionBackgroundColor => ({
    type: SET_THEME_SECTION_BACKGROUND_COLOR,
    themeSectionBackgroundColor
});
export const setSectionBackgroundColor = sectionBackgroundColor => ({
    type: SET_SECTION_BACKGROUND_COLOR,
    sectionBackgroundColor
});
export const setCmsEnableUploadImage = cmsEnableUploadImage => ({
    type: SET_CMS_ENABLE_UPLOAD_IMAGE,
    cmsEnableUploadImage
});
export const setCmsValue = cmsValue => ({
    type: SET_CMS_VALUE,
    cmsValue
});

export const setPreviousColorsValue = previousColors => ({
    type: SET_PREVIOUS_COLORS_VALUE,
    previousColors
});


export const setEnableActiveRoleTab = enableActiveRoleTab => ({
    type: SET_ENABLE_ROLE_ACTIVE_TAB,
    enableActiveRoleTab
});

export const setEnableDISACTIVETab = enableDISACTIVETab => ({
    type: SET_ENABLE_DISACTIVE_Tab,
    enableDISACTIVETab
});
export const setEnableDISACTIVERoleTab = enableDISACTIVERoleTab => ({
    type: SET_ENABLE_DISACTIVE_ROLE_TAB,
    enableDISACTIVERoleTab
});
export const setAddModelData = addModelData => ({
    type: SET_ADD_MODEL_DATA,
    addModelData
});
export const setEnableActive = enableActive => ({
    type: SET_ENABLE_ACTIVE,
    enableActive
});
export const setEnableShow = enableShow => ({
    type: SET_ENABLE_SHOW,
    enableShow
});
export const setEnableTextModel = enableTextModel => ({
    type: SET_ENABLE_TEXT_MODEL,
    enableTextModel
});
export const setNewBlogKey = newBlogKey => ({
    type: SET_NEW_BLOG_KEY,
    newBlogKey
});

export const setEnableTextClone = enableTextClone => ({
    type: SET_ENABLE_TEXT_CLONE,
    enableTextClone
});
export const setAddNewCmsFlag = addNewCmsFlag => ({
    type: SET_ADD_NEW_CMS_FLAG,
    addNewCmsFlag
});
export const setIsHorizontal = isHorizontal => ({
    type: SET_IS_HORIZONTAL,
    isHorizontal
});
export const setFontSize = fontSize => ({
    type: SET_FONT_SIZE,
    fontSize
});
export const setFontFamily = fontFamily => ({
    type: SET_FONT_FAMILY,
    fontFamily
});
export const setInlineStyle = inlineStyle => ({
    type: SET_INLINE_STYLE,
    inlineStyle
});
export const setInlineStyleEnable = inlineStyleEnable => ({
    type: SET_INLINE_STYLE_ENABLE,
    inlineStyleEnable
});
export const setContactUsEmail = enableShow => ({
    type: SET_CONTACT_US_EMAIL,
    enableShow
});
export const setSubscriptionEmail = enableShow => ({
    type: SET_SUBSCRIPTION_EMAIL,
    enableShow
});
export const setPaddingTwo = paddingTwo => ({
    type: SET_PADDING_Two,
    paddingTwo
});
export const setBackgroundCmsKey = backgroundCmsKey => ({
    type: SET_BACKGROUND_CMS_KEY,
    backgroundCmsKey
});
export const setUserData = userData => ({
    type: SET_USER_DATA,
    userData
});
export const setOldPath = oldPath => ({
    type: SET_OLD_PATH,
    oldPath
});
export const setDataTable = dataTable => ({
    type: SET_DATATABLE,
    dataTable
});
export const setUserEditArray = userEditArray => ({
    type: SET_USER_EDIT_ARRAY,
    userEditArray
});
export const setRoleEditArray = roleEditArray => ({
    type: SET_ROLE_EDIT_ARRAY,
    roleEditArray
});
export const setAuditListArray = auditListArray => ({
    type: SET_AUDIT_LIST_ARRAY,
    auditListArray
});
export const setUserlistArray = userlistArray => ({
    type: SET_USER_LIST_ARRAY,
    userlistArray
});
export const setPermissionArray = permissionArray => ({
    type: SET_PERMISSION_ARRAY,
    permissionArray
});
export const setRoleArray = roleArray => ({
    type: SET_ROLE_ARRAY,
    roleArray
});
export const setRoleArrayIndex = roleArrayIndex => ({
    type: SET_ROLE_ARRAY_INDEX,
    roleArrayIndex
});

export const setUserVisible = userVisible => ({
    type: SET_USER_VISIBLE,
    userVisible
});
export const setUserIsEdit = userIsEdit => ({
    type: SET_USER_IS_EDIT,
    userIsEdit
});
export const setRoleIsEdit = roleIsEdit => ({
    type: SET_ROLE_IS_EDIT,
    roleIsEdit
});
export const setCmsArray = cmsArray => ({
    type: SET_CMS_ARRAY,
    cmsArray
});
export const setUserArray = userArray => ({
    type: SET_USER_ARRAY,
    userArray
});
export const setAuditArrayID = auditArrayID => ({
    type: SET_Audit_ARRAY_ID,
    auditArrayID
});
export const setSubscriptionArrayID = subscriptionArrayID => ({
    type: SET_SUBSCRIPTION_ARRAY_ID,
    subscriptionArrayID
});
export const setSubscriptionListArray = subscriptionListArray => ({
    type: SET_SUBSCRIPTION_LIST_ARRAY,
    subscriptionListArray
});
export const setSubscriptionExcel = subscriptionExcel => ({
    type: SET_SUBSCRIPTION_EXCEL,
    subscriptionExcel
});
export const setSubscriptionSearch = subscriptionSearch => ({
    type: SET_SUBSCRIPTION_SEARCH,
    subscriptionSearch
});
export const setContactArrayID = contactArrayID => ({
    type: SET_Contact_ARRAY_ID,
    contactArrayID
});
export const setContactListArray = contactListArray => ({
    type: SET_Contact_LIST_ARRAY,
    contactListArray
});
export const setContactExcel = contactExcel => ({
    type: SET_Contact_EXCEL,
    contactExcel
});
export const setContactSearch = contactSearch => ({
    type: SET_Contact_SEARCH,
    contactSearch
});
export const setCheckboxRoleArray = checkboxRoleArray => ({
    type: SET_CHECKBOX_ROLE_ARRAY,
    checkboxRoleArray
});
export const setEnableBackgroundImage = enableBackgroundImage => ({
    type: SET_ENABLE_BACKGROUND_IMAGE,
    enableBackgroundImage
});

export const setEnableFixedHeader = enableFixedHeader => ({
    type: SET_ENABLE_FIXED_HEADER,
    enableFixedHeader
});

export const setEnableHeaderShadow = enableHeaderShadow => ({
    type: SET_ENABLE_HEADER_SHADOW,
    enableHeaderShadow
});

export const setEnableSidebarShadow = enableSidebarShadow => ({
    type: SET_ENABLE_SIDEBAR_SHADOW,
    enableSidebarShadow
});

export const setEnablePageTitleIcon = enablePageTitleIcon => ({
    type: SET_ENABLE_PAGETITLE_ICON,
    enablePageTitleIcon
});

export const setEnablePageTitleSubheading = enablePageTitleSubheading => ({
    type: SET_ENABLE_PAGETITLE_SUBHEADING,
    enablePageTitleSubheading
});

export const setEnablePageTabsAlt = enablePageTabsAlt => ({
    type: SET_ENABLE_PAGE_TABS_ALT,
    enablePageTabsAlt
});

export const setEnableFixedSidebar = enableFixedSidebar => ({
    type: SET_ENABLE_FIXED_SIDEBAR,
    enableFixedSidebar
});

export const setEnableClosedSidebar = enableClosedSidebar => ({
    type: SET_ENABLE_CLOSED_SIDEBAR,
    enableClosedSidebar
});

export const setEnableMobileMenu = enableMobileMenu => ({
    type: SET_ENABLE_MOBILE_MENU,
    enableMobileMenu
});

export const setEnableMobileMenuSmall = enableMobileMenuSmall => ({
    type: SET_ENABLE_MOBILE_MENU_SMALL,
    enableMobileMenuSmall
});

export const setEnableFixedFooter = enableFixedFooter => ({
    type: SET_ENABLE_FIXED_FOOTER,
    enableFixedFooter
});

export const setBackgroundColor = backgroundColor => ({
    type: SET_BACKGROUND_COLOR,
    backgroundColor
});

export const setHeaderBackgroundColor = headerBackgroundColor => ({
    type: SET_HEADER_BACKGROUND_COLOR,
    headerBackgroundColor
});

export const setColorScheme = colorScheme => ({
    type: SET_COLOR_SCHEME,
    colorScheme
});

export const setBackgroundImageOpacity = backgroundImageOpacity => ({
    type: SET_BACKGROUND_IMAGE_OPACITY,
    backgroundImageOpacity
});

export const setBackgroundImage = backgroundImage  => ({
    type: SET_BACKGROUND_IMAGE,
    backgroundImage
});

export default function reducer(state = {
    paddingTwo:'2vh',
    backgroundCmsKey:null,
    userData:[],
    auditListArray:[],
    userlistArray:[],
    userEditArray:[],
    roleEditArray:[],
    sectionBackgroundColor:[],
    cmsArray:[],
    userArray:[],
    auditArrayID:[],
    checkboxRoleArray:[],
    contactArrayID:null,
    contactListArray:[],
    contactExcel:[],
    contactSearch:null,
    subscriptionArrayID:null,
    subscriptionListArray:[],
    subscriptionExcel:[],
    subscriptionSearch:null,
    oldPath:'',
    dataTable:{
        user:{
            activeTab:true,
            deletedTab:false,
            path:'',
            flag:false,
            activeData:[],
            deletedData:[],
        },
    },
    permissionArray:[],
    roleArray:[],
    roleArrayIndex:[],
    userVisible:false,
    userIsEdit:false,
    roleIsEdit:false,
    backgroundColor: '',
    enableActive:false,
    addModelData:null,
    cmsModel:false,
    typesArray:{},
    richTextArray:[],
    richTextArrayStyle:{},
    richTextArrayStyleSheet:{},
    richTextFontSize:{},
    richTextFontFamily:{},
    typeId: {},
    selectedEdit: {},
    sectionArrayModel:[],
    editSectionModel:null,
    editSectionModelArray:[],
    editSectionModelEnable:false,
    addModel:false,
    socialModel:false,
    iconModel:false,
    placeHolderModel:false,
    cmsSection:[],
    currentSection:"",
    cmsImageArray:false,
    cmsVideoArray: false,
    cmsModelImage:false,
    editorState:'',
    moduleCmsObject:{},
    cmsKey:'',
    cmsKeyInner:'',
    cmsEnable:false,
    cmsEnableBackground:false,
    themeColor:{},
    cmsEnableUploadImage:false,
    themeBackgroundColor:{},
    themeSectionBackgroundColor:false,
    cmsValue:'',
    enableActiveTab:true,
    enableRichTextEditor:false,
    enableActiveRoleTab:true,
    userExcel:[],
    auditExcel:[],
    roleExcel:[],
    userSearch:"",
    roleSearch:"",
    aduitSearch:'',
    enableDISACTIVETab:false,
    enableDISACTIVERoleTab:false,
    enableShow:false,
    enableTextClone:false,
    enableTextModel:false,
    newBlogKey:{},
    addNewCmsFlag:false,
    isHorizontal:false,
    fontSize:16,
    fontFamily:'Lato',
    inlineStyle:{},
    inlineStyleEnable:false,
    headerBackgroundColor: '',
    enableMobileMenuSmall: '',
    enableBackgroundImage: false,
    enableClosedSidebar: false,
    enableFixedHeader: true,
    enableHeaderShadow: true,
    enableSidebarShadow: true,
    enableFixedFooter: true,
    enableFixedSidebar: true,
    colorScheme: 'white',
    backgroundImage: sideBar6,
    backgroundImageOpacity: 'opacity-06',
    enablePageTitleIcon: true,
    enablePageTitleSubheading: true,
    enablePageTabsAlt: true,
    previousColors: [],
    sectionId: "",
    contact_us_email :"",
    subscription_email: "",
    isGlobal: false,
    customStyle: [],
    showIcon: "",
    logoIcon: "",
    placeholderValue:[],
    dynamicPlaceholderValue:[],
    placeholderPosition:null,
    richTextPositionEnd:null,
    showPlaceholder: "",
    showContact: [],
}, action) {
    switch (action.type) {
        case SET_AUDIT_EXCEL:
        return{
            ...state,
            auditExcel: action.auditExcel
        };
        case SET_PLACEHOLDER_VALUE:
        return{
            ...state,
            placeholderValue: action.placeholderValue
        };
        case SET_SHOW_CONTACT:
        return{
            ...state,
            showContact: action.showContact
        };
        case SET_SHOW_PLACEHOLDER:
        return{
            ...state,
            showPlaceholder: action.showPlaceholder
        };
        case SET_DYNAMIC_PLACEHOLDER_VALUE:
        return{
            ...state,
            dynamicPlaceholderValue: action.dynamicPlaceholderValue
        };
        case SET_PLACEHOLDER_POSITION:
        return{
            ...state,
            placeholderPosition: action.placeholderPosition
        };
        case SET_RICHTEXT_POSITION_END:
        return{
            ...state,
            richTextPositionEnd: action.richTextPositionEnd
        };
        case SET_SECTION_ID:
        return{
            ...state,
            sectionId: action.sectionId
        };
        case SET_CUSTOM_STYLE:
        return{
            ...state,
            customStyle: action.customStyle
        };
        case SET_SHOW_ICON:
        return{
            ...state,
            showIcon: action.showIcon
        };
        case SET_LOGO_ICON:
        return{
            ...state,
            logoIcon: action.logoIcon
        };
        case SET_USER_EXCEL:
        return{
            ...state,
            userExcel: action.userExcel
        };
        case SET_ROLE_EXCEL:
        return{
            ...state,
            roleExcel: action.roleExcel
        };
        case SET_IS_GLOBAL:
        return{
            ...state,
            isGlobal: action.isGlobal
        };
        case SET_USER_SEARCH:
        return{
            ...state,
            userSearch: action.userSearch
        };
        case SET_ROLE_SEARCH:
        return{
            ...state,
            roleSearch: action.roleSearch
        };
        case SET_AUDIT_SEARCH:
        return{
            ...state,
            aduitSearch: action.aduitSearch
        };
        case SET_USER_DATA:
        return{
            ...state,
            userData: action.userData
        };
        case SET_ENABLE_ACTIVE_Tab:
        return{
            ...state,
            enableActiveTab: action.enableActiveTab
        };
        case SET_ENABLE_RICHTEXT_EDITOR:
        return{
            ...state,
            enableRichTextEditor: action.enableRichTextEditor
        };
        case SET_ADD_MODEL_DATA:
        return{
            ...state,
            addModelData: action.addModelData
        };
        case SET_CMS_MODEL:
        return{
            ...state,
            cmsModel: action.cmsModel
        };
        case SET_SELECTED_EDIT:
        return{
            ...state,
            selectedEdit: action.selectedEdit
        };
        case SET_TYPES_ARRAY:
        return{
            ...state,
            typesArray: action.typesArray
        };
        case SET_RICHTEXT_ARRAY:
        return{
            ...state,
            richTextArray: action.richTextArray
        };
        case SET_RICHTEXT_ARRAY_STYLE:
        return{
            ...state,
            richTextArrayStyle: action.richTextArrayStyle
        };
        case SET_RICHTEXT_ARRAY_STYLESHEET:
        return{
            ...state,
            richTextArrayStyleSheet: action.richTextArrayStyleSheet
        };
        case SET_RICHTEXT_FONT_SIZE:
        return{
            ...state,
            richTextFontSize: action.richTextFontSize
        };
        case SET_RICHTEXT_FONT_FAMILY:
        return{
            ...state,
            richTextFontFamily: action.richTextFontFamily
        };
        case SET_SECTION_ARRAY_MODEL:
        return{
            ...state,
            sectionArrayModel: action.sectionArrayModel
        };
        case SET_EDIT_SECTION_MODEL_ARRAY:
        return{
            ...state,
            editSectionModelArray: action.editSectionModelArray
        };
        case SET_EDIT_SECTION_MODEL:
        return{
            ...state,
            editSectionModel: action.editSectionModel
        };
        case SET_EDIT_SECTION_MODEL_ENABLE:
        return{
            ...state,
            editSectionModelEnable: action.editSectionModelEnable
        };
        case SET_ADD_MODEL:
        return{
            ...state,
            addModel: action.addModel
        };
        case SET_SOCIAL_MODEL:
        return{
            ...state,
            socialModel: action.socialModel
        };
        case SET_ICON_MODEL:
        return{
            ...state,
            iconModel: action.iconModel
        };
        case SET_PLACEHOLDER_MODEL:
        return{
            ...state,
            placeHolderModel: action.placeHolderModel
        };
        case SET_CMS_SECTION:
        return{
            ...state,
            cmsSection: action.cmsSection
        };
        case SET_CURRENT_SECTION:
        return{
            ...state,
            currentSection: action.currentSection
        };
        case SET_CMS_MODEL_IMAGE:
        return{
            ...state,
            cmsModelImage: action.cmsModelImage
        };
        case SET_CMS_IMAGE_ARRAY:
        return{
            ...state,
            cmsImageArray: action.cmsImageArray
        };
        case SET_CMS_VIDEO_ARRAY:
        return{
            ...state,
            cmsVideoArray: action.cmsVideoArray
        };
        case SET_EDITOR_STATE:
        return{
            ...state,
            editorState: action.editorState
        };
        case SET_MODULE_CMS_OBJECT:
        return{
            ...state,
            moduleCmsObject: action.moduleCmsObject
        };
        case SET_CMS_ENABLE:
        return{
            ...state,
            cmsEnable: action.cmsEnable
        };
        case SET_CMS_ENABLE_BACKGROUND:
        return{
            ...state,
            cmsEnableBackground: action.cmsEnableBackground
        };
        case SET_CMS_KEY:
        return{
            ...state,
            cmsKey: action.cmsKey
        };
        case SET_CMS_KEY_INNER:
        return{
            ...state,
            cmsKeyInner: action.cmsKeyInner
        };
        case SET_THEME_COLOR:
        return{
            ...state,
            themeColor: action.themeColor
        };
        case SET_THEME_SECTION_BACKGROUND_COLOR:
        return{
            ...state,
            themeSectionBackgroundColor: action.themeSectionBackgroundColor
        };
        case SET_SECTION_BACKGROUND_COLOR:
        return{
            ...state,
            sectionBackgroundColor: action.sectionBackgroundColor
        };
        case SET_PREVIOUS_COLORS_VALUE:
        return{
            ...state,
            previousColors: action.setPreviousColorsValue
        };
        case SET_THEME_BACKGROUND_COLOR:
        return{
            ...state,
            themeBackgroundColor: action.themeBackgroundColor
        };
        case SET_CMS_ENABLE_UPLOAD_IMAGE:
        return{
            ...state,
            cmsEnableUploadImage: action.cmsEnableUploadImage
        };
        case SET_CMS_VALUE:
        return{
            ...state,
            cmsValue: action.cmsValue
        };
        case SET_CONTACT_US_EMAIL:
        return{
            ...state,
            contact_us_email: action.enableShow
        };
        case SET_CONTACT_US_EMAIL:
        return{
            ...state,
            contact_us_email: action.enableShow
        };
        case SET_ENABLE_TEXT_CLONE:
        return{
            ...state,
            enableTextClone: action.enableTextClone
        };
        case SET_ENABLE_TEXT_MODEL:
        return{
            ...state,
            enableTextModel: action.enableTextModel
        };
        case SET_NEW_BLOG_KEY:
        return{
            ...state,
            newBlogKey: action.newBlogKey
        };
        case SET_ADD_NEW_CMS_FLAG:
        return{
            ...state,
            addNewCmsFlag: action.addNewCmsFlag
        };
        case SET_IS_HORIZONTAL:
        return{
            ...state,
            isHorizontal: action.isHorizontal
        };
        case SET_FONT_SIZE:
        return{
            ...state,
            fontSize: action.fontSize
        };
        case SET_FONT_FAMILY:
        return{
            ...state,
            fontFamily: action.fontFamily
        };
        case SET_INLINE_STYLE:
        return{
            ...state,
            inlineStyle: action.inlineStyle
        };
        case SET_INLINE_STYLE_ENABLE:
        return{
            ...state,
            inlineStyleEnable: action.inlineStyleEnable
        };
        case SET_SUBSCRIPTION_EMAIL:
        return{
            ...state,
            subscription_email: action.enableShow
        };
        case SET_ENABLE_ROLE_ACTIVE_TAB:
        return{
            ...state,
            enableActiveRoleTab: action.enableActiveRoleTab
        };
        case SET_ENABLE_DISACTIVE_Tab:
        return{
            ...state,
            enableDISACTIVETab: action.enableDISACTIVETab
        };
        case SET_ENABLE_DISACTIVE_ROLE_TAB:
        return{
            ...state,
            enableDISACTIVERoleTab: action.enableDISACTIVERoleTab
        };
        case SET_USER_VISIBLE:
            return{
                ...state,
                userVisible: action.userVisible
            };
        case SET_USER_IS_EDIT:
            return{
                ...state,
                userIsEdit: action.userIsEdit
            };
        case SET_ROLE_IS_EDIT:
            return{
                ...state,
                roleIsEdit: action.roleIsEdit
            };
        case SET_PERMISSION_ARRAY:
            return{
                ...state,
                permissionArray: action.permissionArray
            };
        case SET_ROLE_ARRAY:
            return{
                ...state,
                roleArray: action.roleArray
            };
        case SET_ROLE_ARRAY_INDEX:
            return{
                ...state,
                roleArrayIndex: action.roleArrayIndex
            };
        case SET_AUDIT_LIST_ARRAY:
            return{
                ...state,
                auditListArray: action.auditListArray
            };
        case SET_USER_LIST_ARRAY:
            return{
                ...state,
                userlistArray: action.userlistArray
            };
        case SET_USER_EDIT_ARRAY:
            return{
                ...state,
                userEditArray: action.userEditArray
            };
        case SET_ROLE_EDIT_ARRAY:
            return{
                ...state,
                roleEditArray: action.roleEditArray
            };
        case SET_Audit_ARRAY_ID:
            return{
                ...state,
                auditArrayID: action.auditArrayID
            };
        case SET_CMS_ARRAY:
            return{
                ...state,
                cmsArray: action.cmsArray
            };
        case SET_USER_ARRAY:
            return{
                ...state,
                userArray: action.userArray
            };
        case SET_Contact_SEARCH:
            return{
                ...state,
                contactSearch: action.contactSearch
            };
        case SET_Contact_EXCEL:
            return{
                ...state,
                contactExcel: action.contactExcel
            };
        case SET_Contact_LIST_ARRAY:
            return{
                ...state,
                contactListArray: action.contactListArray
            };
        case SET_CHECKBOX_ROLE_ARRAY:
            return{
                ...state,
                checkboxRoleArray: action.checkboxRoleArray
            };
        case SET_SUBSCRIPTION_ARRAY_ID:
            return{
                ...state,
                subscriptionArrayID: action.subscriptionArrayID
            };
        case SET_SUBSCRIPTION_LIST_ARRAY:
            return{
                ...state,
                subscriptionListArray: action.subscriptionListArray
            };
        case SET_SUBSCRIPTION_EXCEL:
            return{
                ...state,
                subscriptionExcel: action.subscriptionExcel
            };
        case SET_SUBSCRIPTION_SEARCH:
            return{
                ...state,
                subscriptionSearch: action.subscriptionSearch
            };
        case SET_Contact_ARRAY_ID:
            return{
                ...state,
                contactArrayID: action.contactArrayID
            };
        case SET_OLD_PATH:
            return{
                ...state,
                oldPath: action.oldPath
            };
        case SET_DATATABLE:
            return{
                ...state,
                dataTable: action.dataTable
            };
        case SET_PADDING_Two:
            return{
                ...state,
                paddingTwo: action.paddingTwo
            };
        case SET_BACKGROUND_CMS_KEY:
            return{
                ...state,
                backgroundCmsKey: action.backgroundCmsKey
            };
        case SET_ENABLE_ACTIVE:
            return{
                ...state,
                enableActive: action.enableActive
            };
        case SET_ENABLE_SHOW:
            return{
                ...state,
                enableShow: action.enableShow
            };
        case SET_ENABLE_BACKGROUND_IMAGE:
            return {
                ...state,
                enableBackgroundImage: action.enableBackgroundImage
            };

        case SET_ENABLE_FIXED_HEADER:
            return {
                ...state,
                enableFixedHeader: action.enableFixedHeader
            };

        case SET_ENABLE_HEADER_SHADOW:
            return {
                ...state,
                enableHeaderShadow: action.enableHeaderShadow
            };

        case SET_ENABLE_SIDEBAR_SHADOW:
            return {
                ...state,
                enableSidebarShadow: action.enableSidebarShadow
            };

        case SET_ENABLE_PAGETITLE_ICON:
            return {
                ...state,
                enablePageTitleIcon: action.enablePageTitleIcon
            };

        case SET_ENABLE_PAGETITLE_SUBHEADING:
            return {
                ...state,
                enablePageTitleSubheading: action.enablePageTitleSubheading
            };

        case SET_ENABLE_PAGE_TABS_ALT:
            return {
                ...state,
                enablePageTabsAlt: action.enablePageTabsAlt
            };

        case SET_ENABLE_FIXED_SIDEBAR:
            return {
                ...state,
                enableFixedSidebar: action.enableFixedSidebar
            };

        case SET_ENABLE_MOBILE_MENU:
            return {
                ...state,
                enableMobileMenu: action.enableMobileMenu
            };

        case SET_ENABLE_MOBILE_MENU_SMALL:
            return {
                ...state,
                enableMobileMenuSmall: action.enableMobileMenuSmall
            };

        case SET_ENABLE_CLOSED_SIDEBAR:
            return {
                ...state,
                enableClosedSidebar: action.enableClosedSidebar
            };

        case SET_ENABLE_FIXED_FOOTER:
            return {
                ...state,
                enableFixedFooter: action.enableFixedFooter
            };

        case SET_BACKGROUND_COLOR:
            return {
                ...state,
                backgroundColor: action.backgroundColor
            };

        case SET_HEADER_BACKGROUND_COLOR:
            return {
                ...state,
                headerBackgroundColor: action.headerBackgroundColor
            };

        case SET_TYPE_ID:
            return {
                ...state,
                typeId: action.typeId
            };

        case SET_COLOR_SCHEME:
            return {
                ...state,
                colorScheme: action.colorScheme
            };

        case SET_BACKGROUND_IMAGE:
            return {
                ...state,
                backgroundImage: action.backgroundImage
            };

        case SET_BACKGROUND_IMAGE_OPACITY:
            return {
                ...state,
                backgroundImageOpacity: action.backgroundImageOpacity
            };
            default:
    }
    return state;
}
